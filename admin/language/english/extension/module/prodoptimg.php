<?php
// Heading
$_['heading_title']    = 'Product Option Color Image';

// Text
$_['text_module']   = 'Module';
$_['text_extension']   = 'Extension';
$_['text_extension_doc']   = '<a class="btn btn-info" href="http://opencarttools.net/document.php?extid=30744" target="_blank"> Extension Documentation</a>';
$_['text_success']     = 'Success: You have modified Product Option Color Image module!';
$_['text_edit']        = 'Edit Product Option Color Image Module';

$_['text_circle'] = 'Circle';
$_['text_square'] = 'Square';
$_['text_width'] = 'Width';
$_['text_height'] = 'Height';

// Entry
$_['entry_status'] = 'Status';
$_['entry_style'] = 'Style';
$_['entry_dispimgsize'] = 'Display Image Size';
$_['entry_gridimgsize'] = 'Grid Image Size';
$_['entry_prodimgsize'] = 'Product Page Image Size';
$_['entry_prodpopimgsize'] = 'Product Page Popup Image Size';
$_['entry_resetbtntext'] = 'Reset Button Text';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Product Option Color Image module!';

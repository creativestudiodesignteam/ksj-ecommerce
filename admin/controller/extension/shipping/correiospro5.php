<?php
//Loja5.com.br
if(extension_loaded("IonCube Loader")) {
	if(version_compare(PHP_VERSION, '5.4.0', '<')) {
		include(DIR_SYSTEM."../app/correiospro5/include/php53/correiospro5.php");
	}elseif(version_compare(PHP_VERSION, '5.5.0', '<')) {
		include(DIR_SYSTEM."../app/correiospro5/include/php54/correiospro5.php");
	}elseif(version_compare(PHP_VERSION, '5.6.0', '<')){
		include(DIR_SYSTEM."../app/correiospro5/include/php55/correiospro5.php");
	}elseif(version_compare(PHP_VERSION, '7.1.0', '<')){
		include(DIR_SYSTEM."../app/correiospro5/include/php56/correiospro5.php");
	}elseif(version_compare(PHP_VERSION, '7.2.0', '<')){
		include(DIR_SYSTEM."../app/correiospro5/include/php71/correiospro5.php");
	}else{
		include(DIR_SYSTEM."../app/correiospro5/include/php72/correiospro5.php");
	}
}else{
	class ControllerExtensionShippingCorreiosPro5 extends Controller {
		public function index(){
			die('Ops, ative o ioncube loads em sua hospedagem PHP '.PHP_VERSION.'!');
		}
		public function install(){
			$this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "correiospro5_servicos_cadastrados` (
				`id` INT(11) NOT NULL AUTO_INCREMENT,
				`id_servico` VARCHAR(5) NOT NULL DEFAULT '',
				`titulo` VARCHAR(50) NOT NULL DEFAULT '',
				`prazo_extra` INT(11) NOT NULL DEFAULT '0',
				`valor_extra` FLOAT(10,2) NOT NULL DEFAULT '0.00',
				`peso_maximo` FLOAT(10,2) NOT NULL DEFAULT '30.00',
				`total_maximo` FLOAT(10,2) NOT NULL DEFAULT '10000.00',
				`total_minimo` FLOAT(10,2) NOT NULL DEFAULT '0.00',
				`total_minimo_frete` FLOAT(10,2) NOT NULL DEFAULT '0.00',
				`real_porcentagem` INT(1) NOT NULL DEFAULT '0',
				`ceps` TEXT NOT NULL,
				`ceps_excluir` TEXT NOT NULL,
				`status` INT(1) NOT NULL DEFAULT '1',
				PRIMARY KEY (`id`)
			)
			ENGINE=InnoDB;");
			
			$this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "correiospro5_servicos` (
				`id_unico_servico` INT(11) NOT NULL AUTO_INCREMENT,
				`id_servico` VARCHAR(5) NOT NULL DEFAULT '',
				`nome` VARCHAR(50) NOT NULL DEFAULT '',
				`suporte_offline` INT(1) NOT NULL DEFAULT '0',
				`atualizado` TIMESTAMP NULL DEFAULT NULL,
				PRIMARY KEY (`id_unico_servico`),
				UNIQUE INDEX `id_servico` (`id_servico`)
			)
			ENGINE=InnoDB;");
			
			$this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "correiospro5_base` (
				`id` INT(15) NOT NULL AUTO_INCREMENT,
				`uf` VARCHAR(2) NOT NULL,
				`detalhes` VARCHAR(60) NOT NULL,
				`inicio` INT(5) NOT NULL,
				`fim` INT(5) NOT NULL,
				`base_cep` INT(8) NOT NULL,
				`custom` INT(1) NOT NULL DEFAULT '0',
				PRIMARY KEY (`id`)
			)
			COLLATE='latin1_swedish_ci'
			ENGINE=InnoDB
			AUTO_INCREMENT=1;");
			
			$this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "correiospro5_cotacoes` (
				`id` INT(15) NOT NULL AUTO_INCREMENT,
				`id_servico` CHAR(5) NULL DEFAULT NULL,
				`erro` CHAR(50) NULL DEFAULT NULL,
				`valor` FLOAT(10,2) NOT NULL DEFAULT '0.00',
				`peso` FLOAT(10,2) NOT NULL DEFAULT '0.00',
				`prazo` INT(15) NOT NULL DEFAULT '0',
				`cep_base` VARCHAR(9) NOT NULL,
				`cep_inicio` VARCHAR(9) NOT NULL,
				`cep_fim` VARCHAR(9) NOT NULL,
				`atualizado` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
				`custom` INT(1) NOT NULL DEFAULT '0',
				`cliente` INT(1) NOT NULL DEFAULT '0',
				PRIMARY KEY (`id`)
			)
			COLLATE='latin1_swedish_ci'
			ENGINE=InnoDB
			AUTO_INCREMENT=1;");
			
			$this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "correiospro5_cache` (
				`id` INT(15) NOT NULL AUTO_INCREMENT,
				`hash` CHAR(50) NULL DEFAULT NULL,
				`peso` FLOAT(10,2) NULL DEFAULT NULL,
				`total` FLOAT(10,2) NULL DEFAULT NULL,
				`cep_destino` CHAR(10) NULL DEFAULT NULL,
				`json` TEXT NULL,
				`atualizado` DATETIME NULL DEFAULT NULL,
				PRIMARY KEY (`id`)
			)
			COLLATE='latin1_swedish_ci'
			ENGINE=InnoDB
			AUTO_INCREMENT=1;");

			$this->db->query("DELETE FROM `" . DB_PREFIX . "correiospro5_base` WHERE custom = 0");
			$this->db->query("INSERT INTO `" . DB_PREFIX . "correiospro5_base` (`id`, `uf`, `detalhes`, `inicio`, `fim`, `base_cep`, `custom`) VALUES
			(1, 'SP', 'SÃO PAULO - CAPITAL 1', 1000, 5999, 5999999, 0),
			(3, 'SP', 'SÃO PAULO - REGIÃO METROPOLITANA 1', 8500, 9999, 9999999, 0),
			(4, 'SP', 'SÃO PAULO - LITORAL', 10000, 11999, 11999999, 0),
			(5, 'SP', 'SÃO PAULO - INTERIOR 1', 12000, 12999, 12999999, 0),
			(6, 'RJ', 'RIO DE JANEIRO - CAPITAL', 20000, 23799, 23799999, 0),
			(7, 'RJ', 'RIO DE JANEIRO - REGIÃO METROPOLITANA', 23800, 26600, 26600999, 0),
			(8, 'RJ', 'RIO DE JANEIRO - INTERIOR 1', 26601, 27999, 27999999, 0),
			(9, 'ES', 'ESPIRITO SANTO - CAPITAL', 29000, 29099, 29099999, 0),
			(10, 'ES', 'ESPIRITO SANTO - INTERIOR', 29100, 29999, 29999999, 0),
			(11, 'MG', 'MINAS GERAIS - CAPITAL', 30000, 31999, 31999999, 0),
			(12, 'MG', 'MINAS GERAIS - REGIÃO METROPOLITANA', 32000, 34999, 34999999, 0),
			(13, 'MG', 'MINAS GERAIS - INTERIOR 1', 35000, 35999, 35990999, 0),
			(14, 'BA', 'BAHIA - CAPITAL', 40000, 42599, 42599999, 0),
			(15, 'BA', 'BAHIA - REGIÃO METROPOLITANA', 42600, 44470, 44470999, 0),
			(16, 'BA', 'BAHIA - INTERIOR 1', 44471, 44999, 44999999, 0),
			(17, 'SE', 'SERGIPE - CAPITAL', 49000, 49099, 49098999, 0),
			(18, 'SE', 'SERGIPE - INTERIOR', 49100, 49999, 49999999, 0),
			(19, 'PE', 'PERNAMBUCO - CAPITAL', 50000, 52999, 52999999, 0),
			(20, 'PE', 'PERNAMBUCO - REGIÃO METROPOLITANA', 53000, 54999, 54999999, 0),
			(21, 'PE', 'PERNAMBUCO - INTERIOR 1', 55000, 55999, 55999999, 0),
			(22, 'AL', 'ALAGOAS - CAPITAL', 57000, 57099, 57099999, 0),
			(23, 'AL', 'ALAGOAS - INTERIOR', 57100, 57999, 57999999, 0),
			(24, 'PB', 'PARAIBA - CAPITAL', 58000, 58099, 58099999, 0),
			(25, 'PB', 'PARAIBA - INTERIOR', 58100, 58999, 58990999, 0),
			(26, 'RN', 'RIO GRANDE DO NORTE - CAPITAL', 59000, 59139, 59139999, 0),
			(27, 'RN', 'RIO GRANDE DO NORTE - INTERIOR', 59140, 59999, 59999999, 0),
			(28, 'CE', 'CEARA - CAPITAL 1', 60000, 60999, 60999999, 0),
			(29, 'CE', 'CEARA - REGIÃO METROPOLITANA', 61600, 61900, 61900999, 0),
			(30, 'CE', 'CEARA - INTERIOR 1', 61901, 61999, 61999999, 0),
			(31, 'PI', 'PIAUI - CAPITAL', 64000, 64099, 64099999, 0),
			(32, 'PI', 'PIAUI - INTERIOR', 64100, 64999, 64999999, 0),
			(33, 'MA', 'MARANHAO - CAPITAL 1', 65000, 65099, 65099999, 0),
			(34, 'MA', 'MARANHAO - INTERIOR', 65110, 65999, 65999999, 0),
			(35, 'PA', 'PARA - CAPITAL', 66000, 66999, 66999999, 0),
			(36, 'PA', 'PARA - REGIÃO METROPOLITANA', 67000, 67999, 67999999, 0),
			(37, 'PA', 'PARA - INTERIOR', 68000, 68899, 68899999, 0),
			(38, 'AP', 'AMAPA - CAPITAL 1', 68900, 68911, 68911999, 0),
			(39, 'AP', 'AMAPA - INTERIOR', 68915, 68999, 68999999, 0),
			(40, 'AM', 'AMAZONAS - CAPITAL', 69000, 69099, 69099999, 0),
			(41, 'AM', 'AMAZONAS - INTERIOR', 69100, 69299, 69299999, 0),
			(42, 'RR', 'RORAIMA - CAPITAL', 69300, 69339, 69339999, 0),
			(43, 'RR', 'RORAIMA - INTERIOR', 69340, 69399, 69399999, 0),
			(44, 'AC', 'ACRE - CAPITAL', 69900, 69924, 69924999, 0),
			(45, 'AC', 'ACRE - INTERIOR', 69925, 69999, 69999999, 0),
			(46, 'DF', 'BRASILIA 1', 70000, 72799, 72799999, 0),
			(47, 'DF', 'BRASILIA 2', 73000, 73699, 73699999, 0),
			(48, 'GO', 'GOAIS - CAPITAL', 74000, 74899, 74899999, 0),
			(49, 'GO', 'GOAIS - INTERIOR 1', 72800, 72999, 72980999, 0),
			(50, 'TO', 'TOCANTINS - CAPITAL 1', 77000, 77249, 77249999, 0),
			(51, 'TO', 'TOCANTINS - INTERIOR', 77300, 77999, 77999999, 0),
			(52, 'MT', 'MATO GROSSO - CAPITAL 1', 78000, 78099, 78099999, 0),
			(53, 'MT', 'MARO GROSSO - INTERIOR', 78110, 78899, 78899999, 0),
			(54, 'RO', 'RONDONIA - CAPITAL 1', 76800, 76834, 76834999, 0),
			(55, 'RO', 'RONDONIA - INTERIOR', 76850, 76999, 76999999, 0),
			(56, 'MS', 'MATO GROSSO DO SUL - CAPITAL 1', 79000, 79124, 79124999, 0),
			(57, 'MS', 'MATO GROSSO DO SUL - INTERIOR', 79130, 79999, 79999999, 0),
			(58, 'PR', 'PARANA - CAPITAL', 80000, 82999, 82999999, 0),
			(59, 'PR', 'PARANA - REGIAO METROPOLITANA', 83000, 83800, 83800999, 0),
			(60, 'PR', 'PARANA - INTERIOR 1', 83801, 83999, 83999999, 0),
			(61, 'SC', 'SANTA CATARINA - CAPITAL', 88000, 88099, 88099999, 0),
			(62, 'SC', 'SANTA CATARINA - REGIÃO METROPOLITANA', 88100, 88469, 88469999, 0),
			(63, 'SC', 'SANTA CATARINA - INTERIOR 1', 88470, 88999, 88999999, 0),
			(64, 'RS', 'RIO GRANDE DO SUL - CAPITAL', 90000, 91999, 91999999, 0),
			(65, 'RS', 'RIO GRANDE DO SUL - REGIÃO METROPOLITANA', 92000, 94900, 94900999, 0),
			(66, 'RS', 'RIO GRANDE DO SUL - INTERIOR 1', 94901, 94999, 94999999, 0),
			(67, 'SP', 'SÃO PAULO - CAPITAL 2', 8000, 8499, 8499999, 0),
			(68, 'SP', 'SÃO PAULO - REGIÃO METROPOLITANA 2', 6000, 7999, 7999999, 0),
			(69, 'GO', 'GOAIS - INTERIOR 2', 73700, 73999, 73999999, 0),
			(70, 'GO', 'GOIAS - INTERIOR 3', 74900, 76799, 76750999, 0),
			(71, 'CE', 'CEARA - CAPITAL 2', 61000, 61599, 61599999, 0),
			(72, 'MA', 'MARANHAO - CAPITAL 2', 65100, 65109, 65109999, 0),
			(73, 'AP', 'AMAPA - CAPITAL 2', 68912, 68914, 64914999, 0),
			(74, 'RO', 'RONDONIA - CAPITAL 2', 76835, 76849, 76849999, 0),
			(75, 'TO', 'TOCANTINS - CAPITAL 2', 77250, 77299, 77299999, 0),
			(76, 'MT', 'MATO GROSSO - CAPITAL 2', 78100, 78109, 78109999, 0),
			(77, 'MS', 'MATO GROSSO DO SUL - CAPITAL 2', 79125, 79129, 79129999, 0),
			(78, 'SP', 'SÃO PAULO - INTERIOR 2', 13000, 13999, 13999999, 0),
			(79, 'SP', 'SÃO PAULO - INTERIOR 3', 14000, 14999, 14999999, 0),
			(80, 'SP', 'SÃO PAULO - INTERIOR 4', 15000, 15999, 15999999, 0),
			(81, 'SP', 'SÃO PAULO - INTERIOR 5', 16000, 16999, 16999999, 0),
			(82, 'SP', 'SÃO PAULO - INTERIOR 6', 17000, 17999, 17999999, 0),
			(83, 'SP', 'SÃO PAULO - INTERIOR 7', 18000, 18999, 18999999, 0),
			(84, 'SP', 'SÃO PAULO - INTERIOR 8', 19000, 19999, 19999999, 0),
			(85, 'RJ', 'RIO DE JANEIRO - INTERIOR 2', 28000, 28999, 28999999, 0),
			(87, 'BA', 'BAHIA - INTERIOR 2', 45000, 45999, 45999999, 0),
			(88, 'BA', 'BAHIA - INTERIOR 3', 46000, 46999, 46999999, 0),
			(89, 'BA', 'BAHIA - INTERIOR 4', 47000, 47999, 47999999, 0),
			(90, 'BA', 'BAHIA - INTERIOR 5', 48000, 48999, 48999999, 0),
			(91, 'CE', 'CEARA - INTERIOR 2', 62000, 62999, 62999999, 0),
			(92, 'CE', 'CEARA - INTERIOR 3', 63000, 63999, 63999999, 0),
			(93, 'MG', 'MINAS GERAIS - INTERIOR 2', 36000, 36999, 36999999, 0),
			(94, 'MG', 'MINAS GERAIS - INTERIOR 3', 37000, 37999, 37999999, 0),
			(95, 'MG', 'MINAS GERAIS - INTERIOR 4', 38000, 38999, 38999999, 0),
			(96, 'MG', 'MINAS GERAIS - INTERIOR 5', 39000, 39999, 39990000, 0),
			(97, 'PE', 'PERNAMBUCO - INTERIOR 2', 56000, 56999, 56999999, 0),
			(98, 'PR', 'PARANA - INTERIOR 2', 84000, 84999, 84999999, 0),
			(99, 'PR', 'PARANA - INTERIOR 3', 85000, 85999, 85999999, 0),
			(100, 'PR', 'PARANA - INTERIOR 4', 86000, 86999, 86999999, 0),
			(101, 'PR', 'PARANA - INTERIOR 5', 87000, 87999, 87999999, 0),
			(102, 'SC', 'SANTA CATARINA - INTERIOR 2', 89000, 89999, 89999999, 0),
			(103, 'RS', 'RIO GRANDE DO SUL - INTERIOR 2', 95000, 95999, 95999999, 0),
			(104, 'RS', 'RIO GRANDE DO SUL - INTERIOR 3', 96000, 96999, 96999999, 0),
			(105, 'RS', 'RIO GRANDE DO SUL - INTERIOR 4', 97000, 97999, 97999999, 0),
			(106, 'RS', 'RIO GRANDE DO SUL - INTERIOR 5', 98000, 98999, 98999999, 0),
			(107, 'RS', 'RIO GRANDE DO SUL - INTERIOR 6', 99000, 99999, 99999999, 0);");
			
			$this->db->query("DELETE FROM `" . DB_PREFIX . "correiospro5_servicos`");
			$this->db->query("INSERT INTO `" . DB_PREFIX . "correiospro5_servicos` (`id_unico_servico`, `id_servico`, `nome`, `suporte_offline`, `atualizado`) VALUES
			(1, '04162', 'SEDEX CONTRATO AGENCIA', 1, NULL),
			(2, '04553', 'SEDEX CONTRATO AGENCIA TA', 1, NULL),
			(3, '04669', 'PAC CONTRATO AGENCIA', 1, NULL),
			(4, '04596', 'PAC CONTRATO AGENCIA TA', 1, NULL),
			(5, '04510', 'PAC SEM CONTRATO', 1, '2019-05-21 10:36:13'),
			(6, '04014', 'SEDEX SEM CONTRATO', 1, '2019-05-07 13:19:57'),
			(7, '40045', 'SEDEX A COBRAR SEM CONTRATO', 0, NULL),
			(8, '40126', 'SEDEX A COBRAR COM CONTRATO', 0, NULL),
			(9, '40215', 'SEDEX 10', 0, NULL),
			(10, '40290', 'SEDEX HOJE', 0, NULL),
			(11, '04367', 'PAC CONTRATO AGENCIA LM', 1, NULL),
			(12, '04154', 'SEDEX CONTRATO AGENCIA LM', 1, NULL),
			(13, '03085', 'PAC CONTRATO 03085', 1, NULL),
			(14, '03050', 'SEDEX CONTRATO 03050', 1, NULL),
			(15, '03298', 'PAC CONTRATO 03298', 1, NULL),
			(16, '03220', 'SEDEX CONTRATO 03220', 1, NULL),
			(17, '03140', 'SEDEX 12 03140', 0, NULL),
			(18, '03158', 'SEDEX 10 03158', 0, NULL),
			(19, '03204', 'SEDEX HOJE 03204', 0, NULL),
			(20, '04227', 'PAC MINI CONTRATO', 0, NULL);");
		}
	}
}
//Loja5.com.br
?>
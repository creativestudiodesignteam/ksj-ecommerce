<?php
class ControllerExtensionModuleprodoptimg extends Controller {	
	private $error = array();
	private $modpath = 'module/prodoptimg'; 
	private $modtpl = 'module/prodoptimg.tpl';
	private $modname = 'prodoptimg';
	private $modtext = 'Product Option Color Image';
	private $modid = '30744';
	private $modssl = 'SSL';
	private $modemail = 'opencarttools@gmail.com';
	private $token_str = '';
	private $modurl = 'extension/module';
	private $modurltext = '';

	public function __construct($registry) {
		parent::__construct($registry);
 		
		if(substr(VERSION,0,3)>='3.0' || substr(VERSION,0,3)=='2.3') { 
			$this->modtpl = 'extension/module/prodoptimg';
			$this->modpath = 'extension/module/prodoptimg';
		} else if(substr(VERSION,0,3)=='2.2') {
			$this->modtpl = 'module/prodoptimg';
		} 
		
		if(substr(VERSION,0,3)>='3.0') { 
			$this->modname = 'module_prodoptimg';
			$this->modurl = 'marketplace/extension'; 
			$this->token_str = 'user_token=' . $this->session->data['user_token'] . '&type=module';
		} else if(substr(VERSION,0,3)=='2.3') {
			$this->modurl = 'extension/extension';
			$this->token_str = 'token=' . $this->session->data['token'] . '&type=module';
		} else {
			$this->token_str = 'token=' . $this->session->data['token'];
		}
		
		if(substr(VERSION,0,3)>='3.0' || substr(VERSION,0,3)=='2.3' || substr(VERSION,0,3)=='2.2') { 
			$this->modssl = true;
		} 
 	} 
	
	public function index() {
		$data = $this->load->language($this->modpath);
		$this->modurltext = $this->language->get('text_extension');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting($this->modname, $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			if(! (isset($this->request->post['svsty']) && $this->request->post['svsty'] == 1)) {
				$this->response->redirect($this->url->link($this->modurl, $this->token_str, $this->modssl));
			}
		}

		$data['heading_title'] = $this->language->get('heading_title');
 
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', $this->token_str, $this->modssl)
		);
		
		$data['breadcrumbs'][] = array(
			'text' => $this->modurltext,
			'href' => $this->url->link($this->modurl, $this->token_str, $this->modssl)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link($this->modpath, $this->token_str, $this->modssl)
		);

		$data['action'] = $this->url->link($this->modpath, $this->token_str, $this->modssl);
		
		$data['cancel'] = $this->url->link($this->modurl, $this->token_str , $this->modssl); 
		
		if(substr(VERSION,0,3)>='3.0') { 
			$data['user_token'] = $this->session->data['user_token'];
		} else {
			$data['token'] = $this->session->data['token'];
		}
		
		$this->load->model('localisation/language');
  		$languages = $this->model_localisation_language->getLanguages();
		foreach($languages as $language) {
			if(substr(VERSION,0,3)>='3.0' || substr(VERSION,0,3)=='2.3' || substr(VERSION,0,3)=='2.2') {
				$imgsrc = "language/".$language['code']."/".$language['code'].".png";
			} else {
				$imgsrc = "view/image/flags/".$language['image'];
			}
			$data['languages'][] = array("language_id" => $language['language_id'], "name" => $language['name'], "imgsrc" => $imgsrc);
		}
		
		$this->load->model('tool/image');
 		$data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		
		$data[$this->modname.'_status'] = $this->setvalue($this->modname.'_status');
		$data[$this->modname.'_style'] = $this->setvalue($this->modname.'_style');	
		$data[$this->modname.'_gridimgw'] = $this->setvalue($this->modname.'_gridimgw');	
		$data[$this->modname.'_gridimgh'] = $this->setvalue($this->modname.'_gridimgh');
		
		$data[$this->modname.'_dispimgw'] = $this->setvalue($this->modname.'_dispimgw');	
		$data[$this->modname.'_dispimgh'] = $this->setvalue($this->modname.'_dispimgh');
			
		$data[$this->modname.'_prodimgw'] = $this->setvalue($this->modname.'_prodimgw');	
		$data[$this->modname.'_prodimgh'] = $this->setvalue($this->modname.'_prodimgh');
		$data[$this->modname.'_prodpopimgw'] = $this->setvalue($this->modname.'_prodpopimgw');	
		$data[$this->modname.'_prodpopimgh'] = $this->setvalue($this->modname.'_prodpopimgh');
		
		$data[$this->modname.'_resetbtntext'] = $this->setvalue($this->modname.'_resetbtntext');	
		
  		$data['modname'] = $this->modname;
		$data['modemail'] = $this->modemail;
  		  
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view($this->modtpl, $data));
	}
	
	protected function setvalue($postfield) {
		if (isset($this->request->post[$postfield])) {
			$postfield_value = $this->request->post[$postfield];
		} else {
			$postfield_value = $this->config->get($postfield);
		} 	
 		return $postfield_value;
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', $this->modpath)) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
	
	public function install() {
		$query = $this->db->query("SHOW COLUMNS FROM `".DB_PREFIX."option` LIKE 'prodoptimgflag' ");
		if(!$query->num_rows){
			$this->db->query("ALTER TABLE `".DB_PREFIX."option` ADD `prodoptimgflag` tinyint(1) ");
		}
		$query = $this->db->query("SHOW COLUMNS FROM `".DB_PREFIX."product_option_value` LIKE 'prodoptimg' ");
		if(!$query->num_rows){
			$this->db->query("ALTER TABLE `".DB_PREFIX."product_option_value` ADD `prodoptimg` VARCHAR(255) ");
		} 
		$query = $this->db->query("SHOW COLUMNS FROM `".DB_PREFIX."product_option_value` LIKE 'prodoptimg_color' ");
		if(!$query->num_rows){
			$this->db->query("ALTER TABLE `".DB_PREFIX."product_option_value` ADD `prodoptimg_color` VARCHAR(50) ");
		}
		if(function_exists('curl_version')) { 
			$pdata['extid'] = $this->modid;
			$pdata['version'] = VERSION;
			$pdata['installat'] = HTTP_CATALOG;
			$pdata['email'] = $this->config->get('config_email'); 
			
 			$curl = curl_init("http://www.opencarttools.net/licentry.php");
	
 			curl_setopt($curl, CURLOPT_HEADER, 0);
 			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curl, CURLOPT_FORBID_REUSE, 1);
			curl_setopt($curl, CURLOPT_FRESH_CONNECT, 1);
			curl_setopt($curl, CURLOPT_POST, 1);
			curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
			curl_setopt($curl, CURLOPT_TIMEOUT, 10);
			curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($pdata, '', '&'));
	
			$response = curl_exec($curl); 
			
			//echo $response; exit;
		}      
	}
	public function uninstall() { 
		// uninstalled       
	}
}
<?php
// HTTP
define('HTTP_SERVER', 'http://kadoresemijoias.com.br/teste/admin/');
define('HTTP_CATALOG', 'http://kadoresemijoias.com.br/teste/');

// HTTPS
define('HTTPS_SERVER', 'http://kadoresemijoias.com.br/teste/admin/');
define('HTTPS_CATALOG', 'http://kadoresemijoias.com.br/teste/');

// DIR
define('DIR_APPLICATION', '/home2/kadore/public_html/teste/admin/');
define('DIR_SYSTEM', '/home2/kadore/public_html/teste/system/');
define('DIR_IMAGE', '/home2/kadore/public_html/teste/image/');
define('DIR_STORAGE', DIR_SYSTEM . 'storage/');
define('DIR_CATALOG', '/home2/kadore/public_html/teste/catalog/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/template/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD', DIR_STORAGE . 'download/');
define('DIR_LOGS', DIR_STORAGE . 'logs/');
define('DIR_MODIFICATION', DIR_STORAGE . 'modification/');
define('DIR_SESSION', DIR_STORAGE . 'session/');
define('DIR_UPLOAD', DIR_STORAGE . 'upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'kadore_oc2');
define('DB_PASSWORD', 'Kjoias@@@123###');
define('DB_DATABASE', 'kadore_oc2');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');

// OpenCart API
define('OPENCART_SERVER', 'https://www.opencart.com/');

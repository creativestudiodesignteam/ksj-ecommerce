<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* wokiee/template/common/header/header_01.twig */
class __TwigTemplate_fe81c3fc2312e266a27ec654cf0b4c3c00eb8fbc03968dead3f58748aa4632f1 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<header>
\t<!-- tt-mobile-header -->
\t<div class=\"tt-mobile-header\">
\t\t<div class=\"container-fluid\">
\t\t\t<div class=\"tt-header-row\">
\t\t\t\t<div class=\"tt-mobile-parent-menu\">
\t\t\t\t\t<div class=\"tt-menu-toggle\">
\t\t\t\t\t\t<i class=\"icon-03\"></i>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<!-- search -->
\t\t\t\t<div class=\"tt-mobile-parent-search tt-parent-box\"></div>
\t\t\t\t<!-- /search -->
\t\t\t\t<!-- cart -->
\t\t\t\t<div class=\"tt-mobile-parent-cart tt-parent-box\"></div>
\t\t\t\t<!-- /cart -->
\t\t\t\t<!-- account -->
\t\t\t\t<div class=\"tt-mobile-parent-account tt-parent-box\"></div>
\t\t\t\t<!-- /account -->
\t\t\t\t<!-- currency -->
\t\t\t\t<div class=\"tt-mobile-parent-multi tt-parent-box\"></div>
\t\t\t\t<!-- /currency -->
\t\t\t</div>
\t\t</div>
\t\t<div class=\"container-fluid tt-top-line\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"tt-logo-container\">
\t\t\t\t\t";
        // line 28
        if (($context["logo"] ?? null)) {
            echo " 
\t\t\t\t\t<!-- Logo -->
\t\t\t\t\t<a class=\"tt-logo tt-logo-alignment\" href=\"";
            // line 30
            echo ($context["home"] ?? null);
            echo "\"><img src=\"";
            echo ($context["logo"] ?? null);
            echo "\" title=\"";
            echo ($context["name"] ?? null);
            echo "\" alt=\"";
            echo ($context["name"] ?? null);
            echo "\" /></a>
\t\t\t\t\t";
        }
        // line 31
        echo " 
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
\t<!-- tt-desktop-header -->
\t<div class=\"tt-desktop-header\">
\t\t<div class=\"container\">
\t\t\t<div class=\"tt-header-holder\">
\t\t\t\t<div class=\"tt-col-obj tt-obj-logo\">
\t\t\t\t\t";
        // line 41
        if (($context["logo"] ?? null)) {
            echo " 
\t\t\t\t\t<!-- Logo -->
\t\t\t\t\t<a class=\"tt-logo tt-logo-alignment\" href=\"";
            // line 43
            echo ($context["home"] ?? null);
            echo "\"><img src=\"";
            echo ($context["logo"] ?? null);
            echo "\" title=\"";
            echo ($context["name"] ?? null);
            echo "\" alt=\"";
            echo ($context["name"] ?? null);
            echo "\" /></a>
\t\t\t\t\t";
        }
        // line 44
        echo " 
\t\t\t\t</div>
\t\t\t\t<div class=\"tt-col-obj tt-obj-menu\">
\t\t\t\t\t<!-- tt-menu -->
\t\t\t\t\t<div class=\"tt-desctop-parent-menu tt-parent-box\">
\t\t\t\t\t\t<div class=\"tt-desctop-menu\">
\t\t\t\t\t\t\t";
        // line 50
        $context["menu9"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "menu"], "method", false, false, false, 50);
        // line 51
        echo "\t\t\t\t\t\t\t";
        if ((twig_length_filter($this->env, ($context["menu9"] ?? null)) > 0)) {
            echo " 
\t\t\t\t\t\t\t\t";
            // line 52
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["menu9"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                echo " 
\t\t\t\t\t\t\t\t\t";
                // line 53
                echo $context["module"];
                echo "
\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 55
            echo "\t\t\t\t\t\t\t";
        } else {
            // line 56
            echo "\t\t\t\t\t\t\t\t";
            echo ($context["menu"] ?? null);
            echo "
\t\t\t\t\t\t\t";
        }
        // line 58
        echo "\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<!-- /tt-menu -->
\t\t\t\t</div>
\t\t\t\t<div class=\"tt-col-obj tt-obj-options obj-move-right\">
\t\t\t\t\t<!-- tt-search -->
\t\t\t\t\t<div class=\"tt-desctop-parent-search tt-parent-box\">
\t\t\t\t\t\t<div class=\"tt-search tt-dropdown-obj\">
\t\t\t\t\t\t\t<button class=\"tt-dropdown-toggle\" data-tooltip=\"";
        // line 66
        echo ($context["text_search"] ?? null);
        echo "\" data-tposition=\"bottom\">
\t\t\t\t\t\t\t\t<i class=\"icon-f-85\"></i>
\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t<div class=\"tt-dropdown-menu\">
\t\t\t\t\t\t\t\t<div class=\"container\">
\t\t\t\t\t\t\t\t\t<form>
\t\t\t\t\t\t\t\t\t\t<div class=\"tt-col\">
\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"tt-search-input\" placeholder=\"";
        // line 73
        echo ($context["text_search"] ?? null);
        echo "...\">
\t\t\t\t\t\t\t\t\t\t\t<button class=\"tt-btn-search\" type=\"submit\"></button>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"tt-col\">
\t\t\t\t\t\t\t\t\t\t\t<button class=\"tt-btn-close icon-g-80\"></button>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"tt-info-text\">
\t\t\t\t\t\t\t\t\t\t\t";
        // line 80
        if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "what_are_you_looking_for_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 80)], "method", false, false, false, 80) != "")) {
            echo " ";
            echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "what_are_you_looking_for_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 80)], "method", false, false, false, 80);
            echo " ";
        } else {
            echo "What are you Looking for?";
        }
        // line 81
        echo "\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"search-results\">
\t\t\t\t\t\t\t\t\t\t\t<ul>

\t\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"tt-view-all\">";
        // line 86
        if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "view_all_products_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 86)], "method", false, false, false, 86) != "")) {
            echo " ";
            echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "view_all_products_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 86)], "method", false, false, false, 86);
            echo " ";
        } else {
            echo "View all products";
        }
        echo "</button>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<!-- /tt-search -->
\t\t\t\t\t<!-- tt-cart -->
\t\t\t\t\t";
        // line 95
        echo ($context["cart"] ?? null);
        echo "
\t\t\t\t\t<!-- /tt-cart -->
\t\t\t\t\t<!-- tt-account -->
\t\t\t\t\t<div class=\"tt-desctop-parent-account tt-parent-box\">
\t\t\t\t\t\t<div class=\"tt-account tt-dropdown-obj\">
\t\t\t\t\t\t\t<button class=\"tt-dropdown-toggle\"  data-tooltip=\"";
        // line 100
        echo ($context["text_account"] ?? null);
        echo "\" data-tposition=\"bottom\"><i class=\"icon-f-94\"></i></button>
\t\t\t\t\t\t\t<div class=\"tt-dropdown-menu\">
\t\t\t\t\t\t\t\t<div class=\"tt-mobile-add\">
\t\t\t\t\t\t\t\t\t<button class=\"tt-close\">";
        // line 103
        if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "close_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 103)], "method", false, false, false, 103) != "")) {
            echo " ";
            echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "close_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 103)], "method", false, false, false, 103);
            echo " ";
        } else {
            echo "Close";
        }
        echo "</button>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"tt-dropdown-inner\">
\t\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t    <li><a href=\"";
        // line 107
        echo ($context["account"] ?? null);
        echo "\"><i class=\"icon-f-94\"></i>";
        echo ($context["text_account"] ?? null);
        echo "</a></li>
\t\t\t\t\t\t\t\t\t    <li><a href=\"";
        // line 108
        echo ($context["wishlist"] ?? null);
        echo "\"><i class=\"icon-n-072\"></i>";
        echo ($context["text_wishlist"] ?? null);
        echo "</a></li>
\t\t\t\t\t\t\t\t\t    <li><a href=\"";
        // line 109
        echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getLinkOpenCart", [0 => "product/compare"], "method", false, false, false, 109);
        echo "\"><i class=\"icon-n-08\"></i>";
        if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "compare_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 109)], "method", false, false, false, 109) != "")) {
            echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "compare_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 109)], "method", false, false, false, 109);
        } else {
            echo "Compare";
        }
        echo "</a></li>
\t\t\t\t\t\t\t\t\t    <li><a href=\"";
        // line 110
        echo ($context["checkout"] ?? null);
        echo "\"><i class=\"icon-f-68\"></i>";
        echo ($context["text_checkout"] ?? null);
        echo "</a></li>
\t\t\t\t\t\t\t            ";
        // line 111
        if (($context["logged"] ?? null)) {
            // line 112
            echo "\t\t\t\t\t\t\t            <li><a href=\"";
            echo ($context["logout"] ?? null);
            echo "\"><i class=\"icon-f-77\"></i>";
            echo ($context["text_logout"] ?? null);
            echo "</a></li>
\t\t\t\t\t\t\t            ";
        } else {
            // line 114
            echo "\t\t\t\t\t\t\t            <li><a href=\"";
            echo ($context["login"] ?? null);
            echo "\"><i class=\"icon-f-76\"></i>";
            echo ($context["text_login"] ?? null);
            echo "</a></li>
\t\t\t\t\t\t\t            <li><a href=\"";
            // line 115
            echo ($context["register"] ?? null);
            echo "\"><i class=\"icon-f-94\"></i>";
            echo ($context["text_register"] ?? null);
            echo "</a></li>
\t\t\t\t\t\t\t            ";
        }
        // line 117
        echo "\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<!-- /tt-account -->
\t\t\t\t\t<!-- tt-langue and tt-currency -->
\t\t\t\t\t<div class=\"tt-desctop-parent-multi tt-parent-box\">
\t\t\t\t\t\t<div class=\"tt-multi-obj tt-dropdown-obj\">
\t\t\t\t\t\t\t<button class=\"tt-dropdown-toggle\" data-tooltip=\"";
        // line 126
        if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "settings_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 126)], "method", false, false, false, 126) != "")) {
            echo " ";
            echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "settings_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 126)], "method", false, false, false, 126);
            echo " ";
        } else {
            echo "Settings";
        }
        echo "\" data-tposition=\"bottom\"><i class=\"icon-f-79\"></i></button>
\t\t\t\t\t\t\t<div class=\"tt-dropdown-menu\">
\t\t\t\t\t\t\t\t<div class=\"tt-mobile-add\">
\t\t\t\t\t\t\t\t\t<button class=\"tt-close\">";
        // line 129
        if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "close_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 129)], "method", false, false, false, 129) != "")) {
            echo " ";
            echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "close_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 129)], "method", false, false, false, 129);
            echo " ";
        } else {
            echo "Close";
        }
        echo "</button>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"tt-dropdown-inner\">
\t\t\t\t\t\t\t\t\t";
        // line 132
        echo (($context["language"] ?? null) . ($context["currency"] ?? null));
        echo "
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<!-- /tt-langue and tt-currency -->
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
\t<!-- stuck nav -->
\t<div class=\"tt-stuck-nav\">
\t\t<div class=\"container\">
\t\t\t<div class=\"tt-header-row \">
\t\t\t\t<div class=\"tt-stuck-parent-menu\"></div>
\t\t\t\t<div class=\"tt-stuck-parent-search tt-parent-box\"></div>
\t\t\t\t<div class=\"tt-stuck-parent-cart tt-parent-box\"></div>
\t\t\t\t<div class=\"tt-stuck-parent-account tt-parent-box\"></div>
\t\t\t\t<div class=\"tt-stuck-parent-multi tt-parent-box\"></div>
\t\t\t</div>
\t\t</div>
\t</div>
</header>";
    }

    public function getTemplateName()
    {
        return "wokiee/template/common/header/header_01.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  325 => 132,  313 => 129,  301 => 126,  290 => 117,  283 => 115,  276 => 114,  268 => 112,  266 => 111,  260 => 110,  250 => 109,  244 => 108,  238 => 107,  225 => 103,  219 => 100,  211 => 95,  193 => 86,  186 => 81,  178 => 80,  168 => 73,  158 => 66,  148 => 58,  142 => 56,  139 => 55,  131 => 53,  125 => 52,  120 => 51,  118 => 50,  110 => 44,  99 => 43,  94 => 41,  82 => 31,  71 => 30,  66 => 28,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "wokiee/template/common/header/header_01.twig", "/home2/kadore/public_html/teste/catalog/view/theme/wokiee/template/common/header/header_01.twig");
    }
}

<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* wokiee/template/new_elements/product.twig */
class __TwigTemplate_8bd9eadbd2a50072ca5a8cd367ad00d4ab7438994aa6bf9b1e1dd1488a08f131 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["theme_options"] = twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "get", [0 => "theme_options"], "method", false, false, false, 1);
        // line 2
        $context["config"] = twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "get", [0 => "config"], "method", false, false, false, 2);
        // line 3
        echo "
<div class=\"tt-product thumbprod-center\" id=\"product-";
        // line 4
        echo (($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = ($context["product"] ?? null)) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4["product_id"] ?? null) : null);
        echo "\">
\t<div class=\"tt-image-box\">
\t\t<a href=\"index.php?route=product/quickview&amp;product_id=";
        // line 6
        echo (($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = ($context["product"] ?? null)) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144["product_id"] ?? null) : null);
        echo "\" class=\"tt-btn-quickview\" data-toggle=\"modal\" data-target=\"#ModalquickView\"\tdata-tooltip=\"";
        if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "quickview_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 6)], "method", false, false, false, 6) != "")) {
            echo " ";
            echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "html_entity_decode", [0 => twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "quickview_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 6)], "method", false, false, false, 6)], "method", false, false, false, 6);
            echo " ";
        } else {
            echo " ";
            echo "Quickview";
            echo " ";
        }
        echo "\" data-tposition=\"left\"></a>
\t\t";
        // line 7
        if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "display_add_to_wishlist"], "method", false, false, false, 7) != "0")) {
            echo " 
\t\t\t<a href=\"javascript:wishlist.add('";
            // line 8
            echo (($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = ($context["product"] ?? null)) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b["product_id"] ?? null) : null);
            echo "');\" class=\"tt-btn-wishlist\" data-tooltip=\"";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "add_to_wishlist_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 8)], "method", false, false, false, 8) != "")) {
                echo " ";
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "add_to_wishlist_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 8)], "method", false, false, false, 8);
                echo " ";
            } else {
                echo " ";
                echo "Add to wishlist";
                echo " ";
            }
            echo "\" data-tposition=\"left\"></a>
\t\t";
        }
        // line 10
        echo "\t\t";
        if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "display_add_to_compare"], "method", false, false, false, 10) != "0")) {
            echo " 
\t\t\t<a href=\"javascript:compare.add('";
            // line 11
            echo (($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 = ($context["product"] ?? null)) && is_array($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002) || $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 instanceof ArrayAccess ? ($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002["product_id"] ?? null) : null);
            echo "');\" class=\"tt-btn-compare\" data-tooltip=\"";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "add_to_compare_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 11)], "method", false, false, false, 11) != "")) {
                echo " ";
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "add_to_compare_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 11)], "method", false, false, false, 11);
                echo " ";
            } else {
                echo " ";
                echo "Add to compare";
                echo " ";
            }
            echo "\" data-tposition=\"left\"></a>
\t\t";
        }
        // line 13
        echo "\t\t<a href=\"";
        echo (($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 = ($context["product"] ?? null)) && is_array($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4) || $__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 instanceof ArrayAccess ? ($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4["href"] ?? null) : null);
        echo "\">
\t\t\t";
        // line 14
        if ((($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 = ($context["product"] ?? null)) && is_array($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666) || $__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 instanceof ArrayAccess ? ($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666["thumb"] ?? null) : null)) {
            echo " 
\t\t\t\t<span class=\"tt-img\"><img src=\"catalog/view/theme/wokiee/img/loader.svg\" data-src=\"";
            // line 15
            echo (($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e = ($context["product"] ?? null)) && is_array($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e) || $__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e instanceof ArrayAccess ? ($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e["thumb"] ?? null) : null);
            echo "\" alt=\"";
            echo (($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 = ($context["product"] ?? null)) && is_array($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52) || $__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 instanceof ArrayAccess ? ($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52["name"] ?? null) : null);
            echo "\"></span>
\t\t\t\t";
            // line 16
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_image_effect"], "method", false, false, false, 16) == "1")) {
                // line 17
                echo "\t\t\t\t\t";
                $context["nthumb"] = twig_replace_filter((($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 = ($context["product"] ?? null)) && is_array($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136) || $__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 instanceof ArrayAccess ? ($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136["thumb"] ?? null) : null), [" " => "%20"]);
                // line 18
                echo "\t\t\t\t\t";
                $context["adres"] = twig_constant("HTTP_SERVER");
                // line 19
                echo "\t\t\t\t\t";
                $context["nthumb"] = twig_replace_filter(($context["nthumb"] ?? null), ["adres" => ""]);
                // line 20
                echo "\t\t\t\t\t";
                $context["image_size"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getimagesize", [0 => ($context["nthumb"] ?? null)], "method", false, false, false, 20);
                // line 21
                echo "\t\t\t\t\t";
                $context["image_swap"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "productImageSwap", [0 => (($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 = ($context["product"] ?? null)) && is_array($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386) || $__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 instanceof ArrayAccess ? ($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386["product_id"] ?? null) : null), 1 => (($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9 = ($context["image_size"] ?? null)) && is_array($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9) || $__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9 instanceof ArrayAccess ? ($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9[0] ?? null) : null), 2 => (($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae = ($context["image_size"] ?? null)) && is_array($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae) || $__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae instanceof ArrayAccess ? ($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae[1] ?? null) : null)], "method", false, false, false, 21);
                // line 22
                echo "\t\t\t\t\t";
                if ((($context["image_swap"] ?? null) != "")) {
                    echo "<span class=\"tt-img-roll-over\"><img src=\"catalog/view/theme/wokiee/img/loader.svg\" data-src=\"";
                    echo ($context["image_swap"] ?? null);
                    echo "\" alt=\"";
                    echo (($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f = ($context["product"] ?? null)) && is_array($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f) || $__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f instanceof ArrayAccess ? ($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f["name"] ?? null) : null);
                    echo "\"></span>";
                }
                // line 23
                echo "\t\t\t\t";
            }
            // line 24
            echo "\t\t\t";
        } else {
            echo " 
\t\t\t\t<span class=\"tt-img\"><img src=\"catalog/view/theme/wokiee/img/loader.svg\" data-src=\"image/no_image.jpg\" alt=\"\"></span>
\t\t\t";
        }
        // line 26
        echo " 
\t\t\t";
        // line 27
        if (((($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40 = ($context["product"] ?? null)) && is_array($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40) || $__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40 instanceof ArrayAccess ? ($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40["special"] ?? null) : null) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "display_text_sale"], "method", false, false, false, 27) != "0"))) {
            echo " 
\t\t\t\t";
            // line 28
            $context["text_sale"] = "Sale";
            // line 29
            echo "\t\t\t\t";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "sale_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 29)], "method", false, false, false, 29) != "")) {
                // line 30
                echo "\t\t\t\t\t";
                $context["text_sale"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "sale_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 30)], "method", false, false, false, 30);
                // line 31
                echo "\t\t\t\t";
            }
            echo " 

\t\t\t\t";
            // line 33
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "type_sale"], "method", false, false, false, 33) == "1")) {
                echo " 
\t\t\t\t\t";
                // line 34
                $context["product_detail"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getDataProduct", [0 => (($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f = ($context["product"] ?? null)) && is_array($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f) || $__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f instanceof ArrayAccess ? ($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f["product_id"] ?? null) : null)], "method", false, false, false, 34);
                // line 35
                echo "\t\t\t\t\t";
                $context["roznica_ceny"] = ((($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760 = ($context["product_detail"] ?? null)) && is_array($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760) || $__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760 instanceof ArrayAccess ? ($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760["price"] ?? null) : null) - (($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce = ($context["product_detail"] ?? null)) && is_array($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce) || $__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce instanceof ArrayAccess ? ($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce["special"] ?? null) : null));
                // line 36
                echo "\t\t\t\t\t";
                $context["procent"] = ((($context["roznica_ceny"] ?? null) * 100) / (($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b = ($context["product_detail"] ?? null)) && is_array($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b) || $__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b instanceof ArrayAccess ? ($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b["price"] ?? null) : null));
                echo " 
\t\t\t\t\t<span class=\"tt-label-location\">
\t\t\t\t\t\t<span class=\"tt-label-sale\">-";
                // line 38
                echo twig_round(($context["procent"] ?? null));
                echo "%</span>
\t\t\t\t\t</span>
\t\t\t\t";
            } else {
                // line 40
                echo " 
\t\t\t\t\t<span class=\"tt-label-location\">
\t\t\t\t\t\t<span class=\"tt-label-sale\">";
                // line 42
                echo ($context["text_sale"] ?? null);
                echo "</span>
\t\t\t\t\t</span>
\t\t\t\t";
            }
            // line 45
            echo "\t\t\t";
        } elseif (((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "display_text_new"], "method", false, false, false, 45) != "0") && twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "isLatestProduct", [0 => (($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c = ($context["product"] ?? null)) && is_array($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c) || $__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c instanceof ArrayAccess ? ($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c["product_id"] ?? null) : null)], "method", false, false, false, 45))) {
            echo " 
\t\t\t\t<span class=\"tt-label-location\">
\t\t\t\t\t<span class=\"tt-label-new\">";
            // line 47
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "new_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 47)], "method", false, false, false, 47) != "")) {
                echo " ";
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "new_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 47)], "method", false, false, false, 47);
                echo " ";
            } else {
                echo " ";
                echo "New";
                echo " ";
            }
            echo "</span>
\t\t\t\t</span>
\t\t\t";
        }
        // line 49
        echo " 
\t\t</a>
\t\t";
        // line 51
        if (((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "display_specials_countdown"], "method", false, false, false, 51) == "1") && (($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972 = ($context["product"] ?? null)) && is_array($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972) || $__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972 instanceof ArrayAccess ? ($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972["special"] ?? null) : null))) {
            echo " 
\t\t\t";
            // line 52
            $context["product_detail"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getDataProduct", [0 => (($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216 = ($context["product"] ?? null)) && is_array($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216) || $__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216 instanceof ArrayAccess ? ($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216["product_id"] ?? null) : null)], "method", false, false, false, 52);
            // line 53
            echo "\t\t\t";
            $context["date_end"] = (($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0 = ($context["product_detail"] ?? null)) && is_array($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0) || $__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0 instanceof ArrayAccess ? ($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0["date_end"] ?? null) : null);
            // line 54
            echo "\t\t\t";
            if (((($context["date_end"] ?? null) != "0000-00-00") && ($context["date_end"] ?? null))) {
                echo " 
\t\t\t\t<div class=\"tt-countdown_box\">
\t\t\t\t\t<div class=\"tt-countdown_inner\">
\t\t\t\t\t\t<div class=\"tt-countdown\"
\t\t\t\t\t\t\tdata-date=\"";
                // line 58
                echo ($context["date_end"] ?? null);
                echo "\"
\t\t\t\t\t\t\tdata-year=\"";
                // line 59
                if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "yrs_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 59)], "method", false, false, false, 59) != "")) {
                    echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "yrs_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 59)], "method", false, false, false, 59);
                } else {
                    echo "Yrs";
                }
                echo "\"
\t\t\t\t\t\t\tdata-month=\"";
                // line 60
                if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "mths_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 60)], "method", false, false, false, 60) != "")) {
                    echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "mths_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 60)], "method", false, false, false, 60);
                } else {
                    echo "Mths";
                }
                echo "\"
\t\t\t\t\t\t\tdata-week=\"";
                // line 61
                if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "wk_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 61)], "method", false, false, false, 61) != "")) {
                    echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "wk_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 61)], "method", false, false, false, 61);
                } else {
                    echo "Wk";
                }
                echo "\"
\t\t\t\t\t\t\tdata-day=\"";
                // line 62
                if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "day_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 62)], "method", false, false, false, 62) != "")) {
                    echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "day_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 62)], "method", false, false, false, 62);
                } else {
                    echo "Day";
                }
                echo "\"
\t\t\t\t\t\t\tdata-hour=\"";
                // line 63
                if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "hrs_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 63)], "method", false, false, false, 63) != "")) {
                    echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "hrs_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 63)], "method", false, false, false, 63);
                } else {
                    echo "Hrs";
                }
                echo "\"
\t\t\t\t\t\t\tdata-minute=\"";
                // line 64
                if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "min_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 64)], "method", false, false, false, 64) != "")) {
                    echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "min_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 64)], "method", false, false, false, 64);
                } else {
                    echo "Min";
                }
                echo "\"
\t\t\t\t\t\t\tdata-second=\"";
                // line 65
                if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "sec_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 65)], "method", false, false, false, 65) != "")) {
                    echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "sec_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 65)], "method", false, false, false, 65);
                } else {
                    echo "Sec";
                }
                echo "\"></div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t";
            }
            // line 69
            echo "\t\t";
        }
        echo "  
\t</div>
\t<div class=\"tt-description\">
\t\t<div class=\"tt-row\">
\t\t\t<ul class=\"tt-add-info\">
\t\t\t\t";
        // line 74
        $context["product_detail"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getDataProduct", [0 => (($__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c = ($context["product"] ?? null)) && is_array($__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c) || $__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c instanceof ArrayAccess ? ($__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c["product_id"] ?? null) : null)], "method", false, false, false, 74);
        echo " 
\t\t\t\t<li><a href=\"";
        // line 75
        echo (($__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f = ($context["product_detail"] ?? null)) && is_array($__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f) || $__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f instanceof ArrayAccess ? ($__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f["category_url"] ?? null) : null);
        echo "\">";
        echo (($__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc = ($context["product_detail"] ?? null)) && is_array($__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc) || $__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc instanceof ArrayAccess ? ($__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc["category_name"] ?? null) : null);
        echo "</a></li>
\t\t\t</ul>
\t\t\t";
        // line 77
        if ((((($__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55 = ($context["product"] ?? null)) && is_array($__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55) || $__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55 instanceof ArrayAccess ? ($__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55["rating"] ?? null) : null) > 0) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "display_rating"], "method", false, false, false, 77) == "1"))) {
            // line 78
            echo "\t\t\t\t<div class=\"tt-rating\">
\t\t\t\t\t<i class=\"icon-star";
            // line 79
            if (((($__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba = ($context["product"] ?? null)) && is_array($__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba) || $__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba instanceof ArrayAccess ? ($__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba["rating"] ?? null) : null) < 1)) {
                echo "-empty";
            }
            echo "\"></i>
\t\t\t\t\t<i class=\"icon-star";
            // line 80
            if (((($__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78 = ($context["product"] ?? null)) && is_array($__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78) || $__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78 instanceof ArrayAccess ? ($__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78["rating"] ?? null) : null) < 2)) {
                echo "-empty";
            }
            echo "\"></i>
\t\t\t\t\t<i class=\"icon-star";
            // line 81
            if (((($__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de = ($context["product"] ?? null)) && is_array($__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de) || $__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de instanceof ArrayAccess ? ($__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de["rating"] ?? null) : null) < 3)) {
                echo "-empty";
            }
            echo "\"></i>
\t\t\t\t\t<i class=\"icon-star";
            // line 82
            if (((($__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828 = ($context["product"] ?? null)) && is_array($__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828) || $__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828 instanceof ArrayAccess ? ($__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828["rating"] ?? null) : null) < 4)) {
                echo "-empty";
            }
            echo "\"></i>
\t\t\t\t\t<i class=\"icon-star";
            // line 83
            if (((($__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd = ($context["product"] ?? null)) && is_array($__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd) || $__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd instanceof ArrayAccess ? ($__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd["rating"] ?? null) : null) < 5)) {
                echo "-empty";
            }
            echo "\"></i>
\t\t\t\t</div>
\t\t\t";
        }
        // line 86
        echo "\t\t</div>
\t\t<h2 class=\"tt-title\"><a href=\"";
        // line 87
        echo (($__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6 = ($context["product"] ?? null)) && is_array($__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6) || $__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6 instanceof ArrayAccess ? ($__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6["href"] ?? null) : null);
        echo "\">";
        echo (($__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855 = ($context["product"] ?? null)) && is_array($__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855) || $__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855 instanceof ArrayAccess ? ($__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855["name"] ?? null) : null);
        echo "</a></h2>
\t\t";
        // line 88
        if ((($__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b = ($context["product"] ?? null)) && is_array($__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b) || $__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b instanceof ArrayAccess ? ($__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b["price"] ?? null) : null)) {
            echo " 
\t\t\t<div class=\"tt-price\">
\t\t\t\t";
            // line 90
            if ( !(($__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f = ($context["product"] ?? null)) && is_array($__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f) || $__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f instanceof ArrayAccess ? ($__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f["special"] ?? null) : null)) {
                echo " 
\t\t\t\t\t";
                // line 91
                echo (($__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0 = ($context["product"] ?? null)) && is_array($__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0) || $__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0 instanceof ArrayAccess ? ($__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0["price"] ?? null) : null);
                echo " 
\t\t\t\t";
            } else {
                // line 92
                echo " 
\t\t\t\t\t<span class=\"new-price\">";
                // line 93
                echo (($__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55 = ($context["product"] ?? null)) && is_array($__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55) || $__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55 instanceof ArrayAccess ? ($__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55["special"] ?? null) : null);
                echo "</span> <span class=\"old-price\">";
                echo (($__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a = ($context["product"] ?? null)) && is_array($__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a) || $__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a instanceof ArrayAccess ? ($__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a["price"] ?? null) : null);
                echo "</span>
\t\t\t\t";
            }
            // line 94
            echo " 
\t\t\t</div>
\t\t";
        }
        // line 97
        echo "\t\t";
        $context["product_options"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getProductOptions", [0 => (($__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88 = ($context["product"] ?? null)) && is_array($__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88) || $__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88 instanceof ArrayAccess ? ($__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88["product_id"] ?? null) : null)], "method", false, false, false, 97);
        echo " 
\t\t";
        // line 98
        if ((twig_length_filter($this->env, ($context["product_options"] ?? null)) > 0)) {
            echo " 
\t\t\t<div class=\"tt-option-block\">
\t\t\t\t<ul class=\"tt-options-swatch\">
\t\t\t\t\t";
            // line 101
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["product_options"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["product_option"]) {
                // line 102
                echo "\t\t\t\t\t\t<li class=\"\"><a class=\"options-color\" style=\"background-image: url(image/";
                echo (($__internal_be1db6a1ea9fa5c04c40f99df0ec5af053ca391863fc6256c5c4ee249724f758 = $context["product_option"]) && is_array($__internal_be1db6a1ea9fa5c04c40f99df0ec5af053ca391863fc6256c5c4ee249724f758) || $__internal_be1db6a1ea9fa5c04c40f99df0ec5af053ca391863fc6256c5c4ee249724f758 instanceof ArrayAccess ? ($__internal_be1db6a1ea9fa5c04c40f99df0ec5af053ca391863fc6256c5c4ee249724f758["image"] ?? null) : null);
                echo ")\" href=\"#\"></a></li>
\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product_option'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 104
            echo "\t\t\t\t</ul>
\t\t\t</div>
\t\t";
        }
        // line 107
        echo "\t\t<div class=\"tt-product-inside-hover\">
\t\t\t";
        // line 108
        if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "display_add_to_cart"], "method", false, false, false, 108) != "0")) {
            echo " 
\t\t\t\t<div class=\"tt-row-btn\">
\t\t\t\t\t";
            // line 110
            $context["enquiry"] = twig_constant("false");
            echo " 
\t\t\t\t\t";
            // line 111
            if ((twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "product_blocks_module"], "method", false, false, false, 111) != "")) {
                echo " ";
                $context["enquiry"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "productIsEnquiry", [0 => (($__internal_6e6eda1691934a8f5855a3221f5a9f036391304a5cda73a3a2009f2961a84c35 = ($context["product"] ?? null)) && is_array($__internal_6e6eda1691934a8f5855a3221f5a9f036391304a5cda73a3a2009f2961a84c35) || $__internal_6e6eda1691934a8f5855a3221f5a9f036391304a5cda73a3a2009f2961a84c35 instanceof ArrayAccess ? ($__internal_6e6eda1691934a8f5855a3221f5a9f036391304a5cda73a3a2009f2961a84c35["product_id"] ?? null) : null)], "method", false, false, false, 111);
                echo " ";
            }
            // line 112
            echo "\t\t\t\t\t";
            if (twig_test_iterable(($context["enquiry"] ?? null))) {
                echo " 
\t\t\t\t\t\t<a href=\"javascript:openPopup('";
                // line 113
                echo (($__internal_51c633083c79004f3cb5e9e2b2f3504f650f1561800582801028bcbcf733a06b = ($context["enquiry"] ?? null)) && is_array($__internal_51c633083c79004f3cb5e9e2b2f3504f650f1561800582801028bcbcf733a06b) || $__internal_51c633083c79004f3cb5e9e2b2f3504f650f1561800582801028bcbcf733a06b instanceof ArrayAccess ? ($__internal_51c633083c79004f3cb5e9e2b2f3504f650f1561800582801028bcbcf733a06b["popup_module"] ?? null) : null);
                echo "', '";
                echo (($__internal_064553f1273f2ea50405f85092d06733f3f2fe5d0fc42fda135e1fdc91ff26ae = ($context["product"] ?? null)) && is_array($__internal_064553f1273f2ea50405f85092d06733f3f2fe5d0fc42fda135e1fdc91ff26ae) || $__internal_064553f1273f2ea50405f85092d06733f3f2fe5d0fc42fda135e1fdc91ff26ae instanceof ArrayAccess ? ($__internal_064553f1273f2ea50405f85092d06733f3f2fe5d0fc42fda135e1fdc91ff26ae["product_id"] ?? null) : null);
                echo "')\" class=\"tt-btn-addtocart thumbprod-button-bg\">";
                echo (($__internal_7bef02f75e2984f8c7fcd4fd7871e286c87c0fdcb248271a136b01ac6dd5dd54 = ($context["enquiry"] ?? null)) && is_array($__internal_7bef02f75e2984f8c7fcd4fd7871e286c87c0fdcb248271a136b01ac6dd5dd54) || $__internal_7bef02f75e2984f8c7fcd4fd7871e286c87c0fdcb248271a136b01ac6dd5dd54 instanceof ArrayAccess ? ($__internal_7bef02f75e2984f8c7fcd4fd7871e286c87c0fdcb248271a136b01ac6dd5dd54["block_name"] ?? null) : null);
                echo "</a>
\t\t\t\t\t";
            } else {
                // line 114
                echo " 
\t\t\t\t\t\t<a onclick=\"cart.add('";
                // line 115
                echo (($__internal_d6ae6b41786cc4be7778386d06cb288c8e6ffd74e055cfed45d7a5c8854d0c8f = ($context["product"] ?? null)) && is_array($__internal_d6ae6b41786cc4be7778386d06cb288c8e6ffd74e055cfed45d7a5c8854d0c8f) || $__internal_d6ae6b41786cc4be7778386d06cb288c8e6ffd74e055cfed45d7a5c8854d0c8f instanceof ArrayAccess ? ($__internal_d6ae6b41786cc4be7778386d06cb288c8e6ffd74e055cfed45d7a5c8854d0c8f["product_id"] ?? null) : null);
                echo "');\" class=\"tt-btn-addtocart thumbprod-button-bg\">";
                echo ($context["button_cart"] ?? null);
                echo "</a>
\t\t\t\t\t";
            }
            // line 116
            echo " 
\t\t\t\t</div>
\t\t\t";
        }
        // line 119
        echo "\t\t\t<div class=\"tt-row-btn\">
\t\t\t\t<a href=\"index.php?route=product/quickview&amp;product_id=";
        // line 120
        echo (($__internal_1dcdec7ec31e102fbfe45103ea3599c92c8609311e43d40ca0d95d0369434327 = ($context["product"] ?? null)) && is_array($__internal_1dcdec7ec31e102fbfe45103ea3599c92c8609311e43d40ca0d95d0369434327) || $__internal_1dcdec7ec31e102fbfe45103ea3599c92c8609311e43d40ca0d95d0369434327 instanceof ArrayAccess ? ($__internal_1dcdec7ec31e102fbfe45103ea3599c92c8609311e43d40ca0d95d0369434327["product_id"] ?? null) : null);
        echo "\" class=\"tt-btn-quickview\" data-toggle=\"modal\" data-target=\"#ModalquickView\"></a>
\t\t\t\t";
        // line 121
        if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "display_add_to_wishlist"], "method", false, false, false, 121) != "0")) {
            echo " 
\t\t\t\t\t<a href=\"javascript:wishlist.add('";
            // line 122
            echo (($__internal_891ba2f942018e94e4bfa8069988f305bbaad7f54a64aeee069787f1084a9412 = ($context["product"] ?? null)) && is_array($__internal_891ba2f942018e94e4bfa8069988f305bbaad7f54a64aeee069787f1084a9412) || $__internal_891ba2f942018e94e4bfa8069988f305bbaad7f54a64aeee069787f1084a9412 instanceof ArrayAccess ? ($__internal_891ba2f942018e94e4bfa8069988f305bbaad7f54a64aeee069787f1084a9412["product_id"] ?? null) : null);
            echo "');\" class=\"tt-btn-wishlist\"></a>
\t\t\t\t";
        }
        // line 124
        echo "\t\t\t\t";
        if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "display_add_to_compare"], "method", false, false, false, 124) != "0")) {
            echo " 
\t\t\t\t\t<a href=\"javascript:compare.add('";
            // line 125
            echo (($__internal_694b5f53081640f33aab1567e85e28c247e6a7c4674010716df6de8eae4819e9 = ($context["product"] ?? null)) && is_array($__internal_694b5f53081640f33aab1567e85e28c247e6a7c4674010716df6de8eae4819e9) || $__internal_694b5f53081640f33aab1567e85e28c247e6a7c4674010716df6de8eae4819e9 instanceof ArrayAccess ? ($__internal_694b5f53081640f33aab1567e85e28c247e6a7c4674010716df6de8eae4819e9["product_id"] ?? null) : null);
            echo "');\" class=\"tt-btn-compare\"></a>
\t\t\t\t";
        }
        // line 127
        echo "\t\t\t</div>
\t\t</div>
\t</div>
</div>";
    }

    public function getTemplateName()
    {
        return "wokiee/template/new_elements/product.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  506 => 127,  501 => 125,  496 => 124,  491 => 122,  487 => 121,  483 => 120,  480 => 119,  475 => 116,  468 => 115,  465 => 114,  456 => 113,  451 => 112,  445 => 111,  441 => 110,  436 => 108,  433 => 107,  428 => 104,  419 => 102,  415 => 101,  409 => 98,  404 => 97,  399 => 94,  392 => 93,  389 => 92,  384 => 91,  380 => 90,  375 => 88,  369 => 87,  366 => 86,  358 => 83,  352 => 82,  346 => 81,  340 => 80,  334 => 79,  331 => 78,  329 => 77,  322 => 75,  318 => 74,  309 => 69,  298 => 65,  290 => 64,  282 => 63,  274 => 62,  266 => 61,  258 => 60,  250 => 59,  246 => 58,  238 => 54,  235 => 53,  233 => 52,  229 => 51,  225 => 49,  211 => 47,  205 => 45,  199 => 42,  195 => 40,  189 => 38,  183 => 36,  180 => 35,  178 => 34,  174 => 33,  168 => 31,  165 => 30,  162 => 29,  160 => 28,  156 => 27,  153 => 26,  146 => 24,  143 => 23,  134 => 22,  131 => 21,  128 => 20,  125 => 19,  122 => 18,  119 => 17,  117 => 16,  111 => 15,  107 => 14,  102 => 13,  87 => 11,  82 => 10,  67 => 8,  63 => 7,  49 => 6,  44 => 4,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "wokiee/template/new_elements/product.twig", "/home2/kadore/public_html/teste/catalog/view/theme/wokiee/template/new_elements/product.twig");
    }
}

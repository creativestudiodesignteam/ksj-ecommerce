<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* wokiee/template/extension/module/featured.twig */
class __TwigTemplate_1e8c6a7a0f88c86f1cf1e88bda0d42417e8f6c88479239c20c63e0541fd81240 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ((twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "has", [0 => "theme_options"], "method", false, false, false, 1) == twig_constant("true"))) {
            echo " 
  ";
            // line 2
            $context["theme_options"] = twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "get", [0 => "theme_options"], "method", false, false, false, 2);
            // line 3
            echo "  ";
            $context["class"] = "col-6 col-md-4 col-lg-3";
            // line 4
            echo "  ";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_per_pow"], "method", false, false, false, 4) == 1)) {
                $context["class"] = "col-12";
            }
            // line 5
            echo "  ";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_per_pow"], "method", false, false, false, 5) == 2)) {
                $context["class"] = "col-12 col-md-6";
            }
            // line 6
            echo "  ";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_per_pow"], "method", false, false, false, 6) == 3)) {
                $context["class"] = "col-12 col-md-6 col-lg-4";
            }
            // line 7
            echo "  ";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_per_pow"], "method", false, false, false, 7) == 4)) {
                $context["class"] = "col-6 col-md-4 col-lg-3";
            }
            // line 8
            echo "  ";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_per_pow"], "method", false, false, false, 8) == 5)) {
                $context["class"] = "col-6 col-md-4 col-lg-25";
            }
            // line 9
            echo "  ";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_per_pow"], "method", false, false, false, 9) == 6)) {
                $context["class"] = "col-6 col-md-3 col-lg-2";
            }
            // line 10
            echo "
  <div class=\"container-indent\">
    <div class=\"container container-fluid-custom-mobile-padding\">
      <div class=\"tt-block-title\">
        <h1 class=\"tt-title\">";
            // line 14
            echo ($context["heading_title"] ?? null);
            echo "</h1>
      </div>

      ";
            // line 17
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_scroll_featured"], "method", false, false, false, 17) != "0")) {
                echo " 
      <div class=\"tt-carousel-products row arrow-location-tab arrow-location-tab01 tt-alignment-img tt-layout-product-item slick-animated-show-js\" data-item=\"";
                // line 18
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_per_pow"], "method", false, false, false, 18);
                echo "\">
      ";
            } else {
                // line 20
                echo "      <div class=\"row tt-layout-product-item\">
      ";
            }
            // line 22
            echo "        <div class=\"";
            echo ($context["class"] ?? null);
            echo "\">
          ";
            // line 23
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["products"] ?? null));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 24
                echo "            ";
                if ((twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 24) > 1)) {
                    // line 25
                    echo "              </div>
              <div class=\"";
                    // line 26
                    echo ($context["class"] ?? null);
                    echo "\">
            ";
                }
                // line 28
                echo "              ";
                $this->loadTemplate("wokiee/template/new_elements/product.twig", "wokiee/template/extension/module/featured.twig", 28)->display($context);
                // line 29
                echo "          ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo " 
        </div>
      </div>
    </div>
  </div>
";
        }
    }

    public function getTemplateName()
    {
        return "wokiee/template/extension/module/featured.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 29,  134 => 28,  129 => 26,  126 => 25,  123 => 24,  106 => 23,  101 => 22,  97 => 20,  92 => 18,  88 => 17,  82 => 14,  76 => 10,  71 => 9,  66 => 8,  61 => 7,  56 => 6,  51 => 5,  46 => 4,  43 => 3,  41 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "wokiee/template/extension/module/featured.twig", "");
    }
}

<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* wokiee/css/custom_colors.twig */
class __TwigTemplate_51af8ab0e8e91b9b234d52a2633ab43b62bdb7f208df9752238df0f817faf718 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "colors_status"], "method", false, false, false, 1) == "1")) {
            echo " 
<style type=\"text/css\">
\t";
            // line 3
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 3) != "")) {
                echo " 
\t.datepicker table tr td.day:hover {
\t    color: ";
                // line 5
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 5);
                echo ";
\t}
\t.datepicker table tr td.today:hover, .datepicker table tr td.today:hover:hover, .datepicker table tr td.today.disabled:hover, .datepicker table tr td.today.disabled:hover:hover, .datepicker table tr td.today:active, .datepicker table tr td.today:hover:active, .datepicker table tr td.today.disabled:active, .datepicker table tr td.today.disabled:hover:active, .datepicker table tr td.today.active, .datepicker table tr td.today:hover.active, .datepicker table tr td.today.disabled.active, .datepicker table tr td.today.disabled:hover.active, .datepicker table tr td.today.disabled, .datepicker table tr td.today:hover.disabled, .datepicker table tr td.today.disabled.disabled, .datepicker table tr td.today.disabled:hover.disabled, .datepicker table tr td.today[disabled], .datepicker table tr td.today:hover[disabled], .datepicker table tr td.today.disabled[disabled], .datepicker table tr td.today.disabled:hover[disabled] {
\t    color: ";
                // line 8
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 8);
                echo "
\t}
\t.datepicker table tr td.today:active, .datepicker table tr td.today:hover:active, .datepicker table tr td.today.disabled:active, .datepicker table tr td.today.disabled:hover:active, .datepicker table tr td.today.active, .datepicker table tr td.today:hover.active, .datepicker table tr td.today.disabled.active, .datepicker table tr td.today.disabled:hover.active {
\t    color: ";
                // line 11
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 11);
                echo " \\9
\t}
\t.datepicker table tr td.active, .datepicker table tr td.active:hover, .datepicker table tr td.active.disabled, .datepicker table tr td.active.disabled:hover {
\t    color: ";
                // line 14
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 14);
                echo "
\t}
\t.datepicker table tr td.active:hover, .datepicker table tr td.active:hover:hover, .datepicker table tr td.active.disabled:hover, .datepicker table tr td.active.disabled:hover:hover, .datepicker table tr td.active:active, .datepicker table tr td.active:hover:active, .datepicker table tr td.active.disabled:active, .datepicker table tr td.active.disabled:hover:active, .datepicker table tr td.active.active, .datepicker table tr td.active:hover.active, .datepicker table tr td.active.disabled.active, .datepicker table tr td.active.disabled:hover.active, .datepicker table tr td.active.disabled, .datepicker table tr td.active:hover.disabled, .datepicker table tr td.active.disabled.disabled, .datepicker table tr td.active.disabled:hover.disabled, .datepicker table tr td.active[disabled], .datepicker table tr td.active:hover[disabled], .datepicker table tr td.active.disabled[disabled], .datepicker table tr td.active.disabled:hover[disabled] {
\t    color: ";
                // line 17
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 17);
                echo "
\t}
\t.datepicker table tr td.active:active, .datepicker table tr td.active:hover:active, .datepicker table tr td.active.disabled:active, .datepicker table tr td.active.disabled:hover:active, .datepicker table tr td.active.active, .datepicker table tr td.active:hover.active, .datepicker table tr td.active.disabled.active, .datepicker table tr td.active.disabled:hover.active {
\t    color: ";
                // line 20
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 20);
                echo " \\9
\t}
\t.datepicker table tr td span:hover {
\t    color: ";
                // line 23
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 23);
                echo "
\t}
\t.datepicker table tr td span.active, .datepicker table tr td span.active:hover, .datepicker table tr td span.active.disabled, .datepicker table tr td span.active.disabled:hover {
\t    background-color: ";
                // line 26
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 26);
                echo ";
\t}
\t.datepicker table tr td span.active:hover, .datepicker table tr td span.active:hover:hover, .datepicker table tr td span.active.disabled:hover, .datepicker table tr td span.active.disabled:hover:hover, .datepicker table tr td span.active:active, .datepicker table tr td span.active:hover:active, .datepicker table tr td span.active.disabled:active, .datepicker table tr td span.active.disabled:hover:active, .datepicker table tr td span.active.active, .datepicker table tr td span.active:hover.active, .datepicker table tr td span.active.disabled.active, .datepicker table tr td span.active.disabled:hover.active, .datepicker table tr td span.active.disabled, .datepicker table tr td span.active:hover.disabled, .datepicker table tr td span.active.disabled.disabled, .datepicker table tr td span.active.disabled:hover.disabled, .datepicker table tr td span.active[disabled], .datepicker table tr td span.active:hover[disabled], .datepicker table tr td span.active.disabled[disabled], .datepicker table tr td span.active.disabled:hover[disabled] {
\t    background-color: ";
                // line 29
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 29);
                echo "
\t}
\t.datepicker thead tr:first-child th:hover, .datepicker tfoot tr:first-child th:hover {
\t    background: ";
                // line 32
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 32);
                echo "
\t}
\t/* \t\t\t\t\t\t\t\t------------------------ */
\t.mfp-close:hover {
\t    color: ";
                // line 36
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 36);
                echo "
\t}
\t.mfp-arrow:hover, button.mfp-arrow:hover {
\t    color: ";
                // line 39
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 39);
                echo "
\t}
\t.icon-AES2562 .path1:before {
\t    color: ";
                // line 42
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 42);
                echo "
\t}
\t.icon-AES2562 .path11:before {
\t    color: ";
                // line 45
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 45);
                echo "
\t}
\t.icon-AES256 .path1:before {
\t    color: ";
                // line 48
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 48);
                echo "
\t}
\t.icon-AES256 .path11:before {
\t    color: ";
                // line 51
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 51);
                echo "
\t}
\t.tt-top-panel {
\t    background: ";
                // line 54
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 54);
                echo "
\t}
\theader .tt-logo .tt-title {
\t    color: ";
                // line 57
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 57);
                echo ";
\t}
\theader .tt-dropdown-obj .tt-dropdown-toggle:hover {
\t    color: ";
                // line 60
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 60);
                echo "
\t}
\theader .tt-dropdown-obj.active .tt-dropdown-toggle {
\t    color: ";
                // line 63
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 63);
                echo "
\t}
\t@media (max-width: 1024px) {
\t    header .tt-account .tt-dropdown-menu .tt-dropdown-inner ul li a:hover {
\t        color: ";
                // line 67
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 67);
                echo "
\t    }
\t    header .tt-account .tt-dropdown-menu .tt-dropdown-inner ul li a:hover [class^=\"icon-\"] {
\t        color: ";
                // line 70
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 70);
                echo "
\t    }
\t}
\theader .tt-account ul li a:hover {
\t    color: ";
                // line 74
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 74);
                echo "
\t}
\theader .tt-account ul li a:hover [class^=\"icon-\"] {
\t    color: ";
                // line 77
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 77);
                echo "
\t}
\t@media (max-width: 1024px) {
\t    header .tt-multi-obj .tt-dropdown-menu .tt-dropdown-inner ul li a:hover {
\t        color: ";
                // line 81
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 81);
                echo "
\t    }
\t    header .tt-multi-obj .tt-dropdown-menu .tt-dropdown-inner ul li a:hover [class^=\"icon-\"] {
\t        color: ";
                // line 84
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 84);
                echo "
\t    }
\t    header .tt-multi-obj .tt-dropdown-menu .tt-dropdown-inner ul li.active a {
\t        color: ";
                // line 87
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 87);
                echo "
\t    }
\t    header .tt-multi-obj .tt-dropdown-menu .tt-dropdown-inner ul li.active a [class^=\"icon-\"] {
\t        color: ";
                // line 90
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 90);
                echo "
\t    }
\t}
\theader .tt-multi-obj ul li a:hover {
\t    color: ";
                // line 94
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 94);
                echo "
\t}
\theader .tt-multi-obj ul li a:hover [class^=\"icon-\"] {
\t    color: ";
                // line 97
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 97);
                echo "
\t}
\theader .tt-multi-obj ul li.active a {
\t    color: ";
                // line 100
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 100);
                echo "
\t}
\theader .tt-multi-obj ul li.active a [class^=\"icon-\"] {
\t    color: ";
                // line 103
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 103);
                echo "
\t}
\theader .tt-dropdown-obj:not(.tt-search) .tt-mobile-add .tt-close:hover {
\t    color: ";
                // line 106
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 106);
                echo "
\t}
\theader .tt-dropdown-obj:not(.tt-search) .tt-mobile-add .tt-close:hover:before {
\t    color: ";
                // line 109
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 109);
                echo "
\t}
\theader .tt-cart .tt-dropdown-toggle .tt-badge-cart {
\t    background: ";
                // line 112
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 112);
                echo ";
\t}
\theader .tt-cart .tt-cart-layout .tt-cart-content .tt-cart-list .tt-item a:not([class]):hover .tt-title {
\t    color: ";
                // line 115
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 115);
                echo "
\t}
\theader .tt-cart .tt-cart-layout .tt-cart-content .tt-cart-list .tt-item-close .tt-btn-close:hover:before {
\t    color: ";
                // line 118
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 118);
                echo "
\t}
\t@media (min-width: 1025px) {
\t    header .tt-search .tt-dropdown-menu .tt-btn-search:hover {
\t        color: ";
                // line 122
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 122);
                echo "
\t    }
\t    header .tt-search .tt-dropdown-menu .tt-btn-close:hover {
\t        color: ";
                // line 125
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 125);
                echo "
\t    }
\t}
\t@media (max-width: 1024px) {
\t    header .tt-search .tt-dropdown-menu .tt-btn-close:hover {
\t        color: ";
                // line 130
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 130);
                echo "
\t    }
\t    header .tt-search .tt-dropdown-menu .tt-btn-search {
\t        color: ";
                // line 133
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 133);
                echo ";
\t    }
\t}
\t@media (min-width: 1025px) {
\t    header .tt-search .search-results ul>li a:hover .tt-title {
\t        color: ";
                // line 138
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 138);
                echo "
\t    }
\t}
\theader .tt-search .tt-view-all {
\t    color: ";
                // line 142
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 142);
                echo ";
\t}
\theader .tt-color-scheme-01 .tt-desctop-menu.tt-hover-02 nav>ul li.dropdown>a:hover {
\t    background: ";
                // line 145
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 145);
                echo "
\t}
\theader .tt-color-scheme-01 .tt-desctop-menu.tt-hover-02 nav>ul li.dropdown.active>a {
\t    background: ";
                // line 148
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 148);
                echo "
\t}
\theader .tt-color-scheme-01 .tt-desctop-menu.tt-hover-02 nav>ul li.dropdown.selected>a {
\t    background: ";
                // line 151
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 151);
                echo "
\t}
\theader .tt-color-scheme-01 .tt-desctop-menu:not(.tt-hover-02) nav>ul li.dropdown>a:hover {
\t    color: ";
                // line 154
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 154);
                echo ";
\t}
\theader .tt-color-scheme-01 .tt-desctop-menu:not(.tt-hover-02) nav>ul li.dropdown.active>a {
\t    color: ";
                // line 157
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 157);
                echo ";
\t}
\theader .tt-color-scheme-01 .tt-desctop-menu:not(.tt-hover-02) nav>ul li.dropdown.selected>a {
\t    color: ";
                // line 160
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 160);
                echo ";
\t}
\theader .tt-color-scheme-01 .tt-dropdown-obj .tt-dropdown-toggle:hover {
\t    color: ";
                // line 163
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 163);
                echo "
\t}
\theader .tt-color-scheme-02 .tt-box-info ul li [class^=\"icon-\"] {
\t    color: ";
                // line 166
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 166);
                echo "
\t}
\theader .tt-color-scheme-02 .tt-desctop-menu.tt-hover-02 nav>ul li.dropdown>a:hover {
\t    background: ";
                // line 169
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 169);
                echo "
\t}
\theader .tt-color-scheme-02 .tt-desctop-menu.tt-hover-02 nav>ul li.dropdown.active>a {
\t    background: ";
                // line 172
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 172);
                echo "
\t}
\theader .tt-color-scheme-02 .tt-desctop-menu.tt-hover-02 nav>ul li.dropdown.selected>a {
\t    background: ";
                // line 175
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 175);
                echo "
\t}
\theader .tt-color-scheme-02 .tt-desctop-menu:not(.tt-hover-02) nav>ul li.dropdown>a:hover {
\t    color: ";
                // line 178
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 178);
                echo ";
\t}
\theader .tt-color-scheme-02 .tt-desctop-menu:not(.tt-hover-02) nav>ul li.dropdown.active>a {
\t    color: ";
                // line 181
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 181);
                echo ";
\t}
\theader .tt-color-scheme-02 .tt-desctop-menu:not(.tt-hover-02) nav>ul li.dropdown.selected>a {
\t    color: ";
                // line 184
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 184);
                echo ";
\t}
\theader .tt-color-scheme-02 .tt-dropdown-obj .tt-dropdown-toggle:hover {
\t    color: ";
                // line 187
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 187);
                echo "
\t}
\theader .tt-color-scheme-03 .tt-desctop-menu.tt-hover-02 nav>ul li.dropdown>a:hover {
\t    background: ";
                // line 190
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 190);
                echo "
\t}
\theader .tt-color-scheme-03 .tt-desctop-menu.tt-hover-02 nav>ul li.dropdown.active>a {
\t    background: ";
                // line 193
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 193);
                echo "
\t}
\theader .tt-color-scheme-03 .tt-desctop-menu.tt-hover-02 nav>ul li.dropdown.selected>a {
\t    background: ";
                // line 196
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 196);
                echo "
\t}
\theader .tt-color-scheme-03 .tt-desctop-menu:not(.tt-hover-02) nav>ul li.dropdown>a:hover {
\t    color: ";
                // line 199
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 199);
                echo ";
\t}
\theader .tt-color-scheme-03 .tt-desctop-menu:not(.tt-hover-02) nav>ul li.dropdown.active>a {
\t    color: ";
                // line 202
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 202);
                echo ";
\t}
\theader .tt-color-scheme-03 .tt-desctop-menu:not(.tt-hover-02) nav>ul li.dropdown.selected>a {
\t    color: ";
                // line 205
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 205);
                echo ";
\t}
\theader .tt-color-scheme-03 .tt-dropdown-obj .tt-dropdown-toggle:hover {
\t    color: ";
                // line 208
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 208);
                echo "
\t}
\t.tt-desctop-menu .header-menu-product a:hover .tt-title {
\t    color: ";
                // line 211
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 211);
                echo "
\t}
\t.tt-desctop-menu .tt-title-submenu:hover {
\t    color: ";
                // line 214
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 214);
                echo "
\t}
\t.tt-desctop-menu .tt-title-submenu:hover a {
\t    color: ";
                // line 217
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 217);
                echo "
\t}
\t.tt-desctop-menu .tt-title-submenu.active {
\t    color: ";
                // line 220
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 220);
                echo "
\t}
\t.tt-desctop-menu .tt-title-submenu.active a {
\t    color: ";
                // line 223
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 223);
                echo "
\t}
\t.tt-desctop-menu .tt-megamenu-submenu>li>a:hover {
\t    color: ";
                // line 226
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 226);
                echo "
\t}
\t.tt-desctop-menu .tt-megamenu-submenu>li.active>a {
\t    color: ";
                // line 229
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 229);
                echo "
\t}
\t.tt-desctop-menu .tt-megamenu-submenu>li ul li a:hover {
\t    color: ";
                // line 232
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 232);
                echo "
\t}
\t.tt-desctop-menu .tt-megamenu-submenu>li ul li.active>a {
\t    color: ";
                // line 235
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 235);
                echo "
\t}
\t.tt-desctop-menu:not(.tt-hover-02) li.dropdown>a:hover {
\t    color: ";
                // line 238
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 238);
                echo ";
\t}
\t.tt-desctop-menu:not(.tt-hover-02) li.dropdown.active>a {
\t    color: ";
                // line 241
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 241);
                echo ";
\t}
\t.tt-desctop-menu:not(.tt-hover-02) li.dropdown.selected>a {
\t    color: ";
                // line 244
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 244);
                echo ";
\t}
\t.tt-desctop-menu.tt-hover-02 nav>ul li.dropdown>a:hover {
\t    background: ";
                // line 247
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 247);
                echo "
\t}
\t.tt-desctop-menu.tt-hover-02 nav>ul li.dropdown.active>a {
\t    background: ";
                // line 250
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 250);
                echo "
\t}
\t.tt-desctop-menu.tt-hover-02 nav>ul li.dropdown.selected>a {
\t    background: ";
                // line 253
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 253);
                echo "
\t}
\tbody:not(.touch-device) .tt-menu-toggle:hover i {
\t    color: ";
                // line 256
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 256);
                echo "
\t}
\t.panel-menu ul li a:hover {
\t    color: ";
                // line 259
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 259);
                echo ";
\t}
\t.panel-menu #mm0.mmpanel a:not(.mm-close):hover {
\t    color: ";
                // line 262
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 262);
                echo ";
\t}
\t.panel-menu #mm0.mmpanel a:not(.mm-close):hover:after {
\t    color: ";
                // line 265
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 265);
                echo "
\t}
\t.panel-menu .mm-close:hover, .panel-menu .mm-prev-level:hover, .panel-menu .mm-next-level:hover {
\t    color: ";
                // line 268
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 268);
                echo ";
\t}
\t.panel-menu .mm-close:hover:before .mm-prev-level:hover:before, .panel-menu .mm-next-level:hover:after {
\t    color: ";
                // line 271
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 271);
                echo "
\t}
\t.panel-menu li.mm-close-parent .mm-close:hover {
\t    color: ";
                // line 274
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 274);
                echo ";
\t}
\t.panel-menu li.mm-close-parent .mm-close:hover:before {
\t    color: ";
                // line 277
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 277);
                echo "
\t}
\t.panel-menu .mm-prev-level:hover {
\t    color: ";
                // line 280
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 280);
                echo ";
\t}
\t.panel-menu .mm-prev-level:hover:before {
\t    color: ";
                // line 283
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 283);
                echo "
\t}
\t.panel-menu .mm-next-level:hover {
\t    color: ";
                // line 286
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 286);
                echo ";
\t}
\t.panel-menu .mm-next-level:hover:after {
\t    color: ";
                // line 289
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 289);
                echo "
\t}
\t.panel-menu .mm-original-link:hover {
\t    color: ";
                // line 292
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 292);
                echo ";
\t}
\t.panel-menu .mm-original-link:hover:before {
\t    color: ";
                // line 295
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 295);
                echo "
\t}
\tfooter .tt-logo .tt-title {
\t    color: ";
                // line 298
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 298);
                echo ";
\t}
\t@media (min-width: 790px) {
\t    footer .tt-color-scheme-01 .tt-collapse-title a:hover {
\t        color: ";
                // line 302
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 302);
                echo "
\t    }
\t    footer .tt-color-scheme-01 .tt-collapse-content a {
\t        color: ";
                // line 305
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 305);
                echo "
\t    }
\t    footer .tt-color-scheme-01 .tt-list li a:hover, footer .tt-color-scheme-01 .tt-mobile-collapse .tt-collapse-content .tt-list li a:hover {
\t        color: ";
                // line 308
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 308);
                echo "
\t    }
\t    footer .tt-color-scheme-01 .tt-newsletter .form-control:focus {
\t        border-color: ";
                // line 311
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 311);
                echo ";
\t    }
\t    footer .tt-color-scheme-01 .tt-newsletter .btn {
\t        background: ";
                // line 314
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 314);
                echo ";
\t    }
\t    footer .tt-color-scheme-02 {
\t        background: ";
                // line 317
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 317);
                echo ";
\t    }
\t    footer .tt-color-scheme-03 .tt-mobile-collapse .tt-collapse-content a {
\t        color: ";
                // line 320
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 320);
                echo "
\t    }
\t    footer .tt-color-scheme-03 .tt-list li a:hover, footer .tt-color-scheme-03 .tt-mobile-collapse .tt-collapse-content .tt-list li a:hover {
\t        color: ";
                // line 323
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 323);
                echo "
\t    }
\t    footer .tt-color-scheme-03 .tt-list li.active a, footer .tt-color-scheme-03 .tt-mobile-collapse .tt-collapse-content .tt-list li.active a {
\t        color: ";
                // line 326
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 326);
                echo "
\t    }
\t    footer .tt-color-scheme-03 .tt-newsletter .form-control:focus {
\t        border-color: ";
                // line 329
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 329);
                echo ";
\t    }
\t    footer .tt-color-scheme-03 .tt-newsletter .btn {
\t        background: ";
                // line 332
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 332);
                echo ";
\t    }
\t    footer .tt-color-scheme-04 .tt-newsletter .form-control:focus {
\t        border-color: ";
                // line 335
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 335);
                echo ";
\t    }
\t    footer .tt-color-scheme-04 .tt-newsletter .btn {
\t        background: ";
                // line 338
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 338);
                echo ";
\t    }
\t}
\t@media (max-width: 789px) {
\t    footer .tt-mobile-collapse .tt-collapse-title:hover:not(:focus) {
\t        color: ";
                // line 343
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 343);
                echo "
\t    }
\t    footer .tt-newsletter .form-control:focus {
\t        border-color: ";
                // line 346
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 346);
                echo ";
\t    }
\t    footer .tt-newsletter .btn {
\t        background: ";
                // line 349
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 349);
                echo ";
\t    }
\t    footer .tt-list li a:hover {
\t        color: ";
                // line 352
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 352);
                echo "
\t    }
\t    footer address a {
\t        color: ";
                // line 355
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 355);
                echo "
\t    }
\t    footer .tt-logo .tt-title {
\t        color: ";
                // line 358
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 358);
                echo "
\t    }
\t}
\t@media (max-width: 789px) {
\t    .f-mobile-dark+.tt-back-to-top:hover {
\t        color: ";
                // line 363
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 363);
                echo "
\t    }
\t}
\t.tt-listing-post .tt-post .tt-post-content .tt-tag a {
\t    color: ";
                // line 367
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 367);
                echo ";
\t}

\t.tt-listing-post .tt-post .tt-post-content .tt-title a:hover {
\t    color: ";
                // line 371
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 371);
                echo "
\t}
\t.tt-listing-post .tt-post .tt-post-content .tt-meta .tt-comments a:hover {
\t    color: ";
                // line 374
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 374);
                echo "
\t}
\t.tt-listing-post .tt-post .tt-post-content .tt-meta .tt-comments a:hover .tt-icon {
\t    color: ";
                // line 377
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 377);
                echo "
\t}
\t.tt-video-block .link-video:hover:before {
\t    color: ";
                // line 380
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 380);
                echo "
\t}
\t.tt-box-link {
\t    color: ";
                // line 383
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 383);
                echo ";
\t}
\t.tt-post-single .tt-tag a {
\t    color: ";
                // line 386
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 386);
                echo ";
\t}
\t.tt-post-single .post-meta a {
\t    color: ";
                // line 389
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 389);
                echo ";
\t}
\t.tt-comments-layout .tt-item div[class^=\"tt-comments-level-\"] .tt-content .tt-btn {
\t    color: ";
                // line 392
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 392);
                echo ";
\t}
\t.tt-blog-thumb .tt-title-description .tt-tag {
\t    color: ";
                // line 395
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 395);
                echo "
\t}
\t.tt-blog-thumb .tt-title-description .tt-tag a {
\t    color: ";
                // line 398
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 398);
                echo ";
\t}
\t.tt-blog-thumb .tt-title-description .tt-title a:hover{
\t\tcolor: ";
                // line 401
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 401);
                echo ";
\t}
\t.tt-blog-thumb .tt-title-description .tt-meta .tt-comments a:hover {
\t    color: ";
                // line 404
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 404);
                echo "
\t}
\t.tt-blog-thumb .tt-title-description .tt-meta .tt-comments a:hover .tt-icon {
\t    color: ";
                // line 407
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 407);
                echo "
\t}
\t.tt-form-search .tt-btn-icon:hover {
\t    color: ";
                // line 410
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 410);
                echo "
\t}
\t.tt-list-inline li a:hover {
\t    color: ";
                // line 413
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 413);
                echo "
\t}
\t.tt-aside-post .item .tt-tag {
\t    color: ";
                // line 416
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 416);
                echo ";
\t}
\t.tt-aside-post .item .tt-tag a {
\t    color: ";
                // line 419
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 419);
                echo ";
\t}
\t.tt-aside-post .item>a:not([class]):hover .tt-title {
\t    color: ";
                // line 422
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 422);
                echo "
\t}
\t.tt-pagination ul li a:hover {
\t    color: ";
                // line 425
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 425);
                echo "
\t}
\t.tt-pagination ul li.active a {
\t    color: ";
                // line 428
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 428);
                echo "
\t}
\t.tt-pagination .btn-pagination:hover {
\t    color: ";
                // line 431
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 431);
                echo "
\t}
\t.tt-layout-01-post .tt-post .tt-post-content .tt-tag a {
\t    color: ";
                // line 434
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 434);
                echo ";
\t}
\t.tt-layout-01-post .tt-post .tt-post-content .tt-title a:hover {
\t    color: ";
                // line 437
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 437);
                echo "
\t}
\t.tt-layout-01-post .tt-post .tt-post-content .tt-meta .tt-comments a:hover {
\t    color: ";
                // line 440
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 440);
                echo "
\t}
\t.tt-layout-01-post .tt-post .tt-post-content .tt-meta .tt-comments a:hover .tt-icon {
\t    color: ";
                // line 443
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 443);
                echo "
\t}
\t.tt-layout-01-post .tt-post:hover .tt-post-img i {
\t    color: ";
                // line 446
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 446);
                echo "
\t}
\t.tt-portfolio-content figure figcaption .tt-btn-zomm:hover {
\t    color: ";
                // line 449
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 449);
                echo "
\t}
\t.tt-portfolio-content figure figcaption .tt-title a:hover {
\t    color: ";
                // line 452
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 452);
                echo "
\t}
\t.tt-product-single-info .tt-add-info ul li a:hover {
\t    color: ";
                // line 455
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 455);
                echo "
\t}
\t.tt-product-single-info .tt-price {
\t    color: ";
                // line 458
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 458);
                echo "
\t}
\t.tt-product-single-info .tt-price .new-price {
\t    color: ";
                // line 461
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 461);
                echo ";
\t}
\t.tt-product-single-info .tt-review a {
\t    color: ";
                // line 464
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 464);
                echo ";
\t}
\t.tt-review-block .tt-row-custom-02 a {
\t    color: ";
                // line 467
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 467);
                echo ";
\t}
\t.tt-review-block .tt-review-form .tt-message-info span {
\t    color: ";
                // line 470
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 470);
                echo "
\t}
\t.video-link-product [class^=\"icon-\"] {
\t    color: ";
                // line 473
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 473);
                echo ";
\t}
\t.product-information-buttons a {
\t    color: ";
                // line 476
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 476);
                echo ";
\t}
\tul.tt-options-swatch li:hover a:not(.options-color), ul.tt-options-swatch li.active a:not(.options-color) {
\t    background: ";
                // line 479
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 479);
                echo ";
\t}
\tul.tt-options-swatch li .options-color:after {
\t    border: 0px solid ";
                // line 482
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 482);
                echo "
\t}
\t.tt-product-single-img .tt-btn-zomm:hover {
\t    background: ";
                // line 485
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 485);
                echo ";
\t}
\t.tt-modal-addtocart.desctope .tt-modal-messages [class^=\"icon-\"] {
\t    color: ";
                // line 488
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 488);
                echo "
\t}
\t.tt-modal-addtocart.desctope .tt-modal-product .tt-title a:hover {
\t    color: ";
                // line 491
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 491);
                echo "
\t}
\t.tt-modal-addtocart.desctope .tt-cart-total .tt-total .tt-price {
\t    color: ";
                // line 494
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 494);
                echo ";
\t}
\t.tt-modal-addtocart.mobile .tt-modal-messages [class^=\"icon-\"] {
\t    color: ";
                // line 497
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 497);
                echo "
\t}
\t.modal .modal-header .close:hover {
\t    color: ";
                // line 500
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 500);
                echo "
\t}
\t.tt-modal-newsletter .tt-modal-newsletter-promo .tt-title-large {
\t    color: ";
                // line 503
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 503);
                echo ";
\t}
\t.tt-modal-newsletter .row-social-icon .tt-social-icon li a:hover {
\t    color: ";
                // line 506
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 506);
                echo "
\t}
\t.tt-modal-newsletter .checkbox-group label:hover {
\t    color: ";
                // line 509
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 509);
                echo "
\t}
\t.tt-layout-product-info-02 ul:not([class])>li a:hover {
\t    color: ";
                // line 512
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 512);
                echo "
\t}
\t.tt-modal-subsribe-good i {
\t    color: ";
                // line 515
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 515);
                echo ";
\t}
\t.tt-filters-options .tt-btn-toggle a {
\t    color: ";
                // line 518
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 518);
                echo ";
\t}
\t.tt-filters-options .tt-btn-toggle a:before {
\t    color: ";
                // line 521
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 521);
                echo "
\t}
\t@media (min-width: 1025px) {
\t    .tt-btn-col-close a:hover {
\t        color: ";
                // line 525
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 525);
                echo "
\t    }
\t}
\t@media (max-width: 1024px) {
\t    .tt-btn-col-close a:hover {
\t        color: ";
                // line 530
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 530);
                echo "
\t    }
\t}
\t.tt-collapse .tt-collapse-title:hover {
\t    color: ";
                // line 534
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 534);
                echo "
\t}
\t.tt-filter-list li a:hover {
\t    color: ";
                // line 537
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 537);
                echo "
\t}
\t.tt-filter-list li.active {
\t    color: ";
                // line 540
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 540);
                echo "
\t}
\t.tt-filter-list li.active a:before {
\t    color: ";
                // line 543
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 543);
                echo "
\t}
\t.tt-list-row li a:hover {
\t    color: ";
                // line 546
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 546);
                echo "
\t}
\t.tt-list-row li.active a {
\t    color: ";
                // line 549
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 549);
                echo "
\t}
\t.tt-aside .tt-item:hover .tt-title {
\t    color: ";
                // line 552
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 552);
                echo "
\t}
\t.tt-product.tt-view .tt-description .tt-add-info li a:hover, .tt-product-design02.tt-view .tt-description .tt-add-info li a:hover {
\t    color: ";
                // line 555
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 555);
                echo "
\t}
\t.tt-product.tt-view .tt-description .tt-title a:hover, .tt-product-design02.tt-view .tt-description .tt-title a:hover {
\t    color: ";
                // line 558
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 558);
                echo "
\t}
\t.tt-product.tt-view .tt-description .tt-btn-addtocart, .tt-product-design02.tt-view .tt-description .tt-btn-addtocart {
\t    color: ";
                // line 561
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 561);
                echo ";
\t}
\t.tt-product.tt-view .tt-description .tt-btn-addtocart:before, .tt-product-design02.tt-view .tt-description .tt-btn-addtocart:before {
\t    color: ";
                // line 564
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 564);
                echo ";
\t}
\t.tt-product.tt-view .tt-description .tt-btn-quickview:hover, .tt-product-design02.tt-view .tt-description .tt-btn-quickview:hover {
\t    color: ";
                // line 567
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 567);
                echo "
\t}
\t@media (min-width: 1025px) {
\t    .tt-product:not(.tt-view) .tt-image-box .tt-btn-quickview:hover {
\t        background: ";
                // line 571
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 571);
                echo ";
\t    }
\t    .tt-product:not(.tt-view) .tt-description .tt-add-info li a:hover {
\t        color: ";
                // line 574
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 574);
                echo "
\t    }
\t    .tt-product:not(.tt-view) .tt-description .tt-title a:hover {
\t        color: ";
                // line 577
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 577);
                echo "
\t    }
\t}
\t@media (min-width: 1025px) {
\t    .tt-product:not(.tt-view) .tt-description .tt-btn-addtocart {
\t        color: ";
                // line 582
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 582);
                echo ";
\t    }
\t    .tt-product:not(.tt-view) .tt-description .tt-btn-addtocart.thumbprod-button-bg {
\t        background-color: ";
                // line 585
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 585);
                echo ";
\t    }
\t    .tt-product:not(.tt-view) .tt-description .tt-btn-addtocart.thumbprod-button-bg:hover {
\t        background-color: ";
                // line 588
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 588);
                echo ";
\t    }
\t    .tt-product:not(.tt-view) .tt-description .tt-btn-addtocart:before {
\t        color: ";
                // line 591
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 591);
                echo ";
\t    }
\t}
\t@media (min-width: 1025px) {
\t    .tt-product-design02:not(.tt-view) .tt-description .tt-add-info li a:hover {
\t        color: ";
                // line 596
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 596);
                echo "
\t    }
\t    .tt-product-design02:not(.tt-view) .tt-description .tt-title a:hover {
\t        color: ";
                // line 599
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 599);
                echo "
\t    }
\t    .tt-product-design02:not(.tt-view) .tt-options-swatch li:hover a:not(.options-color) {
\t        background: ";
                // line 602
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 602);
                echo "
\t    }
\t    .tt-product-design02:not(.tt-view) .tt-options-swatch li.active a:not(.options-color) {
\t        background: ";
                // line 605
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 605);
                echo "
\t    }
\t    .tt-product-design02:not(.tt-view) .tt-btn-addtocart {
\t        color: ";
                // line 608
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 608);
                echo ";
\t    }
\t    .tt-product-design02:not(.tt-view) .tt-btn-addtocart:before {
\t        color: ";
                // line 611
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 611);
                echo ";
\t    }
\t    .tt-product-design02:not(.tt-view) .tt-btn-quickview:hover {
\t        color: ";
                // line 614
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 614);
                echo "
\t    }
\t    .tt-product-design02:not(.tt-view) .tt-btn-link:hover {
\t        color: ";
                // line 617
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 617);
                echo "
\t    }
\t}
\t@media (max-width: 1024px) {
\t    .tt-product:not(.tt-view) .tt-description .tt-add-info li a:hover, .tt-product-design02:not(.tt-view) .tt-description .tt-add-info li a:hover {
\t        color: ";
                // line 622
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 622);
                echo "
\t    }
\t    .tt-product:not(.tt-view) .tt-description .tt-title a:hover, .tt-product-design02:not(.tt-view) .tt-description .tt-title a:hover {
\t        color: ";
                // line 625
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 625);
                echo "
\t    }
\t}
\t@media (max-width: 1024px) {
\t    .tt-product:not(.tt-view) .tt-description .tt-btn-addtocart, .tt-product-design02:not(.tt-view) .tt-description .tt-btn-addtocart {
\t        background: ";
                // line 630
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 630);
                echo ";
\t    }
\t    .tt-product:not(.tt-view) .tt-description .tt-btn-quickview:hover, .tt-product-design02:not(.tt-view) .tt-description .tt-btn-quickview:hover {
\t        color: ";
                // line 633
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 633);
                echo "
\t    }
\t}
\t.tt-countdown_box .countdown-row .countdown-section {
\t    color: ";
                // line 637
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 637);
                echo ";
\t}
\t.tt-promo-box:not(.hover-type-2):hover .btn-underline {
\t    color: ";
                // line 640
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 640);
                echo "
\t}
\t.tt-collection-item:hover .tt-description .tt-title {
\t    color: ";
                // line 643
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 643);
                echo "
\t}
\t.tt-promo-fixed .tt-btn-close:hover {
\t    color: ";
                // line 646
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 646);
                echo "
\t}
\t.tt-promo-fixed .tt-description a:hover {
\t    color: ";
                // line 649
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 649);
                echo "
\t}
\t.tt-shopcart-table .tt-btn-close:hover {
\t    color: ";
                // line 652
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 652);
                echo "
\t}
\t.tt-shopcart-table .tt-title a:hover {
\t    color: ";
                // line 655
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 655);
                echo "
\t}
\t.tt-shopcart-table01 tfoot tr td {
\t    color: ";
                // line 658
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 658);
                echo ";
\t}
\t.tt-shopcart-table-02 .tt-btn-close:hover {
\t    color: ";
                // line 661
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 661);
                echo "
\t}
\t.tt-shopcart-table-02 .tt-title a:hover {
\t    color: ";
                // line 664
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 664);
                echo "
\t}
\t.tt-shopping-layout .tt-link-back {
\t    color: ";
                // line 667
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 667);
                echo ";
\t}
\t.tt-shopping-layout .tt-shop-btn svg {
\t    fill: ";
                // line 670
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 670);
                echo ";
\t}
\t.tt-table-shop-01 tbody td a {
\t    color: ";
                // line 673
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 673);
                echo ";
\t}
\t.tt-table-shop-02 tbody td a {
\t    color: ";
                // line 676
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 676);
                echo ";
\t}
\t.tt-shop-info .tt-item .tt-description strong {
\t    color: ";
                // line 679
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 679);
                echo "
\t}
\t.tt-shop-info .tt-item .tt-description a {
\t    color: ";
                // line 682
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 682);
                echo ";
\t}
\t.tt-lookbook .tt-hotspot .tt-btn:after {
\t    background-color: ";
                // line 685
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 685);
                echo ";
\t}
\t.tt-hotspot-content .tt-btn-close:hover {
\t    color: ";
                // line 688
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 688);
                echo "
\t}
\t.tt-hotspot-content .tt-description .tt-title a:hover {
\t    color: ";
                // line 691
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 691);
                echo "
\t}
\t.slider-revolution .video-play a:hover {
\t    color: ";
                // line 694
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 694);
                echo "
\t}
\t.slider-revolution [class^=\"btn\"],
\t.slider-revolution [class^=\"btn\"]:hover {
\t    background-color: ";
                // line 698
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 698);
                echo ";
\t}
\t#loader .dot {
\t    background: ";
                // line 701
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 701);
                echo "
\t}
\t@media (min-width: 1025px) {
\t    #tt-boxedbutton .rtlbutton .box-btn {
\t        background-color: ";
                // line 705
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 705);
                echo "
\t    }
\t    #tt-boxedbutton .rtlbutton .box-description {
\t        background-color: ";
                // line 708
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 708);
                echo ";
\t    }
\t    #tt-boxedbutton .rtlbutton .box-disable {
\t        color: ";
                // line 711
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 711);
                echo ";
\t    }
\t}
\t.btn,
\t.btn:hover {
\t    background: ";
                // line 716
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 716);
                echo ";
\t}
\t.btn.btn-dark:hover {
\t    background: ";
                // line 719
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 719);
                echo ";
\t}
\t.btn.btn-border {
\t    border: 2px solid ";
                // line 722
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 722);
                echo ";
\t    color: ";
                // line 723
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 723);
                echo ";
\t}
\t.btn-link, .btn-link:focus {
\t    color: ";
                // line 726
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 726);
                echo ";
\t}
\t.btn-link-02, .btn-link-02:focus {
\t    color: ";
                // line 729
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 729);
                echo ";
\t}
\t.btn-underline {
\t    border-bottom: 1px solid ";
                // line 732
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 732);
                echo ";
\t}
\t.btn-underline:not([class\$=\"color\"]) {
\t    color: ";
                // line 735
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 735);
                echo "
\t}
\t.tt-link {
\t    color: ";
                // line 738
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 738);
                echo ";
\t}
\th1 a:hover, h2 a:hover, h3 a:hover, h4 a:hover, h5 a:hover, h6 a:hover {
\t    color: ";
                // line 741
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 741);
                echo "
\t}
\t.link {
\t    color: ";
                // line 744
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 744);
                echo ";
\t}
\t.tt-list-dot>li a:hover {
\t    color: ";
                // line 747
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 747);
                echo "
\t}
\t.tt-list-dot>li:before {
\t    background: ";
                // line 750
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 750);
                echo ";
\t}
\t.tt-blockquote .tt-icon {
\t    color: ";
                // line 753
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 753);
                echo "
\t}
\t.tt-blockquote.add-hover:hover .tt-title {
\t    color: ";
                // line 756
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 756);
                echo "
\t}
\t.tt-block-title .tt-title a {
\t    color: ";
                // line 759
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 759);
                echo ";
\t}
\t.tt-block-title .tt-title a:before {
\t    background: ";
                // line 762
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 762);
                echo "
\t}
\t.tt-block-title .tt-title a:hover {
\t    color: ";
                // line 765
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 765);
                echo "
\t}
\t.tt-block-title .tt-title-small a {
\t    color: ";
                // line 768
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 768);
                echo ";
\t}
\t.tt-block-title .tt-title-small a:hover {
\t    color: ";
                // line 771
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 771);
                echo ";
\t}
\t.tt-box-faq-listing .tt-box-faq .tt-title a:hover {
\t    color: ";
                // line 774
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 774);
                echo "
\t}
\t@media (max-width: 790px) {
\t    .tt-about-box .tt-blockquote-02 .tt-icon {
\t        color: ";
                // line 778
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 778);
                echo "
\t    }
\t}
\t@media (min-width: 791px) {
\t    .tt-contact-box a:hover {
\t        color: ";
                // line 783
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 783);
                echo "
\t    }
\t}
\t.tt-contact-info .tt-icon {
\t    color: ";
                // line 787
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 787);
                echo "
\t}
\t.form-default .form-control:focus {
\t    border-color: ";
                // line 790
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 790);
                echo ";
\t}
\t.checkbox-group label .check {
\t    background: ";
                // line 793
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 793);
                echo ";
\t}
\t.checkbox-group:hover label {
\t    color: ";
                // line 796
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 796);
                echo "
\t}
\t.radio input:focus+.outer .inner {
\t    background-color: ";
                // line 799
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 799);
                echo "
\t}
\t.radio .inner {
\t    background-color: ";
                // line 802
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 802);
                echo ";
\t}
\t.radio:hover {
\t    color: ";
                // line 805
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 805);
                echo "
\t}
\t.tt-box-thumb .tt-title a:hover {
\t    color: ";
                // line 808
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 808);
                echo "
\t}
\t.tt-login-form .tt-item .additional-links a {
\t    color: ";
                // line 811
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 811);
                echo ";
\t}
\t.tt-slick-button .slick-arrow:hover {
\t    background: ";
                // line 814
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 814);
                echo ";
\t}
\t.tt-slick-button-vertical .slick-arrow:hover {
\t    background: ";
                // line 817
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 817);
                echo ";
\t}
\t.arrow-location-01 .slick-arrow:hover {
\t    background: ";
                // line 820
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 820);
                echo ";
\t}
\t.arrow-location-02 .slick-arrow:hover {
\t    background: ";
                // line 823
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 823);
                echo ";
\t}
\t.arrow-location-03 .slick-arrow:hover {
\t    color: ";
                // line 826
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 826);
                echo ";
\t}
\t.arrow-location-tab .slick-arrow:hover {
\t    background: ";
                // line 829
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 829);
                echo ";
\t}
\t.arrow-location-right-top .slick-arrow:hover {
\t    background: ";
                // line 832
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 832);
                echo ";
\t}
\t.arrow-location-center-02 .slick-arrow:hover {
\t    background: ";
                // line 835
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 835);
                echo "
\t}
\t@media (max-width: 789px) {
\t    .tt-back-to-top:hover {
\t        color: ";
                // line 839
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 839);
                echo "
\t    }
\t}
\t.tt-breadcrumb ul li a:hover {
\t    color: ";
                // line 843
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 843);
                echo "
\t}
\t.tt-services-block .tt-col-icon {
\t    color: ";
                // line 846
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 846);
                echo "
\t}
\t.tt-services-block:hover .tt-title {
\t    color: ";
                // line 849
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 849);
                echo "
\t}
\t.tt-img-box:hover {
\t    border-color: ";
                // line 852
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 852);
                echo "
\t}
\t.tt-layout-vertical .tt-title a:hover {
\t    color: ";
                // line 855
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 855);
                echo "
\t}
\t.tt-layout-vertical .tt-description .tt-add-info li a:hover {
\t    color: ";
                // line 858
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 858);
                echo "
\t}
\t.tt-content-info:hover .tt-title {
\t    color: ";
                // line 861
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 861);
                echo "
\t}
\t.tt-items-categories ul li a:hover {
\t    color: ";
                // line 864
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 864);
                echo "
\t}
\t.tt-items-categories ul li.active a {
\t    color: ";
                // line 867
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 867);
                echo "
\t}
\t.tt-items-categories.active .tt-title {
\t    color: ";
                // line 870
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 870);
                echo "
\t}
\t.tt-loader svg path, .tt-loader svg rect {
\t    fill: ";
                // line 873
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 873);
                echo "
\t}
\t.tt-base-color {
\t    color: ";
                // line 876
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 876);
                echo "
\t}


\t/* !!!!!!!!!!!!!!! rgba */
\t@media (min-width: 790px) {
\t\t.tt-back-to-top{
\t\t\tbackground-color: ";
                // line 883
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 883);
                echo ";
\t\t}
\t}
\t.btn:hover{
\t\tbackground: ";
                // line 887
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 887);
                echo ";
\t}
\t.tt-promo-box.hover-type-2:hover .tt-description-wrapper .tt-background{
\t\tbackground: ";
                // line 890
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 890);
                echo ";
\t}
\t.tt-promo-box.hover-type-3:hover .tt-description-wrapper .tt-title-large:not(.tt-base-color){
\t    color: ";
                // line 893
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 893);
                echo ";
\t}
\t.tt-promo-box.hover-type-3:hover .tt-description-wrapper .tt-title-large:not(.tt-base-color) span{
\t    color: ";
                // line 896
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 896);
                echo ";
\t}
\t.tt-top-panel.tt-color-dark a:not([class]) {
\t  color: ";
                // line 899
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 899);
                echo ";
\t}
\t.tt-top-panel.tt-color-dark a:not([class]):before {
\t  background-color: ";
                // line 902
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 902);
                echo ";
\t}
\t.tt-promo02 .tt-description a.tt-title:hover > *,
\t.tt-promo02.tt-no-btn:hover .tt-description .tt-title > *{
\t    color: ";
                // line 906
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 906);
                echo ";
\t}


\t.tt-product.tt-view .tt-description .tt-btn-compare:hover,
\t.tt-product-design02.tt-view .tt-description .tt-btn-compare:hover {
\t    color: ";
                // line 912
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 912);
                echo "
\t}
\t.tt-product.tt-view .tt-description .tt-btn-wishlist:hover,
\t.tt-product-design02.tt-view .tt-description .tt-btn-wishlist:hover {
\t    color: ";
                // line 916
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 916);
                echo "
\t}
\t@media (min-width: 1025px) {
\t .tt-product:not(.tt-view) .tt-image-box .tt-btn-wishlist:hover {
\t        background: ";
                // line 920
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 920);
                echo ";
\t    }
\t    .tt-product:not(.tt-view) .tt-image-box .tt-btn-compare:hover {
\t        background: ";
                // line 923
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 923);
                echo ";
\t    }
\t}
\t@media (min-width: 1025px) {
\t .tt-product-design02:not(.tt-view) .tt-btn-wishlist:hover {
\t        color: ";
                // line 928
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 928);
                echo "
\t    }
\t    .tt-product-design02:not(.tt-view) .tt-btn-compare:hover {
\t        color: ";
                // line 931
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 931);
                echo "
\t    }
\t}
\t@media (max-width: 1024px) {
\t.tt-product:not(.tt-view) .tt-description .tt-btn-wishlist:hover,
\t    .tt-product-design02:not(.tt-view) .tt-description .tt-btn-wishlist:hover {
\t        color: ";
                // line 937
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 937);
                echo "
\t    }
\t    .tt-product:not(.tt-view) .tt-description .tt-btn-compare:hover,
\t    .tt-product-design02:not(.tt-view) .tt-description .tt-btn-compare:hover {
\t        color: ";
                // line 941
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 941);
                echo "
\t    }
\t}


\t@media (min-width: 1025px){
\t    .tt-product:not(.tt-view) .tt-image-box .tt-btn-wishlist.active{
\t        background: ";
                // line 948
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 948);
                echo ";
\t    }
\t     .tt-product:not(.tt-view) .tt-image-box .tt-btn-compare.active{
\t        background: ";
                // line 951
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 951);
                echo ";
\t    }
\t}
\t@media (max-width: 1024px){
\t    .tt-product-design02:not(.tt-view) .tt-description .tt-btn-wishlist.active,
\t    .tt-product:not(.tt-view) .tt-description .tt-btn-wishlist.active {
\t        color: ";
                // line 957
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 957);
                echo ";
\t    }
\t    .tt-product-design02:not(.tt-view) .tt-description .tt-btn-compare.active,
\t    .tt-product:not(.tt-view) .tt-description .tt-btn-compare.active {
\t        color: ";
                // line 961
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 961);
                echo ";
\t    }
\t}

\t/* Custom */
\tbody .mfilter-slider-slider .ui-slider-range, body #mfilter-price-slider .ui-slider-range,
\tbody .mfilter-slider-slider .ui-slider-handle, body #mfilter-price-slider .ui-slider-handle {
\t    background: ";
                // line 968
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 968);
                echo " !important;
\t}

\t.mfilter-image ul li label:after, .mfilter-image_radio ul li label:after {
\t    border-color: ";
                // line 972
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 972);
                echo " !important;
\t}

\t.tt-product:not(.tt-view) .tt-description .tt-price .new-price,
\t.tt-aside .tt-item .tt-content .tt-price .sale-price {
\t\tcolor: ";
                // line 977
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "main_color"], "method", false, false, false, 977);
                echo ";
\t}
\t";
            }
            // line 979
            echo " 
</style>
";
        }
    }

    public function getTemplateName()
    {
        return "wokiee/css/custom_colors.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1925 => 979,  1919 => 977,  1911 => 972,  1904 => 968,  1894 => 961,  1887 => 957,  1878 => 951,  1872 => 948,  1862 => 941,  1855 => 937,  1846 => 931,  1840 => 928,  1832 => 923,  1826 => 920,  1819 => 916,  1812 => 912,  1803 => 906,  1796 => 902,  1790 => 899,  1784 => 896,  1778 => 893,  1772 => 890,  1766 => 887,  1759 => 883,  1749 => 876,  1743 => 873,  1737 => 870,  1731 => 867,  1725 => 864,  1719 => 861,  1713 => 858,  1707 => 855,  1701 => 852,  1695 => 849,  1689 => 846,  1683 => 843,  1676 => 839,  1669 => 835,  1663 => 832,  1657 => 829,  1651 => 826,  1645 => 823,  1639 => 820,  1633 => 817,  1627 => 814,  1621 => 811,  1615 => 808,  1609 => 805,  1603 => 802,  1597 => 799,  1591 => 796,  1585 => 793,  1579 => 790,  1573 => 787,  1566 => 783,  1558 => 778,  1551 => 774,  1545 => 771,  1539 => 768,  1533 => 765,  1527 => 762,  1521 => 759,  1515 => 756,  1509 => 753,  1503 => 750,  1497 => 747,  1491 => 744,  1485 => 741,  1479 => 738,  1473 => 735,  1467 => 732,  1461 => 729,  1455 => 726,  1449 => 723,  1445 => 722,  1439 => 719,  1433 => 716,  1425 => 711,  1419 => 708,  1413 => 705,  1406 => 701,  1400 => 698,  1393 => 694,  1387 => 691,  1381 => 688,  1375 => 685,  1369 => 682,  1363 => 679,  1357 => 676,  1351 => 673,  1345 => 670,  1339 => 667,  1333 => 664,  1327 => 661,  1321 => 658,  1315 => 655,  1309 => 652,  1303 => 649,  1297 => 646,  1291 => 643,  1285 => 640,  1279 => 637,  1272 => 633,  1266 => 630,  1258 => 625,  1252 => 622,  1244 => 617,  1238 => 614,  1232 => 611,  1226 => 608,  1220 => 605,  1214 => 602,  1208 => 599,  1202 => 596,  1194 => 591,  1188 => 588,  1182 => 585,  1176 => 582,  1168 => 577,  1162 => 574,  1156 => 571,  1149 => 567,  1143 => 564,  1137 => 561,  1131 => 558,  1125 => 555,  1119 => 552,  1113 => 549,  1107 => 546,  1101 => 543,  1095 => 540,  1089 => 537,  1083 => 534,  1076 => 530,  1068 => 525,  1061 => 521,  1055 => 518,  1049 => 515,  1043 => 512,  1037 => 509,  1031 => 506,  1025 => 503,  1019 => 500,  1013 => 497,  1007 => 494,  1001 => 491,  995 => 488,  989 => 485,  983 => 482,  977 => 479,  971 => 476,  965 => 473,  959 => 470,  953 => 467,  947 => 464,  941 => 461,  935 => 458,  929 => 455,  923 => 452,  917 => 449,  911 => 446,  905 => 443,  899 => 440,  893 => 437,  887 => 434,  881 => 431,  875 => 428,  869 => 425,  863 => 422,  857 => 419,  851 => 416,  845 => 413,  839 => 410,  833 => 407,  827 => 404,  821 => 401,  815 => 398,  809 => 395,  803 => 392,  797 => 389,  791 => 386,  785 => 383,  779 => 380,  773 => 377,  767 => 374,  761 => 371,  754 => 367,  747 => 363,  739 => 358,  733 => 355,  727 => 352,  721 => 349,  715 => 346,  709 => 343,  701 => 338,  695 => 335,  689 => 332,  683 => 329,  677 => 326,  671 => 323,  665 => 320,  659 => 317,  653 => 314,  647 => 311,  641 => 308,  635 => 305,  629 => 302,  622 => 298,  616 => 295,  610 => 292,  604 => 289,  598 => 286,  592 => 283,  586 => 280,  580 => 277,  574 => 274,  568 => 271,  562 => 268,  556 => 265,  550 => 262,  544 => 259,  538 => 256,  532 => 253,  526 => 250,  520 => 247,  514 => 244,  508 => 241,  502 => 238,  496 => 235,  490 => 232,  484 => 229,  478 => 226,  472 => 223,  466 => 220,  460 => 217,  454 => 214,  448 => 211,  442 => 208,  436 => 205,  430 => 202,  424 => 199,  418 => 196,  412 => 193,  406 => 190,  400 => 187,  394 => 184,  388 => 181,  382 => 178,  376 => 175,  370 => 172,  364 => 169,  358 => 166,  352 => 163,  346 => 160,  340 => 157,  334 => 154,  328 => 151,  322 => 148,  316 => 145,  310 => 142,  303 => 138,  295 => 133,  289 => 130,  281 => 125,  275 => 122,  268 => 118,  262 => 115,  256 => 112,  250 => 109,  244 => 106,  238 => 103,  232 => 100,  226 => 97,  220 => 94,  213 => 90,  207 => 87,  201 => 84,  195 => 81,  188 => 77,  182 => 74,  175 => 70,  169 => 67,  162 => 63,  156 => 60,  150 => 57,  144 => 54,  138 => 51,  132 => 48,  126 => 45,  120 => 42,  114 => 39,  108 => 36,  101 => 32,  95 => 29,  89 => 26,  83 => 23,  77 => 20,  71 => 17,  65 => 14,  59 => 11,  53 => 8,  47 => 5,  42 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "wokiee/css/custom_colors.twig", "/home2/kadore/public_html/teste/catalog/view/theme/wokiee/css/custom_colors.twig");
    }
}

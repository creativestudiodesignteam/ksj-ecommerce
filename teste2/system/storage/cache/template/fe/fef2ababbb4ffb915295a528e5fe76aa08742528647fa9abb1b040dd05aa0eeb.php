<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* wokiee/template/common/header.twig */
class __TwigTemplate_1585b3bb56ed9031ccf1e834125ceaee17619cffc763e0914a7bd07930e55e70 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ((twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "has", [0 => "theme_options"], "method", false, false, false, 1) == twig_constant("false"))) {
            echo " 
  <style>
    body {
      display: none !important;
    }
  </style>
  <script>
    window.location = 'themeinstall/index.php';
  </script>
";
        } else {
            // line 11
            echo "\t";
            $context["theme_options"] = twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "get", [0 => "theme_options"], "method", false, false, false, 11);
            // line 12
            echo "\t";
            $context["config"] = twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "get", [0 => "config"], "method", false, false, false, 12);
            // line 13
            echo "\t";
            $context["request"] = twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "get", [0 => "request"], "method", false, false, false, 13);
            // line 14
            echo "\t";
            $context["page_direction"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "page_direction"], "method", false, false, false, 14);
            echo " 
\t";
            // line 15
            $context["language_id"] = twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 15);
            echo " 
\t";
            // line 16
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["request"] ?? null), "get", [], "any", false, true, false, 16), "route", [], "array", true, true, false, 16)) {
                // line 17
                echo "\t\t";
                if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["request"] ?? null), "get", [], "any", false, true, false, 17), "product_id", [], "array", true, true, false, 17)) {
                    // line 18
                    echo "\t\t\t";
                    $context["class"] = ("-" . (($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = twig_get_attribute($this->env, $this->source, ($context["request"] ?? null), "get", [], "any", false, false, false, 18)) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4["product_id"] ?? null) : null));
                    // line 19
                    echo "\t\t";
                } elseif (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["request"] ?? null), "get", [], "any", false, true, false, 19), "path", [], "array", true, true, false, 19)) {
                    // line 20
                    echo "\t\t\t";
                    $context["class"] = ("-" . (($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = twig_get_attribute($this->env, $this->source, ($context["request"] ?? null), "get", [], "any", false, false, false, 20)) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144["path"] ?? null) : null));
                    // line 21
                    echo "\t\t";
                } elseif (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["request"] ?? null), "get", [], "any", false, true, false, 21), "manufacturer_id", [], "array", true, true, false, 21)) {
                    // line 22
                    echo "\t\t\t";
                    $context["class"] = ("-" . (($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = twig_get_attribute($this->env, $this->source, ($context["request"] ?? null), "get", [], "any", false, false, false, 22)) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b["manufacturer_id"] ?? null) : null));
                    // line 23
                    echo "\t\t";
                } elseif (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["request"] ?? null), "get", [], "any", false, true, false, 23), "information_id", [], "array", true, true, false, 23)) {
                    // line 24
                    echo "\t\t\t";
                    $context["class"] = ("-" . (($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 = twig_get_attribute($this->env, $this->source, ($context["request"] ?? null), "get", [], "any", false, false, false, 24)) && is_array($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002) || $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 instanceof ArrayAccess ? ($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002["information_id"] ?? null) : null));
                    // line 25
                    echo "\t\t";
                } else {
                    echo " 
\t\t\t";
                    // line 26
                    $context["class"] = "";
                    // line 27
                    echo "\t\t";
                }
                // line 28
                echo "
\t\t";
                // line 29
                $context["klasa"] = (twig_replace_filter((($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 = twig_get_attribute($this->env, $this->source, ($context["request"] ?? null), "get", [], "any", false, false, false, 29)) && is_array($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4) || $__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 instanceof ArrayAccess ? ($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4["route"] ?? null) : null), ["/" => "-"]) . ($context["class"] ?? null));
                // line 30
                echo "\t";
            } else {
                echo " 
\t\t";
                // line 31
                $context["klasa"] = "common-home";
                // line 32
                echo "\t";
            }
            echo " 
\t<!DOCTYPE html>
\t<html lang=\"";
            // line 34
            echo ($context["lang"] ?? null);
            echo "\" class=\"";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "layout_type"], "method", false, false, false, 34) == "1")) {
                echo "tt-boxed";
            }
            echo "\">
\t<head>
\t\t<title>";
            // line 36
            echo ($context["title"] ?? null);
            echo "</title>
\t\t<base href=\"";
            // line 37
            echo ($context["base"] ?? null);
            echo "\" />

\t\t<!-- Meta -->
\t\t<meta charset=\"utf-8\">
\t\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />
\t\t";
            // line 42
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "responsive_design"], "method", false, false, false, 42) != "0")) {
                echo " 
\t\t\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
\t\t";
            }
            // line 44
            echo " 
\t\t";
            // line 45
            if (($context["description"] ?? null)) {
                echo " 
\t\t\t<meta name=\"description\" content=\"";
                // line 46
                echo strip_tags(($context["description"] ?? null));
                echo "\" />
\t\t";
            }
            // line 47
            echo " 
\t\t";
            // line 48
            if (($context["keywords"] ?? null)) {
                echo " 
\t\t\t<meta name=\"keywords\" content=\"";
                // line 49
                echo strip_tags(($context["keywords"] ?? null));
                echo "\" />
\t\t";
            }
            // line 50
            echo " 
\t\t
\t\t";
            // line 52
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["links"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["link"]) {
                echo " 
\t\t\t<link href=\"";
                // line 53
                echo (($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 = $context["link"]) && is_array($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666) || $__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 instanceof ArrayAccess ? ($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666["href"] ?? null) : null);
                echo "\" rel=\"";
                echo (($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e = $context["link"]) && is_array($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e) || $__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e instanceof ArrayAccess ? ($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e["rel"] ?? null) : null);
                echo "\" />
\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['link'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 54
            echo " 

\t\t";
            // line 56
            $context["lista_plikow"] = [0 => "catalog/view/theme/wokiee/css/stylesheet.css", 1 => "catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css"];
            // line 59
            echo " 
\t\t
\t\t";
            // line 61
            if (((($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 = ($context["page_direction"] ?? null)) && is_array($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52) || $__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 instanceof ArrayAccess ? ($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52[($context["language_id"] ?? null)] ?? null) : null) == "RTL")) {
                // line 62
                echo "\t\t\t";
                $context["lista_plikow"] = twig_array_merge(($context["lista_plikow"] ?? null), [0 => "catalog/view/theme/wokiee/css/rtl.css"]);
                // line 63
                echo "\t\t";
            }
            // line 64
            echo "
\t\t";
            // line 65
            echo "<link href=\"//fonts.googleapis.com/css?family=Hind:800,700,600,500,400,300,200,100\" rel=\"stylesheet\" type=\"text/css\">";
            echo "

\t\t";
            // line 67
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "load_styles"], "method", false, false, false, 67) == "3")) {
                // line 68
                echo "\t\t\t";
                echo "<link href=\"//fonts.googleapis.com/css?family=Roboto:800,700,600,500,400,300,200,100\" rel=\"stylesheet\" type=\"text/css\">";
                echo "
\t\t\t";
                // line 69
                echo "<link href=\"//fonts.googleapis.com/css?family=Roboto+Slab:800,700,600,500,400,300,200,100\" rel=\"stylesheet\" type=\"text/css\">";
                echo "
\t\t\t";
                // line 70
                $context["lista_plikow"] = twig_array_merge(($context["lista_plikow"] ?? null), [0 => "catalog/view/theme/wokiee/css/books.css"]);
                // line 71
                echo "\t\t";
            }
            // line 72
            echo "
\t\t";
            // line 73
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "load_styles"], "method", false, false, false, 73) == "4")) {
                // line 74
                echo "\t\t\t";
                echo "<link href=\"//fonts.googleapis.com/css?family=Montserrat:800,700,600,500,400,300,200,100\" rel=\"stylesheet\" type=\"text/css\">";
                echo "
\t\t\t";
                // line 75
                $context["lista_plikow"] = twig_array_merge(($context["lista_plikow"] ?? null), [0 => "catalog/view/theme/wokiee/css/yoga.css"]);
                // line 76
                echo "\t\t";
            }
            // line 77
            echo "
\t\t";
            // line 78
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "load_styles"], "method", false, false, false, 78) == "5")) {
                // line 79
                echo "\t\t\t";
                echo "<link href=\"//fonts.googleapis.com/css?family=Roboto+Slab:800,700,600,500,400,300,200,100\" rel=\"stylesheet\" type=\"text/css\">";
                echo "
\t\t\t";
                // line 80
                echo "<link href=\"//fonts.googleapis.com/css?family=Playfair+Display:800,700,600,500,400,300,200,100\" rel=\"stylesheet\" type=\"text/css\">";
                echo "
\t\t\t";
                // line 81
                $context["lista_plikow"] = twig_array_merge(($context["lista_plikow"] ?? null), [0 => "catalog/view/theme/wokiee/css/glasses.css"]);
                // line 82
                echo "\t\t";
            }
            // line 83
            echo "
\t\t";
            // line 84
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "load_styles"], "method", false, false, false, 84) == "6")) {
                // line 85
                echo "\t\t\t";
                $context["lista_plikow"] = twig_array_merge(($context["lista_plikow"] ?? null), [0 => "catalog/view/theme/wokiee/css/kids_clothes.css"]);
                // line 86
                echo "\t\t";
            }
            // line 87
            echo "
\t\t";
            // line 88
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "load_styles"], "method", false, false, false, 88) == "7")) {
                // line 89
                echo "\t\t\t";
                echo "<link href=\"//fonts.googleapis.com/css?family=Roboto:800,700,600,500,400,300,200,100\" rel=\"stylesheet\" type=\"text/css\">";
                echo "
\t\t\t";
                // line 90
                $context["lista_plikow"] = twig_array_merge(($context["lista_plikow"] ?? null), [0 => "catalog/view/theme/wokiee/css/medical.css"]);
                // line 91
                echo "\t\t";
            }
            // line 92
            echo "
\t\t";
            // line 93
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "load_styles"], "method", false, false, false, 93) == "8")) {
                // line 94
                echo "\t\t\t";
                echo "<link href=\"//fonts.googleapis.com/css?family=Rokkitt:800,700,600,500,400,300,200,100\" rel=\"stylesheet\" type=\"text/css\">";
                echo "
\t\t\t";
                // line 95
                $context["lista_plikow"] = twig_array_merge(($context["lista_plikow"] ?? null), [0 => "catalog/view/theme/wokiee/css/jewelry.css"]);
                // line 96
                echo "\t\t";
            }
            // line 97
            echo "
\t\t";
            // line 98
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "load_styles"], "method", false, false, false, 98) == "9")) {
                // line 99
                echo "\t\t\t";
                echo "<link href=\"//fonts.googleapis.com/css?family=Helvetica:800,700,600,500,400,300,200,100\" rel=\"stylesheet\" type=\"text/css\">";
                echo "
\t\t\t";
                // line 100
                $context["lista_plikow"] = twig_array_merge(($context["lista_plikow"] ?? null), [0 => "catalog/view/theme/wokiee/css/weapons.css"]);
                // line 101
                echo "\t\t";
            }
            // line 102
            echo "
\t\t";
            // line 103
            echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "compressorCodeCss", [0 => "wokiee", 1 => ($context["lista_plikow"] ?? null), 2 => 0, 3 => twig_constant("HTTP_SERVER")], "method", false, false, false, 103);
            echo "
\t\t
\t\t";
            // line 105
            $this->loadTemplate("wokiee/css/custom_colors.twig", "wokiee/template/common/header.twig", 105)->display($context);
            // line 106
            echo "
\t\t";
            // line 107
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "custom_code_css_status"], "method", false, false, false, 107) == 1)) {
                echo " 
\t\t\t<link rel=\"stylesheet\" href=\"catalog/view/theme/wokiee/skins/store_";
                // line 108
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "store"], "method", false, false, false, 108);
                echo "/";
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "skin"], "method", false, false, false, 108);
                echo "/css/custom_code.css\">
\t\t";
            }
            // line 109
            echo " 
\t\t
\t\t<link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.7.2/css/all.css\" integrity=\"sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr\" crossorigin=\"anonymous\">

\t\t";
            // line 113
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["styles"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["style"]) {
                echo " 
\t\t\t";
                // line 114
                if (twig_in_filter("mf/style.css", (($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 = $context["style"]) && is_array($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136) || $__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 instanceof ArrayAccess ? ($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136["href"] ?? null) : null))) {
                    echo " 
\t\t\t\t<link rel=\"";
                    // line 115
                    echo (($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 = $context["style"]) && is_array($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386) || $__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 instanceof ArrayAccess ? ($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386["rel"] ?? null) : null);
                    echo "\" type=\"text/css\" href=\"catalog/view/theme/wokiee/css/mega_filter.css\" media=\"";
                    echo (($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9 = $context["style"]) && is_array($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9) || $__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9 instanceof ArrayAccess ? ($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9["media"] ?? null) : null);
                    echo "\" />
\t\t\t";
                } elseif (((((($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae =                 // line 116
$context["style"]) && is_array($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae) || $__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae instanceof ArrayAccess ? ($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae["href"] ?? null) : null) != "catalog/view/javascript/jquery/owl-carousel/owl.carousel.css") && ((($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f = $context["style"]) && is_array($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f) || $__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f instanceof ArrayAccess ? ($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f["href"] ?? null) : null) != "catalog/view/javascript/jquery/magnific/magnific-popup.css")) && ((($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40 = $context["style"]) && is_array($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40) || $__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40 instanceof ArrayAccess ? ($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40["href"] ?? null) : null) != "catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css"))) {
                    echo " 
\t\t\t\t<link rel=\"";
                    // line 117
                    echo (($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f = $context["style"]) && is_array($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f) || $__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f instanceof ArrayAccess ? ($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f["rel"] ?? null) : null);
                    echo "\" type=\"text/css\" href=\"";
                    echo (($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760 = $context["style"]) && is_array($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760) || $__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760 instanceof ArrayAccess ? ($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760["href"] ?? null) : null);
                    echo "\" media=\"";
                    echo (($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce = $context["style"]) && is_array($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce) || $__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce instanceof ArrayAccess ? ($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce["media"] ?? null) : null);
                    echo "\" />
\t\t\t";
                }
                // line 118
                echo " 
\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['style'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 119
            echo " 

\t\t<script type=\"text/javascript\" src=\"catalog/view/theme/wokiee/js/jquery.min.js\"></script>

\t\t";
            // line 123
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["scripts"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["script"]) {
                echo " 
\t\t     ";
                // line 124
                if (($context["script"] != "catalog/view/javascript/jquery/owl-carousel/owl.carousel.min.js")) {
                    echo " 
\t\t          <script type=\"text/javascript\" src=\"";
                    // line 125
                    echo $context["script"];
                    echo "\"></script>
\t\t     ";
                }
                // line 126
                echo " 
\t\t\t";
                // line 127
                if (twig_in_filter("mega_filter.js", $context["script"])) {
                    echo " 
\t\t\t\t<script type=\"text/javascript\">
\t\t\t\t\tfunction display_MFP(view) {
\t\t\t\t\t\t";
                    // line 130
                    if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_grid_type"], "method", false, false, false, 130) == 3)) {
                        // line 131
                        echo "\t\t\t\t\t        \$('.tt-product-listing-masonry .tt-product-init').isotope({
\t\t\t\t\t            itemSelector: '.element-item',
\t\t\t\t\t            layoutMode: 'masonry',
\t\t\t\t\t        });
\t\t\t\t\t\t";
                    } else {
                        // line 136
                        echo "\t\t\t\t\t\t    // lazyLoad
\t\t\t\t\t\t    (function () {
\t\t\t\t\t\t        new LazyLoad();
\t\t\t\t\t\t        new LazyLoad({
\t\t\t\t\t\t           elements_selector: \"iframe\"
\t\t\t\t\t\t        });
\t\t\t\t\t\t        new LazyLoad({
\t\t\t\t\t\t           elements_selector: \"video\"
\t\t\t\t\t\t        });
\t\t\t\t\t\t    }());
\t\t\t\t\t    ";
                    }
                    // line 147
                    echo "
\t\t\t\t\t    var showZero = true;
\t\t\t\t\t    \$(\".tt-countdown\").each(function() {
\t\t\t\t\t        var \$this = \$(this),
\t\t\t\t\t          date = \$this.data('date'),
\t\t\t\t\t          set_year = \$this.data('year') || 'Yrs',
\t\t\t\t\t          set_month = \$this.data('month') || 'Mths',
\t\t\t\t\t          set_week = \$this.data('week') || 'Wk',
\t\t\t\t\t          set_day = \$this.data('day') || 'Day',
\t\t\t\t\t          set_hour = \$this.data('hour') || 'Hrs',
\t\t\t\t\t          set_minute = \$this.data('minute') || 'Min',
\t\t\t\t\t          set_second = \$this.data('second') || 'Sec';

\t\t\t\t\t        if (date = date.split('-')) {
\t\t\t\t\t          date = date.join('/');
\t\t\t\t\t        } else return;

\t\t\t\t\t        \$this.countdown(date , function(e) {
\t\t\t\t\t          var format = '<span class=\"countdown-row\">';

\t\t\t\t\t          function addFormat(func, timeNum, showZero) {
\t\t\t\t\t            if(timeNum === 0 && !showZero) return;

\t\t\t\t\t            func(format);
\t\t\t\t\t          };

\t\t\t\t\t          addFormat(function() {
\t\t\t\t\t            format += '<span class=\"countdown-section\">'
\t\t\t\t\t                    + '<span class=\"countdown-amount\">' + e.offset.totalDays + '</span>'
\t\t\t\t\t                    + '<span class=\"countdown-period\">' + set_day + '</span>'
\t\t\t\t\t                  + '</span>';
\t\t\t\t\t          }, e.offset.totalDays, showZero);

\t\t\t\t\t          addFormat(function() {
\t\t\t\t\t            format += '<span class=\"countdown-section\">'
\t\t\t\t\t                    + '<span class=\"countdown-amount\">' + e.offset.hours + '</span>'
\t\t\t\t\t                    + '<span class=\"countdown-period\">' + set_hour + '</span>'
\t\t\t\t\t                  + '</span>';
\t\t\t\t\t          }, e.offset.hours, showZero);

\t\t\t\t\t          addFormat(function() {
\t\t\t\t\t            format += '<span class=\"countdown-section\">'
\t\t\t\t\t                    + '<span class=\"countdown-amount\">' + e.offset.minutes + '</span>'
\t\t\t\t\t                    + '<span class=\"countdown-period\">' + set_minute + '</span>'
\t\t\t\t\t                  + '</span>';
\t\t\t\t\t          }, e.offset.minutes, showZero);

\t\t\t\t\t          addFormat(function() {
\t\t\t\t\t            format += '<span class=\"countdown-section\">'
\t\t\t\t\t                    + '<span class=\"countdown-amount\">' + e.offset.seconds + '</span>'
\t\t\t\t\t                    + '<span class=\"countdown-period\">' + set_second + '</span>'
\t\t\t\t\t                  + '</span>';
\t\t\t\t\t          }, e.offset.seconds, showZero);

\t\t\t\t\t          format += '</span>';

\t\t\t\t\t            \$(this).html(format);
\t\t\t\t\t        });
\t\t\t\t\t    });
\t\t\t\t\t}
\t\t\t\t</script>
\t\t\t";
                }
                // line 209
                echo "\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['script'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo " 

\t\t";
            // line 211
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["analytics"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["analytic"]) {
                echo " 
\t\t";
                // line 212
                echo $context["analytic"];
                echo " 
\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['analytic'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 213
            echo "  
\t</head>\t
\t<body class=\"";
            // line 215
            echo ($context["klasa"] ?? null);
            echo "\">
\t<div id=\"loader-wrapper\">
\t\t<div id=\"loader\">
\t\t\t<div class=\"dot\"></div>
\t\t\t<div class=\"dot\"></div>
\t\t\t<div class=\"dot\"></div>
\t\t\t<div class=\"dot\"></div>
\t\t\t<div class=\"dot\"></div>
\t\t\t<div class=\"dot\"></div>
\t\t\t<div class=\"dot\"></div>
\t\t</div>
\t</div>

\t";
            // line 228
            $context["popup"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "popup"], "method", false, false, false, 228);
            // line 229
            echo "\t";
            if ((twig_length_filter($this->env, ($context["popup"] ?? null)) > 0)) {
                echo " 
\t\t";
                // line 230
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["popup"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                    echo " 
\t\t\t";
                    // line 231
                    echo $context["module"];
                    echo "
\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 233
                echo "\t";
            }
            echo " 

\t";
            // line 235
            $context["header_notice"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "header_notice"], "method", false, false, false, 235);
            // line 236
            echo "\t";
            if ((twig_length_filter($this->env, ($context["header_notice"] ?? null)) > 0)) {
                echo " 
\t\t";
                // line 237
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["header_notice"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                    echo " 
\t\t\t";
                    // line 238
                    echo $context["module"];
                    echo "
\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 240
                echo "\t";
            }
            // line 241
            echo "
\t";
            // line 242
            $context["cookie"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "cookie"], "method", false, false, false, 242);
            // line 243
            echo "\t";
            if ((twig_length_filter($this->env, ($context["cookie"] ?? null)) > 0)) {
                echo " 
\t\t";
                // line 244
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["cookie"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                    echo " 
\t\t\t";
                    // line 245
                    echo $context["module"];
                    echo "
\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 247
                echo "\t";
            }
            echo " 

\t";
            // line 249
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "header_type"], "method", false, false, false, 249) == 2)) {
                // line 250
                echo "\t\t";
                $this->loadTemplate("wokiee/template/common/header/header_02.twig", "wokiee/template/common/header.twig", 250)->display($context);
                // line 251
                echo "\t";
            } elseif ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "header_type"], "method", false, false, false, 251) == 3)) {
                // line 252
                echo "\t\t";
                $this->loadTemplate("wokiee/template/common/header/header_03.twig", "wokiee/template/common/header.twig", 252)->display($context);
                // line 253
                echo "\t";
            } elseif ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "header_type"], "method", false, false, false, 253) == 4)) {
                // line 254
                echo "\t\t";
                $this->loadTemplate("wokiee/template/common/header/header_04.twig", "wokiee/template/common/header.twig", 254)->display($context);
                // line 255
                echo "\t";
            } elseif ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "header_type"], "method", false, false, false, 255) == 5)) {
                // line 256
                echo "\t\t";
                $this->loadTemplate("wokiee/template/common/header/header_05.twig", "wokiee/template/common/header.twig", 256)->display($context);
                // line 257
                echo "\t";
            } elseif ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "header_type"], "method", false, false, false, 257) == 6)) {
                // line 258
                echo "\t\t";
                $this->loadTemplate("wokiee/template/common/header/header_06.twig", "wokiee/template/common/header.twig", 258)->display($context);
                // line 259
                echo "\t";
            } elseif ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "header_type"], "method", false, false, false, 259) == 7)) {
                // line 260
                echo "\t\t";
                $this->loadTemplate("wokiee/template/common/header/header_07.twig", "wokiee/template/common/header.twig", 260)->display($context);
                // line 261
                echo "\t";
            } elseif ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "header_type"], "method", false, false, false, 261) == 8)) {
                // line 262
                echo "\t\t";
                $this->loadTemplate("wokiee/template/common/header/header_08.twig", "wokiee/template/common/header.twig", 262)->display($context);
                // line 263
                echo "\t";
            } elseif ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "header_type"], "method", false, false, false, 263) == 9)) {
                // line 264
                echo "\t\t";
                $this->loadTemplate("wokiee/template/common/header/header_09.twig", "wokiee/template/common/header.twig", 264)->display($context);
                // line 265
                echo "\t";
            } else {
                echo " 
\t\t";
                // line 266
                $this->loadTemplate("wokiee/template/common/header/header_01.twig", "wokiee/template/common/header.twig", 266)->display($context);
                // line 267
                echo "\t";
            }
            echo " 

\t";
            // line 269
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "custom_code_javascript_status"], "method", false, false, false, 269) == 1)) {
                echo " 
\t\t<script type=\"text/javascript\" src=\"catalog/view/theme/wokiee/skins/store_";
                // line 270
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "store"], "method", false, false, false, 270);
                echo "/";
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "skin"], "method", false, false, false, 270);
                echo "/js/custom_code.js\"></script>
\t";
            }
            // line 271
            echo " 
";
        }
    }

    public function getTemplateName()
    {
        return "wokiee/template/common/header.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  702 => 271,  695 => 270,  691 => 269,  685 => 267,  683 => 266,  678 => 265,  675 => 264,  672 => 263,  669 => 262,  666 => 261,  663 => 260,  660 => 259,  657 => 258,  654 => 257,  651 => 256,  648 => 255,  645 => 254,  642 => 253,  639 => 252,  636 => 251,  633 => 250,  631 => 249,  625 => 247,  617 => 245,  611 => 244,  606 => 243,  604 => 242,  601 => 241,  598 => 240,  590 => 238,  584 => 237,  579 => 236,  577 => 235,  571 => 233,  563 => 231,  557 => 230,  552 => 229,  550 => 228,  534 => 215,  530 => 213,  522 => 212,  516 => 211,  507 => 209,  443 => 147,  430 => 136,  423 => 131,  421 => 130,  415 => 127,  412 => 126,  407 => 125,  403 => 124,  397 => 123,  391 => 119,  384 => 118,  375 => 117,  371 => 116,  365 => 115,  361 => 114,  355 => 113,  349 => 109,  342 => 108,  338 => 107,  335 => 106,  333 => 105,  328 => 103,  325 => 102,  322 => 101,  320 => 100,  315 => 99,  313 => 98,  310 => 97,  307 => 96,  305 => 95,  300 => 94,  298 => 93,  295 => 92,  292 => 91,  290 => 90,  285 => 89,  283 => 88,  280 => 87,  277 => 86,  274 => 85,  272 => 84,  269 => 83,  266 => 82,  264 => 81,  260 => 80,  255 => 79,  253 => 78,  250 => 77,  247 => 76,  245 => 75,  240 => 74,  238 => 73,  235 => 72,  232 => 71,  230 => 70,  226 => 69,  221 => 68,  219 => 67,  214 => 65,  211 => 64,  208 => 63,  205 => 62,  203 => 61,  199 => 59,  197 => 56,  193 => 54,  183 => 53,  177 => 52,  173 => 50,  168 => 49,  164 => 48,  161 => 47,  156 => 46,  152 => 45,  149 => 44,  143 => 42,  135 => 37,  131 => 36,  122 => 34,  116 => 32,  114 => 31,  109 => 30,  107 => 29,  104 => 28,  101 => 27,  99 => 26,  94 => 25,  91 => 24,  88 => 23,  85 => 22,  82 => 21,  79 => 20,  76 => 19,  73 => 18,  70 => 17,  68 => 16,  64 => 15,  59 => 14,  56 => 13,  53 => 12,  50 => 11,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "wokiee/template/common/header.twig", "");
    }
}

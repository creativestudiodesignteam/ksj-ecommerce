<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* wokiee/template/new_elements/wrapper_top.twig */
class __TwigTemplate_b84b1d7078a34dedbe66c62cc776dd208a5993758d2f3740251a6b92c46a46c7 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ((twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "has", [0 => "theme_options"], "method", false, false, false, 1) == twig_constant("true"))) {
            echo " 
\t";
            // line 2
            $context["theme_options"] = twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "get", [0 => "theme_options"], "method", false, false, false, 2);
            // line 3
            echo "\t";
            $context["config"] = twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "get", [0 => "config"], "method", false, false, false, 3);
            // line 4
            echo "\t<div class=\"tt-breadcrumb\">
\t\t<div class=\"container\">
\t\t\t<ul>
\t\t     \t";
            // line 7
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
                echo " 
\t\t     \t<li><a href=\"";
                // line 8
                echo (($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = $context["breadcrumb"]) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4["href"] ?? null) : null);
                echo "\">";
                if (((($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = $context["breadcrumb"]) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144["text"] ?? null) : null) != "<i class=\"fa fa-home\"></i>")) {
                    echo " ";
                    echo (($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = $context["breadcrumb"]) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b["text"] ?? null) : null);
                    echo " ";
                } else {
                    echo " ";
                    if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "home_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 8)], "method", false, false, false, 8) != "")) {
                        echo " ";
                        echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "home_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 8)], "method", false, false, false, 8);
                        echo " ";
                    } else {
                        echo " ";
                        echo "Home";
                        echo " ";
                    }
                    echo " ";
                }
                echo "</a></li>
\t\t     \t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 9
            echo " 
\t\t\t</ul>
\t\t</div>
\t</div>

\t";
            // line 14
            $context["slideshow"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "slideshow"], "method", false, false, false, 14);
            // line 15
            echo "\t";
            if ((twig_length_filter($this->env, ($context["slideshow"] ?? null)) > 0)) {
                echo " 
\t\t";
                // line 16
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["slideshow"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                    echo " 
\t\t\t";
                    // line 17
                    echo $context["module"];
                    echo "
\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 19
                echo "\t";
            }
            echo " 

\t<div id=\"tt-pageContent\">
\t\t";
            // line 22
            if ( !($context["product_page"] ?? null)) {
                // line 23
                echo "\t\t<div class=\"container-indent\">
\t\t\t<div ";
                // line 24
                if (( !(($context["categoryPage"] ?? null) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_grid_type"], "method", false, false, false, 24) == "2")) &&  !(($context["categoryPage"] ?? null) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_grid_type"], "method", false, false, false, 24) == "3")))) {
                    echo "class=\"container container-fluid-custom-mobile-padding\"";
                } else {
                    echo "class=\"container-fluid-custom container-fluid-custom-mobile-padding\"";
                }
                echo ">
\t\t\t\t";
                // line 25
                $context["preface_left"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "preface_left"], "method", false, false, false, 25);
                // line 26
                echo "\t\t\t\t";
                $context["preface_right"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "preface_right"], "method", false, false, false, 26);
                // line 27
                echo "\t\t\t\t
\t\t\t\t";
                // line 28
                if (((twig_length_filter($this->env, ($context["preface_left"] ?? null)) > 0) || (twig_length_filter($this->env, ($context["preface_right"] ?? null)) > 0))) {
                    echo " 
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-9\">
\t\t\t\t\t\t";
                    // line 31
                    if ((twig_length_filter($this->env, ($context["preface_left"] ?? null)) > 0)) {
                        // line 32
                        echo "\t\t\t\t\t\t\t";
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(($context["preface_left"] ?? null));
                        foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                            echo " 
\t\t\t\t\t\t\t\t";
                            // line 33
                            echo $context["module"];
                            echo "
\t\t\t\t\t\t\t";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 35
                        echo "\t\t\t\t\t\t";
                    }
                    echo " 
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t<div class=\"col-md-3\">
\t\t\t\t\t\t";
                    // line 39
                    if ((twig_length_filter($this->env, ($context["preface_right"] ?? null)) > 0)) {
                        // line 40
                        echo "\t\t\t\t\t\t\t";
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(($context["preface_right"] ?? null));
                        foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                            echo " 
\t\t\t\t\t\t\t\t";
                            // line 41
                            echo $context["module"];
                            echo "
\t\t\t\t\t\t\t";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 43
                        echo "\t\t\t\t\t\t";
                    }
                    echo " 
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t";
                }
                // line 46
                echo " 
\t\t\t\t
\t\t\t\t
\t\t\t\t";
                // line 49
                $context["preface_fullwidth"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "preface_fullwidth"], "method", false, false, false, 49);
                // line 50
                echo "\t\t\t\t";
                if ((twig_length_filter($this->env, ($context["preface_fullwidth"] ?? null)) > 0)) {
                    // line 51
                    echo "\t\t\t\t\t";
                    echo "<div class=\"row\"><div class=\"col-md-12\">";
                    echo "
\t\t\t\t\t";
                    // line 52
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(($context["preface_fullwidth"] ?? null));
                    foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                        echo " 
\t\t\t\t\t\t";
                        // line 53
                        echo $context["module"];
                        echo "
\t\t\t\t\t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 55
                    echo "\t\t\t\t\t";
                    echo "</div></div>";
                    echo "
\t\t\t\t";
                }
                // line 56
                echo " 

\t\t\t\t";
                // line 58
                if (($context["blog"] ?? null)) {
                    // line 59
                    echo "\t\t\t\t\t<h1 class=\"tt-title-subpages noborder\">";
                    echo ($context["heading_title"] ?? null);
                    echo "</h1>
\t\t\t\t";
                }
                // line 61
                echo "\t\t\t\t
\t\t\t\t<div class=\"row\">
\t\t\t\t\t";
                // line 63
                $context["columnleft"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "column_left"], "method", false, false, false, 63);
                // line 64
                echo "\t\t\t\t\t";
                if ((twig_length_filter($this->env, ($context["columnleft"] ?? null)) > 0)) {
                    echo " 
\t\t\t\t\t<div class=\"col-lg-3 leftColumn ";
                    // line 65
                    if (($context["categoryPage"] ?? null)) {
                        echo "aside";
                    }
                    echo " ";
                    if ((((($context["categoryPage"] ?? null) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_grid_type"], "method", false, false, false, 65) == "1")) || (($context["categoryPage"] ?? null) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_grid_type"], "method", false, false, false, 65) == "2"))) || (($context["categoryPage"] ?? null) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_grid_type"], "method", false, false, false, 65) == "3")))) {
                        echo "desctop-no-sidebar";
                    }
                    echo "\" id=\"column-left\">
\t\t\t\t\t\t";
                    // line 66
                    if (($context["categoryPage"] ?? null)) {
                        // line 67
                        echo "\t\t\t\t\t\t\t<div class=\"tt-btn-col-close\">
\t\t\t\t\t\t\t\t<a href=\"#\">";
                        // line 68
                        if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "close_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 68)], "method", false, false, false, 68) != "")) {
                            echo " ";
                            echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "close_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 68)], "method", false, false, false, 68);
                            echo " ";
                        } else {
                            echo "Close";
                        }
                        echo "</a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"tt-collapse open tt-filter-detach-option\">
\t\t\t\t\t\t\t\t<div class=\"tt-collapse-content\" style=\"\">
\t\t\t\t\t\t\t\t\t<div class=\"filters-mobile\">
\t\t\t\t\t\t\t\t\t\t<div class=\"filters-row-select\">

\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
                    }
                    // line 80
                    echo "\t\t\t\t\t\t";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(($context["columnleft"] ?? null));
                    foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                        echo " 
\t\t\t\t\t\t\t";
                        // line 81
                        echo $context["module"];
                        echo "
\t\t\t\t\t\t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 83
                    echo "\t\t\t\t\t</div>
\t\t\t\t\t";
                }
                // line 84
                echo " 
\t\t\t\t\t
\t\t\t\t\t";
                // line 86
                $context["grid_center"] = 12;
                echo " ";
                if ((twig_length_filter($this->env, ($context["columnleft"] ?? null)) > 0)) {
                    echo " ";
                    $context["grid_center"] = 9;
                    echo " ";
                }
                echo " 
\t\t\t\t\t<div class=\"col-lg-";
                // line 87
                if ((((($context["categoryPage"] ?? null) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_grid_type"], "method", false, false, false, 87) == "1")) || (($context["categoryPage"] ?? null) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_grid_type"], "method", false, false, false, 87) == "2"))) || (($context["categoryPage"] ?? null) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_grid_type"], "method", false, false, false, 87) == "3")))) {
                    echo "12";
                } else {
                    echo ($context["grid_center"] ?? null);
                }
                echo "\">
\t\t\t\t\t\t
\t\t\t\t\t\t";
                // line 89
                $context["content_big_column"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "content_big_column"], "method", false, false, false, 89);
                // line 90
                echo "\t\t\t\t\t\t";
                if ((twig_length_filter($this->env, ($context["content_big_column"] ?? null)) > 0)) {
                    echo " 
\t\t\t\t\t\t\t";
                    // line 91
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(($context["content_big_column"] ?? null));
                    foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                        echo " 
\t\t\t\t\t\t\t\t";
                        // line 92
                        echo $context["module"];
                        echo "
\t\t\t\t\t\t\t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 94
                    echo "\t\t\t\t\t\t";
                }
                echo " 
\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t\t";
                // line 97
                $context["content_top"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "content_top"], "method", false, false, false, 97);
                // line 98
                echo "\t\t\t\t\t\t";
                if ((twig_length_filter($this->env, ($context["content_top"] ?? null)) > 0)) {
                    echo " 
\t\t\t\t\t\t\t";
                    // line 99
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(($context["content_top"] ?? null));
                    foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                        echo " 
\t\t\t\t\t\t\t\t";
                        // line 100
                        echo $context["module"];
                        echo "
\t\t\t\t\t\t\t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 102
                    echo "\t\t\t\t\t\t";
                }
                echo " 
\t\t\t\t\t\t
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t";
                // line 106
                $context["grid_content_top"] = 12;
                echo " 
\t\t\t\t\t\t\t";
                // line 107
                $context["grid_content_right"] = 3;
                // line 108
                echo "\t\t\t\t\t\t\t";
                $context["column_right"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "column_right"], "method", false, false, false, 108);
                echo " 
\t\t\t\t\t\t\t";
                // line 109
                if ((twig_length_filter($this->env, ($context["column_right"] ?? null)) > 0)) {
                    // line 110
                    echo "\t\t\t\t\t\t\t\t";
                    if ((($context["grid_center"] ?? null) == 9)) {
                        // line 111
                        echo "\t\t\t\t\t\t\t\t\t";
                        $context["grid_content_top"] = 8;
                        // line 112
                        echo "\t\t\t\t\t\t\t\t\t";
                        $context["grid_content_right"] = 4;
                        // line 113
                        echo "\t\t\t\t\t\t\t\t";
                    } else {
                        echo " 
\t\t\t\t\t\t\t\t\t";
                        // line 114
                        $context["grid_content_top"] = 9;
                        // line 115
                        echo "\t\t\t\t\t\t\t\t\t";
                        $context["grid_content_right"] = 3;
                        // line 116
                        echo "\t\t\t\t\t\t\t\t";
                    }
                    // line 117
                    echo "\t\t\t\t\t\t\t";
                }
                // line 118
                echo "\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"col-lg-";
                // line 119
                if ((((($context["categoryPage"] ?? null) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_grid_type"], "method", false, false, false, 119) == "1")) || (($context["categoryPage"] ?? null) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_grid_type"], "method", false, false, false, 119) == "2"))) || (($context["categoryPage"] ?? null) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_grid_type"], "method", false, false, false, 119) == "3")))) {
                    echo "12";
                } else {
                    echo ($context["grid_content_top"] ?? null);
                }
                echo " center-column\" id=\"content\">

\t\t\t\t\t\t\t\t";
                // line 121
                if (((isset($context["error_warning"]) || array_key_exists("error_warning", $context)) &&  !(isset($context["checkoutPage"]) || array_key_exists("checkoutPage", $context)))) {
                    echo " 
\t\t\t\t\t\t\t\t\t";
                    // line 122
                    if (($context["error_warning"] ?? null)) {
                        echo " 
\t\t\t\t\t\t\t\t\t<div class=\"warning\">
\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
\t\t\t\t\t\t\t\t\t\t";
                        // line 125
                        echo ($context["error_warning"] ?? null);
                        echo " 
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t";
                    }
                    // line 127
                    echo " 
\t\t\t\t\t\t\t\t";
                }
                // line 128
                echo " 
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t";
                // line 130
                if ((isset($context["success"]) || array_key_exists("success", $context))) {
                    echo " 
\t\t\t\t\t\t\t\t\t";
                    // line 131
                    if (($context["success"] ?? null)) {
                        echo " 
\t\t\t\t\t\t\t\t\t<div class=\"success\">
\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
\t\t\t\t\t\t\t\t\t\t";
                        // line 134
                        echo ($context["success"] ?? null);
                        echo " 
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t";
                    }
                    // line 136
                    echo " 
\t\t\t\t\t\t\t\t";
                }
                // line 138
                echo "\t\t";
            } else {
                // line 139
                echo "\t\t\t";
                $context["preface_left"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "preface_left"], "method", false, false, false, 139);
                // line 140
                echo "\t\t\t";
                $context["preface_right"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "preface_right"], "method", false, false, false, 140);
                // line 141
                echo "\t\t\t";
                if (((twig_length_filter($this->env, ($context["preface_left"] ?? null)) > 0) || (twig_length_filter($this->env, ($context["preface_right"] ?? null)) > 0))) {
                    echo " 
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-9\">
\t\t\t\t\t";
                    // line 144
                    if ((twig_length_filter($this->env, ($context["preface_left"] ?? null)) > 0)) {
                        // line 145
                        echo "\t\t\t\t\t\t";
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(($context["preface_left"] ?? null));
                        foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                            echo " 
\t\t\t\t\t\t\t";
                            // line 146
                            echo $context["module"];
                            echo "
\t\t\t\t\t\t";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 148
                        echo "\t\t\t\t\t";
                    }
                    echo " 
\t\t\t\t</div>
\t\t\t\t
\t\t\t\t<div class=\"col-md-3\">
\t\t\t\t\t";
                    // line 152
                    if ((twig_length_filter($this->env, ($context["preface_right"] ?? null)) > 0)) {
                        // line 153
                        echo "\t\t\t\t\t\t";
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(($context["preface_right"] ?? null));
                        foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                            echo " 
\t\t\t\t\t\t\t";
                            // line 154
                            echo $context["module"];
                            echo "
\t\t\t\t\t\t";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 156
                        echo "\t\t\t\t\t";
                    }
                    echo " 
\t\t\t\t</div>
\t\t\t</div>
\t\t\t";
                }
                // line 159
                echo " 
\t\t\t
\t\t\t";
                // line 161
                $context["columnleft"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "column_left"], "method", false, false, false, 161);
                // line 162
                echo "\t\t\t";
                if ((twig_length_filter($this->env, ($context["columnleft"] ?? null)) > 0)) {
                    echo " 
\t\t\t\t";
                    // line 163
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(($context["columnleft"] ?? null));
                    foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                        echo " 
\t\t\t\t\t";
                        // line 164
                        echo $context["module"];
                        echo "
\t\t\t\t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 166
                    echo "\t\t\t";
                }
                echo " 

\t\t\t";
                // line 168
                $context["content_big_column"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "content_big_column"], "method", false, false, false, 168);
                // line 169
                echo "\t\t\t";
                if ((twig_length_filter($this->env, ($context["content_big_column"] ?? null)) > 0)) {
                    echo " 
\t\t\t\t";
                    // line 170
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(($context["content_big_column"] ?? null));
                    foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                        echo " 
\t\t\t\t\t";
                        // line 171
                        echo $context["module"];
                        echo "
\t\t\t\t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 173
                    echo "\t\t\t";
                }
                echo " 
\t\t\t
\t\t\t";
                // line 175
                $context["content_top"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "content_top"], "method", false, false, false, 175);
                // line 176
                echo "\t\t\t";
                if ((twig_length_filter($this->env, ($context["content_top"] ?? null)) > 0)) {
                    echo " 
\t\t\t\t";
                    // line 177
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(($context["content_top"] ?? null));
                    foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                        echo " 
\t\t\t\t\t";
                        // line 178
                        echo $context["module"];
                        echo "
\t\t\t\t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 180
                    echo "\t\t\t";
                }
                echo " 
\t\t";
            }
        }
    }

    public function getTemplateName()
    {
        return "wokiee/template/new_elements/wrapper_top.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  627 => 180,  619 => 178,  613 => 177,  608 => 176,  606 => 175,  600 => 173,  592 => 171,  586 => 170,  581 => 169,  579 => 168,  573 => 166,  565 => 164,  559 => 163,  554 => 162,  552 => 161,  548 => 159,  540 => 156,  532 => 154,  525 => 153,  523 => 152,  515 => 148,  507 => 146,  500 => 145,  498 => 144,  491 => 141,  488 => 140,  485 => 139,  482 => 138,  478 => 136,  472 => 134,  466 => 131,  462 => 130,  458 => 128,  454 => 127,  448 => 125,  442 => 122,  438 => 121,  429 => 119,  426 => 118,  423 => 117,  420 => 116,  417 => 115,  415 => 114,  410 => 113,  407 => 112,  404 => 111,  401 => 110,  399 => 109,  394 => 108,  392 => 107,  388 => 106,  380 => 102,  372 => 100,  366 => 99,  361 => 98,  359 => 97,  352 => 94,  344 => 92,  338 => 91,  333 => 90,  331 => 89,  322 => 87,  312 => 86,  308 => 84,  304 => 83,  296 => 81,  289 => 80,  268 => 68,  265 => 67,  263 => 66,  253 => 65,  248 => 64,  246 => 63,  242 => 61,  236 => 59,  234 => 58,  230 => 56,  224 => 55,  216 => 53,  210 => 52,  205 => 51,  202 => 50,  200 => 49,  195 => 46,  187 => 43,  179 => 41,  172 => 40,  170 => 39,  162 => 35,  154 => 33,  147 => 32,  145 => 31,  139 => 28,  136 => 27,  133 => 26,  131 => 25,  123 => 24,  120 => 23,  118 => 22,  111 => 19,  103 => 17,  97 => 16,  92 => 15,  90 => 14,  83 => 9,  57 => 8,  51 => 7,  46 => 4,  43 => 3,  41 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "wokiee/template/new_elements/wrapper_top.twig", "/home2/kadore/public_html/teste/catalog/view/theme/wokiee/template/new_elements/wrapper_top.twig");
    }
}

<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* wokiee/template/product/review.twig */
class __TwigTemplate_23fb3531e35ad92af923e013dd3ad189ce69ee144a27b146771c2c93436e2581 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (($context["reviews"] ?? null)) {
            echo " 
\t<div class=\"tt-review-block\">
\t\t<div class=\"tt-review-comments\">
\t\t\t";
            // line 4
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["reviews"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["review"]) {
                echo " 
\t\t\t\t<div class=\"tt-item\">
\t\t\t\t\t<div class=\"tt-content\">
\t\t\t\t\t\t<div class=\"tt-rating\">
\t\t\t\t\t\t\t<i class=\"icon-star";
                // line 8
                if ( !((($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = $context["review"]) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4["rating"] ?? null) : null) >= 1)) {
                    echo "-empty";
                }
                echo "\"></i>
\t\t\t\t\t\t\t<i class=\"icon-star";
                // line 9
                if ( !((($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = $context["review"]) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144["rating"] ?? null) : null) >= 2)) {
                    echo "-empty";
                }
                echo "\"></i>
\t\t\t\t\t\t\t<i class=\"icon-star";
                // line 10
                if ( !((($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = $context["review"]) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b["rating"] ?? null) : null) >= 3)) {
                    echo "-empty";
                }
                echo "\"></i>
\t\t\t\t\t\t\t<i class=\"icon-star";
                // line 11
                if ( !((($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 = $context["review"]) && is_array($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002) || $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 instanceof ArrayAccess ? ($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002["rating"] ?? null) : null) >= 4)) {
                    echo "-empty";
                }
                echo "\"></i>
\t\t\t\t\t\t\t<i class=\"icon-star";
                // line 12
                if ( !((($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 = $context["review"]) && is_array($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4) || $__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 instanceof ArrayAccess ? ($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4["rating"] ?? null) : null) >= 5)) {
                    echo "-empty";
                }
                echo "\"></i>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"tt-comments-info\">
\t\t\t\t\t\t\t<span class=\"username\">by <span>";
                // line 15
                echo (($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 = $context["review"]) && is_array($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666) || $__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 instanceof ArrayAccess ? ($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666["author"] ?? null) : null);
                echo "</span></span>
\t\t\t\t\t\t\t<span class=\"time\">";
                // line 16
                echo (($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e = $context["review"]) && is_array($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e) || $__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e instanceof ArrayAccess ? ($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e["date_added"] ?? null) : null);
                echo "</span>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<p style=\"padding-top: 5px\">
\t\t\t\t\t\t\t";
                // line 19
                echo (($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 = $context["review"]) && is_array($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52) || $__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 instanceof ArrayAccess ? ($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52["text"] ?? null) : null);
                echo "
\t\t\t\t\t\t</p>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['review'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 24
            echo "\t\t</div>
\t\t<div class=\"row pagination-results\">
\t\t  <div class=\"col-sm-6 text-left\">";
            // line 26
            echo ($context["pagination"] ?? null);
            echo "</div>
\t\t  <div class=\"col-sm-6 text-right\">";
            // line 27
            echo ($context["results"] ?? null);
            echo "</div>
\t\t</div>
\t</div>
";
        } else {
            // line 30
            echo " 
\t<p style=\"padding-bottom: 10px\">";
            // line 31
            echo ($context["text_no_reviews"] ?? null);
            echo "</p>
";
        }
        // line 32
        echo " ";
    }

    public function getTemplateName()
    {
        return "wokiee/template/product/review.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  128 => 32,  123 => 31,  120 => 30,  113 => 27,  109 => 26,  105 => 24,  94 => 19,  88 => 16,  84 => 15,  76 => 12,  70 => 11,  64 => 10,  58 => 9,  52 => 8,  43 => 4,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "wokiee/template/product/review.twig", "");
    }
}

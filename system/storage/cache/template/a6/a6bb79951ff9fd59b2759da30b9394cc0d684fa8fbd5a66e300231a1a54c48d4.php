<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* wokiee/template/product/manufacturer_info.twig */
class __TwigTemplate_e9efae40f65e39bb8561a19fcfdad10819353cabcb080309505d508f063102b6 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo " 
";
        // line 2
        $context["theme_options"] = twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "get", [0 => "theme_options"], "method", false, false, false, 2);
        // line 3
        $context["config"] = twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "get", [0 => "config"], "method", false, false, false, 3);
        echo " 
";
        // line 4
        $context["categoryPage"] = twig_constant("true");
        // line 5
        $this->loadTemplate("wokiee/template/new_elements/wrapper_top.twig", "wokiee/template/product/manufacturer_info.twig", 5)->display($context);
        // line 6
        echo "
<div id=\"mfilter-content-container\">
  ";
        // line 8
        if (($context["products"] ?? null)) {
            echo " 
    ";
            // line 9
            $context["currently"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getCurrently", [], "method", false, false, false, 9);
            // line 10
            echo "    <div class=\"tt-filters-options ";
            if ((((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_grid_type"], "method", false, false, false, 10) == "1") || (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_grid_type"], "method", false, false, false, 10) == "2")) || (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_grid_type"], "method", false, false, false, 10) == "3"))) {
                echo "desctop-no-sidebar";
            }
            echo "\">
      <h1 class=\"tt-title\">
        ";
            // line 12
            echo ($context["heading_title"] ?? null);
            echo "
      </h1>
      <div class=\"tt-btn-toggle\">
        <a href=\"#\">";
            // line 15
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "filter_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 15)], "method", false, false, false, 15) != "")) {
                echo " ";
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "filter_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 15)], "method", false, false, false, 15);
                echo " ";
            } else {
                echo "FILTER";
            }
            echo "</a>
      </div>
      <div class=\"tt-sort d-none d-md-block\">
        <select onchange=\"location = this.value;\">
          ";
            // line 19
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["sorts"]);
            foreach ($context['_seq'] as $context["_key"] => $context["sorts"]) {
                echo " 
          ";
                // line 20
                if (((($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = $context["sorts"]) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4["value"] ?? null) : null) == ((($context["sort"] ?? null) . "-") . ($context["order"] ?? null)))) {
                    echo " 
          <option value=\"";
                    // line 21
                    echo (($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = $context["sorts"]) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144["href"] ?? null) : null);
                    echo "\" selected=\"selected\">";
                    echo (($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = $context["sorts"]) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b["text"] ?? null) : null);
                    echo "</option>
          ";
                } else {
                    // line 22
                    echo " 
          <option value=\"";
                    // line 23
                    echo (($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 = $context["sorts"]) && is_array($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002) || $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 instanceof ArrayAccess ? ($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002["href"] ?? null) : null);
                    echo "\">";
                    echo (($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 = $context["sorts"]) && is_array($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4) || $__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 instanceof ArrayAccess ? ($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4["text"] ?? null) : null);
                    echo "</option>
          ";
                }
                // line 24
                echo " 
          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sorts'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 25
            echo " 
        </select>
        <select onchange=\"location = this.value;\">
          ";
            // line 28
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["limits"]);
            foreach ($context['_seq'] as $context["_key"] => $context["limits"]) {
                echo " 
          ";
                // line 29
                if (((($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 = $context["limits"]) && is_array($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666) || $__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 instanceof ArrayAccess ? ($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666["value"] ?? null) : null) == ($context["limit"] ?? null))) {
                    echo " 
          <option value=\"";
                    // line 30
                    echo (($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e = $context["limits"]) && is_array($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e) || $__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e instanceof ArrayAccess ? ($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e["href"] ?? null) : null);
                    echo "\" selected=\"selected\">";
                    echo (($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 = $context["limits"]) && is_array($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52) || $__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 instanceof ArrayAccess ? ($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52["text"] ?? null) : null);
                    echo "</option>
          ";
                } else {
                    // line 31
                    echo " 
          <option value=\"";
                    // line 32
                    echo (($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 = $context["limits"]) && is_array($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136) || $__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 instanceof ArrayAccess ? ($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136["href"] ?? null) : null);
                    echo "\">";
                    echo (($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 = $context["limits"]) && is_array($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386) || $__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 instanceof ArrayAccess ? ($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386["text"] ?? null) : null);
                    echo "</option>
          ";
                }
                // line 33
                echo " 
          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['limits'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 34
            echo " 
        </select>
      </div>
      ";
            // line 37
            if ( !(twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_grid_type"], "method", false, false, false, 37) == "3")) {
                // line 38
                echo "        <div class=\"tt-quantity\">
          <a href=\"#\" class=\"tt-col-one d-block d-md-none ";
                // line 39
                if ((($context["currently"] ?? null) == "verylarge")) {
                    echo "active";
                }
                echo "\" data-value=\"tt-col-one\"></a>
          <a href=\"#\" class=\"tt-col-two ";
                // line 40
                if (((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_grid_type"], "method", false, false, false, 40) == "1") || (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_grid_type"], "method", false, false, false, 40) == "2"))) {
                    echo "d-block d-md-none";
                }
                echo " ";
                if ((($context["currently"] ?? null) == "large")) {
                    echo "active";
                }
                echo "\" data-value=\"tt-col-two\"></a>
          <a href=\"#\" class=\"tt-col-three d-none d-md-block ";
                // line 41
                if (((((($context["currently"] ?? null) != "large") && (($context["currently"] ?? null) != "small")) && (($context["currently"] ?? null) != "verysmall")) && (($context["currently"] ?? null) != "verylarge"))) {
                    echo "active";
                }
                echo "\" data-value=\"tt-col-three\"></a>
          <a href=\"#\" class=\"tt-col-four d-none d-md-block ";
                // line 42
                if ((($context["currently"] ?? null) == "small")) {
                    echo "active";
                }
                echo "\" data-value=\"tt-col-four\"></a>
          ";
                // line 43
                if (((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_grid_type"], "method", false, false, false, 43) == "1") || (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_grid_type"], "method", false, false, false, 43) == "2"))) {
                    // line 44
                    echo "            <a href=\"#\" class=\"tt-col-six d-none d-md-block ";
                    if ((($context["currently"] ?? null) == "verysmall")) {
                        echo "active";
                    }
                    echo "\" data-value=\"tt-col-six\"></a>
          ";
                }
                // line 46
                echo "        </div>
      ";
            }
            // line 48
            echo "    </div>

    ";
            // line 50
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_grid_type"], "method", false, false, false, 50) == "3")) {
                // line 51
                echo "      <div class=\"tt-product-listing-masonry\">
        <div class=\"tt-product-init tt-add-item\">
          ";
                // line 53
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["products"] ?? null));
                $context['loop'] = [
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                ];
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                    echo " 
            <div class=\"element-item ";
                    // line 54
                    if (((twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 54) == 3) || (twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 54) == 6))) {
                        echo "double-size";
                    }
                    echo "\">
                ";
                    // line 55
                    $this->loadTemplate("wokiee/template/new_elements/product2.twig", "wokiee/template/product/manufacturer_info.twig", 55)->display($context);
                    // line 56
                    echo "            </div>
          ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 57
                echo " 
        </div>
      </div>
    ";
            } else {
                // line 61
                echo "      ";
                $context["class"] = "4";
                // line 62
                echo "      ";
                if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_grid_type"], "method", false, false, false, 62) == "1")) {
                    // line 63
                    echo "        ";
                    $context["class"] = "3";
                    // line 64
                    echo "      ";
                }
                // line 65
                echo "      ";
                if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_grid_type"], "method", false, false, false, 65) == "2")) {
                    // line 66
                    echo "        ";
                    $context["class"] = "2";
                    // line 67
                    echo "      ";
                }
                // line 68
                echo "      ";
                if ((($context["currently"] ?? null) == "large")) {
                    $context["class"] = "6";
                }
                // line 69
                echo "      ";
                if ((($context["currently"] ?? null) == "small")) {
                    $context["class"] = "3";
                }
                // line 70
                echo "      ";
                if ((($context["currently"] ?? null) == "medium")) {
                    $context["class"] = "4";
                }
                // line 71
                echo "      ";
                if ((($context["currently"] ?? null) == "verylarge")) {
                    $context["class"] = "12";
                }
                // line 72
                echo "      ";
                if ((($context["currently"] ?? null) == "verysmall")) {
                    $context["class"] = "2";
                }
                // line 73
                echo "      <div class=\"tt-product-listing row\">
        ";
                // line 74
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["products"] ?? null));
                $context['loop'] = [
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                ];
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                    echo " 
          <div class=\"col-";
                    // line 75
                    if ((($context["class"] ?? null) == "12")) {
                        echo "12";
                    } else {
                        echo "6";
                    }
                    echo " ";
                    if ((($context["class"] ?? null) == "3")) {
                        echo "col-md-4 col-lg-";
                        echo ($context["class"] ?? null);
                    } elseif ((($context["class"] ?? null) == "2")) {
                        echo "col-md-4 col-lg-3 col-xl-";
                        echo ($context["class"] ?? null);
                    } else {
                        echo "col-md-";
                        echo ($context["class"] ?? null);
                    }
                    echo " tt-col-item\">
              ";
                    // line 76
                    $this->loadTemplate("wokiee/template/new_elements/product.twig", "wokiee/template/product/manufacturer_info.twig", 76)->display($context);
                    // line 77
                    echo "          </div>
        ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 78
                echo " 
      </div>
    ";
            }
            // line 81
            echo "    
    <div class=\"row pagination-results\">
      <div class=\"col-sm-6 text-left\">";
            // line 83
            echo ($context["pagination"] ?? null);
            echo "</div>
      <div class=\"col-sm-6 text-right\">";
            // line 84
            echo ($context["results"] ?? null);
            echo "</div>
    </div>
  ";
        } else {
            // line 86
            echo " 
    <p style=\"padding-bottom: 15px\">";
            // line 87
            echo ($context["text_empty"] ?? null);
            echo "</p>
    <div class=\"buttons\">
      <div class=\"pull-right\"><a href=\"";
            // line 89
            echo ($context["continue"] ?? null);
            echo "\" class=\"btn btn-primary\">";
            echo ($context["button_continue"] ?? null);
            echo "</a></div>
    </div>
  ";
        }
        // line 91
        echo " 
<script type=\"text/javascript\"><!--
function setCookie(key, value) {
    var expires = new Date();
    expires.setTime(expires.getTime() + (1 * 24 * 60 * 60 * 1000));
    document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
}
\$(document).ready(function () {
  \$(\"body\").on(\"click\", \".tt-quantity a\", function () {
    if(\$(this).attr(\"data-value\") == 'tt-col-two') {
      setCookie('display','large');
    } else if(\$(this).attr(\"data-value\") == 'tt-col-four') {
      setCookie('display','small');
    } else if(\$(this).attr(\"data-value\") == 'tt-col-six') {
      setCookie('display','verysmall');
    } else if(\$(this).attr(\"data-value\") == 'tt-col-one') {
      setCookie('display','verylarge');
    } else {
      setCookie('display','medium');
    }
  });
});
//--></script> 

</div>

";
        // line 117
        $this->loadTemplate("wokiee/template/new_elements/wrapper_bottom.twig", "wokiee/template/product/manufacturer_info.twig", 117)->display($context);
        // line 118
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "wokiee/template/product/manufacturer_info.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  440 => 118,  438 => 117,  410 => 91,  402 => 89,  397 => 87,  394 => 86,  388 => 84,  384 => 83,  380 => 81,  375 => 78,  360 => 77,  358 => 76,  339 => 75,  320 => 74,  317 => 73,  312 => 72,  307 => 71,  302 => 70,  297 => 69,  292 => 68,  289 => 67,  286 => 66,  283 => 65,  280 => 64,  277 => 63,  274 => 62,  271 => 61,  265 => 57,  250 => 56,  248 => 55,  242 => 54,  223 => 53,  219 => 51,  217 => 50,  213 => 48,  209 => 46,  201 => 44,  199 => 43,  193 => 42,  187 => 41,  177 => 40,  171 => 39,  168 => 38,  166 => 37,  161 => 34,  154 => 33,  147 => 32,  144 => 31,  137 => 30,  133 => 29,  127 => 28,  122 => 25,  115 => 24,  108 => 23,  105 => 22,  98 => 21,  94 => 20,  88 => 19,  75 => 15,  69 => 12,  61 => 10,  59 => 9,  55 => 8,  51 => 6,  49 => 5,  47 => 4,  43 => 3,  41 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "wokiee/template/product/manufacturer_info.twig", "");
    }
}

<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* wokiee/template/account/login.twig */
class __TwigTemplate_476019e29a41cec537eaabf86f2a5a6aaceebad80a737159c5991ee204ed7bec extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo " 
";
        // line 2
        $this->loadTemplate("wokiee/template/new_elements/wrapper_top.twig", "wokiee/template/account/login.twig", 2)->display($context);
        // line 3
        echo "
<div class=\"row form-default\">
  <div class=\"col-md-6\">
    <div class=\"well\">
      <h5>";
        // line 7
        echo ($context["text_new_customer"] ?? null);
        echo "</h5>
      <p style=\"margin: 0px 0px 16px 0px\"><strong>";
        // line 8
        echo ($context["text_register"] ?? null);
        echo "</strong></p>
      <p style=\"padding-bottom: 20px\">";
        // line 9
        echo ($context["text_register_account"] ?? null);
        echo "</p>
      <a href=\"";
        // line 10
        echo ($context["register"] ?? null);
        echo "\" class=\"btn btn-primary\">";
        echo ($context["button_continue"] ?? null);
        echo "</a></div>
  </div>
  <div class=\"col-md-6\">
    <div class=\"d-block d-md-none\" style=\"height: 30px\"></div>
    <div class=\"well\">
      <h5>";
        // line 15
        echo ($context["text_returning_customer"] ?? null);
        echo "</h5>
      <p style=\"margin: 0px 0px 16px 0px\"><strong>";
        // line 16
        echo ($context["text_i_am_returning_customer"] ?? null);
        echo "</strong></p>
      <form action=\"";
        // line 17
        echo ($context["action"] ?? null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\">
        <div class=\"form-group\">
          <label class=\"control-label\" for=\"input-email\">";
        // line 19
        echo ($context["entry_email"] ?? null);
        echo "</label>
          <input type=\"text\" name=\"email\" value=\"";
        // line 20
        echo ($context["email"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_email"] ?? null);
        echo "\" id=\"input-email\" class=\"form-control\" />
        </div>
        <div class=\"form-group\" style=\"padding-bottom: 10px\">
          <label class=\"control-label\" for=\"input-password\">";
        // line 23
        echo ($context["entry_password"] ?? null);
        echo "</label>
          <input type=\"password\" name=\"password\" value=\"";
        // line 24
        echo ($context["password"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_password"] ?? null);
        echo "\" id=\"input-password\" class=\"form-control\" />
          <a href=\"";
        // line 25
        echo ($context["forgotten"] ?? null);
        echo "\" style=\"margin-top: 7px;display: inline-block\">";
        echo ($context["text_forgotten"] ?? null);
        echo "</a></div>
        <input type=\"submit\" value=\"";
        // line 26
        echo ($context["button_login"] ?? null);
        echo "\" class=\"btn btn-primary\" />
        ";
        // line 27
        if (($context["redirect"] ?? null)) {
            echo " 
        <input type=\"hidden\" name=\"redirect\" value=\"";
            // line 28
            echo ($context["redirect"] ?? null);
            echo "\" />
        ";
        }
        // line 29
        echo " 
      </form>
    </div>
  </div>
</div>

";
        // line 35
        $this->loadTemplate("wokiee/template/new_elements/wrapper_bottom.twig", "wokiee/template/account/login.twig", 35)->display($context);
        // line 36
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "wokiee/template/account/login.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  135 => 36,  133 => 35,  125 => 29,  120 => 28,  116 => 27,  112 => 26,  106 => 25,  100 => 24,  96 => 23,  88 => 20,  84 => 19,  79 => 17,  75 => 16,  71 => 15,  61 => 10,  57 => 9,  53 => 8,  49 => 7,  43 => 3,  41 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "wokiee/template/account/login.twig", "");
    }
}

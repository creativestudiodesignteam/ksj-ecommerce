<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* wokiee/template/common/footer.twig */
class __TwigTemplate_9fdc166c1ed4b95224b9174b1f21f8282d9790bfd35955895a3b0fe4fb22505b extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ((twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "has", [0 => "theme_options"], "method", false, false, false, 1) == twig_constant("true"))) {
            echo " 
     ";
            // line 2
            $context["theme_options"] = twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "get", [0 => "theme_options"], "method", false, false, false, 2);
            // line 3
            echo "     ";
            $context["config"] = twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "get", [0 => "config"], "method", false, false, false, 3);
            // line 4
            echo "          
     ";
            // line 5
            $context["language_id"] = twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 5);
            // line 6
            echo "     ";
            $context["customfooter"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "customfooter"], "method", false, false, false, 6);
            // line 7
            echo "          
     ";
            // line 8
            $context["customfooter_top"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "customfooter_top"], "method", false, false, false, 8);
            // line 9
            echo "     ";
            $context["customfooter_bottom"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "customfooter_bottom"], "method", false, false, false, 9);
            // line 10
            echo "     ";
            $context["customfooter_center"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "customfooter"], "method", false, false, false, 10);
            // line 11
            echo "     ";
            $context["footer_center"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "footer"], "method", false, false, false, 11);
            // line 12
            echo "
<footer>             
     ";
            // line 14
            $context["footer_top"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "footer_top"], "method", false, false, false, 14);
            // line 15
            echo "     ";
            if ((twig_length_filter($this->env, ($context["footer_top"] ?? null)) > 0)) {
                echo " 
          ";
                // line 16
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["footer_top"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                    echo " 
               ";
                    // line 17
                    echo $context["module"];
                    echo "
          ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 19
                echo "     ";
            }
            echo "       
           
     <div class=\"tt-footer-col tt-color-scheme-";
            // line 21
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "footer_colors"], "method", false, false, false, 21) == 3)) {
                echo "03";
            } else {
                echo "01";
            }
            echo "\">
          <div class=\"container\">  
               ";
            // line 23
            if ((twig_length_filter($this->env, ($context["footer_center"] ?? null)) > 0)) {
                echo " 
                    ";
                // line 24
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["footer_center"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                    echo " 
                         ";
                    // line 25
                    echo $context["module"];
                    echo "
                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 27
                echo "               ";
            } else {
                echo "            
                    <div class=\"row\">
                         ";
                // line 29
                $context["footer_left"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "footer_left"], "method", false, false, false, 29);
                // line 30
                echo "                         ";
                $context["footer_right"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "footer_right"], "method", false, false, false, 30);
                // line 31
                echo "                         
                         ";
                // line 32
                $context["span"] = 3;
                // line 33
                echo "                         ";
                if (((twig_length_filter($this->env, ($context["footer_left"] ?? null)) > 0) && (twig_length_filter($this->env, ($context["footer_right"] ?? null)) > 0))) {
                    // line 34
                    echo "                              ";
                    $context["span"] = 2;
                    // line 35
                    echo "                         ";
                } elseif (((twig_length_filter($this->env, ($context["footer_left"] ?? null)) > 0) || (twig_length_filter($this->env, ($context["footer_right"] ?? null)) > 0))) {
                    // line 36
                    echo "                              ";
                    $context["span"] = 25;
                    // line 37
                    echo "                         ";
                }
                echo " 
                         
                         ";
                // line 39
                if ((twig_length_filter($this->env, ($context["footer_left"] ?? null)) > 0)) {
                    echo " 
                              <div class=\"col-md-";
                    // line 40
                    echo ($context["span"] ?? null);
                    echo "\">
                                   ";
                    // line 41
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(($context["footer_left"] ?? null));
                    foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                        echo " 
                                        ";
                        // line 42
                        echo $context["module"];
                        echo "
                                   ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 43
                    echo " 
                              </div>
                         ";
                }
                // line 45
                echo " 
                         
                         <!-- Information -->
                         <div class=\"col-md-";
                // line 48
                echo ($context["span"] ?? null);
                echo "\">
                              <div class=\"tt-mobile-collapse\">
                                   <h4 class=\"tt-collapse-title\">";
                // line 50
                echo ($context["text_information"] ?? null);
                echo "</h4>
                                   <div class=\"tt-collapse-content\">
                                        <ul class=\"tt-list\">
                                             ";
                // line 53
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["informations"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["information"]) {
                    echo " 
                                             <li><a href=\"";
                    // line 54
                    echo (($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = $context["information"]) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4["href"] ?? null) : null);
                    echo "\">";
                    echo (($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = $context["information"]) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144["title"] ?? null) : null);
                    echo "</a></li>
                                             ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['information'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 55
                echo " 
                                        </ul>
                                   </div>
                              </div>
                         </div>
                         
                         <!-- Customer Service -->
                         <div class=\"col-md-";
                // line 62
                echo ($context["span"] ?? null);
                echo "\">
                              <div class=\"tt-mobile-collapse\">
                                   <h4 class=\"tt-collapse-title\">";
                // line 64
                echo ($context["text_service"] ?? null);
                echo "</h4>
                                   <div class=\"tt-collapse-content\">
                                        <ul class=\"tt-list\">
                                             <li><a href=\"";
                // line 67
                echo ($context["contact"] ?? null);
                echo "\">";
                echo ($context["text_contact"] ?? null);
                echo "</a></li>
                                             <li><a href=\"";
                // line 68
                echo ($context["return"] ?? null);
                echo "\">";
                echo ($context["text_return"] ?? null);
                echo "</a></li>
                                             <li><a href=\"";
                // line 69
                echo ($context["sitemap"] ?? null);
                echo "\">";
                echo ($context["text_sitemap"] ?? null);
                echo "</a></li>
                                        </ul> 
                                   </div>
                              </div>
                         </div>
                         
                         <!-- Extras -->
                         <div class=\"col-md-";
                // line 76
                echo ($context["span"] ?? null);
                echo "\">
                              <div class=\"tt-mobile-collapse\">
                                   <h4 class=\"tt-collapse-title\">";
                // line 78
                echo ($context["text_extra"] ?? null);
                echo "</h4>
                                   <div class=\"tt-collapse-content\">
                                        <ul class=\"tt-list\">
                                             <li><a href=\"";
                // line 81
                echo ($context["manufacturer"] ?? null);
                echo "\">";
                echo ($context["text_manufacturer"] ?? null);
                echo "</a></li>
                                             <li><a href=\"";
                // line 82
                echo ($context["voucher"] ?? null);
                echo "\">";
                echo ($context["text_voucher"] ?? null);
                echo "</a></li>
                                             <li><a href=\"";
                // line 83
                echo ($context["affiliate"] ?? null);
                echo "\">";
                echo ($context["text_affiliate"] ?? null);
                echo "</a></li>
                                             <li><a href=\"";
                // line 84
                echo ($context["special"] ?? null);
                echo "\">";
                echo ($context["text_special"] ?? null);
                echo " </a></li>
                                        </ul>
                                   </div>
                              </div>
                         </div>
                         
                         <!-- My Account -->
                         <div class=\"col-md-";
                // line 91
                echo ($context["span"] ?? null);
                echo "\">
                              <div class=\"tt-mobile-collapse\">
                                   <h4 class=\"tt-collapse-title\">";
                // line 93
                echo ($context["text_account"] ?? null);
                echo "</h4>
                                   <div class=\"tt-collapse-content\">
                                        <ul class=\"tt-list\">
                                             <li><a href=\"";
                // line 96
                echo ($context["account"] ?? null);
                echo "\">";
                echo ($context["text_account"] ?? null);
                echo "</a></li>
                                             <li><a href=\"";
                // line 97
                echo ($context["order"] ?? null);
                echo "\">";
                echo ($context["text_order"] ?? null);
                echo "</a></li>
                                             <li><a href=\"";
                // line 98
                echo ($context["wishlist"] ?? null);
                echo "\">";
                echo ($context["text_wishlist"] ?? null);
                echo "</a></li>
                                             <li><a href=\"";
                // line 99
                echo ($context["newsletter"] ?? null);
                echo "\">";
                echo ($context["text_newsletter"] ?? null);
                echo "</a></li>
                                        </ul>
                                   </div>
                              </div>
                         </div>
                         
                         ";
                // line 105
                if ((twig_length_filter($this->env, ($context["footer_right"] ?? null)) > 0)) {
                    echo " 
                              <div class=\"col-md-";
                    // line 106
                    echo ($context["span"] ?? null);
                    echo "\">
                                   ";
                    // line 107
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(($context["footer_right"] ?? null));
                    foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                        echo " 
                                        ";
                        // line 108
                        echo $context["module"];
                        echo "
                                   ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 109
                    echo " 
                              </div>
                         ";
                }
                // line 111
                echo " 
                    </div>
                       
                    ";
                // line 114
                $context["footer_bottom"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "footer_bottom"], "method", false, false, false, 114);
                // line 115
                echo "                    ";
                if ((twig_length_filter($this->env, ($context["footer_bottom"] ?? null)) > 0)) {
                    echo " 
                         ";
                    // line 116
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(($context["footer_bottom"] ?? null));
                    foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                        echo " 
                              ";
                        // line 117
                        echo $context["module"];
                        echo "
                         ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 119
                    echo "                    ";
                }
                // line 120
                echo "               ";
            }
            echo " 
          </div>
     </div> 

     ";
            // line 124
            $context["bottom"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "bottom"], "method", false, false, false, 124);
            // line 125
            echo "     ";
            if ((twig_length_filter($this->env, ($context["bottom"] ?? null)) > 0)) {
                echo " 
          ";
                // line 126
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["bottom"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                    echo " 
               ";
                    // line 127
                    echo $context["module"];
                    echo "
          ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 129
                echo "     ";
            }
            echo " 
</footer>                                         
<a href=\"#\" class=\"tt-back-to-top\">";
            // line 131
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "back_to_top_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 131)], "method", false, false, false, 131) != "")) {
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "back_to_top_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 131)], "method", false, false, false, 131);
            } else {
                echo "BACK TO TOP";
            }
            echo "</a>
<!-- modal (AddToCartProduct) -->
<div class=\"modal  fade\"  id=\"modalAddToCartProduct\" tabindex=\"-1\" role=\"dialog\" aria-label=\"myModalLabel\" aria-hidden=\"true\">
     <div class=\"modal-dialog\">
          <div class=\"modal-content \">
               <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\"><span class=\"icon icon-clear\"></span></button>
               </div>
               <div class=\"modal-body\">
                    <div class=\"tt-modal-addtocart mobile\">
                         <div class=\"tt-modal-messages\">
                              <i class=\"icon-f-68\"></i> ";
            // line 142
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "added_to_cart_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 142)], "method", false, false, false, 142) != "")) {
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "added_to_cart_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 142)], "method", false, false, false, 142);
            } else {
                echo "Added to cart successfully!";
            }
            // line 143
            echo "                         </div>
                         <a href=\"#\" class=\"btn-link btn-close-popup\">";
            // line 144
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "continue_shopping_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 144)], "method", false, false, false, 144) != "")) {
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "continue_shopping_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 144)], "method", false, false, false, 144);
            } else {
                echo "CONTINUE SHOPPING";
            }
            echo "</a>
                       <a href=\"";
            // line 145
            echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getLinkOpenCart", [0 => "checkout/cart"], "method", false, false, false, 145);
            echo "\" class=\"btn-link\">";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "view_cart_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 145)], "method", false, false, false, 145) != "")) {
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "view_cart_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 145)], "method", false, false, false, 145);
            } else {
                echo "VIEW CART";
            }
            echo "</a>
                       <a href=\"";
            // line 146
            echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getLinkOpenCart", [0 => "checkout/checkout"], "method", false, false, false, 146);
            echo "\" class=\"btn-link\">";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "proceed_to_checkout_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 146)], "method", false, false, false, 146) != "")) {
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "proceed_to_checkout_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 146)], "method", false, false, false, 146);
            } else {
                echo "PROCEED TO CHECKOUT";
            }
            echo "</a>
                    </div>
                    <div class=\"tt-modal-addtocart desctope\">
                         <div class=\"row\">
                              <div class=\"col-12 col-lg-6\">
                                   <div class=\"tt-modal-messages\">
                                        <i class=\"icon-f-68\"></i> ";
            // line 152
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "added_to_cart_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 152)], "method", false, false, false, 152) != "")) {
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "added_to_cart_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 152)], "method", false, false, false, 152);
            } else {
                echo "Added to cart successfully!";
            }
            // line 153
            echo "                                   </div>
                                   <div class=\"tt-modal-product\">
                                        <div class=\"tt-img\">
                                             <img src=\"catalog/view/theme/wokiee/img/loader.svg\" alt=\"\">
                                        </div>
                                        <h2 class=\"tt-title\"><a href=\"product.html\">Flared Shift Dress</a></h2>
                                        <div class=\"tt-qty\">
                                             ";
            // line 160
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "qty_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 160)], "method", false, false, false, 160) != "")) {
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "qty_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 160)], "method", false, false, false, 160);
            } else {
                echo "QTY";
            }
            echo ": <span>1</span>
                                        </div>
                                   </div>
                                   <div class=\"tt-product-total\">
                                        <div class=\"tt-total\">
                                             <span class=\"tt-price\">\$324</span>
                                        </div>
                                   </div>
                              </div>
                              <div class=\"col-12 col-lg-6\">
                                   <a href=\"#\" class=\"tt-cart-total\">
                                        <p class=\"text-total\" style=\"margin: 0px\">";
            // line 171
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "there_are_one_items_in_your_cart_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 171)], "method", false, false, false, 171) != "")) {
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "there_are_one_items_in_your_cart_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 171)], "method", false, false, false, 171);
            } else {
                echo "There are 1 items in your cart";
            }
            echo "</p>
                                        <div class=\"tt-total\">
                                             ";
            // line 173
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "total_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 173)], "method", false, false, false, 173) != "")) {
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "total_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 173)], "method", false, false, false, 173);
            } else {
                echo "TOTAL";
            }
            echo ": <span class=\"tt-price\">\$324</span>
                                        </div>
                                   </a>
                                   <a href=\"#\" class=\"btn btn-border btn-close-popup\">";
            // line 176
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "continue_shopping_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 176)], "method", false, false, false, 176) != "")) {
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "continue_shopping_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 176)], "method", false, false, false, 176);
            } else {
                echo "CONTINUE SHOPPING";
            }
            echo "</a>
                                   <a href=\"";
            // line 177
            echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getLinkOpenCart", [0 => "checkout/cart"], "method", false, false, false, 177);
            echo "\" class=\"btn btn-border\">";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "view_cart_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 177)], "method", false, false, false, 177) != "")) {
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "view_cart_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 177)], "method", false, false, false, 177);
            } else {
                echo "VIEW CART";
            }
            echo "</a>
                                   <a href=\"";
            // line 178
            echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getLinkOpenCart", [0 => "checkout/checkout"], "method", false, false, false, 178);
            echo "\" class=\"btn\">";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "proceed_to_checkout_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 178)], "method", false, false, false, 178) != "")) {
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "proceed_to_checkout_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 178)], "method", false, false, false, 178);
            } else {
                echo "PROCEED TO CHECKOUT";
            }
            echo "</a>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>
<!-- modal (quickViewModal) -->
<div class=\"modal  fade\"  id=\"ModalquickView\" tabindex=\"-1\" role=\"dialog\" aria-label=\"myModalLabel\" aria-hidden=\"true\">
     <div class=\"modal-dialog modal-lg\">
          <div class=\"modal-content \">
               <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\"><span class=\"icon icon-clear\"></span></button>
               </div>
               <div class=\"modal-body\">
                    <div class=\"loader\"><img src=\"catalog/view/theme/wokiee/img/loader.svg\" alt=\"Loader\"></div>
                    <div class=\"quickview-content\">

                    </div>
               </div>
          </div>
     </div>
</div>
<div id=\"modalPartialView\"></div>
";
            // line 203
            $context["lista_plikow"] = [];
            echo " 

";
            // line 205
            $context["lista_plikow"] = twig_array_merge(($context["lista_plikow"] ?? null), [0 => "catalog/view/theme/wokiee/js/bootstrap.min.js"]);
            // line 206
            $context["lista_plikow"] = twig_array_merge(($context["lista_plikow"] ?? null), [0 => "catalog/view/theme/wokiee/js/slick.min.js"]);
            // line 207
            $context["lista_plikow"] = twig_array_merge(($context["lista_plikow"] ?? null), [0 => "catalog/view/theme/wokiee/js/perfect-scrollbar.min.js"]);
            // line 208
            $context["lista_plikow"] = twig_array_merge(($context["lista_plikow"] ?? null), [0 => "catalog/view/theme/wokiee/js/panelmenu.js"]);
            // line 209
            $context["lista_plikow"] = twig_array_merge(($context["lista_plikow"] ?? null), [0 => "catalog/view/theme/wokiee/js/instafeed.min.js"]);
            // line 210
            $context["lista_plikow"] = twig_array_merge(($context["lista_plikow"] ?? null), [0 => "catalog/view/theme/wokiee/js/jquery.plugin.min.js"]);
            // line 211
            $context["lista_plikow"] = twig_array_merge(($context["lista_plikow"] ?? null), [0 => "catalog/view/theme/wokiee/js/jquery.countdown.min.js"]);
            // line 212
            $context["lista_plikow"] = twig_array_merge(($context["lista_plikow"] ?? null), [0 => "catalog/view/theme/wokiee/js/lazyload.min.js"]);
            // line 213
            $context["lista_plikow"] = twig_array_merge(($context["lista_plikow"] ?? null), [0 => "catalog/view/theme/wokiee/js/bootstrap-notify.js"]);
            // line 214
            $context["lista_plikow"] = twig_array_merge(($context["lista_plikow"] ?? null), [0 => "catalog/view/javascript/jquery/datetimepicker/moment/moment.min.js"]);
            // line 215
            $context["lista_plikow"] = twig_array_merge(($context["lista_plikow"] ?? null), [0 => "catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js"]);
            // line 216
            $context["lista_plikow"] = twig_array_merge(($context["lista_plikow"] ?? null), [0 => "catalog/view/theme/wokiee/js/jquery.elevatezoom.js"]);
            // line 217
            $context["lista_plikow"] = twig_array_merge(($context["lista_plikow"] ?? null), [0 => "catalog/view/theme/wokiee/js/jquery.magnific-popup.min.js"]);
            // line 218
            $context["lista_plikow"] = twig_array_merge(($context["lista_plikow"] ?? null), [0 => "catalog/view/theme/wokiee/js/air-sticky-block.min.js"]);
            // line 219
            $context["lista_plikow"] = twig_array_merge(($context["lista_plikow"] ?? null), [0 => "catalog/view/theme/wokiee/js/imagesloaded.js"]);
            // line 220
            $context["lista_plikow"] = twig_array_merge(($context["lista_plikow"] ?? null), [0 => "catalog/view/theme/wokiee/js/isotope.pkgd.min.js"]);
            // line 221
            $context["lista_plikow"] = twig_array_merge(($context["lista_plikow"] ?? null), [0 => "catalog/view/theme/wokiee/js/common.js"]);
            // line 222
            echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "compressorCodeJs", [0 => "wokiee", 1 => ($context["lista_plikow"] ?? null), 2 => 0, 3 => twig_constant("HTTPS_SERVER")], "method", false, false, false, 222);
            echo "

<script type=\"text/javascript\">
\$('#product .datetimepicker-date').datetimepicker({
     pickTime: false
});

\$('#product .datetimepicker-datetime').datetimepicker({
     pickDate: true,
     pickTime: true
});

\$('#product .datetimepicker-time').datetimepicker({
     pickDate: false
});
</script>
";
        }
        // line 238
        echo " 
</body>
</html>";
    }

    public function getTemplateName()
    {
        return "wokiee/template/common/footer.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  658 => 238,  638 => 222,  636 => 221,  634 => 220,  632 => 219,  630 => 218,  628 => 217,  626 => 216,  624 => 215,  622 => 214,  620 => 213,  618 => 212,  616 => 211,  614 => 210,  612 => 209,  610 => 208,  608 => 207,  606 => 206,  604 => 205,  599 => 203,  565 => 178,  555 => 177,  547 => 176,  537 => 173,  528 => 171,  510 => 160,  501 => 153,  495 => 152,  480 => 146,  470 => 145,  462 => 144,  459 => 143,  453 => 142,  435 => 131,  429 => 129,  421 => 127,  415 => 126,  410 => 125,  408 => 124,  400 => 120,  397 => 119,  389 => 117,  383 => 116,  378 => 115,  376 => 114,  371 => 111,  366 => 109,  358 => 108,  352 => 107,  348 => 106,  344 => 105,  333 => 99,  327 => 98,  321 => 97,  315 => 96,  309 => 93,  304 => 91,  292 => 84,  286 => 83,  280 => 82,  274 => 81,  268 => 78,  263 => 76,  251 => 69,  245 => 68,  239 => 67,  233 => 64,  228 => 62,  219 => 55,  209 => 54,  203 => 53,  197 => 50,  192 => 48,  187 => 45,  182 => 43,  174 => 42,  168 => 41,  164 => 40,  160 => 39,  154 => 37,  151 => 36,  148 => 35,  145 => 34,  142 => 33,  140 => 32,  137 => 31,  134 => 30,  132 => 29,  126 => 27,  118 => 25,  112 => 24,  108 => 23,  99 => 21,  93 => 19,  85 => 17,  79 => 16,  74 => 15,  72 => 14,  68 => 12,  65 => 11,  62 => 10,  59 => 9,  57 => 8,  54 => 7,  51 => 6,  49 => 5,  46 => 4,  43 => 3,  41 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "wokiee/template/common/footer.twig", "");
    }
}

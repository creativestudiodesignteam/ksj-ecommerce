<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* wokiee/template/extension/module/account.twig */
class __TwigTemplate_58799650c040b5da24115acc3e1a43e52c07d91bf2f31772972e32d4cfd3761b extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"tt-collapse open\">
  <h3 class=\"tt-collapse-title\">";
        // line 2
        echo ($context["heading_title"] ?? null);
        echo "</h3>
  <div class=\"tt-collapse-content\" style=\"\">
    <ul class=\"tt-list-row\">
      ";
        // line 5
        if ( !($context["logged"] ?? null)) {
            echo " 
      <li><a href=\"";
            // line 6
            echo ($context["login"] ?? null);
            echo "\">";
            echo ($context["text_login"] ?? null);
            echo "</a></li>
      <li><a href=\"";
            // line 7
            echo ($context["register"] ?? null);
            echo "\">";
            echo ($context["text_register"] ?? null);
            echo "</a></li>
      <li><a href=\"";
            // line 8
            echo ($context["forgotten"] ?? null);
            echo "\">";
            echo ($context["text_forgotten"] ?? null);
            echo "</a></li>
      ";
        }
        // line 9
        echo " 
      <li><a href=\"";
        // line 10
        echo ($context["account"] ?? null);
        echo "\">";
        echo ($context["text_account"] ?? null);
        echo "</a></li>
      ";
        // line 11
        if (($context["logged"] ?? null)) {
            echo " 
      <li><a href=\"";
            // line 12
            echo ($context["edit"] ?? null);
            echo "\">";
            echo ($context["text_edit"] ?? null);
            echo "</a></li>
      <li><a href=\"";
            // line 13
            echo ($context["password"] ?? null);
            echo "\">";
            echo ($context["text_password"] ?? null);
            echo "</a></li>
      ";
        }
        // line 14
        echo " 
      <li><a href=\"";
        // line 15
        echo ($context["address"] ?? null);
        echo "\">";
        echo ($context["text_address"] ?? null);
        echo "</a></li>
      <li><a href=\"";
        // line 16
        echo ($context["wishlist"] ?? null);
        echo "\">";
        echo ($context["text_wishlist"] ?? null);
        echo "</a></li>
      <li><a href=\"";
        // line 17
        echo ($context["order"] ?? null);
        echo "\">";
        echo ($context["text_order"] ?? null);
        echo "</a></li>
      <li><a href=\"";
        // line 18
        echo ($context["download"] ?? null);
        echo "\">";
        echo ($context["text_download"] ?? null);
        echo "</a></li>
      <li><a href=\"";
        // line 19
        echo ($context["recurring"] ?? null);
        echo "\">";
        echo ($context["text_recurring"] ?? null);
        echo "</a></li>
      <li><a href=\"";
        // line 20
        echo ($context["reward"] ?? null);
        echo "\">";
        echo ($context["text_reward"] ?? null);
        echo "</a></li>
      <li><a href=\"";
        // line 21
        echo ($context["return"] ?? null);
        echo "\">";
        echo ($context["text_return"] ?? null);
        echo "</a></li>
      <li><a href=\"";
        // line 22
        echo ($context["transaction"] ?? null);
        echo "\">";
        echo ($context["text_transaction"] ?? null);
        echo "</a></li>
      <li><a href=\"";
        // line 23
        echo ($context["newsletter"] ?? null);
        echo "\">";
        echo ($context["text_newsletter"] ?? null);
        echo "</a></li>
      ";
        // line 24
        if (($context["logged"] ?? null)) {
            echo " 
      <li><a href=\"";
            // line 25
            echo ($context["logout"] ?? null);
            echo "\">";
            echo ($context["text_logout"] ?? null);
            echo "</a></li>
      ";
        }
        // line 26
        echo " 
    </ul>
  </div>
</div>";
    }

    public function getTemplateName()
    {
        return "wokiee/template/extension/module/account.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  163 => 26,  156 => 25,  152 => 24,  146 => 23,  140 => 22,  134 => 21,  128 => 20,  122 => 19,  116 => 18,  110 => 17,  104 => 16,  98 => 15,  95 => 14,  88 => 13,  82 => 12,  78 => 11,  72 => 10,  69 => 9,  62 => 8,  56 => 7,  50 => 6,  46 => 5,  40 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "wokiee/template/extension/module/account.twig", "");
    }
}

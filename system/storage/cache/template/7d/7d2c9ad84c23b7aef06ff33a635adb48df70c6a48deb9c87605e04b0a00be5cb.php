<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* wokiee/template/account/account.twig */
class __TwigTemplate_e2ff63419ce34907a2f4e15a9e80d191969a15d4644d7f244a0a9c716b154721 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo " 
";
        // line 2
        $this->loadTemplate("wokiee/template/new_elements/wrapper_top.twig", "wokiee/template/account/account.twig", 2)->display($context);
        // line 3
        echo "
<h5>";
        // line 4
        echo ($context["text_my_account"] ?? null);
        echo "</h5>
<ul class=\"tt-list-dash\">
  <li><a href=\"";
        // line 6
        echo ($context["edit"] ?? null);
        echo "\">";
        echo ($context["text_edit"] ?? null);
        echo "</a></li>
  <li><a href=\"";
        // line 7
        echo ($context["password"] ?? null);
        echo "\">";
        echo ($context["text_password"] ?? null);
        echo "</a></li>
  <li><a href=\"";
        // line 8
        echo ($context["address"] ?? null);
        echo "\">";
        echo ($context["text_address"] ?? null);
        echo "</a></li>
  <li><a href=\"";
        // line 9
        echo ($context["wishlist"] ?? null);
        echo "\">";
        echo ($context["text_wishlist"] ?? null);
        echo "</a></li>
</ul>
";
        // line 11
        if (($context["credit_cards"] ?? null)) {
            // line 12
            echo "<h5 style=\"padding-top: 30px\">";
            echo ($context["text_credit_card"] ?? null);
            echo "</h5>
<ul class=\"tt-list-dash\">
  ";
            // line 14
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["credit_cards"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["credit_card"]) {
                // line 15
                echo "  <li><a href=\"";
                echo twig_get_attribute($this->env, $this->source, $context["credit_card"], "href", [], "any", false, false, false, 15);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["credit_card"], "name", [], "any", false, false, false, 15);
                echo "</a></li>
  ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['credit_card'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 17
            echo "</ul>
";
        }
        // line 19
        echo "<h5 style=\"padding-top: 30px\">";
        echo ($context["text_my_orders"] ?? null);
        echo "</h5>
<ul class=\"tt-list-dash\">
  <li><a href=\"";
        // line 21
        echo ($context["order"] ?? null);
        echo "\">";
        echo ($context["text_order"] ?? null);
        echo "</a></li>
  <li><a href=\"";
        // line 22
        echo ($context["download"] ?? null);
        echo "\">";
        echo ($context["text_download"] ?? null);
        echo "</a></li>
  ";
        // line 23
        if (($context["reward"] ?? null)) {
            // line 24
            echo "  <li><a href=\"";
            echo ($context["reward"] ?? null);
            echo "\">";
            echo ($context["text_reward"] ?? null);
            echo "</a></li>
  ";
        }
        // line 26
        echo "  <li><a href=\"";
        echo ($context["return"] ?? null);
        echo "\">";
        echo ($context["text_return"] ?? null);
        echo "</a></li>
  <li><a href=\"";
        // line 27
        echo ($context["transaction"] ?? null);
        echo "\">";
        echo ($context["text_transaction"] ?? null);
        echo "</a></li>
  <li><a href=\"";
        // line 28
        echo ($context["recurring"] ?? null);
        echo "\">";
        echo ($context["text_recurring"] ?? null);
        echo "</a></li>
</ul>
<h5 style=\"padding-top: 30px\">";
        // line 30
        echo ($context["text_my_affiliate"] ?? null);
        echo "</h5>
<ul class=\"tt-list-dash\">
  ";
        // line 32
        if ( !($context["tracking"] ?? null)) {
            // line 33
            echo "  <li><a href=\"";
            echo ($context["affiliate"] ?? null);
            echo "\">";
            echo ($context["text_affiliate_add"] ?? null);
            echo "</a></li>
  ";
        } else {
            // line 35
            echo "  <li><a href=\"";
            echo ($context["affiliate"] ?? null);
            echo "\">";
            echo ($context["text_affiliate_edit"] ?? null);
            echo "</a></li>
  <li><a href=\"";
            // line 36
            echo ($context["tracking"] ?? null);
            echo "\">";
            echo ($context["text_tracking"] ?? null);
            echo "</a></li>
  ";
        }
        // line 38
        echo "</ul>
<h5 style=\"padding-top: 30px\">";
        // line 39
        echo ($context["text_my_newsletter"] ?? null);
        echo "</h5>
<ul class=\"tt-list-dash\">
  <li><a href=\"";
        // line 41
        echo ($context["newsletter"] ?? null);
        echo "\">";
        echo ($context["text_newsletter"] ?? null);
        echo "</a></li>
</ul>

";
        // line 44
        $this->loadTemplate("wokiee/template/new_elements/wrapper_bottom.twig", "wokiee/template/account/account.twig", 44)->display($context);
        // line 45
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "wokiee/template/account/account.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  198 => 45,  196 => 44,  188 => 41,  183 => 39,  180 => 38,  173 => 36,  166 => 35,  158 => 33,  156 => 32,  151 => 30,  144 => 28,  138 => 27,  131 => 26,  123 => 24,  121 => 23,  115 => 22,  109 => 21,  103 => 19,  99 => 17,  88 => 15,  84 => 14,  78 => 12,  76 => 11,  69 => 9,  63 => 8,  57 => 7,  51 => 6,  46 => 4,  43 => 3,  41 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "wokiee/template/account/account.twig", "");
    }
}

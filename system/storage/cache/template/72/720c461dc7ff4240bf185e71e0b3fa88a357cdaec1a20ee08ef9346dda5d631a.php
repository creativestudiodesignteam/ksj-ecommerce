<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* extension/module/custom_module.twig */
class __TwigTemplate_4345b6d7ee4780c0b8b2c6a17d4d62b5b8486340dab128774e479f8cbdd15a2e extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo ($context["column_left"] ?? null);
        echo " 
<div id=\"content\"><div class=\"container-fluid\">
\t<div class=\"page-header\">
\t    <h1>Custom Module</h1>
\t    <ul class=\"breadcrumb\">
\t\t     ";
        // line 6
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            echo " 
\t\t      <li><a href=\"";
            // line 7
            echo (($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = $context["breadcrumb"]) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4["href"] ?? null) : null);
            echo "\">";
            echo (($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = $context["breadcrumb"]) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144["text"] ?? null) : null);
            echo "</a></li>
\t\t      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 8
        echo " 
\t    </ul>
\t  </div>
\t  
\t  <script type=\"text/javascript\" src=\"view/javascript/summernote/summernote.js\"></script>
\t  <link href=\"view/javascript/summernote/summernote.css\" rel=\"stylesheet\" />
\t  <script type=\"text/javascript\" src=\"view/javascript/summernote/opencart.js\"></script>  
\t  
    <link href='https://fonts.googleapis.com/css?family=Poppins:700,600,500,400,300' rel='stylesheet' type='text/css'>
    
\t<script type=\"text/javascript\">
\t\$.fn.tabs = function() {
\t\tvar selector = this;
\t\t
\t\tthis.each(function() {
\t\t\tvar obj = \$(this); 
\t\t\t
\t\t\t\$(obj.attr('href')).hide();
\t\t\t
\t\t\t\$(obj).click(function() {
\t\t\t\t\$(selector).removeClass('selected');
\t\t\t\t
\t\t\t\t\$(selector).each(function(i, element) {
\t\t\t\t\t\$(\$(element).attr('href')).hide();
\t\t\t\t});
\t\t\t\t
\t\t\t\t\$(this).addClass('selected');
\t\t\t\t
\t\t\t\t\$(\$(this).attr('href')).show();
\t\t\t\t
\t\t\t\treturn false;
\t\t\t});
\t\t});
\t
\t\t\$(this).show();
\t\t
\t\t\$(this).first().click();
\t};
\t</script>

\t";
        // line 48
        if (($context["error_warning"] ?? null)) {
            echo " 
\t\t<div class=\"alert alert-danger\"><i class=\"fa fa-exclamation-circle\"></i> ";
            // line 49
            echo ($context["error_warning"] ?? null);
            echo " 
\t\t  <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
\t\t</div>
\t";
        } elseif (        // line 52
($context["success"] ?? null)) {
            echo " 
\t\t<div class=\"alert alert-success\"><i class=\"fa fa-exclamation-circle\"></i> ";
            // line 53
            echo ($context["success"] ?? null);
            echo " 
\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
\t\t</div>
\t";
        }
        // line 56
        echo " 
\t
\t<form action=\"";
        // line 58
        echo ($context["action"] ?? null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form\">
\t\t<div class=\"set-size\" id=\"custom_module\">
\t\t\t<div class=\"content\">
\t\t\t\t<div>
\t\t\t\t\t<div class=\"tabs clearfix\">
\t\t\t\t\t\t<!-- Tabs module -->
\t\t\t\t\t\t<div id=\"tabs\" class=\"htabs main-tabs\">
\t\t\t\t\t\t\t";
        // line 65
        $context["module_row"] = 1;
        echo " 
\t\t\t\t\t\t\t";
        // line 66
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["modules"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
            echo " 
\t\t\t\t\t\t\t<a href=\"#tab-module-";
            // line 67
            echo ($context["module_row"] ?? null);
            echo "\" id=\"module-";
            echo ($context["module_row"] ?? null);
            echo "\">Module ";
            echo ($context["module_row"] ?? null);
            echo " &nbsp;<img src=\"view/image/module_template/delete-slider.png\"  alt=\"\" onclick=\"\$('.vtabs a:first').trigger('click'); \$('#module-";
            echo ($context["module_row"] ?? null);
            echo "').remove(); \$('#tab-module-";
            echo ($context["module_row"] ?? null);
            echo "').remove(); return false;\" /></a>
\t\t\t\t\t\t\t";
            // line 68
            $context["module_row"] = (($context["module_row"] ?? null) + 1);
            echo " 
\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 69
        echo " 
                            <span id=\"module-add\" onclick=\"addModule();\">Add Module <img src=\"view/image/module_template/add.png\" alt=\"\" /></span>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t
\t\t\t\t\t\t";
        // line 73
        $context["module_row"] = 1;
        echo " 
\t\t\t\t\t\t";
        // line 74
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["modules"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
            echo " 
\t\t\t\t\t\t<div id=\"tab-module-";
            // line 75
            echo ($context["module_row"] ?? null);
            echo "\" class=\"tab-content\">
\t\t\t\t\t\t\t<table class=\"form\" style=\"margin-bottom:10px\">
\t\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t\t    <td style=\"border:none;padding-top:7px\">Type:</td>
\t\t\t\t\t\t\t    <td style=\"border:none;padding-top:7px\"><select name=\"custom_module_module[";
            // line 79
            echo ($context["module_row"] ?? null);
            echo "][type]\" class=\"select-type\" id=\"";
            echo ($context["module_row"] ?? null);
            echo "\">
\t\t\t\t\t\t\t    \t";
            // line 80
            if ((1 == (($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = $context["module"]) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b["type"] ?? null) : null))) {
                echo " 
\t\t\t\t\t\t\t    \t<option value=\"1\" selected=\"selected\">Block</option>
\t\t\t\t\t\t\t    \t";
            } else {
                // line 82
                echo " 
\t\t\t\t\t\t\t    \t<option value=\"1\">Block</option>
\t\t\t\t\t\t\t    \t";
            }
            // line 84
            echo " 
\t\t\t\t\t\t\t    \t";
            // line 85
            if ((2 == (($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 = $context["module"]) && is_array($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002) || $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 instanceof ArrayAccess ? ($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002["type"] ?? null) : null))) {
                echo " 
\t\t\t\t\t\t\t    \t<option value=\"2\" selected=\"selected\">HTML</option>
\t\t\t\t\t\t\t    \t";
            } else {
                // line 87
                echo " 
\t\t\t\t\t\t\t    \t<option value=\"2\">HTML</option>
\t\t\t\t\t\t\t    \t";
            }
            // line 89
            echo " 
\t\t\t\t\t\t\t      </select></td>
\t\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t<div id=\"language-";
            // line 93
            echo ($context["module_row"] ?? null);
            echo "\" class=\"htabs\">
\t\t\t\t\t\t\t  ";
            // line 94
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["languages"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
                echo " 
\t\t\t\t\t\t\t  <a href=\"#tab-language-";
                // line 95
                echo ($context["module_row"] ?? null);
                echo "-";
                echo (($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 = $context["language"]) && is_array($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4) || $__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 instanceof ArrayAccess ? ($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4["language_id"] ?? null) : null);
                echo "\"><img src=\"language/";
                echo (($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 = $context["language"]) && is_array($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666) || $__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 instanceof ArrayAccess ? ($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666["code"] ?? null) : null);
                echo "/";
                echo (($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e = $context["language"]) && is_array($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e) || $__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e instanceof ArrayAccess ? ($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e["code"] ?? null) : null);
                echo ".png\" title=\"";
                echo (($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 = $context["language"]) && is_array($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52) || $__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 instanceof ArrayAccess ? ($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52["name"] ?? null) : null);
                echo "\" /> ";
                echo (($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 = $context["language"]) && is_array($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136) || $__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 instanceof ArrayAccess ? ($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136["name"] ?? null) : null);
                echo "</a>
\t\t\t\t\t\t\t  ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 96
            echo " 
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t";
            // line 98
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["languages"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
                echo " 
\t\t\t\t\t\t\t<div id=\"tab-language-";
                // line 99
                echo ($context["module_row"] ?? null);
                echo "-";
                echo (($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 = $context["language"]) && is_array($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386) || $__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 instanceof ArrayAccess ? ($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386["language_id"] ?? null) : null);
                echo "\">
\t\t\t\t\t\t\t  <div class=\"block";
                // line 100
                echo ($context["module_row"] ?? null);
                echo "\" ";
                if ((2 == (($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9 = $context["module"]) && is_array($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9) || $__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9 instanceof ArrayAccess ? ($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9["type"] ?? null) : null))) {
                    echo " ";
                    echo "style=\"display:none\"";
                    echo " ";
                }
                echo ">
\t\t\t\t\t\t\t\t  <table class=\"form\">
\t\t\t\t\t\t\t\t    <tr>
\t\t\t\t\t\t\t\t      <td>Block heading:</td>
\t\t\t\t\t\t\t\t      <td><input type=\"text\" value=\"";
                // line 104
                echo ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["module"], "block_heading", [], "array", false, true, false, 104), (($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae = $context["language"]) && is_array($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae) || $__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae instanceof ArrayAccess ? ($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae["language_id"] ?? null) : null), [], "array", true, true, false, 104)) ? ((($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f = (($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40 = $context["module"]) && is_array($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40) || $__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40 instanceof ArrayAccess ? ($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40["block_heading"] ?? null) : null)) && is_array($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f) || $__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f instanceof ArrayAccess ? ($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f[(($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f = $context["language"]) && is_array($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f) || $__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f instanceof ArrayAccess ? ($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f["language_id"] ?? null) : null)] ?? null) : null)) : (""));
                echo "\" name=\"custom_module_module[";
                echo ($context["module_row"] ?? null);
                echo "][block_heading][";
                echo (($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760 = $context["language"]) && is_array($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760) || $__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760 instanceof ArrayAccess ? ($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760["language_id"] ?? null) : null);
                echo "]\" style=\"width:250px\"></td>
\t\t\t\t\t\t\t\t    </tr>
\t\t\t\t\t\t\t\t    <tr>
\t\t\t\t\t\t\t\t      <td>Block content:</td>
\t\t\t\t\t\t\t\t      <td><textarea name=\"custom_module_module[";
                // line 108
                echo ($context["module_row"] ?? null);
                echo "][block_content][";
                echo (($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce = $context["language"]) && is_array($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce) || $__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce instanceof ArrayAccess ? ($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce["language_id"] ?? null) : null);
                echo "]\" id=\"block-content-";
                echo ($context["module_row"] ?? null);
                echo "-";
                echo (($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b = $context["language"]) && is_array($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b) || $__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b instanceof ArrayAccess ? ($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b["language_id"] ?? null) : null);
                echo "\" data-toggle=\"summernote\">";
                echo ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["module"], "block_content", [], "array", false, true, false, 108), (($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c = $context["language"]) && is_array($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c) || $__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c instanceof ArrayAccess ? ($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c["language_id"] ?? null) : null), [], "array", true, true, false, 108)) ? ((($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972 = (($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216 = $context["module"]) && is_array($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216) || $__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216 instanceof ArrayAccess ? ($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216["block_content"] ?? null) : null)) && is_array($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972) || $__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972 instanceof ArrayAccess ? ($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972[(($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0 = $context["language"]) && is_array($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0) || $__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0 instanceof ArrayAccess ? ($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0["language_id"] ?? null) : null)] ?? null) : null)) : (""));
                echo "</textarea></td>
\t\t\t\t\t\t\t\t    </tr>
\t\t\t\t\t\t\t\t  </table>\t
\t\t\t\t\t\t\t  </div>
\t\t\t\t\t\t\t  <div class=\"html";
                // line 112
                echo ($context["module_row"] ?? null);
                echo "\" ";
                if ((2 != (($__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c = $context["module"]) && is_array($__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c) || $__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c instanceof ArrayAccess ? ($__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c["type"] ?? null) : null))) {
                    echo " ";
                    echo "style=\"display:none\"";
                    echo " ";
                }
                echo ">
\t\t\t\t\t\t\t\t  <table class=\"form\">
\t\t\t\t\t\t\t\t    <tr>
\t\t\t\t\t\t\t\t      <td>HTML:</td>
\t\t\t\t\t\t\t\t      <td><textarea name=\"custom_module_module[";
                // line 116
                echo ($context["module_row"] ?? null);
                echo "][html][";
                echo (($__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f = $context["language"]) && is_array($__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f) || $__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f instanceof ArrayAccess ? ($__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f["language_id"] ?? null) : null);
                echo "]\" class=\"html\">";
                echo ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["module"], "html", [], "array", false, true, false, 116), (($__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc = $context["language"]) && is_array($__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc) || $__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc instanceof ArrayAccess ? ($__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc["language_id"] ?? null) : null), [], "array", true, true, false, 116)) ? ((($__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55 = (($__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba = $context["module"]) && is_array($__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba) || $__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba instanceof ArrayAccess ? ($__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba["html"] ?? null) : null)) && is_array($__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55) || $__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55 instanceof ArrayAccess ? ($__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55[(($__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78 = $context["language"]) && is_array($__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78) || $__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78 instanceof ArrayAccess ? ($__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78["language_id"] ?? null) : null)] ?? null) : null)) : (""));
                echo "</textarea></td>
\t\t\t\t\t\t\t\t    </tr>
\t\t\t\t\t\t\t\t  </table>
\t\t\t\t\t\t\t  </div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 121
            echo " 
\t\t\t\t\t\t\t<table class=\"form\">
\t\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t\t    <td>Layout:</td>
\t\t\t\t\t\t\t    <td><select name=\"custom_module_module[";
            // line 125
            echo ($context["module_row"] ?? null);
            echo "][layout_id]\">
\t\t\t\t\t\t\t    \t";
            // line 126
            if ((99999 == (($__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de = $context["module"]) && is_array($__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de) || $__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de instanceof ArrayAccess ? ($__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de["layout_id"] ?? null) : null))) {
                echo " 
\t\t\t\t\t\t\t    \t<option value=\"99999\" selected=\"selected\">All pages</option>
\t\t\t\t\t\t\t    \t";
            } else {
                // line 128
                echo " 
\t\t\t\t\t\t\t    \t<option value=\"99999\">All pages</option>
\t\t\t\t\t\t\t    \t";
            }
            // line 130
            echo " 
\t\t\t\t\t\t\t    \t
\t\t\t\t\t\t\t    \t";
            // line 132
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["stores"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["store"]) {
                echo " 
\t\t\t\t\t\t\t    \t     ";
                // line 133
                if ((("99999" . (($__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828 = $context["store"]) && is_array($__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828) || $__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828 instanceof ArrayAccess ? ($__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828["store_id"] ?? null) : null)) == (($__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd = $context["module"]) && is_array($__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd) || $__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd instanceof ArrayAccess ? ($__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd["layout_id"] ?? null) : null))) {
                    echo " 
\t\t\t\t\t\t\t    \t     <option value=\"99999";
                    // line 134
                    echo (($__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6 = $context["store"]) && is_array($__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6) || $__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6 instanceof ArrayAccess ? ($__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6["store_id"] ?? null) : null);
                    echo "\" selected=\"selected\">All pages - Store ";
                    echo (($__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855 = $context["store"]) && is_array($__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855) || $__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855 instanceof ArrayAccess ? ($__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855["name"] ?? null) : null);
                    echo "</option>
\t\t\t\t\t\t\t    \t     ";
                } else {
                    // line 135
                    echo " 
\t\t\t\t\t\t\t    \t     <option value=\"99999";
                    // line 136
                    echo (($__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b = $context["store"]) && is_array($__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b) || $__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b instanceof ArrayAccess ? ($__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b["store_id"] ?? null) : null);
                    echo "\">All pages - Store ";
                    echo (($__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f = $context["store"]) && is_array($__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f) || $__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f instanceof ArrayAccess ? ($__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f["name"] ?? null) : null);
                    echo "</option>
\t\t\t\t\t\t\t    \t     ";
                }
                // line 137
                echo " 
\t\t\t\t\t\t\t    \t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['store'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 138
            echo " 
\t\t\t\t\t\t\t    \t
\t\t\t\t\t\t\t        ";
            // line 140
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["layouts"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["layout"]) {
                echo " 
\t\t\t\t\t\t\t        ";
                // line 141
                if (((($__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0 = $context["layout"]) && is_array($__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0) || $__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0 instanceof ArrayAccess ? ($__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0["layout_id"] ?? null) : null) == (($__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55 = $context["module"]) && is_array($__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55) || $__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55 instanceof ArrayAccess ? ($__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55["layout_id"] ?? null) : null))) {
                    echo " 
\t\t\t\t\t\t\t        <option value=\"";
                    // line 142
                    echo (($__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a = $context["layout"]) && is_array($__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a) || $__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a instanceof ArrayAccess ? ($__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a["layout_id"] ?? null) : null);
                    echo "\" selected=\"selected\">";
                    echo (($__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88 = $context["layout"]) && is_array($__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88) || $__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88 instanceof ArrayAccess ? ($__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88["name"] ?? null) : null);
                    echo "</option>
\t\t\t\t\t\t\t        ";
                } else {
                    // line 143
                    echo " 
\t\t\t\t\t\t\t        <option value=\"";
                    // line 144
                    echo (($__internal_be1db6a1ea9fa5c04c40f99df0ec5af053ca391863fc6256c5c4ee249724f758 = $context["layout"]) && is_array($__internal_be1db6a1ea9fa5c04c40f99df0ec5af053ca391863fc6256c5c4ee249724f758) || $__internal_be1db6a1ea9fa5c04c40f99df0ec5af053ca391863fc6256c5c4ee249724f758 instanceof ArrayAccess ? ($__internal_be1db6a1ea9fa5c04c40f99df0ec5af053ca391863fc6256c5c4ee249724f758["layout_id"] ?? null) : null);
                    echo "\">";
                    echo (($__internal_6e6eda1691934a8f5855a3221f5a9f036391304a5cda73a3a2009f2961a84c35 = $context["layout"]) && is_array($__internal_6e6eda1691934a8f5855a3221f5a9f036391304a5cda73a3a2009f2961a84c35) || $__internal_6e6eda1691934a8f5855a3221f5a9f036391304a5cda73a3a2009f2961a84c35 instanceof ArrayAccess ? ($__internal_6e6eda1691934a8f5855a3221f5a9f036391304a5cda73a3a2009f2961a84c35["name"] ?? null) : null);
                    echo "</option>
\t\t\t\t\t\t\t        ";
                }
                // line 145
                echo " 
\t\t\t\t\t\t\t        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['layout'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 146
            echo " 
\t\t\t\t\t\t\t      </select></td>
\t\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t\t    <td>Position:</td>
\t\t\t\t\t\t\t    <td><select name=\"custom_module_module[";
            // line 151
            echo ($context["module_row"] ?? null);
            echo "][position]\">
\t\t\t\t\t\t     \t";
            // line 152
            if (((($__internal_51c633083c79004f3cb5e9e2b2f3504f650f1561800582801028bcbcf733a06b = $context["module"]) && is_array($__internal_51c633083c79004f3cb5e9e2b2f3504f650f1561800582801028bcbcf733a06b) || $__internal_51c633083c79004f3cb5e9e2b2f3504f650f1561800582801028bcbcf733a06b instanceof ArrayAccess ? ($__internal_51c633083c79004f3cb5e9e2b2f3504f650f1561800582801028bcbcf733a06b["position"] ?? null) : null) == "header_notice")) {
                echo " 
\t\t\t\t\t\t     \t<option value=\"header_notice\" selected=\"selected\">Header notice</option>
\t\t\t\t\t\t     \t";
            } else {
                // line 154
                echo " 
\t\t\t\t\t\t     \t<option value=\"header_notice\">Header notice</option>
\t\t\t\t\t\t     \t";
            }
            // line 156
            echo " 
\t\t\t\t\t\t     \t";
            // line 157
            if (((($__internal_064553f1273f2ea50405f85092d06733f3f2fe5d0fc42fda135e1fdc91ff26ae = $context["module"]) && is_array($__internal_064553f1273f2ea50405f85092d06733f3f2fe5d0fc42fda135e1fdc91ff26ae) || $__internal_064553f1273f2ea50405f85092d06733f3f2fe5d0fc42fda135e1fdc91ff26ae instanceof ArrayAccess ? ($__internal_064553f1273f2ea50405f85092d06733f3f2fe5d0fc42fda135e1fdc91ff26ae["position"] ?? null) : null) == "top_block")) {
                echo " 
\t\t\t\t\t\t     \t<option value=\"top_block\" selected=\"selected\">Top block</option>
\t\t\t\t\t\t     \t";
            } else {
                // line 159
                echo " 
\t\t\t\t\t\t     \t<option value=\"top_block\">Top block</option>
\t\t\t\t\t\t     \t";
            }
            // line 161
            echo " 
\t\t\t\t\t\t\t    \t";
            // line 162
            if (((($__internal_7bef02f75e2984f8c7fcd4fd7871e286c87c0fdcb248271a136b01ac6dd5dd54 = $context["module"]) && is_array($__internal_7bef02f75e2984f8c7fcd4fd7871e286c87c0fdcb248271a136b01ac6dd5dd54) || $__internal_7bef02f75e2984f8c7fcd4fd7871e286c87c0fdcb248271a136b01ac6dd5dd54 instanceof ArrayAccess ? ($__internal_7bef02f75e2984f8c7fcd4fd7871e286c87c0fdcb248271a136b01ac6dd5dd54["position"] ?? null) : null) == "menu")) {
                echo " 
\t\t\t\t\t\t\t    \t<option value=\"menu\" selected=\"selected\">Menu</option>
\t\t\t\t\t\t\t    \t";
            } else {
                // line 164
                echo " 
\t\t\t\t\t\t\t    \t<option value=\"menu\">Menu</option>
\t\t\t\t\t\t\t    \t";
            }
            // line 166
            echo " 
\t\t\t\t\t\t\t    \t";
            // line 167
            if (((($__internal_d6ae6b41786cc4be7778386d06cb288c8e6ffd74e055cfed45d7a5c8854d0c8f = $context["module"]) && is_array($__internal_d6ae6b41786cc4be7778386d06cb288c8e6ffd74e055cfed45d7a5c8854d0c8f) || $__internal_d6ae6b41786cc4be7778386d06cb288c8e6ffd74e055cfed45d7a5c8854d0c8f instanceof ArrayAccess ? ($__internal_d6ae6b41786cc4be7778386d06cb288c8e6ffd74e055cfed45d7a5c8854d0c8f["position"] ?? null) : null) == "menu2")) {
                echo " 
\t\t\t\t\t\t\t    \t<option value=\"menu2\" selected=\"selected\">Menu2</option>
\t\t\t\t\t\t\t    \t";
            } else {
                // line 169
                echo " 
\t\t\t\t\t\t\t    \t<option value=\"menu2\">Menu2</option>
\t\t\t\t\t\t\t    \t";
            }
            // line 171
            echo " 
\t\t\t\t\t\t\t    \t";
            // line 172
            if (((($__internal_1dcdec7ec31e102fbfe45103ea3599c92c8609311e43d40ca0d95d0369434327 = $context["module"]) && is_array($__internal_1dcdec7ec31e102fbfe45103ea3599c92c8609311e43d40ca0d95d0369434327) || $__internal_1dcdec7ec31e102fbfe45103ea3599c92c8609311e43d40ca0d95d0369434327 instanceof ArrayAccess ? ($__internal_1dcdec7ec31e102fbfe45103ea3599c92c8609311e43d40ca0d95d0369434327["position"] ?? null) : null) == "slideshow")) {
                echo " 
\t\t\t\t\t\t\t    \t<option value=\"slideshow\" selected=\"selected\">Slideshow</option>
\t\t\t\t\t\t\t    \t";
            } else {
                // line 174
                echo " 
\t\t\t\t\t\t\t    \t<option value=\"slideshow\">Slideshow</option>
\t\t\t\t\t\t\t    \t";
            }
            // line 176
            echo " 
\t\t\t\t\t\t\t    \t";
            // line 177
            if (((($__internal_891ba2f942018e94e4bfa8069988f305bbaad7f54a64aeee069787f1084a9412 = $context["module"]) && is_array($__internal_891ba2f942018e94e4bfa8069988f305bbaad7f54a64aeee069787f1084a9412) || $__internal_891ba2f942018e94e4bfa8069988f305bbaad7f54a64aeee069787f1084a9412 instanceof ArrayAccess ? ($__internal_891ba2f942018e94e4bfa8069988f305bbaad7f54a64aeee069787f1084a9412["position"] ?? null) : null) == "preface_left")) {
                echo " 
\t\t\t\t\t\t\t    \t<option value=\"preface_left\" selected=\"selected\">Preface left</option>
\t\t\t\t\t\t\t    \t";
            } else {
                // line 179
                echo " 
\t\t\t\t\t\t\t    \t<option value=\"preface_left\">Preface left</option>
\t\t\t\t\t\t\t    \t";
            }
            // line 181
            echo " 
\t\t\t\t\t\t\t    \t";
            // line 182
            if (((($__internal_694b5f53081640f33aab1567e85e28c247e6a7c4674010716df6de8eae4819e9 = $context["module"]) && is_array($__internal_694b5f53081640f33aab1567e85e28c247e6a7c4674010716df6de8eae4819e9) || $__internal_694b5f53081640f33aab1567e85e28c247e6a7c4674010716df6de8eae4819e9 instanceof ArrayAccess ? ($__internal_694b5f53081640f33aab1567e85e28c247e6a7c4674010716df6de8eae4819e9["position"] ?? null) : null) == "preface_right")) {
                echo " 
\t\t\t\t\t\t\t    \t<option value=\"preface_right\" selected=\"selected\">Preface right</option>
\t\t\t\t\t\t\t    \t";
            } else {
                // line 184
                echo " 
\t\t\t\t\t\t\t    \t<option value=\"preface_right\">Preface right</option>
\t\t\t\t\t\t\t    \t";
            }
            // line 186
            echo " 
\t\t\t\t\t\t\t    \t";
            // line 187
            if (((($__internal_91b272a21580197773f482962c8b92637a641a749832ee390d7d386a58d1912e = $context["module"]) && is_array($__internal_91b272a21580197773f482962c8b92637a641a749832ee390d7d386a58d1912e) || $__internal_91b272a21580197773f482962c8b92637a641a749832ee390d7d386a58d1912e instanceof ArrayAccess ? ($__internal_91b272a21580197773f482962c8b92637a641a749832ee390d7d386a58d1912e["position"] ?? null) : null) == "preface_fullwidth")) {
                echo " 
\t\t\t\t\t\t\t    \t<option value=\"preface_fullwidth\" selected=\"selected\">Preface fullwidth</option>
\t\t\t\t\t\t\t    \t";
            } else {
                // line 189
                echo " 
\t\t\t\t\t\t\t    \t<option value=\"preface_fullwidth\">Preface fullwidth</option>
\t\t\t\t\t\t\t    \t";
            }
            // line 191
            echo " 
\t\t\t\t\t\t\t    \t";
            // line 192
            if (((($__internal_7f8d0071642f16d6b4720f8eef58ffd71faf0c4d7a772c0eb6842d5e9d901ca5 = $context["module"]) && is_array($__internal_7f8d0071642f16d6b4720f8eef58ffd71faf0c4d7a772c0eb6842d5e9d901ca5) || $__internal_7f8d0071642f16d6b4720f8eef58ffd71faf0c4d7a772c0eb6842d5e9d901ca5 instanceof ArrayAccess ? ($__internal_7f8d0071642f16d6b4720f8eef58ffd71faf0c4d7a772c0eb6842d5e9d901ca5["position"] ?? null) : null) == "column_left")) {
                echo " 
\t\t\t\t\t\t\t    \t<option value=\"column_left\" selected=\"selected\">Column left</option>
\t\t\t\t\t\t\t    \t";
            } else {
                // line 194
                echo " 
\t\t\t\t\t\t\t    \t<option value=\"column_left\">Column left</option>
\t\t\t\t\t\t\t    \t";
            }
            // line 196
            echo " 
\t\t\t\t\t\t\t    \t";
            // line 197
            if (((($__internal_0aa0713b35e28227396d65db75a1a4277b081ff4e08585143330919af9d1bf0a = $context["module"]) && is_array($__internal_0aa0713b35e28227396d65db75a1a4277b081ff4e08585143330919af9d1bf0a) || $__internal_0aa0713b35e28227396d65db75a1a4277b081ff4e08585143330919af9d1bf0a instanceof ArrayAccess ? ($__internal_0aa0713b35e28227396d65db75a1a4277b081ff4e08585143330919af9d1bf0a["position"] ?? null) : null) == "content_big_column")) {
                echo " 
\t\t\t\t\t\t\t    \t<option value=\"content_big_column\" selected=\"selected\">Content big column</option>
\t\t\t\t\t\t\t    \t";
            } else {
                // line 199
                echo " 
\t\t\t\t\t\t\t    \t<option value=\"content_big_column\">Content big column</option>
\t\t\t\t\t\t\t    \t";
            }
            // line 201
            echo " 
\t\t\t\t\t\t\t    \t";
            // line 202
            if (((($__internal_51b47659448148079c55eb5fc84ce5e7b27c8ff1fadeba243d0bf4a59f102eb4 = $context["module"]) && is_array($__internal_51b47659448148079c55eb5fc84ce5e7b27c8ff1fadeba243d0bf4a59f102eb4) || $__internal_51b47659448148079c55eb5fc84ce5e7b27c8ff1fadeba243d0bf4a59f102eb4 instanceof ArrayAccess ? ($__internal_51b47659448148079c55eb5fc84ce5e7b27c8ff1fadeba243d0bf4a59f102eb4["position"] ?? null) : null) == "content_top")) {
                echo " 
\t\t\t\t\t\t\t    \t<option value=\"content_top\" selected=\"selected\">Content top</option>
\t\t\t\t\t\t\t    \t";
            } else {
                // line 204
                echo " 
\t\t\t\t\t\t\t    \t<option value=\"content_top\">Content top</option>
\t\t\t\t\t\t\t    \t";
            }
            // line 206
            echo " 
\t\t\t\t\t\t\t    \t";
            // line 207
            if (((($__internal_7954abe9e82b868b32e99deec50bc82d0cf006d569340d1981c528f484e4306d = $context["module"]) && is_array($__internal_7954abe9e82b868b32e99deec50bc82d0cf006d569340d1981c528f484e4306d) || $__internal_7954abe9e82b868b32e99deec50bc82d0cf006d569340d1981c528f484e4306d instanceof ArrayAccess ? ($__internal_7954abe9e82b868b32e99deec50bc82d0cf006d569340d1981c528f484e4306d["position"] ?? null) : null) == "column_right")) {
                echo " 
\t\t\t\t\t\t\t    \t<option value=\"column_right\" selected=\"selected\">Column right</option>
\t\t\t\t\t\t\t    \t";
            } else {
                // line 209
                echo " 
\t\t\t\t\t\t\t    \t<option value=\"column_right\">Column right</option>
\t\t\t\t\t\t\t    \t";
            }
            // line 211
            echo " 
\t\t\t\t\t\t\t    \t";
            // line 212
            if (((($__internal_edc3933374aa0ae65dd90505a315fe17c24a986a5b064b0f4774e7dc68df29b5 = $context["module"]) && is_array($__internal_edc3933374aa0ae65dd90505a315fe17c24a986a5b064b0f4774e7dc68df29b5) || $__internal_edc3933374aa0ae65dd90505a315fe17c24a986a5b064b0f4774e7dc68df29b5 instanceof ArrayAccess ? ($__internal_edc3933374aa0ae65dd90505a315fe17c24a986a5b064b0f4774e7dc68df29b5["position"] ?? null) : null) == "content_bottom")) {
                echo " 
\t\t\t\t\t\t\t    \t<option value=\"content_bottom\" selected=\"selected\">Content bottom</option>
\t\t\t\t\t\t\t    \t";
            } else {
                // line 214
                echo " 
\t\t\t\t\t\t\t    \t<option value=\"content_bottom\">Content bottom</option>
\t\t\t\t\t\t\t    \t";
            }
            // line 216
            echo " 
\t\t\t\t\t\t\t    \t";
            // line 217
            if (((($__internal_78a78e2af552daad30f9bd5ea90c17811faa9f63aaaf1d1d527de70902fe2a7a = $context["module"]) && is_array($__internal_78a78e2af552daad30f9bd5ea90c17811faa9f63aaaf1d1d527de70902fe2a7a) || $__internal_78a78e2af552daad30f9bd5ea90c17811faa9f63aaaf1d1d527de70902fe2a7a instanceof ArrayAccess ? ($__internal_78a78e2af552daad30f9bd5ea90c17811faa9f63aaaf1d1d527de70902fe2a7a["position"] ?? null) : null) == "customfooter_top")) {
                echo " 
\t\t\t\t\t\t\t    \t<option value=\"customfooter_top\" selected=\"selected\">CustomFooter Top</option>
\t\t\t\t\t\t\t    \t";
            } else {
                // line 219
                echo " 
\t\t\t\t\t\t\t    \t<option value=\"customfooter_top\">CustomFooter Top</option>
\t\t\t\t\t\t\t    \t";
            }
            // line 221
            echo " 
\t\t\t\t\t\t\t    \t";
            // line 222
            if (((($__internal_68329f830f66b3d66aa25264abe6d152d460842b92be66836c0d8febb9fe46da = $context["module"]) && is_array($__internal_68329f830f66b3d66aa25264abe6d152d460842b92be66836c0d8febb9fe46da) || $__internal_68329f830f66b3d66aa25264abe6d152d460842b92be66836c0d8febb9fe46da instanceof ArrayAccess ? ($__internal_68329f830f66b3d66aa25264abe6d152d460842b92be66836c0d8febb9fe46da["position"] ?? null) : null) == "customfooter_bottom")) {
                echo " 
\t\t\t\t\t\t\t    \t<option value=\"customfooter_bottom\" selected=\"selected\">CustomFooter Bottom</option>
\t\t\t\t\t\t\t    \t";
            } else {
                // line 224
                echo " 
\t\t\t\t\t\t\t    \t<option value=\"customfooter_bottom\">CustomFooter Bottom</option>
\t\t\t\t\t\t\t    \t";
            }
            // line 226
            echo " 
\t\t\t\t\t\t\t    \t";
            // line 227
            if (((($__internal_0c0a6bc8299d1416ae3339265b194ff43aaec7fc209979ab91c947804ef09b38 = $context["module"]) && is_array($__internal_0c0a6bc8299d1416ae3339265b194ff43aaec7fc209979ab91c947804ef09b38) || $__internal_0c0a6bc8299d1416ae3339265b194ff43aaec7fc209979ab91c947804ef09b38 instanceof ArrayAccess ? ($__internal_0c0a6bc8299d1416ae3339265b194ff43aaec7fc209979ab91c947804ef09b38["position"] ?? null) : null) == "footer_top")) {
                echo " 
\t\t\t\t\t\t\t    \t<option value=\"footer_top\" selected=\"selected\">Footer top</option>
\t\t\t\t\t\t\t    \t";
            } else {
                // line 229
                echo " 
\t\t\t\t\t\t\t    \t<option value=\"footer_top\">Footer top</option>
\t\t\t\t\t\t\t    \t";
            }
            // line 231
            echo " 
\t\t\t\t\t\t\t    \t";
            // line 232
            if (((($__internal_c5373d6c112ec7cfa0d260a8ea49b75af689c74c186cb9e1d12f91be2f3451ec = $context["module"]) && is_array($__internal_c5373d6c112ec7cfa0d260a8ea49b75af689c74c186cb9e1d12f91be2f3451ec) || $__internal_c5373d6c112ec7cfa0d260a8ea49b75af689c74c186cb9e1d12f91be2f3451ec instanceof ArrayAccess ? ($__internal_c5373d6c112ec7cfa0d260a8ea49b75af689c74c186cb9e1d12f91be2f3451ec["position"] ?? null) : null) == "footer_left")) {
                echo " 
\t\t\t\t\t\t\t    \t<option value=\"footer_left\" selected=\"selected\">Footer left</option>
\t\t\t\t\t\t\t    \t";
            } else {
                // line 234
                echo " 
\t\t\t\t\t\t\t    \t<option value=\"footer_left\">Footer left</option>
\t\t\t\t\t\t\t    \t";
            }
            // line 236
            echo " 
\t\t\t\t\t\t\t    \t";
            // line 237
            if (((($__internal_a13b5858c5824edc0cf555cffe22c4f90468c22ef1115c74916647af2c9b8574 = $context["module"]) && is_array($__internal_a13b5858c5824edc0cf555cffe22c4f90468c22ef1115c74916647af2c9b8574) || $__internal_a13b5858c5824edc0cf555cffe22c4f90468c22ef1115c74916647af2c9b8574 instanceof ArrayAccess ? ($__internal_a13b5858c5824edc0cf555cffe22c4f90468c22ef1115c74916647af2c9b8574["position"] ?? null) : null) == "footer_right")) {
                echo " 
\t\t\t\t\t\t\t    \t<option value=\"footer_right\" selected=\"selected\">Footer right</option>
\t\t\t\t\t\t\t    \t";
            } else {
                // line 239
                echo " 
\t\t\t\t\t\t\t    \t<option value=\"footer_right\">Footer right</option>
\t\t\t\t\t\t\t    \t";
            }
            // line 241
            echo " 
\t\t\t\t\t\t\t    \t";
            // line 242
            if (((($__internal_8273200462706e912633c1bd12ca5fc5736d038390c29954112cb78d56c3075c = $context["module"]) && is_array($__internal_8273200462706e912633c1bd12ca5fc5736d038390c29954112cb78d56c3075c) || $__internal_8273200462706e912633c1bd12ca5fc5736d038390c29954112cb78d56c3075c instanceof ArrayAccess ? ($__internal_8273200462706e912633c1bd12ca5fc5736d038390c29954112cb78d56c3075c["position"] ?? null) : null) == "footer_bottom")) {
                echo " 
\t\t\t\t\t\t\t    \t<option value=\"footer_bottom\" selected=\"selected\">Footer bottom</option>
\t\t\t\t\t\t\t    \t";
            } else {
                // line 244
                echo " 
\t\t\t\t\t\t\t    \t<option value=\"footer_bottom\">Footer bottom</option>
\t\t\t\t\t\t\t    \t";
            }
            // line 246
            echo " 
\t\t\t\t\t\t\t    \t";
            // line 247
            if (((($__internal_ba7685baed7d294d6f9f021c094359707afc43c727e6a2d19ff1d176796bbda0 = $context["module"]) && is_array($__internal_ba7685baed7d294d6f9f021c094359707afc43c727e6a2d19ff1d176796bbda0) || $__internal_ba7685baed7d294d6f9f021c094359707afc43c727e6a2d19ff1d176796bbda0 instanceof ArrayAccess ? ($__internal_ba7685baed7d294d6f9f021c094359707afc43c727e6a2d19ff1d176796bbda0["position"] ?? null) : null) == "bottom")) {
                echo " 
\t\t\t\t\t\t\t    \t<option value=\"bottom\" selected=\"selected\">Bottom</option>
\t\t\t\t\t\t\t    \t";
            } else {
                // line 249
                echo " 
\t\t\t\t\t\t\t    \t<option value=\"bottom\">Bottom</option>
\t\t\t\t\t\t\t    \t";
            }
            // line 251
            echo " 
\t\t\t\t\t\t\t      </select></td>
\t\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t\t    <td>Status:</td>
\t\t\t\t\t\t\t    <td><select name=\"custom_module_module[";
            // line 256
            echo ($context["module_row"] ?? null);
            echo "][status]\">
\t\t\t\t\t\t\t        ";
            // line 257
            if ((($__internal_101f955954d09941874d68c1bc31b2171b1313930c7c7163a30d4c0951b92adc = $context["module"]) && is_array($__internal_101f955954d09941874d68c1bc31b2171b1313930c7c7163a30d4c0951b92adc) || $__internal_101f955954d09941874d68c1bc31b2171b1313930c7c7163a30d4c0951b92adc instanceof ArrayAccess ? ($__internal_101f955954d09941874d68c1bc31b2171b1313930c7c7163a30d4c0951b92adc["status"] ?? null) : null)) {
                echo " 
\t\t\t\t\t\t\t        <option value=\"1\" selected=\"selected\">Enabled</option>
\t\t\t\t\t\t\t        <option value=\"0\">Disabled</option>
\t\t\t\t\t\t\t        ";
            } else {
                // line 260
                echo " 
\t\t\t\t\t\t\t        <option value=\"1\">Enabled</option>
\t\t\t\t\t\t\t        <option value=\"0\" selected=\"selected\">Disabled</option>
\t\t\t\t\t\t\t        ";
            }
            // line 263
            echo " 
\t\t\t\t\t\t\t      </select></td>
\t\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t\t    <td>Sort Order:</td>
\t\t\t\t\t\t\t    <td><input type=\"text\" name=\"custom_module_module[";
            // line 268
            echo ($context["module_row"] ?? null);
            echo "][sort_order]\" value=\"";
            echo (($__internal_d19b8970b34a70cf90f25bc70d063a8b0fc361c2ffc373a6176195b465bc0ccd = $context["module"]) && is_array($__internal_d19b8970b34a70cf90f25bc70d063a8b0fc361c2ffc373a6176195b465bc0ccd) || $__internal_d19b8970b34a70cf90f25bc70d063a8b0fc361c2ffc373a6176195b465bc0ccd instanceof ArrayAccess ? ($__internal_d19b8970b34a70cf90f25bc70d063a8b0fc361c2ffc373a6176195b465bc0ccd["sort_order"] ?? null) : null);
            echo "\" size=\"3\" /></td>
\t\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
            // line 272
            $context["module_row"] = (($context["module_row"] ?? null) + 1);
            echo " 
\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 273
        echo " 
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t<!-- Buttons -->
\t\t\t\t\t<div class=\"buttons\"><input type=\"submit\" name=\"button-save\" class=\"button-save\" value=\"\"></div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</form>
</div>
<script type=\"text/javascript\"><!--
\$('.main-tabs a').tabs();
//--></script> 

<script type=\"text/javascript\"><!--
";
        // line 288
        $context["module_row"] = 1;
        echo " 
";
        // line 289
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["modules"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
            echo " 
\$('#language-";
            // line 290
            echo ($context["module_row"] ?? null);
            echo " a').tabs();
";
            // line 291
            $context["module_row"] = (($context["module_row"] ?? null) + 1);
            echo " 
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 292
        echo " 
//--></script> 

<script type=\"text/javascript\"><!--
";
        // line 296
        $context["module_row"] = 1;
        echo " 
";
        // line 297
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["modules"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
            echo " 
";
            // line 298
            $context["module_row"] = (($context["module_row"] ?? null) + 1);
            echo " 
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 299
        echo " 
//--></script> 

<script type=\"text/javascript\">
\$(document).ready(function() {
\t
\t\$('#custom_module').on('change', 'select.select-type', function () {
\t\tvar id_module = \$(this).attr(\"id\");
\t\t\$(\"#\" + id_module +\" option:selected\").each(function() {
\t\t\tif(\$(this).val() == 1) {
\t\t\t\t\$(\".html\" + id_module + \"\").hide();
\t\t\t\t\$(\".block\" + id_module + \"\").show();
\t\t\t} else {
\t\t\t\t\$(\".html\" + id_module + \"\").show();
\t\t\t\t\$(\".block\" + id_module + \"\").hide();
\t\t\t}
\t\t});
\t});
\t
});
</script>

<script type=\"text/javascript\"><!--
var module_row = ";
        // line 322
        echo ($context["module_row"] ?? null);
        echo ";

function addModule() {\t
\thtml  = '<div id=\"tab-module-' + module_row + '\" class=\"tab-content\">';

\t\thtml += '\t<table class=\"form\" style=\"margin-bottom:10px\">';
\t\thtml += '\t\t<tr>';
\t\thtml += '\t\t\t<td style=\"border:none;padding-top:7px\">Type:</td>';
\t\thtml += '\t\t\t<td style=\"border:none;padding-top:7px\">';
\t\thtml += '\t\t\t\t<select name=\"custom_module_module[' + module_row + '][type]\" class=\"select-type\" id=\"' + module_row + '\">';
\t\thtml += '\t\t\t\t\t<option value=\"1\" selected=\"selected\">Block</option>';
\t\thtml += '\t\t\t\t\t<option value=\"2\">HTML</option>';
\t\thtml += '\t\t\t\t</select>';
\t\thtml += '\t\t\t</td>';
\t\thtml += '\t\t</tr>';
\t\thtml += '   </table>';
\t\t
\t\thtml += '  <div id=\"language-' + module_row + '\" class=\"htabs\">';
\t    ";
        // line 340
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["languages"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            echo " 
\t    html += '    <a href=\"#tab-language-'+ module_row + '-";
            // line 341
            echo (($__internal_7f22f462d0a079e9b28d4dd0209cce239cda0d0c81b8f79d4d6355c3a5eedc81 = $context["language"]) && is_array($__internal_7f22f462d0a079e9b28d4dd0209cce239cda0d0c81b8f79d4d6355c3a5eedc81) || $__internal_7f22f462d0a079e9b28d4dd0209cce239cda0d0c81b8f79d4d6355c3a5eedc81 instanceof ArrayAccess ? ($__internal_7f22f462d0a079e9b28d4dd0209cce239cda0d0c81b8f79d4d6355c3a5eedc81["language_id"] ?? null) : null);
            echo "\"><img src=\"language/";
            echo (($__internal_08d357d6c6cc179c7eaa6aa16ae7c13336efbc0aa5669c58d46cab7f2ce96007 = $context["language"]) && is_array($__internal_08d357d6c6cc179c7eaa6aa16ae7c13336efbc0aa5669c58d46cab7f2ce96007) || $__internal_08d357d6c6cc179c7eaa6aa16ae7c13336efbc0aa5669c58d46cab7f2ce96007 instanceof ArrayAccess ? ($__internal_08d357d6c6cc179c7eaa6aa16ae7c13336efbc0aa5669c58d46cab7f2ce96007["code"] ?? null) : null);
            echo "/";
            echo (($__internal_6d2de8847ca935d43c4b17225dc2537ff47d9b1c0e614e4fed558aa26b7f934d = $context["language"]) && is_array($__internal_6d2de8847ca935d43c4b17225dc2537ff47d9b1c0e614e4fed558aa26b7f934d) || $__internal_6d2de8847ca935d43c4b17225dc2537ff47d9b1c0e614e4fed558aa26b7f934d instanceof ArrayAccess ? ($__internal_6d2de8847ca935d43c4b17225dc2537ff47d9b1c0e614e4fed558aa26b7f934d["code"] ?? null) : null);
            echo ".png\" title=\"";
            echo (($__internal_14ec589d07a85756e12acaaf8d41cc27621a5a03ce9e1c2835143b81f89a8dba = $context["language"]) && is_array($__internal_14ec589d07a85756e12acaaf8d41cc27621a5a03ce9e1c2835143b81f89a8dba) || $__internal_14ec589d07a85756e12acaaf8d41cc27621a5a03ce9e1c2835143b81f89a8dba instanceof ArrayAccess ? ($__internal_14ec589d07a85756e12acaaf8d41cc27621a5a03ce9e1c2835143b81f89a8dba["name"] ?? null) : null);
            echo "\" /> ";
            echo (($__internal_15cadc33e29273b0be5cf970bdbb25fb0d962f226cb329dfeb89075c4a503f49 = $context["language"]) && is_array($__internal_15cadc33e29273b0be5cf970bdbb25fb0d962f226cb329dfeb89075c4a503f49) || $__internal_15cadc33e29273b0be5cf970bdbb25fb0d962f226cb329dfeb89075c4a503f49 instanceof ArrayAccess ? ($__internal_15cadc33e29273b0be5cf970bdbb25fb0d962f226cb329dfeb89075c4a503f49["name"] ?? null) : null);
            echo "</a>';
\t    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 342
        echo " 
\t\thtml += '  </div>';
\t
\t\t";
        // line 345
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["languages"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            echo " 
\t\thtml += '    <div id=\"tab-language-'+ module_row + '-";
            // line 346
            echo (($__internal_fdffc6d7d2105180aa5315b58ff859ceee4ece5e5b7b2601a851c7a60a10d639 = $context["language"]) && is_array($__internal_fdffc6d7d2105180aa5315b58ff859ceee4ece5e5b7b2601a851c7a60a10d639) || $__internal_fdffc6d7d2105180aa5315b58ff859ceee4ece5e5b7b2601a851c7a60a10d639 instanceof ArrayAccess ? ($__internal_fdffc6d7d2105180aa5315b58ff859ceee4ece5e5b7b2601a851c7a60a10d639["language_id"] ?? null) : null);
            echo "\">';
\t\t
\t\thtml += '\t <div class=\"block' + module_row + '\">';
\t\thtml += '\t   <table class=\"form\">';
\t\thtml += '\t\t\t<tr>';
\t\thtml += '\t\t\t\t<td>Block heading:</td>';
\t\thtml += '\t\t\t\t<td><input type=\"text\" value=\"\" name=\"custom_module_module[' + module_row + '][block_heading][";
            // line 352
            echo (($__internal_d3425701b9a0a8a13b32495933a7425cc5de4c0e5eb576b5e6202e761600efaf = $context["language"]) && is_array($__internal_d3425701b9a0a8a13b32495933a7425cc5de4c0e5eb576b5e6202e761600efaf) || $__internal_d3425701b9a0a8a13b32495933a7425cc5de4c0e5eb576b5e6202e761600efaf instanceof ArrayAccess ? ($__internal_d3425701b9a0a8a13b32495933a7425cc5de4c0e5eb576b5e6202e761600efaf["language_id"] ?? null) : null);
            echo "]\" style=\"width:250px\"></td>';
\t\thtml += '\t\t\t</tr>';
\t\thtml += '\t\t\t<tr>';
\t\thtml += '\t\t\t\t<td>Block content:</td>';
\t\thtml += '\t\t\t\t<td><textarea name=\"custom_module_module[' + module_row + '][block_content][";
            // line 356
            echo (($__internal_aee130853742ef3e066bee9d5b201f026709112632574a72409cce5c24f44921 = $context["language"]) && is_array($__internal_aee130853742ef3e066bee9d5b201f026709112632574a72409cce5c24f44921) || $__internal_aee130853742ef3e066bee9d5b201f026709112632574a72409cce5c24f44921 instanceof ArrayAccess ? ($__internal_aee130853742ef3e066bee9d5b201f026709112632574a72409cce5c24f44921["language_id"] ?? null) : null);
            echo "]\" id=\"block-content-' + module_row + '-";
            echo (($__internal_34bfc9d3bb99a6e1ea80e9e1e16e70ac03c16465a14de0faf0a7d7df04205a7a = $context["language"]) && is_array($__internal_34bfc9d3bb99a6e1ea80e9e1e16e70ac03c16465a14de0faf0a7d7df04205a7a) || $__internal_34bfc9d3bb99a6e1ea80e9e1e16e70ac03c16465a14de0faf0a7d7df04205a7a instanceof ArrayAccess ? ($__internal_34bfc9d3bb99a6e1ea80e9e1e16e70ac03c16465a14de0faf0a7d7df04205a7a["language_id"] ?? null) : null);
            echo "\"></textarea></td>';
\t\thtml += '\t\t\t</tr>';
\t\thtml += '\t   </table>';
\t\thtml += '\t </div>';
\t\t
\t\thtml += '\t <div class=\"html' + module_row + '\" style=\"display:none\">';
\t\thtml += '      <table class=\"form\">';
\t\thtml += '        <tr>';
\t\thtml += '          <td>HTML:</td>';
\t\thtml += '          <td><textarea name=\"custom_module_module[' + module_row + '][html][";
            // line 365
            echo (($__internal_975fa751a8f688c78873ea77782d85542baaefa8277fb1ae2e9b2a7d8eed4ca4 = $context["language"]) && is_array($__internal_975fa751a8f688c78873ea77782d85542baaefa8277fb1ae2e9b2a7d8eed4ca4) || $__internal_975fa751a8f688c78873ea77782d85542baaefa8277fb1ae2e9b2a7d8eed4ca4 instanceof ArrayAccess ? ($__internal_975fa751a8f688c78873ea77782d85542baaefa8277fb1ae2e9b2a7d8eed4ca4["language_id"] ?? null) : null);
            echo "]\" class=\"html\"></textarea></td>';
\t\thtml += '        </tr>';
\t\thtml += '      </table>';
\t\thtml += '\t  </div>';
\t\t
\t\thtml += '    </div>';
\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 371
        echo " 
\t
\t\thtml += '  <table class=\"form\">';
\t\thtml += '    <tr>';
\t\thtml += '      <td>Layout:</td>';
\t\thtml += '      <td><select name=\"custom_module_module[' + module_row + '][layout_id]\">';
\t\thtml += '           <option value=\"99999\">All pages</option>';
\t\t
\t\t          ";
        // line 379
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["stores"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["store"]) {
            echo " 
\t\thtml += '           \t\t<option value=\"99999";
            // line 380
            echo (($__internal_3a29dd8c635325e3d124a8a60682c8c1d72c8f81204e952bf98350c9fabbc985 = $context["store"]) && is_array($__internal_3a29dd8c635325e3d124a8a60682c8c1d72c8f81204e952bf98350c9fabbc985) || $__internal_3a29dd8c635325e3d124a8a60682c8c1d72c8f81204e952bf98350c9fabbc985 instanceof ArrayAccess ? ($__internal_3a29dd8c635325e3d124a8a60682c8c1d72c8f81204e952bf98350c9fabbc985["store_id"] ?? null) : null);
            echo "\">All pages - Store ";
            echo (($__internal_245fa8e4b1f2520e359eeb249916824c4d6f6fcce189eedb4956fb1747c4eb51 = $context["store"]) && is_array($__internal_245fa8e4b1f2520e359eeb249916824c4d6f6fcce189eedb4956fb1747c4eb51) || $__internal_245fa8e4b1f2520e359eeb249916824c4d6f6fcce189eedb4956fb1747c4eb51 instanceof ArrayAccess ? ($__internal_245fa8e4b1f2520e359eeb249916824c4d6f6fcce189eedb4956fb1747c4eb51["name"] ?? null) : null);
            echo "</option>';
\t\t          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['store'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 381
        echo " 
\t\t          
\t\t";
        // line 383
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["layouts"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["layout"]) {
            echo " 
\t\thtml += '           <option value=\"";
            // line 384
            echo (($__internal_9e80f0131faf7c30fa2ce2a767187df174f9da8bcbd50f87a692ce0bfaa1635a = $context["layout"]) && is_array($__internal_9e80f0131faf7c30fa2ce2a767187df174f9da8bcbd50f87a692ce0bfaa1635a) || $__internal_9e80f0131faf7c30fa2ce2a767187df174f9da8bcbd50f87a692ce0bfaa1635a instanceof ArrayAccess ? ($__internal_9e80f0131faf7c30fa2ce2a767187df174f9da8bcbd50f87a692ce0bfaa1635a["layout_id"] ?? null) : null);
            echo "\">";
            echo twig_escape_filter($this->env, (($__internal_451826e8bdee9d18aea0e33bdc5ff98645a3591151f15890bdcbf323f991d762 = $context["layout"]) && is_array($__internal_451826e8bdee9d18aea0e33bdc5ff98645a3591151f15890bdcbf323f991d762) || $__internal_451826e8bdee9d18aea0e33bdc5ff98645a3591151f15890bdcbf323f991d762 instanceof ArrayAccess ? ($__internal_451826e8bdee9d18aea0e33bdc5ff98645a3591151f15890bdcbf323f991d762["name"] ?? null) : null), "html");
            echo "</option>';
\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['layout'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 385
        echo " 
\t\thtml += '      </select></td>';
\t\thtml += '    </tr>';
\t\thtml += '    <tr>';
\t\thtml += '      <td>Position:</td>';
\t\thtml += '      <td><select name=\"custom_module_module[' + module_row + '][position]\">';
\t\thtml += '       \t\t<option value=\"header_notice\">Header notice</option>';
\t\thtml += '       \t\t<option value=\"top_block\">Top block</option>';
\t\thtml += '       \t\t<option value=\"menu\">Menu</option>';
\t\thtml += '\t\t\t\t<option value=\"slideshow\">Slideshow</option>';
\t\thtml += '\t\t\t\t<option value=\"preface_left\">Preface left</option>';
\t\thtml += '\t\t\t\t<option value=\"preface_right\">Preface right</option>';
\t\thtml += '\t\t\t\t<option value=\"preface_fullwidth\">Preface fullwidth</option>';
\t\thtml += '\t\t\t\t<option value=\"column_left\">Column left</option>';
\t\thtml += '\t\t\t\t<option value=\"content_big_column\">Content big column</option>';
\t\thtml += '\t\t\t\t<option value=\"content_top\">Content top</option>';
\t\thtml += '\t\t\t\t<option value=\"column_right\">Column right</option>';
\t\thtml += '\t\t\t\t<option value=\"content_bottom\">Content bottom</option>';
\t\thtml += '\t\t\t\t<option value=\"customfooter_top\">CustomFooter Top</option>';
\t\thtml += '\t\t\t\t<option value=\"customfooter_bottom\">CustomFooter Bottom</option>';
\t\thtml += '\t\t\t\t<option value=\"footer_top\">Footer top</option>';
\t\thtml += '\t\t\t\t<option value=\"footer_left\">Footer left</option>';
\t\thtml += '\t\t\t\t<option value=\"footer_right\">Footer right</option>';
\t\thtml += '\t\t\t\t<option value=\"footer_bottom\">Footer bottom</option>';
\t\thtml += '\t\t\t\t<option value=\"bottom\">Bottom</option>';
\t\thtml += '      </select></td>';
\t\thtml += '    </tr>';
\t\thtml += '    <tr>';
\t\thtml += '      <td>Status:</td>';
\t\thtml += '      <td><select name=\"custom_module_module[' + module_row + '][status]\">';
\t\thtml += '        <option value=\"1\">Enabled</option>';
\t\thtml += '        <option value=\"0\">Disabled</option>';
\t\thtml += '      </select></td>';
\t\thtml += '    </tr>';
\t\thtml += '    <tr>';
\t\thtml += '      <td>Sort Order:</td>';
\t\thtml += '      <td><input type=\"text\" name=\"custom_module_module[' + module_row + '][sort_order]\" value=\"\" size=\"3\" /></td>';
\t\thtml += '    </tr>';
\t\thtml += '  </table>'; 
\thtml += '</div>';
\t
\t\$('.tabs').append(html);
\t
\t\$('#language-' + module_row + ' a').tabs();

\t\$('#module-add').before('<a href=\"#tab-module-' + module_row + '\" id=\"module-' + module_row + '\">Module ' + module_row + ' &nbsp;<img src=\"view/image/module_template/delete-slider.png\" alt=\"\" onclick=\"\$(\\'.vtabs a:first\\').trigger(\\'click\\'); \$(\\'#module-' + module_row + '\\').remove(); \$(\\'#tab-module-' + module_row + '\\').remove(); return false;\" /></a>');
\t
\t\$('.main-tabs a').tabs();
\t
\t\$('#module-' + module_row).trigger('click');
\t
\t";
        // line 436
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["languages"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            echo " 
\t\t\$('#html-' + module_row + '-";
            // line 437
            echo (($__internal_1d091d83c8b124c871d72f3d4f6fd41a4ee9660a12b13118ed628d413c8f7053 = $context["language"]) && is_array($__internal_1d091d83c8b124c871d72f3d4f6fd41a4ee9660a12b13118ed628d413c8f7053) || $__internal_1d091d83c8b124c871d72f3d4f6fd41a4ee9660a12b13118ed628d413c8f7053 instanceof ArrayAccess ? ($__internal_1d091d83c8b124c871d72f3d4f6fd41a4ee9660a12b13118ed628d413c8f7053["language_id"] ?? null) : null);
            echo ", #block-content-";
            echo ($context["module_row"] ?? null);
            echo "-";
            echo (($__internal_65ca6abbb047147adc36adc2b2eee31db7dcbf18e71e872be20ddfaa1118c70c = $context["language"]) && is_array($__internal_65ca6abbb047147adc36adc2b2eee31db7dcbf18e71e872be20ddfaa1118c70c) || $__internal_65ca6abbb047147adc36adc2b2eee31db7dcbf18e71e872be20ddfaa1118c70c instanceof ArrayAccess ? ($__internal_65ca6abbb047147adc36adc2b2eee31db7dcbf18e71e872be20ddfaa1118c70c["language_id"] ?? null) : null);
            echo "').summernote({
\t\t\theight: 300
\t\t});
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 440
        echo " 
\t
\tmodule_row++;
}
//--></script> 
";
        // line 445
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "extension/module/custom_module.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1085 => 445,  1078 => 440,  1064 => 437,  1058 => 436,  1005 => 385,  995 => 384,  989 => 383,  985 => 381,  975 => 380,  969 => 379,  959 => 371,  946 => 365,  932 => 356,  925 => 352,  916 => 346,  910 => 345,  905 => 342,  889 => 341,  883 => 340,  862 => 322,  837 => 299,  829 => 298,  823 => 297,  819 => 296,  813 => 292,  805 => 291,  801 => 290,  795 => 289,  791 => 288,  774 => 273,  766 => 272,  757 => 268,  750 => 263,  744 => 260,  737 => 257,  733 => 256,  726 => 251,  721 => 249,  715 => 247,  712 => 246,  707 => 244,  701 => 242,  698 => 241,  693 => 239,  687 => 237,  684 => 236,  679 => 234,  673 => 232,  670 => 231,  665 => 229,  659 => 227,  656 => 226,  651 => 224,  645 => 222,  642 => 221,  637 => 219,  631 => 217,  628 => 216,  623 => 214,  617 => 212,  614 => 211,  609 => 209,  603 => 207,  600 => 206,  595 => 204,  589 => 202,  586 => 201,  581 => 199,  575 => 197,  572 => 196,  567 => 194,  561 => 192,  558 => 191,  553 => 189,  547 => 187,  544 => 186,  539 => 184,  533 => 182,  530 => 181,  525 => 179,  519 => 177,  516 => 176,  511 => 174,  505 => 172,  502 => 171,  497 => 169,  491 => 167,  488 => 166,  483 => 164,  477 => 162,  474 => 161,  469 => 159,  463 => 157,  460 => 156,  455 => 154,  449 => 152,  445 => 151,  438 => 146,  431 => 145,  424 => 144,  421 => 143,  414 => 142,  410 => 141,  404 => 140,  400 => 138,  393 => 137,  386 => 136,  383 => 135,  376 => 134,  372 => 133,  366 => 132,  362 => 130,  357 => 128,  351 => 126,  347 => 125,  341 => 121,  325 => 116,  312 => 112,  297 => 108,  286 => 104,  273 => 100,  267 => 99,  261 => 98,  257 => 96,  239 => 95,  233 => 94,  229 => 93,  223 => 89,  218 => 87,  212 => 85,  209 => 84,  204 => 82,  198 => 80,  192 => 79,  185 => 75,  179 => 74,  175 => 73,  169 => 69,  161 => 68,  149 => 67,  143 => 66,  139 => 65,  129 => 58,  125 => 56,  118 => 53,  114 => 52,  108 => 49,  104 => 48,  62 => 8,  52 => 7,  46 => 6,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "extension/module/custom_module.twig", "");
    }
}

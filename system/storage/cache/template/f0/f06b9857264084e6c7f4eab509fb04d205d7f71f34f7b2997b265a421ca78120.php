<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* wokiee/template/product/category.twig */
class __TwigTemplate_1fd7d61926d16e539ee5ce99b8bac61decaf6db69ae9f3c6c75537f93c6b2504 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo " 
";
        // line 2
        $context["theme_options"] = twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "get", [0 => "theme_options"], "method", false, false, false, 2);
        // line 3
        $context["config"] = twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "get", [0 => "config"], "method", false, false, false, 3);
        echo " 
";
        // line 4
        $context["categoryPage"] = twig_constant("true");
        // line 5
        $this->loadTemplate("wokiee/template/new_elements/wrapper_top.twig", "wokiee/template/product/category.twig", 5)->display($context);
        // line 6
        echo "
<div id=\"mfilter-content-container\">
  ";
        // line 8
        if ((($context["thumb"] ?? null) || ($context["description"] ?? null))) {
            echo " 
  <div class=\"category-info clearfix\">
    ";
            // line 10
            if (($context["thumb"] ?? null)) {
                echo " 
      <div class=\"image\"><img src=\"";
                // line 11
                echo ($context["thumb"] ?? null);
                echo "\" alt=\"";
                echo ($context["heading_title"] ?? null);
                echo "\" /></div>
    ";
            }
            // line 12
            echo " 
    ";
            // line 13
            if (($context["description"] ?? null)) {
                echo " 
      ";
                // line 14
                echo ($context["description"] ?? null);
                echo " 
    ";
            }
            // line 15
            echo " 
  </div>
  ";
        }
        // line 17
        echo " 
  ";
        // line 18
        if ((($context["categories"] ?? null) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "refine_search_style"], "method", false, false, false, 18) != "2"))) {
            echo " 
  <div class=\"refine_search_overflow text-center\"><h4 class=\"refine_search\">";
            // line 19
            echo ($context["text_refine"] ?? null);
            echo "</h4></div>
  <div class=\"category-list";
            // line 20
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "refine_search_style"], "method", false, false, false, 20) == "1")) {
                echo " ";
                echo " category-list-text-only";
                echo " ";
            }
            echo "\">
    <div class=\"row\">
      
   ";
            // line 23
            $context["class"] = 3;
            echo "    
   ";
            // line 24
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "refine_search_number"], "method", false, false, false, 24) == 2)) {
                echo " ";
                $context["class"] = 62;
                echo " ";
            }
            // line 25
            echo "   ";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "refine_search_number"], "method", false, false, false, 25) == 5)) {
                echo " ";
                $context["class"] = 25;
                echo " ";
            }
            // line 26
            echo "   ";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "refine_search_number"], "method", false, false, false, 26) == 3)) {
                echo " ";
                $context["class"] = 4;
                echo " ";
            }
            // line 27
            echo "   ";
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "refine_search_number"], "method", false, false, false, 27) == 6)) {
                echo " ";
                $context["class"] = 2;
                echo " ";
            }
            // line 28
            echo "      
    ";
            // line 29
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "refineSearch", [], "method", false, false, false, 29));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 30
                echo "      ";
                if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "refine_search_style"], "method", false, false, false, 30) != "1")) {
                    // line 31
                    echo "        ";
                    $context["width"] = 250;
                    // line 32
                    echo "        ";
                    $context["height"] = 250;
                    // line 33
                    echo "        ";
                    if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "refine_image_width"], "method", false, false, false, 33) > 20)) {
                        echo " ";
                        $context["width"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "refine_image_width"], "method", false, false, false, 33);
                    }
                    // line 34
                    echo "        ";
                    if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "refine_image_height"], "method", false, false, false, 34) > 20)) {
                        echo " ";
                        $context["height"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "refine_image_height"], "method", false, false, false, 34);
                    }
                    // line 35
                    echo "        ";
                    $context["model_tool_image"] = twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "get", [0 => "model_tool_image"], "method", false, false, false, 35);
                    // line 36
                    echo "        ";
                    if (((($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = $context["category"]) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4["thumb"] ?? null) : null) != "")) {
                        echo " 
          ";
                        // line 37
                        $context["image"] = twig_get_attribute($this->env, $this->source, ($context["model_tool_image"] ?? null), "resize", [0 => (($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = $context["category"]) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144["thumb"] ?? null) : null), 1 => ($context["width"] ?? null), 2 => ($context["height"] ?? null)], "method", false, false, false, 37);
                        echo " 
        ";
                    } else {
                        // line 38
                        echo " 
          ";
                        // line 39
                        $context["image"] = twig_get_attribute($this->env, $this->source, ($context["model_tool_image"] ?? null), "resize", [0 => "no_image.jpg", 1 => ($context["width"] ?? null), 2 => ($context["height"] ?? null)], "method", false, false, false, 39);
                        echo " 
        ";
                    }
                    // line 40
                    echo " 
      ";
                }
                // line 42
                echo "    
          <div class=\"col-6 col-md-";
                // line 43
                echo ($context["class"] ?? null);
                echo "\">
          ";
                // line 44
                if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "refine_search_style"], "method", false, false, false, 44) != "1")) {
                    echo " 
            <a href=\"";
                    // line 45
                    echo (($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = $context["category"]) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b["href"] ?? null) : null);
                    echo "\"><img src=\"";
                    echo ($context["image"] ?? null);
                    echo "\" alt=\"";
                    echo (($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 = $context["category"]) && is_array($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002) || $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 instanceof ArrayAccess ? ($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002["name"] ?? null) : null);
                    echo "\" /></a>
          ";
                }
                // line 46
                echo " 
          <a href=\"";
                // line 47
                echo (($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 = $context["category"]) && is_array($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4) || $__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 instanceof ArrayAccess ? ($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4["href"] ?? null) : null);
                echo "\">";
                echo (($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 = $context["category"]) && is_array($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666) || $__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 instanceof ArrayAccess ? ($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666["name"] ?? null) : null);
                echo "</a>
        </div>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 49
            echo " 
  </div>
  </div>
  ";
        }
        // line 52
        echo " 
  ";
        // line 53
        if (($context["products"] ?? null)) {
            echo " 
    ";
            // line 54
            $context["currently"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getCurrently", [], "method", false, false, false, 54);
            // line 55
            echo "    <div class=\"tt-filters-options ";
            if ((((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_grid_type"], "method", false, false, false, 55) == "1") || (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_grid_type"], "method", false, false, false, 55) == "2")) || (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_grid_type"], "method", false, false, false, 55) == "3"))) {
                echo "desctop-no-sidebar";
            }
            echo "\">
      <h1 class=\"tt-title\">
        ";
            // line 57
            echo ($context["heading_title"] ?? null);
            echo "
      </h1>
      <div class=\"tt-btn-toggle\">
        <a href=\"#\">";
            // line 60
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "filter_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 60)], "method", false, false, false, 60) != "")) {
                echo " ";
                echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "filter_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 60)], "method", false, false, false, 60);
                echo " ";
            } else {
                echo "FILTER";
            }
            echo "</a>
      </div>
      <div class=\"tt-sort d-none d-md-block\">
        <select onchange=\"location = this.value;\">
          ";
            // line 64
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["sorts"]);
            foreach ($context['_seq'] as $context["_key"] => $context["sorts"]) {
                echo " 
          ";
                // line 65
                if (((($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e = $context["sorts"]) && is_array($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e) || $__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e instanceof ArrayAccess ? ($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e["value"] ?? null) : null) == ((($context["sort"] ?? null) . "-") . ($context["order"] ?? null)))) {
                    echo " 
          <option value=\"";
                    // line 66
                    echo (($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 = $context["sorts"]) && is_array($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52) || $__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 instanceof ArrayAccess ? ($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52["href"] ?? null) : null);
                    echo "\" selected=\"selected\">";
                    echo (($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 = $context["sorts"]) && is_array($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136) || $__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 instanceof ArrayAccess ? ($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136["text"] ?? null) : null);
                    echo "</option>
          ";
                } else {
                    // line 67
                    echo " 
          <option value=\"";
                    // line 68
                    echo (($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 = $context["sorts"]) && is_array($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386) || $__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 instanceof ArrayAccess ? ($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386["href"] ?? null) : null);
                    echo "\">";
                    echo (($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9 = $context["sorts"]) && is_array($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9) || $__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9 instanceof ArrayAccess ? ($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9["text"] ?? null) : null);
                    echo "</option>
          ";
                }
                // line 69
                echo " 
          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sorts'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 70
            echo " 
        </select>
        <select onchange=\"location = this.value;\">
          ";
            // line 73
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["limits"]);
            foreach ($context['_seq'] as $context["_key"] => $context["limits"]) {
                echo " 
          ";
                // line 74
                if (((($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae = $context["limits"]) && is_array($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae) || $__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae instanceof ArrayAccess ? ($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae["value"] ?? null) : null) == ($context["limit"] ?? null))) {
                    echo " 
          <option value=\"";
                    // line 75
                    echo (($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f = $context["limits"]) && is_array($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f) || $__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f instanceof ArrayAccess ? ($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f["href"] ?? null) : null);
                    echo "\" selected=\"selected\">";
                    echo (($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40 = $context["limits"]) && is_array($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40) || $__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40 instanceof ArrayAccess ? ($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40["text"] ?? null) : null);
                    echo "</option>
          ";
                } else {
                    // line 76
                    echo " 
          <option value=\"";
                    // line 77
                    echo (($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f = $context["limits"]) && is_array($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f) || $__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f instanceof ArrayAccess ? ($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f["href"] ?? null) : null);
                    echo "\">";
                    echo (($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760 = $context["limits"]) && is_array($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760) || $__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760 instanceof ArrayAccess ? ($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760["text"] ?? null) : null);
                    echo "</option>
          ";
                }
                // line 78
                echo " 
          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['limits'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 79
            echo " 
        </select>
      </div>
      ";
            // line 82
            if ( !(twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_grid_type"], "method", false, false, false, 82) == "3")) {
                // line 83
                echo "        <div class=\"tt-quantity\">
          <a href=\"#\" class=\"tt-col-one d-block d-md-none ";
                // line 84
                if ((($context["currently"] ?? null) == "verylarge")) {
                    echo "active";
                }
                echo "\" data-value=\"tt-col-one\"></a>
          <a href=\"#\" class=\"tt-col-two ";
                // line 85
                if (((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_grid_type"], "method", false, false, false, 85) == "1") || (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_grid_type"], "method", false, false, false, 85) == "2"))) {
                    echo "d-block d-md-none";
                }
                echo " ";
                if ((($context["currently"] ?? null) == "large")) {
                    echo "active";
                }
                echo "\" data-value=\"tt-col-two\"></a>
          <a href=\"#\" class=\"tt-col-three d-none d-md-block ";
                // line 86
                if (((((($context["currently"] ?? null) != "large") && (($context["currently"] ?? null) != "small")) && (($context["currently"] ?? null) != "verysmall")) && (($context["currently"] ?? null) != "verylarge"))) {
                    echo "active";
                }
                echo "\" data-value=\"tt-col-three\"></a>
          <a href=\"#\" class=\"tt-col-four d-none d-md-block ";
                // line 87
                if ((($context["currently"] ?? null) == "small")) {
                    echo "active";
                }
                echo "\" data-value=\"tt-col-four\"></a>
          ";
                // line 88
                if (((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_grid_type"], "method", false, false, false, 88) == "1") || (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_grid_type"], "method", false, false, false, 88) == "2"))) {
                    // line 89
                    echo "            <a href=\"#\" class=\"tt-col-six d-none d-md-block ";
                    if ((($context["currently"] ?? null) == "verysmall")) {
                        echo "active";
                    }
                    echo "\" data-value=\"tt-col-six\"></a>
          ";
                }
                // line 91
                echo "        </div>
      ";
            }
            // line 93
            echo "    </div>

    ";
            // line 95
            if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_grid_type"], "method", false, false, false, 95) == "3")) {
                // line 96
                echo "      <div class=\"tt-product-listing-masonry\">
        <div class=\"tt-product-init tt-add-item\">
          ";
                // line 98
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["products"] ?? null));
                $context['loop'] = [
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                ];
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                    echo " 
            <div class=\"element-item ";
                    // line 99
                    if (((twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 99) == 3) || (twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 99) == 6))) {
                        echo "double-size";
                    }
                    echo "\">
                ";
                    // line 100
                    $this->loadTemplate("wokiee/template/new_elements/product2.twig", "wokiee/template/product/category.twig", 100)->display($context);
                    // line 101
                    echo "            </div>
          ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 102
                echo " 
        </div>
      </div>
    ";
            } else {
                // line 106
                echo "      ";
                $context["class"] = "4";
                // line 107
                echo "      ";
                if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_grid_type"], "method", false, false, false, 107) == "1")) {
                    // line 108
                    echo "        ";
                    $context["class"] = "3";
                    // line 109
                    echo "      ";
                }
                // line 110
                echo "      ";
                if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_grid_type"], "method", false, false, false, 110) == "2")) {
                    // line 111
                    echo "        ";
                    $context["class"] = "2";
                    // line 112
                    echo "      ";
                }
                // line 113
                echo "      ";
                if ((($context["currently"] ?? null) == "large")) {
                    $context["class"] = "6";
                }
                // line 114
                echo "      ";
                if ((($context["currently"] ?? null) == "small")) {
                    $context["class"] = "3";
                }
                // line 115
                echo "      ";
                if ((($context["currently"] ?? null) == "medium")) {
                    $context["class"] = "4";
                }
                // line 116
                echo "      ";
                if ((($context["currently"] ?? null) == "verylarge")) {
                    $context["class"] = "12";
                }
                // line 117
                echo "      ";
                if ((($context["currently"] ?? null) == "verysmall")) {
                    $context["class"] = "2";
                }
                // line 118
                echo "      <div class=\"tt-product-listing row\">
        ";
                // line 119
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["products"] ?? null));
                $context['loop'] = [
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                ];
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                    echo " 
          <div class=\"col-";
                    // line 120
                    if ((($context["class"] ?? null) == "12")) {
                        echo "12";
                    } else {
                        echo "6";
                    }
                    echo " ";
                    if ((($context["class"] ?? null) == "3")) {
                        echo "col-md-4 col-lg-";
                        echo ($context["class"] ?? null);
                    } elseif ((($context["class"] ?? null) == "2")) {
                        echo "col-md-4 col-lg-3 col-xl-";
                        echo ($context["class"] ?? null);
                    } else {
                        echo "col-md-";
                        echo ($context["class"] ?? null);
                    }
                    echo " tt-col-item\">
              ";
                    // line 121
                    $this->loadTemplate("wokiee/template/new_elements/product.twig", "wokiee/template/product/category.twig", 121)->display($context);
                    // line 122
                    echo "          </div>
        ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 123
                echo " 
      </div>
    ";
            }
            // line 126
            echo "    
    <div class=\"row pagination-results\">
      <div class=\"col-sm-6 text-left\">";
            // line 128
            echo ($context["pagination"] ?? null);
            echo "</div>
      <div class=\"col-sm-6 text-right\">";
            // line 129
            echo ($context["results"] ?? null);
            echo "</div>
    </div>
  ";
        }
        // line 131
        echo " 
  ";
        // line 132
        if (( !($context["categories"] ?? null) &&  !($context["products"] ?? null))) {
            echo " 
    <p style=\"padding-bottom: 15px\">";
            // line 133
            echo ($context["text_empty"] ?? null);
            echo "</p>
    <div class=\"buttons\">
      <div class=\"pull-right\"><a href=\"";
            // line 135
            echo ($context["continue"] ?? null);
            echo "\" class=\"btn btn-primary\">";
            echo ($context["button_continue"] ?? null);
            echo "</a></div>
    </div>
  ";
        }
        // line 137
        echo " 
<script type=\"text/javascript\"><!--
function setCookie(key, value) {
    var expires = new Date();
    expires.setTime(expires.getTime() + (1 * 24 * 60 * 60 * 1000));
    document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
}
\$(document).ready(function () {
  \$(\"body\").on(\"click\", \".tt-quantity a\", function () {
    if(\$(this).attr(\"data-value\") == 'tt-col-two') {
      setCookie('display','large');
    } else if(\$(this).attr(\"data-value\") == 'tt-col-four') {
      setCookie('display','small');
    } else if(\$(this).attr(\"data-value\") == 'tt-col-six') {
      setCookie('display','verysmall');
    } else if(\$(this).attr(\"data-value\") == 'tt-col-one') {
      setCookie('display','verylarge');
    } else {
      setCookie('display','medium');
    }
  });
});
//--></script> 
</div>

";
        // line 162
        $this->loadTemplate("wokiee/template/new_elements/wrapper_bottom.twig", "wokiee/template/product/category.twig", 162)->display($context);
        // line 163
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "wokiee/template/product/category.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  624 => 163,  622 => 162,  595 => 137,  587 => 135,  582 => 133,  578 => 132,  575 => 131,  569 => 129,  565 => 128,  561 => 126,  556 => 123,  541 => 122,  539 => 121,  520 => 120,  501 => 119,  498 => 118,  493 => 117,  488 => 116,  483 => 115,  478 => 114,  473 => 113,  470 => 112,  467 => 111,  464 => 110,  461 => 109,  458 => 108,  455 => 107,  452 => 106,  446 => 102,  431 => 101,  429 => 100,  423 => 99,  404 => 98,  400 => 96,  398 => 95,  394 => 93,  390 => 91,  382 => 89,  380 => 88,  374 => 87,  368 => 86,  358 => 85,  352 => 84,  349 => 83,  347 => 82,  342 => 79,  335 => 78,  328 => 77,  325 => 76,  318 => 75,  314 => 74,  308 => 73,  303 => 70,  296 => 69,  289 => 68,  286 => 67,  279 => 66,  275 => 65,  269 => 64,  256 => 60,  250 => 57,  242 => 55,  240 => 54,  236 => 53,  233 => 52,  227 => 49,  216 => 47,  213 => 46,  204 => 45,  200 => 44,  196 => 43,  193 => 42,  189 => 40,  184 => 39,  181 => 38,  176 => 37,  171 => 36,  168 => 35,  162 => 34,  156 => 33,  153 => 32,  150 => 31,  147 => 30,  143 => 29,  140 => 28,  133 => 27,  126 => 26,  119 => 25,  113 => 24,  109 => 23,  99 => 20,  95 => 19,  91 => 18,  88 => 17,  83 => 15,  78 => 14,  74 => 13,  71 => 12,  64 => 11,  60 => 10,  55 => 8,  51 => 6,  49 => 5,  47 => 4,  43 => 3,  41 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "wokiee/template/product/category.twig", "");
    }
}

<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* extension/payment/mp_transparente.twig */
class __TwigTemplate_d74a0f972e4960e24848bd0ed45704f6487ad110a009cfaf3b1af0a2e5a57957 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo ($context["column_left"] ?? null);
        echo "
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"pull-right\">
        <a id=\"btn_save\" class=\"btn btn-primary\">";
        // line 6
        echo ($context["button_save"] ?? null);
        echo "<i class=\"fa fa-save\"></i></a>
        <a onclick=\"location = '";
        // line 7
        echo ($context["cancel"] ?? null);
        echo "';\" class=\"btn btn-default\">";
        echo ($context["button_cancel"] ?? null);
        echo "<i class=\"fa fa-reply\"></i></a>
      </div>
      <h1>";
        // line 9
        echo ($context["heading_title"] ?? null);
        echo "</h1>
      <ul class=\"breadcrumb\">
        ";
        // line 11
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 12
            echo "          <li><a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 12);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 12);
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 14
        echo "      </ul>
    </div>
  </div>
  <div class=\"container-fluid\">
    ";
        // line 18
        if (($context["error_warning"] ?? null)) {
            // line 19
            echo "      <div class=\"alert alert-danger\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo ($context["error_warning"] ?? null);
            echo "
        <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
      </div>
    ";
        }
        // line 23
        echo "    <div class=\"panel panel-default\">
      <div class=\"panel-heading\">
        <h3 class=\"panel-title\"><i class=\"fa fa-pencil\"></i>Edit MercadoPago</h3>
      </div>
      <div class=\"panel-body\">
        <form action=\"";
        // line 28
        echo ($context["action"] ?? null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form_mp\" class=\"form-horizontal\">
         <div class=\"form-group\">
          <label class=\"col-sm-2 control-label\" for=\"input-status\">";
        // line 30
        echo ($context["entry_status"] ?? null);
        echo "</label>
          <div class=\"col-sm-10\">
            <select name=\"payment_mp_transparente_status\" id=\"input-status\" class=\"form-control\">
              ";
        // line 33
        if (($context["payment_mp_transparente_status"] ?? null)) {
            // line 34
            echo "                <option value=\"1\" selected=\"selected\">";
            echo ($context["text_enabled"] ?? null);
            echo "</option>
                <option value=\"0\">";
            // line 35
            echo ($context["text_disabled"] ?? null);
            echo "</option>
               ";
        } else {
            // line 37
            echo "                <option value=\"1\">";
            echo ($context["text_enabled"] ?? null);
            echo "</option>
                <option value=\"0\" selected=\"selected\">";
            // line 38
            echo ($context["text_disabled"] ?? null);
            echo "</option>
              ";
        }
        // line 40
        echo "            </select>
          </div>
        </div>
        <div class=\"form-group required\">
          <label class=\"col-sm-2 control-label\" for=\"payment_mp_transparente_order_status_id\">";
        // line 44
        echo ($context["entry_country"] ?? null);
        echo "</label>
          <div class=\"col-sm-10\">
            <input type=\"hidden\" value=\"";
        // line 46
        if (($context["payment_mp_transparente_country"] ?? null)) {
            echo ($context["payment_mp_transparente_country"] ?? null);
        }
        echo "\" id=\"countryName\" />
            <select class=\"form-control\" name=\"payment_mp_transparente_country\" id=\"country\">
             <option id=\"MLA\" value=\"MLA\">Argentina</option>
             <option id=\"MLB\" value=\"MLB\">Brasil</option>
             <option id=\"MLC\" value=\"MLC\">Chile</option>
             <option id=\"MCO\" value=\"MCO\">Colombia</option>
             <option id=\"MLM\" value=\"MLM\">Mexico</option>
             <option id=\"MPE\" value=\"MPE\">Peru</option>
             <option id=\"MLU\" value=\"MLU\">Uruguay</option>
             <option id=\"MLV\" value=\"MLV\">Venezuela</option>
           </select>
         </div>
       </div>
       <div class=\"form-group\"  id=\"div_sponsor\">
         <label class=\"col-sm-2 control-label\" for=\"payment_mp_transparente_sponsor\">
          <span data-toggle=\"tooltip\" data-trigger=\"click\" title='Sponsor ID'>";
        // line 61
        echo ($context["entry_sponsor"] ?? null);
        echo "</span></label>
          <div class=\"col-sm-10\">
           <input type=\"text\" class=\"form-control\" id=\"payment_mp_transparente_sponsor\" name=\"payment_mp_transparente_sponsor\" value=\"";
        // line 63
        echo ($context["payment_mp_transparente_sponsor"] ?? null);
        echo "\" disabled=\"disabled\" placeholder=\"Em Breve - En Breve - Coming soon\"/>
           ";
        // line 64
        if ((isset($context["error_sponsor_spann"]) || array_key_exists("error_sponsor_spann", $context))) {
            // line 65
            echo "             <div class=\"text-danger\">";
            echo ($context["error_sponsor_spann"] ?? null);
            echo "</div>
           ";
        }
        // line 67
        echo "         </div>
      </div>
       <div class=\"form-group required\" id=\"div_public_key\">
         <label class=\"col-sm-2 control-label\" for=\"payment_mp_transparente_public_key\">
          <span data-toggle=\"tooltip\" data-trigger=\"click\" title='";
        // line 71
        echo ($context["entry_public_key_tooltip"] ?? null);
        echo " '>";
        echo ($context["entry_public_key"] ?? null);
        echo "</span></label>
          <div class=\"col-sm-10\">
           <input type=\"text\" class=\"form-control\" id=\"payment_mp_transparente_public_key\" name=\"payment_mp_transparente_public_key\" value=\"";
        // line 73
        echo ($context["payment_mp_transparente_public_key"] ?? null);
        echo "\" />
           ";
        // line 74
        if ((isset($context["error_public_key_span"]) || array_key_exists("error_public_key_span", $context))) {
            // line 75
            echo "             <div class=\"text-danger\">";
            echo ($context["error_public_key"] ?? null);
            echo "</div>
           ";
        }
        // line 77
        echo "         </div>
       </div>
       <div class=\"form-group required\" id=\"div_access_token\">
         <label class=\"col-sm-2 control-label\" for=\"payment_mp_transparente_access_token\">
          <span data-toggle=\"tooltip\" data-trigger=\"click\" title='";
        // line 81
        echo ($context["entry_access_token_tooltip"] ?? null);
        echo " '>";
        echo ($context["entry_access_token"] ?? null);
        echo "</span></label>
          <div class=\"col-sm-10\">
           <input type=\"text\" class=\"form-control\" id=\"payment_mp_transparente_access_token\" name=\"payment_mp_transparente_access_token\" value=\"";
        // line 83
        echo ($context["payment_mp_transparente_access_token"] ?? null);
        echo "\" />
           ";
        // line 84
        if ((isset($context["error_access_token_span"]) || array_key_exists("error_access_token_span", $context))) {
            // line 85
            echo "             <div class=\"text-danger\">";
            echo ($context["error_access_token"] ?? null);
            echo "</div>
           ";
        }
        // line 87
        echo "         </div>
       </div>

       <div class=\"form-group required\">
        <label class=\"col-sm-2 control-label\" for=\"payment_mp_transparente_category_id\"> <span data-toggle=\"tooltip\" data-trigger=\"click\" title='";
        // line 91
        echo ($context["entry_category_tooltip"] ?? null);
        echo "'>
          ";
        // line 92
        echo ($context["entry_category"] ?? null);
        echo "</label>
          <div class=\"col-sm-10\">
            <select class=\"form-control\" name=\"payment_mp_transparente_category_id\" id=\"payment_mp_transparente_category_id\">
              ";
        // line 95
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["category_list"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 96
            echo "                ";
            if (twig_in_filter(twig_get_attribute($this->env, $this->source, $context["category"], "id", [], "any", false, false, false, 96), ($context["payment_mp_transparente_category_id"] ?? null))) {
                // line 97
                echo "                  <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["category"], "id", [], "any", false, false, false, 97);
                echo "\" selected=\"selected\" id=\"";
                echo twig_get_attribute($this->env, $this->source, $context["category"], "description", [], "any", false, false, false, 97);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["category"], "description", [], "any", false, false, false, 97);
                echo "</option>
                 ";
            } else {
                // line 99
                echo "                  <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["category"], "id", [], "any", false, false, false, 99);
                echo "\" id=\"";
                echo twig_get_attribute($this->env, $this->source, $context["category"], "description", [], "any", false, false, false, 99);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["category"], "description", [], "any", false, false, false, 99);
                echo "</option>
                ";
            }
            // line 101
            echo "              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 102
        echo "            </select>
          </div>
        </div>
        <div class=\"form-group required\">
          <label class=\"col-sm-2 control-label\" for=\"payment_mp_transparente_debug\"><span data-toggle=\"tooltip\" data-trigger=\"click\" title='";
        // line 106
        echo ($context["entry_debug_tooltip"] ?? null);
        echo " '> ";
        echo ($context["entry_debug"] ?? null);
        echo "</span></label>
          <div class=\"col-sm-10\">
            <select class=\"form-control\" name=\"payment_mp_transparente_debug\" id=\"payment_mp_transparente_debug\">
              ";
        // line 109
        if (($context["payment_mp_transparente_debug"] ?? null)) {
            // line 110
            echo "                <option value=\"1\" selected=\"selected\">";
            echo ($context["text_enabled"] ?? null);
            echo "</option>
                <option value=\"0\">";
            // line 111
            echo ($context["text_disabled"] ?? null);
            echo "</option>
              ";
        } else {
            // line 113
            echo "                <option value=\"1\">";
            echo ($context["text_enabled"] ?? null);
            echo "</option>
                <option value=\"0\" selected=\"selected\">";
            // line 114
            echo ($context["text_disabled"] ?? null);
            echo "</option>
              ";
        }
        // line 116
        echo "            </select>
          </div>
        </div>

        <div class=\"form-group\">
          <label class=\"col-sm-2 control-label\" for=\"payment_mp_transparente_coupon\"><span data-toggle=\"tooltip\" data-trigger=\"click\" title='";
        // line 121
        echo ($context["entry_coupon_tooltip"] ?? null);
        echo "'>
            ";
        // line 122
        echo ($context["entry_coupon"] ?? null);
        echo "</span></label>
            <div class=\"col-sm-10\">
              <select class=\"form-control\" name=\"payment_mp_transparente_coupon\" id=\"payment_mp_transparente_coupon\">
                ";
        // line 125
        if (($context["payment_mp_transparente_coupon"] ?? null)) {
            // line 126
            echo "                  <option value=\"1\" selected=\"selected\">";
            echo ($context["text_enabled"] ?? null);
            echo "</option>
                  <option value=\"0\">";
            // line 127
            echo ($context["text_disabled"] ?? null);
            echo "</option>
                ";
        } else {
            // line 129
            echo "                  <option value=\"1\">";
            echo ($context["text_enabled"] ?? null);
            echo "</option>
                  <option value=\"0\" selected=\"selected\">";
            // line 130
            echo ($context["text_disabled"] ?? null);
            echo "</option>
                ";
        }
        // line 132
        echo "              </select>
            </div>
          </div>
          <div class=\"form-group required\">
            <label class=\"col-sm-2 control-label\" for=\"payment_mp_transparente_installments\">";
        // line 136
        echo ($context["entry_installments"] ?? null);
        echo "</label>
            <div class=\"col-sm-10\">
             <select class=\"form-control\" name=\"payment_mp_transparente_installments\" id=\"payment_mp_transparente_installments\">
              ";
        // line 139
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["installments"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["installment"]) {
            // line 140
            echo "                ";
            if (twig_in_filter(twig_get_attribute($this->env, $this->source, $context["installment"], "id", [], "any", false, false, false, 140), ($context["payment_mp_transparente_installments"] ?? null))) {
                // line 141
                echo "                  <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["installment"], "id", [], "any", false, false, false, 141);
                echo "\" selected=\"selected\" id=\"";
                echo twig_get_attribute($this->env, $this->source, $context["installment"], "id", [], "any", false, false, false, 141);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["installment"], "value", [], "any", false, false, false, 141);
                echo "</option>
                ";
            } else {
                // line 143
                echo "                  <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["installment"], "id", [], "any", false, false, false, 143);
                echo "\" id=\"";
                echo twig_get_attribute($this->env, $this->source, $context["installment"], "id", [], "any", false, false, false, 143);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["installment"], "value", [], "any", false, false, false, 143);
                echo "</option>
                ";
            }
            // line 145
            echo "              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['installment'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 146
        echo "            </select>
          </div>
        </div>

          ";
        // line 151
        echo "          ";
        if ((($context["methods"] ?? null) > 0)) {
            // line 152
            echo "            <div class=\"form-group\">
              <label class=\"col-sm-2\"></label>
              <div class=\"col-sm-10\">
                ";
            // line 155
            echo ($context["entry_payments_not_accept_tooltip"] ?? null);
            echo "
              </div>
              <label class=\"col-sm-2 control-label\" for=\"payment_mp_transparente_methods\">";
            // line 157
            echo ($context["entry_payments_not_accept"] ?? null);
            echo "</label>
              <div class=\"col-sm-10\" id=\"div_payments\">
                <div style=\"margin-top:8px;\">
                  <input type=\"hidden\" class=\"form-control\" id=\"payment_nro_count_payment_methods\" name=\"payment_nro_count_payment_methods\" value=\"";
            // line 160
            echo ($context["count_payment_methods"] ?? null);
            echo "\" />
                  ";
            // line 161
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["methods"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["method"]) {
                // line 162
                echo "                    <div style=\"";
                echo ($context["payment_style"] ?? null);
                echo "\" id=\"";
                echo twig_get_attribute($this->env, $this->source, $context["method"], "name", [], "any", false, false, false, 162);
                echo "\">
                      ";
                // line 163
                if (twig_in_filter(twig_get_attribute($this->env, $this->source, $context["method"], "id", [], "any", false, false, false, 163), ($context["payment_mp_transparente_methods"] ?? null))) {
                    // line 164
                    echo "                        <img src=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["method"], "secure_thumbnail", [], "any", false, false, false, 164);
                    echo "\" height=\"24\"><br/>
                        <input name=\"payment_mp_transparente_methods[]\" type=\"checkbox\" checked=\"yes\" value=\"";
                    // line 165
                    echo twig_get_attribute($this->env, $this->source, $context["method"], "id", [], "any", false, false, false, 165);
                    echo "\" style=\"margin-top:25%; margin-top:4px; border:1px solid #888; width:16px; height:16px;\">
                      ";
                } else {
                    // line 167
                    echo "                        <img src=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["method"], "secure_thumbnail", [], "any", false, false, false, 167);
                    echo "\" height=\"24\"><br/>
                        <input name=\"payment_mp_transparente_methods[]\" type=\"checkbox\" value=\"";
                    // line 168
                    echo twig_get_attribute($this->env, $this->source, $context["method"], "id", [], "any", false, false, false, 168);
                    echo "\" style=\"margin-left:25%; margin-top:4px; border:1px solid #888; width:16px; height:16px;\">
                      ";
                }
                // line 170
                echo "                    </div>
                  ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['method'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 172
            echo "                </div>
              </div>
              <label class=\"col-sm-2\"></label>
              <div class=\"col-sm-10\">
                ";
            // line 176
            if (($context["error_has_no_payments"] ?? null)) {
                // line 177
                echo "                  <div class=\"text-danger\">";
                echo ($context["error_entry_no_payments"] ?? null);
                echo "</div>
                ";
            }
            // line 179
            echo "              </div>
            </div>
          ";
        }
        // line 182
        echo "


      <div class=\"form-group\">
        <label class=\"col-sm-3 control-label\" for=\"payment_mp_transparente_order_status_id_completed\">";
        // line 186
        echo ($context["entry_order_status_general"] ?? null);
        echo "</label>
      </div>
      <div class=\"form-group required\">
        <label class=\"col-sm-2 control-label\" for=\"payment_mp_transparente_order_status_id\"><span data-toggle=\"tooltip\" data-trigger=\"click\" title='";
        // line 189
        echo ($context["entry_order_status_tooltip"] ?? null);
        echo " '>";
        echo ($context["entry_order_status"] ?? null);
        echo " </span></label>
        <div class=\"col-sm-10\">
         <select class=\"form-control\" name=\"payment_mp_transparente_order_status_id\" id=\"payment_mp_transparente_order_status_id\">
          ";
        // line 192
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["order_statuses"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["order_status"]) {
            // line 193
            echo "            ";
            if (twig_in_filter(twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 193), ($context["payment_mp_transparente_order_status_id"] ?? null))) {
                // line 194
                echo "              <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 194);
                echo "\" selected=\"selected\">";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "name", [], "any", false, false, false, 194);
                echo "</option>
            ";
            } else {
                // line 196
                echo "              <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 196);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "name", [], "any", false, false, false, 196);
                echo "</option>
            ";
            }
            // line 198
            echo "          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['order_status'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 199
        echo "        </select>
      </div>
    </div>
    <div class=\"form-group\">
      <label class=\"col-sm-2 control-label\" for=\"payment_mp_transparente_order_status_id_completed\"><span data-toggle=\"tooltip\" data-trigger=\"click\" title='";
        // line 203
        echo ($context["entry_order_status_completed_tooltip"] ?? null);
        echo " '>";
        echo ($context["entry_order_status_completed"] ?? null);
        echo "</span></label>
      <div class=\"col-sm-10\">
        <select class=\"form-control\" name=\"payment_mp_transparente_order_status_id_completed\" id=\"payment_mp_transparente_order_status_id_completed\">
         ";
        // line 206
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["order_statuses"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["order_status"]) {
            // line 207
            echo "          ";
            if (twig_in_filter(twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 207), ($context["payment_mp_transparente_order_status_id_completed"] ?? null))) {
                // line 208
                echo "            <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 208);
                echo "\" selected=\"selected\">";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "name", [], "any", false, false, false, 208);
                echo "</option>
          ";
            } else {
                // line 210
                echo "            <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 210);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "name", [], "any", false, false, false, 210);
                echo "</option>
          ";
            }
            // line 212
            echo "        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['order_status'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 213
        echo "      </select>
    </div>
  </div>
  <div class=\"form-group\">
    <label class=\"col-sm-2 control-label\" for=\"payment_mp_transparente_order_status_id_pending\"><span data-toggle=\"tooltip\" data-trigger=\"click\" title='";
        // line 217
        echo ($context["entry_order_status_pending_tooltip"] ?? null);
        echo " '>";
        echo ($context["entry_order_status_pending"] ?? null);
        echo "</span></label>
    <div class=\"col-sm-10\">
      <select class=\"form-control\" name=\"payment_mp_transparente_order_status_id_pending\" id=\"payment_mp_transparente_order_status_id_pending\">
       ";
        // line 220
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["order_statuses"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["order_status"]) {
            // line 221
            echo "        ";
            if (twig_in_filter(twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 221), ($context["payment_mp_transparente_order_status_id_pending"] ?? null))) {
                // line 222
                echo "          <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 222);
                echo "\" selected=\"selected\">";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "name", [], "any", false, false, false, 222);
                echo "</option>
        ";
            } else {
                // line 224
                echo "          <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 224);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "name", [], "any", false, false, false, 224);
                echo "</option>
        ";
            }
            // line 226
            echo "      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['order_status'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 227
        echo "    </select>
  </div>
</div>
<div class=\"form-group\">
  <label class=\"col-sm-2 control-label\" for=\"payment_mp_transparente_order_status_id_canceled\">
    <span data-toggle=\"tooltip\" data-trigger=\"click\" title='";
        // line 232
        echo ($context["entry_order_status_canceled_tooltip"] ?? null);
        echo " '>";
        echo ($context["entry_order_status_canceled"] ?? null);
        echo "</span></label>
    <div class=\"col-sm-10\">
      <select class=\"form-control\" name=\"payment_mp_transparente_order_status_id_canceled\" id=\"payment_mp_transparente_order_status_id_canceled\">
       ";
        // line 235
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["order_statuses"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["order_status"]) {
            // line 236
            echo "        ";
            if (twig_in_filter(twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 236), ($context["payment_mp_transparente_order_status_id_canceled"] ?? null))) {
                // line 237
                echo "          <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 237);
                echo "\" selected=\"selected\">";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "name", [], "any", false, false, false, 237);
                echo "</option>
        ";
            } else {
                // line 239
                echo "          <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 239);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "name", [], "any", false, false, false, 239);
                echo "</option>
        ";
            }
            // line 241
            echo "      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['order_status'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 242
        echo "    </select>
  </div>
</div>

<div class=\"form-group\">
  <label class=\"col-sm-2 control-label\" for=\"payment_mp_transparente_order_status_id_in_process\">
    <span data-toggle=\"tooltip\" data-trigger=\"click\" title='";
        // line 248
        echo ($context["entry_order_status_in_process_tooltip"] ?? null);
        echo " '>";
        echo ($context["entry_order_status_in_process"] ?? null);
        echo "</span></label>
    <div class=\"col-sm-10\">
      <select class=\"form-control\" name=\"payment_mp_transparente_order_status_id_in_process\" id=\"payment_mp_transparente_order_status_id_in_process\">
       ";
        // line 251
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["order_statuses"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["order_status"]) {
            // line 252
            echo "        ";
            if (twig_in_filter(twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 252), ($context["payment_mp_transparente_order_status_id_in_process"] ?? null))) {
                // line 253
                echo "          <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 253);
                echo "\" selected=\"selected\">";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "name", [], "any", false, false, false, 253);
                echo "</option>
        ";
            } else {
                // line 255
                echo "          <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 255);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "name", [], "any", false, false, false, 255);
                echo "</option>
        ";
            }
            // line 257
            echo "      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['order_status'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 258
        echo "    </select>
  </div>
</div>
<div class=\"form-group\">
  <label class=\"col-sm-2 control-label\" for=\"payment_mp_transparente_order_status_id_rejected\"><span data-toggle=\"tooltip\" data-trigger=\"click\" title='";
        // line 262
        echo ($context["entry_order_status_rejected_tooltip"] ?? null);
        echo " '>";
        echo ($context["entry_order_status_rejected"] ?? null);
        echo "</span></label>
  <div class=\"col-sm-10\">
    <select class=\"form-control\" name=\"payment_mp_transparente_order_status_id_rejected\" id=\"payment_mp_transparente_order_status_id_rejected\">
     ";
        // line 265
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["order_statuses"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["order_status"]) {
            // line 266
            echo "      ";
            if (twig_in_filter(twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 266), ($context["payment_mp_transparente_order_status_id_rejected"] ?? null))) {
                // line 267
                echo "        <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 267);
                echo "\" selected=\"selected\">";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "name", [], "any", false, false, false, 267);
                echo "</option>
      ";
            } else {
                // line 269
                echo "        <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 269);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "name", [], "any", false, false, false, 269);
                echo "</option>
      ";
            }
            // line 271
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['order_status'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 272
        echo "  </select>
</div>
</div>
<div class=\"form-group\">
  <label class=\"col-sm-2 control-label\" for=\"payment_mp_transparente_order_status_id_refunded\">
    <span data-toggle=\"tooltip\" data-trigger=\"click\" title='";
        // line 277
        echo ($context["entry_order_status_refunded_tooltip"] ?? null);
        echo " '>";
        echo ($context["entry_order_status_refunded"] ?? null);
        echo "</span></label>
    <div class=\"col-sm-10\">
      <select class=\"form-control\" name=\"payment_mp_transparente_order_status_id_refunded\" id=\"payment_mp_transparente_order_status_id_refunded\">
        ";
        // line 280
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["order_statuses"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["order_status"]) {
            // line 281
            echo "          ";
            if (twig_in_filter(twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 281), ($context["payment_mp_transparente_order_status_id_refunded"] ?? null))) {
                // line 282
                echo "            <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 282);
                echo "\" selected=\"selected\">";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "name", [], "any", false, false, false, 282);
                echo "</option>
           ";
            } else {
                // line 284
                echo "            <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 284);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "name", [], "any", false, false, false, 284);
                echo "</option>
          ";
            }
            // line 286
            echo "        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['order_status'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 287
        echo "      </select>
    </div>
  </div>
  <div class=\"form-group\">
    <label class=\"col-sm-2 control-label\" for=\"payment_mp_transparente_order_status_id_in_mediation\">
      <span data-toggle=\"tooltip\" data-trigger=\"click\" title='";
        // line 292
        echo ($context["entry_order_status_in_mediation_tooltip"] ?? null);
        echo " '>";
        echo ($context["entry_order_status_in_mediation"] ?? null);
        echo "</span></label>
      <div class=\"col-sm-10\">
        <select class=\"form-control\" name=\"payment_mp_transparente_order_status_id_in_mediation\" id=\"payment_mp_transparente_order_status_id_in_mediation\">
         ";
        // line 295
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["order_statuses"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["order_status"]) {
            // line 296
            echo "          ";
            if (twig_in_filter(twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 296), ($context["payment_mp_transparente_order_status_id_in_mediation"] ?? null))) {
                // line 297
                echo "            <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 297);
                echo "\" selected=\"selected\">";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "name", [], "any", false, false, false, 297);
                echo "</option>
          ";
            } else {
                // line 299
                echo "            <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 299);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "name", [], "any", false, false, false, 299);
                echo "</option>
          ";
            }
            // line 301
            echo "        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['order_status'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 302
        echo "      </select>
    </div>
  </div>
  <div class=\"form-group\">
    <label class=\"col-sm-2 control-label\" for=\"payment_mp_transparente_order_status_chargeback\">
      <span data-toggle=\"tooltip\" data-trigger=\"click\" title='";
        // line 307
        echo ($context["entry_order_status_chargeback_tooltip"] ?? null);
        echo "'>
        ";
        // line 308
        echo ($context["entry_order_status_chargeback"] ?? null);
        echo "
      </span>
    </label>
    <div class=\"col-sm-10\">
      <select class=\"form-control\" name=\"payment_mp_transparente_order_status_chargeback\" id=\"payment_mp_transparente_order_status_chargeback\">
        ";
        // line 313
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["order_statuses"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["order_status"]) {
            // line 314
            echo "          ";
            if ((($context["payment_mp_transparente_order_status_chargeback"] ?? null) && twig_in_filter(twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 314), ($context["payment_mp_transparente_order_status_chargeback"] ?? null)))) {
                // line 315
                echo "            <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 315);
                echo "\" selected=\"selected\">";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "name", [], "any", false, false, false, 315);
                echo "</option>
          ";
            } else {
                // line 317
                echo "            <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 317);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "name", [], "any", false, false, false, 317);
                echo "</option>
          ";
            }
            // line 319
            echo "        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['order_status'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 320
        echo "
      </select>
    </div>
  </div>
</form>
</div>
</div>
</div>
<script type=\"text/javascript\" src=\"./view/javascript/mp/transparente/mp_transparente.js\"></script>
<script type=\"text/javascript\" src=\"./view/javascript/mp/spinner.min.js\"></script>
";
    }

    public function getTemplateName()
    {
        return "extension/payment/mp_transparente.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  891 => 320,  885 => 319,  877 => 317,  869 => 315,  866 => 314,  862 => 313,  854 => 308,  850 => 307,  843 => 302,  837 => 301,  829 => 299,  821 => 297,  818 => 296,  814 => 295,  806 => 292,  799 => 287,  793 => 286,  785 => 284,  777 => 282,  774 => 281,  770 => 280,  762 => 277,  755 => 272,  749 => 271,  741 => 269,  733 => 267,  730 => 266,  726 => 265,  718 => 262,  712 => 258,  706 => 257,  698 => 255,  690 => 253,  687 => 252,  683 => 251,  675 => 248,  667 => 242,  661 => 241,  653 => 239,  645 => 237,  642 => 236,  638 => 235,  630 => 232,  623 => 227,  617 => 226,  609 => 224,  601 => 222,  598 => 221,  594 => 220,  586 => 217,  580 => 213,  574 => 212,  566 => 210,  558 => 208,  555 => 207,  551 => 206,  543 => 203,  537 => 199,  531 => 198,  523 => 196,  515 => 194,  512 => 193,  508 => 192,  500 => 189,  494 => 186,  488 => 182,  483 => 179,  477 => 177,  475 => 176,  469 => 172,  462 => 170,  457 => 168,  452 => 167,  447 => 165,  442 => 164,  440 => 163,  433 => 162,  429 => 161,  425 => 160,  419 => 157,  414 => 155,  409 => 152,  406 => 151,  400 => 146,  394 => 145,  384 => 143,  374 => 141,  371 => 140,  367 => 139,  361 => 136,  355 => 132,  350 => 130,  345 => 129,  340 => 127,  335 => 126,  333 => 125,  327 => 122,  323 => 121,  316 => 116,  311 => 114,  306 => 113,  301 => 111,  296 => 110,  294 => 109,  286 => 106,  280 => 102,  274 => 101,  264 => 99,  254 => 97,  251 => 96,  247 => 95,  241 => 92,  237 => 91,  231 => 87,  225 => 85,  223 => 84,  219 => 83,  212 => 81,  206 => 77,  200 => 75,  198 => 74,  194 => 73,  187 => 71,  181 => 67,  175 => 65,  173 => 64,  169 => 63,  164 => 61,  144 => 46,  139 => 44,  133 => 40,  128 => 38,  123 => 37,  118 => 35,  113 => 34,  111 => 33,  105 => 30,  100 => 28,  93 => 23,  85 => 19,  83 => 18,  77 => 14,  66 => 12,  62 => 11,  57 => 9,  50 => 7,  46 => 6,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "extension/payment/mp_transparente.twig", "");
    }
}

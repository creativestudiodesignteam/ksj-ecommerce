<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* wokiee/template/extension/module/filter_product.twig */
class __TwigTemplate_21e796f865bfb89e6626c9698ae473f323c59e4333d93760b427a6731ed10003 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ((twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "has", [0 => "theme_options"], "method", false, false, false, 1) == twig_constant("true"))) {
            echo " 
\t";
            // line 2
            $context["class"] = "col-6 col-md-4 col-lg-3";
            // line 3
            echo "\t";
            if ((($context["itemsperpage"] ?? null) == 1)) {
                $context["class"] = "col-12";
            }
            // line 4
            echo "\t";
            if ((($context["itemsperpage"] ?? null) == 2)) {
                $context["class"] = "col-12 col-md-6";
            }
            // line 5
            echo "\t";
            if ((($context["itemsperpage"] ?? null) == 3)) {
                $context["class"] = "col-12 col-md-6 col-lg-4";
            }
            // line 6
            echo "\t";
            if ((($context["itemsperpage"] ?? null) == 4)) {
                $context["class"] = "col-6 col-md-4 col-lg-3";
            }
            // line 7
            echo "\t";
            if ((($context["itemsperpage"] ?? null) == 5)) {
                $context["class"] = "col-6 col-md-4 col-lg-25";
            }
            // line 8
            echo "\t";
            if ((($context["itemsperpage"] ?? null) == 6)) {
                $context["class"] = "col-6 col-md-3 col-lg-2";
            }
            // line 9
            echo "\t";
            $context["tabs_id"] = (twig_random($this->env, 50000) * twig_random($this->env, 50000));
            // line 10
            echo "
\t";
            // line 11
            if ((twig_length_filter($this->env, ($context["tabs"] ?? null)) > 1)) {
                echo " 
\t\t<div class=\"container-indent\">
\t\t\t<div class=\"container container-fluid-custom-mobile-padding\">
\t\t\t\t<ul class=\"nav nav-tabs tt-tabs-default\" role=\"tablist\">
\t\t\t\t\t";
                // line 15
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["tabs"] ?? null));
                $context['loop'] = [
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                ];
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["tab"]) {
                    echo " 
\t\t\t\t\t<li class=\"nav-item\">
\t\t\t\t\t\t<a class=\"nav-link ";
                    // line 17
                    if ((twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 17) == 1)) {
                        echo "active";
                    }
                    echo "\" data-toggle=\"tab\" href=\"#tt-tab";
                    echo ($context["tabs_id"] ?? null);
                    echo "-";
                    echo twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 17);
                    echo "\">";
                    echo (($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = $context["tab"]) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4["heading"] ?? null) : null);
                    echo "</a>
\t\t\t\t\t</li>
\t\t\t\t\t";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tab'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 19
                echo " 
\t\t\t\t</ul>
\t\t\t\t<div class=\"tab-content\">
\t\t\t\t\t";
                // line 22
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["tabs"] ?? null));
                $context['loop'] = [
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                ];
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["tab"]) {
                    echo " 
\t\t\t\t\t\t<div class=\"tab-pane ";
                    // line 23
                    if ((twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 23) == 1)) {
                        echo "active";
                    }
                    echo "\" id=\"tt-tab";
                    echo ($context["tabs_id"] ?? null);
                    echo "-";
                    echo twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 23);
                    echo "\">
\t\t\t\t\t\t\t";
                    // line 24
                    if ((($context["carousel"] ?? null) != 0)) {
                        echo " 
\t\t\t\t\t\t\t<div class=\"tt-carousel-products row arrow-location-tab arrow-location-tab01 tt-alignment-img tt-layout-product-item slick-animated-show-js\" data-item=\"";
                        // line 25
                        echo ($context["itemsperpage"] ?? null);
                        echo "\">
\t\t\t\t\t\t\t";
                    } else {
                        // line 27
                        echo "\t\t\t\t\t\t\t<div class=\"row tt-layout-product-item\">
\t\t\t\t\t\t\t";
                    }
                    // line 29
                    echo "\t\t\t\t\t\t\t\t<div class=\"";
                    echo ($context["class"] ?? null);
                    echo "\">
\t\t\t\t\t\t\t\t\t";
                    // line 30
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable((($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = $context["tab"]) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144["products"] ?? null) : null));
                    $context['loop'] = [
                      'parent' => $context['_parent'],
                      'index0' => 0,
                      'index'  => 1,
                      'first'  => true,
                    ];
                    if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                        $length = count($context['_seq']);
                        $context['loop']['revindex0'] = $length - 1;
                        $context['loop']['revindex'] = $length;
                        $context['loop']['length'] = $length;
                        $context['loop']['last'] = 1 === $length;
                    }
                    foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                        // line 31
                        echo "\t\t\t\t\t\t\t\t\t\t";
                        if (((((($context["cols"] ?? null) == 1) && (twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 31) > 1)) || ((($context["carousel"] ?? null) == 0) && (twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 31) > 1))) || (((($context["carousel"] ?? null) == 1) && (0 == (twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 31) - 1) % ($context["cols"] ?? null))) && (twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 31) > 1)))) {
                            // line 32
                            echo "\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"";
                            // line 33
                            echo ($context["class"] ?? null);
                            echo "\">
\t\t\t\t\t\t\t\t\t\t";
                        }
                        // line 35
                        echo "\t\t\t\t\t\t\t\t\t\t";
                        $this->loadTemplate("wokiee/template/new_elements/product.twig", "wokiee/template/extension/module/filter_product.twig", 35)->display($context);
                        // line 36
                        echo "\t\t\t\t\t\t\t\t\t";
                        ++$context['loop']['index0'];
                        ++$context['loop']['index'];
                        $context['loop']['first'] = false;
                        if (isset($context['loop']['length'])) {
                            --$context['loop']['revindex0'];
                            --$context['loop']['revindex'];
                            $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                        }
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 37
                    echo "\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tab'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 41
                echo "\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t";
            } else {
                // line 44
                echo " 
\t\t";
                // line 45
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["tabs"] ?? null));
                $context['loop'] = [
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                ];
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["tab"]) {
                    echo " 
\t\t\t<div class=\"container-indent\">
\t\t\t\t<div class=\"container container-fluid-custom-mobile-padding\">
\t\t\t\t\t<div class=\"tt-block-title\">
\t\t\t\t\t\t";
                    // line 49
                    $context["title"] = twig_split_filter($this->env, (($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = $context["tab"]) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b["heading"] ?? null) : null), ",");
                    // line 50
                    echo "\t\t\t\t\t\t<h1 class=\"tt-title\">";
                    echo (($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 = ($context["title"] ?? null)) && is_array($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002) || $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 instanceof ArrayAccess ? ($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002[0] ?? null) : null);
                    echo "</h1>
\t\t\t\t\t\t<div class=\"tt-description\">";
                    // line 51
                    echo (($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 = ($context["title"] ?? null)) && is_array($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4) || $__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 instanceof ArrayAccess ? ($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4[1] ?? null) : null);
                    echo "</div>
\t\t\t\t\t</div>

\t\t\t\t\t";
                    // line 54
                    if ((($context["carousel"] ?? null) != 0)) {
                        echo " 
\t\t\t\t\t<div class=\"tt-carousel-products row arrow-location-tab arrow-location-tab01 tt-alignment-img tt-layout-product-item slick-animated-show-js\" data-item=\"";
                        // line 55
                        echo ($context["itemsperpage"] ?? null);
                        echo "\">
\t\t\t\t\t";
                    } else {
                        // line 57
                        echo "\t\t\t\t\t<div class=\"row tt-layout-product-item\">
\t\t\t\t\t";
                    }
                    // line 59
                    echo "\t\t\t\t\t\t<div class=\"";
                    echo ($context["class"] ?? null);
                    echo "\">
\t\t\t\t\t\t\t";
                    // line 60
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable((($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 = $context["tab"]) && is_array($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666) || $__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 instanceof ArrayAccess ? ($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666["products"] ?? null) : null));
                    $context['loop'] = [
                      'parent' => $context['_parent'],
                      'index0' => 0,
                      'index'  => 1,
                      'first'  => true,
                    ];
                    if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                        $length = count($context['_seq']);
                        $context['loop']['revindex0'] = $length - 1;
                        $context['loop']['revindex'] = $length;
                        $context['loop']['length'] = $length;
                        $context['loop']['last'] = 1 === $length;
                    }
                    foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                        // line 61
                        echo "\t\t\t\t\t\t\t\t";
                        if (((((($context["cols"] ?? null) == 1) && (twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 61) > 1)) || ((($context["carousel"] ?? null) == 0) && (twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 61) > 1))) || (((($context["carousel"] ?? null) == 1) && (0 == (twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 61) - 1) % ($context["cols"] ?? null))) && (twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 61) > 1)))) {
                            // line 62
                            echo "\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"";
                            // line 63
                            echo ($context["class"] ?? null);
                            echo "\">
\t\t\t\t\t\t\t\t";
                        }
                        // line 65
                        echo "\t\t\t\t\t\t\t\t\t";
                        $this->loadTemplate("wokiee/template/new_elements/product.twig", "wokiee/template/extension/module/filter_product.twig", 65)->display($context);
                        // line 66
                        echo "\t\t\t\t\t\t\t";
                        ++$context['loop']['index0'];
                        ++$context['loop']['index'];
                        $context['loop']['first'] = false;
                        if (isset($context['loop']['length'])) {
                            --$context['loop']['revindex0'];
                            --$context['loop']['revindex'];
                            $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                        }
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    echo " 
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tab'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 72
                echo "\t";
            }
            echo " 
";
        }
    }

    public function getTemplateName()
    {
        return "wokiee/template/extension/module/filter_product.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  370 => 72,  338 => 66,  335 => 65,  330 => 63,  327 => 62,  324 => 61,  307 => 60,  302 => 59,  298 => 57,  293 => 55,  289 => 54,  283 => 51,  278 => 50,  276 => 49,  254 => 45,  251 => 44,  245 => 41,  228 => 37,  214 => 36,  211 => 35,  206 => 33,  203 => 32,  200 => 31,  183 => 30,  178 => 29,  174 => 27,  169 => 25,  165 => 24,  155 => 23,  136 => 22,  131 => 19,  106 => 17,  86 => 15,  79 => 11,  76 => 10,  73 => 9,  68 => 8,  63 => 7,  58 => 6,  53 => 5,  48 => 4,  43 => 3,  41 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "wokiee/template/extension/module/filter_product.twig", "");
    }
}

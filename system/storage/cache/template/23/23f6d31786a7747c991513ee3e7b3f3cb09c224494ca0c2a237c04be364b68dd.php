<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* wokiee/template/common/success.twig */
class __TwigTemplate_a8f710448eb42a2f0771a7c91b31fa34437023fddd04a5e1a7921eba062aab6c extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo " 
";
        // line 2
        $this->loadTemplate("wokiee/template/new_elements/wrapper_top.twig", "wokiee/template/common/success.twig", 2)->display($context);
        // line 3
        echo "
";
        // line 4
        if ((isset($context["text_message"]) || array_key_exists("text_message", $context))) {
            echo " ";
            echo ($context["text_message"] ?? null);
        }
        echo " 
<div class=\"buttons\">
  <div class=\"pull-right\"><a href=\"";
        // line 6
        echo ($context["continue"] ?? null);
        echo "\" class=\"btn btn-primary\">";
        echo ($context["button_continue"] ?? null);
        echo "</a></div>
</div>
  
";
        // line 9
        $this->loadTemplate("wokiee/template/new_elements/wrapper_bottom.twig", "wokiee/template/common/success.twig", 9)->display($context);
        // line 10
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "wokiee/template/common/success.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 10,  62 => 9,  54 => 6,  46 => 4,  43 => 3,  41 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "wokiee/template/common/success.twig", "");
    }
}

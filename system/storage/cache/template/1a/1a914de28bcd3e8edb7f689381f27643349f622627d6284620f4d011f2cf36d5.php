<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* wokiee/template/common/header/header_05.twig */
class __TwigTemplate_4b69d5719c3448c2734f816ad3dfc78ceeec7ec3cee8b555e88891161724d381 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<header>
\t<!-- tt-mobile-header -->
\t<div class=\"tt-mobile-header\">
\t\t<div class=\"container-fluid\">
\t\t\t<div class=\"tt-header-row\">
\t\t\t\t<div class=\"tt-mobile-parent-menu\">
\t\t\t\t\t<div class=\"tt-menu-toggle\">
\t\t\t\t\t\t<i class=\"icon-03\"></i>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<!-- search -->
\t\t\t\t<div class=\"tt-mobile-parent-search tt-parent-box\"></div>
\t\t\t\t<!-- /search -->
\t\t\t\t<!-- cart -->
\t\t\t\t<div class=\"tt-mobile-parent-cart tt-parent-box\"></div>
\t\t\t\t<!-- /cart -->
\t\t\t\t<!-- account -->
\t\t\t\t<div class=\"tt-mobile-parent-account tt-parent-box\"></div>
\t\t\t\t<!-- /account -->
\t\t\t\t<!-- currency -->
\t\t\t\t<div class=\"tt-mobile-parent-multi tt-parent-box\"></div>
\t\t\t\t<!-- /currency -->
\t\t\t</div>
\t\t</div>
\t\t<div class=\"container-fluid tt-top-line\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"tt-logo-container\">
\t\t\t\t\t";
        // line 28
        if (($context["logo"] ?? null)) {
            echo " 
\t\t\t\t\t<!-- Logo -->
\t\t\t\t\t<a class=\"tt-logo tt-logo-alignment\" href=\"";
            // line 30
            echo ($context["home"] ?? null);
            echo "\"><img src=\"";
            echo ($context["logo"] ?? null);
            echo "\" title=\"";
            echo ($context["name"] ?? null);
            echo "\" alt=\"";
            echo ($context["name"] ?? null);
            echo "\" /></a>
\t\t\t\t\t";
        }
        // line 31
        echo " 
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
\t<!-- tt-desktop-header -->
\t<div class=\"tt-desktop-header\">
\t\t<div class=\"container\">
\t\t\t<div class=\"tt-header-holder\">
\t\t\t\t<div class=\"tt-col-obj tt-obj-logo obj-aligment-center\">
\t\t\t\t\t";
        // line 41
        if (($context["logo"] ?? null)) {
            echo " 
\t\t\t\t\t<!-- Logo -->
\t\t\t\t\t<a class=\"tt-logo tt-logo-alignment\" href=\"";
            // line 43
            echo ($context["home"] ?? null);
            echo "\"><img src=\"";
            echo ($context["logo"] ?? null);
            echo "\" title=\"";
            echo ($context["name"] ?? null);
            echo "\" alt=\"";
            echo ($context["name"] ?? null);
            echo "\" /></a>
\t\t\t\t\t";
        }
        // line 44
        echo " 
\t\t\t\t</div>
\t\t\t\t<div class=\"tt-obj-options obj-move-right tt-position-absolute\">
\t\t\t\t\t<!-- tt-search -->
\t\t\t\t\t<div class=\"tt-desctop-parent-search tt-parent-box\">
\t\t\t\t\t\t<div class=\"tt-search tt-dropdown-obj\">
\t\t\t\t\t\t\t<button class=\"tt-dropdown-toggle\" data-tooltip=\"";
        // line 50
        echo ($context["text_search"] ?? null);
        echo "\" data-tposition=\"bottom\">
\t\t\t\t\t\t\t\t<i class=\"icon-f-85\"></i>
\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t<div class=\"tt-dropdown-menu\">
\t\t\t\t\t\t\t\t<div class=\"container\">
\t\t\t\t\t\t\t\t\t<form>
\t\t\t\t\t\t\t\t\t\t<div class=\"tt-col\">
\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"tt-search-input\" placeholder=\"";
        // line 57
        echo ($context["text_search"] ?? null);
        echo "...\">
\t\t\t\t\t\t\t\t\t\t\t<button class=\"tt-btn-search\" type=\"submit\"></button>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"tt-col\">
\t\t\t\t\t\t\t\t\t\t\t<button class=\"tt-btn-close icon-g-80\"></button>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"tt-info-text\">
\t\t\t\t\t\t\t\t\t\t\t";
        // line 64
        if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "what_are_you_looking_for_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 64)], "method", false, false, false, 64) != "")) {
            echo " ";
            echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "what_are_you_looking_for_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 64)], "method", false, false, false, 64);
            echo " ";
        } else {
            echo "What are you Looking for?";
        }
        // line 65
        echo "\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"search-results\">
\t\t\t\t\t\t\t\t\t\t\t<ul>

\t\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"tt-view-all\">";
        // line 70
        if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "view_all_products_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 70)], "method", false, false, false, 70) != "")) {
            echo " ";
            echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "view_all_products_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 70)], "method", false, false, false, 70);
            echo " ";
        } else {
            echo "View all products";
        }
        echo "</button>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<!-- /tt-search -->
\t\t\t\t\t<!-- tt-cart -->
\t\t\t\t\t";
        // line 79
        echo ($context["cart"] ?? null);
        echo "
\t\t\t\t\t<!-- /tt-cart -->
\t\t\t\t\t<!-- tt-account -->
\t\t\t\t\t<div class=\"tt-desctop-parent-account tt-parent-box\">
\t\t\t\t\t\t<div class=\"tt-account tt-dropdown-obj\">
\t\t\t\t\t\t\t<button class=\"tt-dropdown-toggle\"  data-tooltip=\"";
        // line 84
        echo ($context["text_account"] ?? null);
        echo "\" data-tposition=\"bottom\"><i class=\"icon-f-94\"></i></button>
\t\t\t\t\t\t\t<div class=\"tt-dropdown-menu\">
\t\t\t\t\t\t\t\t<div class=\"tt-mobile-add\">
\t\t\t\t\t\t\t\t\t<button class=\"tt-close\">";
        // line 87
        if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "close_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 87)], "method", false, false, false, 87) != "")) {
            echo " ";
            echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "close_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 87)], "method", false, false, false, 87);
            echo " ";
        } else {
            echo "Close";
        }
        echo "</button>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"tt-dropdown-inner\">
\t\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t    <li><a href=\"";
        // line 91
        echo ($context["account"] ?? null);
        echo "\"><i class=\"icon-f-94\"></i>";
        echo ($context["text_account"] ?? null);
        echo "</a></li>
\t\t\t\t\t\t\t\t\t    <li><a href=\"";
        // line 92
        echo ($context["wishlist"] ?? null);
        echo "\"><i class=\"icon-n-072\"></i>";
        echo ($context["text_wishlist"] ?? null);
        echo "</a></li>
\t\t\t\t\t\t\t\t\t    <li><a href=\"";
        // line 93
        echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getLinkOpenCart", [0 => "product/compare"], "method", false, false, false, 93);
        echo "\"><i class=\"icon-n-08\"></i>";
        if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "compare_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 93)], "method", false, false, false, 93) != "")) {
            echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "compare_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 93)], "method", false, false, false, 93);
        } else {
            echo "Compare";
        }
        echo "</a></li>
\t\t\t\t\t\t\t\t\t    <li><a href=\"";
        // line 94
        echo ($context["checkout"] ?? null);
        echo "\"><i class=\"icon-f-68\"></i>";
        echo ($context["text_checkout"] ?? null);
        echo "</a></li>
\t\t\t\t\t\t\t            ";
        // line 95
        if (($context["logged"] ?? null)) {
            // line 96
            echo "\t\t\t\t\t\t\t            <li><a href=\"";
            echo ($context["logout"] ?? null);
            echo "\"><i class=\"icon-f-77\"></i>";
            echo ($context["text_logout"] ?? null);
            echo "</a></li>
\t\t\t\t\t\t\t            ";
        } else {
            // line 98
            echo "\t\t\t\t\t\t\t            <li><a href=\"";
            echo ($context["login"] ?? null);
            echo "\"><i class=\"icon-f-76\"></i>";
            echo ($context["text_login"] ?? null);
            echo "</a></li>
\t\t\t\t\t\t\t            <li><a href=\"";
            // line 99
            echo ($context["register"] ?? null);
            echo "\"><i class=\"icon-f-94\"></i>";
            echo ($context["text_register"] ?? null);
            echo "</a></li>
\t\t\t\t\t\t\t            ";
        }
        // line 101
        echo "\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<!-- /tt-account -->
\t\t\t\t\t<!-- tt-langue and tt-currency -->
\t\t\t\t\t<div class=\"tt-desctop-parent-multi tt-parent-box\">
\t\t\t\t\t\t<div class=\"tt-multi-obj tt-dropdown-obj\">
\t\t\t\t\t\t\t<button class=\"tt-dropdown-toggle\" data-tooltip=\"";
        // line 110
        if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "settings_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 110)], "method", false, false, false, 110) != "")) {
            echo " ";
            echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "settings_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 110)], "method", false, false, false, 110);
            echo " ";
        } else {
            echo "Settings";
        }
        echo "\" data-tposition=\"bottom\"><i class=\"icon-f-79\"></i></button>
\t\t\t\t\t\t\t<div class=\"tt-dropdown-menu\">
\t\t\t\t\t\t\t\t<div class=\"tt-mobile-add\">
\t\t\t\t\t\t\t\t\t<button class=\"tt-close\">";
        // line 113
        if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "close_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 113)], "method", false, false, false, 113) != "")) {
            echo " ";
            echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "close_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 113)], "method", false, false, false, 113);
            echo " ";
        } else {
            echo "Close";
        }
        echo "</button>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"tt-dropdown-inner\">
\t\t\t\t\t\t\t\t\t";
        // line 116
        echo (($context["language"] ?? null) . ($context["currency"] ?? null));
        echo "
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<!-- /tt-langue and tt-currency -->
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"container\">
\t\t\t<div class=\"tt-header-holder\">
\t\t\t\t<div class=\"tt-obj-menu obj-aligment-center\">
\t\t\t\t\t<!-- tt-menu -->
\t\t\t\t\t<div class=\"tt-desctop-parent-menu tt-parent-box\">
\t\t\t\t\t\t<div class=\"tt-desctop-menu tt-menu-small\">
\t\t\t\t\t\t\t";
        // line 131
        $context["menu9"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "menu"], "method", false, false, false, 131);
        // line 132
        echo "\t\t\t\t\t\t\t";
        if ((twig_length_filter($this->env, ($context["menu9"] ?? null)) > 0)) {
            echo " 
\t\t\t\t\t\t\t\t";
            // line 133
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["menu9"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                echo " 
\t\t\t\t\t\t\t\t\t";
                // line 134
                echo $context["module"];
                echo "
\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 136
            echo "\t\t\t\t\t\t\t";
        } else {
            // line 137
            echo "\t\t\t\t\t\t\t\t";
            echo ($context["menu"] ?? null);
            echo "
\t\t\t\t\t\t\t";
        }
        // line 139
        echo "\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<!-- /tt-menu -->
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
\t<!-- stuck nav -->
\t<div class=\"tt-stuck-nav\">
\t\t<div class=\"container\">
\t\t\t<div class=\"tt-header-row \">
\t\t\t\t<div class=\"tt-stuck-parent-menu\"></div>
\t\t\t\t<div class=\"tt-stuck-parent-search tt-parent-box\"></div>
\t\t\t\t<div class=\"tt-stuck-parent-cart tt-parent-box\"></div>
\t\t\t\t<div class=\"tt-stuck-parent-account tt-parent-box\"></div>
\t\t\t\t<div class=\"tt-stuck-parent-multi tt-parent-box\"></div>
\t\t\t</div>
\t\t</div>
\t</div>
</header>";
    }

    public function getTemplateName()
    {
        return "wokiee/template/common/header/header_05.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  333 => 139,  327 => 137,  324 => 136,  316 => 134,  310 => 133,  305 => 132,  303 => 131,  285 => 116,  273 => 113,  261 => 110,  250 => 101,  243 => 99,  236 => 98,  228 => 96,  226 => 95,  220 => 94,  210 => 93,  204 => 92,  198 => 91,  185 => 87,  179 => 84,  171 => 79,  153 => 70,  146 => 65,  138 => 64,  128 => 57,  118 => 50,  110 => 44,  99 => 43,  94 => 41,  82 => 31,  71 => 30,  66 => 28,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "wokiee/template/common/header/header_05.twig", "/home2/kadore/public_html/catalog/view/theme/wokiee/template/common/header/header_05.twig");
    }
}

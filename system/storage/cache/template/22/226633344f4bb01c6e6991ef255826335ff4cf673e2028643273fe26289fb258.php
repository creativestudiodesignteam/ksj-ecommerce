<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* wokiee/template/extension/module/advanced_grid/advanced_grid.twig */
class __TwigTemplate_35004a52a15b7e5cc91a4c5fc5065903c3b1a4233315fedd34c515a0867f725e extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ((twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "has", [0 => "theme_options"], "method", false, false, false, 1) == twig_constant("true"))) {
            echo " 
     ";
            // line 2
            $context["theme_options"] = twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "get", [0 => "theme_options"], "method", false, false, false, 2);
            // line 3
            echo "     ";
            $context["config"] = twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "get", [0 => "config"], "method", false, false, false, 3);
            // line 4
            echo "     ";
            $context["page_direction"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "page_direction"], "method", false, false, false, 4);
            echo " 
     ";
            // line 5
            $context["language_id"] = twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 5);
            // line 6
            echo "     ";
            $context["advanced_id"] = ($context["id"] ?? null);
            echo " 

     <div class=\"advanced-grid advanced-grid-";
            // line 8
            echo ($context["advanced_id"] ?? null);
            echo " ";
            echo ($context["custom_class"] ?? null);
            echo " ";
            if ((($context["disable_on_mobile"] ?? null) == 1)) {
                echo " ";
                echo "d-none d-md-block";
            }
            echo "\" style=\"";
            if ((($context["margin_top"] ?? null) != "auto")) {
                echo "margin-top: ";
                echo ($context["margin_top"] ?? null);
                echo "px;";
            }
            if ((($context["force_full_width"] ?? null) != 1)) {
                if ((($context["margin_left"] ?? null) != "auto")) {
                    echo "margin-left: ";
                    echo ($context["margin_left"] ?? null);
                    echo "px;";
                }
                if ((($context["margin_right"] ?? null) != "auto")) {
                    echo "margin-right: ";
                    echo ($context["margin_right"] ?? null);
                    echo "px;";
                }
            }
            if ((($context["margin_bottom"] ?? null) != "auto")) {
                echo "margin-bottom: ";
                echo ($context["margin_bottom"] ?? null);
                echo "px;";
            }
            echo "\">
          ";
            // line 9
            if (((($context["background_image_type"] ?? null) == 1) || (($context["background_image_type"] ?? null) == 2))) {
                echo "<div style=\"";
                if ((($context["background_color"] ?? null) != "")) {
                    echo " ";
                    echo (("background-color: " . ($context["background_color"] ?? null)) . ";");
                    echo " ";
                }
                echo " ";
                if ((($context["background_image"] ?? null) != "")) {
                    echo " ";
                    echo (("background-image: url(image/" . ($context["background_image"] ?? null)) . ");");
                    echo " ";
                }
                echo " ";
                echo (((((("background-position: " . ($context["background_image_position"] ?? null)) . ";background-repeat: ") . ($context["background_image_repeat"] ?? null)) . ";background-attachment: ") . ($context["background_image_attachment"] ?? null)) . ";");
                echo "\">";
            }
            echo " 
          ";
            // line 10
            if ((($context["background_image_type"] ?? null) == 0)) {
                echo "<div style=\"";
                if ((($context["background_color"] ?? null) != "")) {
                    echo " ";
                    echo (("background-color: " . ($context["background_color"] ?? null)) . ";");
                    echo " ";
                }
                echo "\">";
            }
            echo " 
               <div style=\"padding-top: ";
            // line 11
            echo ($context["padding_top"] ?? null);
            echo "px;padding-left: ";
            echo ($context["padding_left"] ?? null);
            echo "px;padding-bottom: ";
            echo ($context["padding_bottom"] ?? null);
            echo "px;padding-right: ";
            echo ($context["padding_right"] ?? null);
            echo "px;\">
                    <div class=\"row\">
                         ";
            // line 13
            $context["row"] = 0;
            echo " 
                         ";
            // line 14
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["columns"] ?? null));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["column"]) {
                echo " 
                              
                              ";
                // line 16
                if (((($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = $context["column"]) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4["width"] ?? null) : null) == "advanced")) {
                    // line 17
                    echo "                                   ";
                    $context["column_width"] = (((((((("col-" . (($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = $context["column"]) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144["width_xs"] ?? null) : null)) . " col-md-") . (($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = $context["column"]) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b["width_sm"] ?? null) : null)) . " col-lg-") . (($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 = $context["column"]) && is_array($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002) || $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 instanceof ArrayAccess ? ($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002["width_md"] ?? null) : null)) . " col-xl-") . (($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 = $context["column"]) && is_array($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4) || $__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 instanceof ArrayAccess ? ($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4["width_lg"] ?? null) : null)) . "");
                    // line 18
                    echo "                                   ";
                    if (((($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 = $context["column"]) && is_array($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666) || $__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 instanceof ArrayAccess ? ($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666["width_lg"] ?? null) : null) == "hidden")) {
                        echo " 
                                        ";
                        // line 19
                        $context["column_width"] = (($context["column_width"] ?? null) . " d-block d-xl-none");
                        // line 20
                        echo "                                   ";
                    }
                    // line 21
                    echo "                                   
                                   ";
                    // line 22
                    if (((($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e = $context["column"]) && is_array($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e) || $__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e instanceof ArrayAccess ? ($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e["width_md"] ?? null) : null) == "hidden")) {
                        echo " 
                                        ";
                        // line 23
                        $context["column_width"] = (($context["column_width"] ?? null) . " d-block d-lg-none d-xl-block");
                        // line 24
                        echo "                                   ";
                    }
                    // line 25
                    echo "
                                   ";
                    // line 26
                    if (((($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 = $context["column"]) && is_array($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52) || $__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 instanceof ArrayAccess ? ($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52["width_sm"] ?? null) : null) == "hidden")) {
                        echo " 
                                        ";
                        // line 27
                        $context["column_width"] = (($context["column_width"] ?? null) . " d-block d-md-none d-lg-block");
                        // line 28
                        echo "                                   ";
                    }
                    // line 29
                    echo "
                                   ";
                    // line 30
                    if (((($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 = $context["column"]) && is_array($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136) || $__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 instanceof ArrayAccess ? ($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136["width_xs"] ?? null) : null) == "hidden")) {
                        echo " 
                                        ";
                        // line 31
                        $context["column_width"] = (($context["column_width"] ?? null) . " d-none d-md-block");
                        // line 32
                        echo "                                   ";
                    }
                    // line 33
                    echo "                              ";
                } else {
                    echo " 
                                   ";
                    // line 34
                    $context["row"] = (($context["row"] ?? null) + (($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 = $context["column"]) && is_array($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386) || $__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 instanceof ArrayAccess ? ($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386["width"] ?? null) : null));
                    // line 35
                    echo "                                   ";
                    $context["column_width"] = ("col-md-" . (($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9 = $context["column"]) && is_array($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9) || $__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9 instanceof ArrayAccess ? ($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9["width"] ?? null) : null));
                    // line 36
                    echo "                                   ";
                    if (((($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae = $context["column"]) && is_array($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae) || $__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae instanceof ArrayAccess ? ($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae["disable_on_mobile"] ?? null) : null) == "1")) {
                        echo " 
                                        ";
                        // line 37
                        $context["column_width"] = (($context["column_width"] ?? null) . " d-none d-md-block");
                        // line 38
                        echo "                                   ";
                    }
                    // line 39
                    echo "                              ";
                }
                // line 40
                echo " 
                              <div class=\"";
                // line 41
                echo ($context["column_width"] ?? null);
                echo "\">
                                   ";
                // line 42
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f = $context["column"]) && is_array($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f) || $__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f instanceof ArrayAccess ? ($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f["modules"] ?? null) : null));
                $context['loop'] = [
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                ];
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                    echo " 
                                        ";
                    // line 43
                    if (((($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40 = $context["module"]) && is_array($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40) || $__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40 instanceof ArrayAccess ? ($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40["type"] ?? null) : null) == "load_module")) {
                        echo " 
                                             ";
                        // line 44
                        echo (($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f = (($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760 = $context["module"]) && is_array($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760) || $__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760 instanceof ArrayAccess ? ($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760["content"] ?? null) : null)) && is_array($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f) || $__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f instanceof ArrayAccess ? ($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f["module"] ?? null) : null);
                        echo " 
                                        ";
                    } elseif (((($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce =                     // line 45
$context["module"]) && is_array($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce) || $__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce instanceof ArrayAccess ? ($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce["type"] ?? null) : null) == "html")) {
                        echo " 
                                             ";
                        // line 46
                        echo (($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b = (($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c = $context["module"]) && is_array($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c) || $__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c instanceof ArrayAccess ? ($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c["content"] ?? null) : null)) && is_array($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b) || $__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b instanceof ArrayAccess ? ($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b["html"] ?? null) : null);
                        echo " 
                                        ";
                    } elseif (((($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972 =                     // line 47
$context["module"]) && is_array($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972) || $__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972 instanceof ArrayAccess ? ($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972["type"] ?? null) : null) == "box")) {
                        echo " 
                                             ";
                        // line 48
                        if (((((((((($context["position"] ?? null) == "footer_bottom") || (($context["position"] ?? null) == "footer")) || (($context["position"] ?? null) == "footer_top")) || (($context["position"] ?? null) == "footer_left")) || (($context["position"] ?? null) == "footer_right")) || (($context["position"] ?? null) == "customfooter_top")) || (($context["position"] ?? null) == "customfooter_bottom")) || (($context["position"] ?? null) == "customfooter"))) {
                            // line 49
                            echo "                                                  ";
                            echo (("<h4>" . (($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216 = (($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0 = $context["module"]) && is_array($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0) || $__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0 instanceof ArrayAccess ? ($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0["content"] ?? null) : null)) && is_array($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216) || $__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216 instanceof ArrayAccess ? ($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216["title"] ?? null) : null)) . "</h4>");
                            echo "
                                                  ";
                            // line 50
                            echo "<div class=\"strip-line\"></div>";
                            echo "
                                                  ";
                            // line 51
                            echo (("<div class=\"clearfix\" style=\"clear:both\">" . (($__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c = (($__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f = $context["module"]) && is_array($__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f) || $__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f instanceof ArrayAccess ? ($__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f["content"] ?? null) : null)) && is_array($__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c) || $__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c instanceof ArrayAccess ? ($__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c["text"] ?? null) : null)) . "</div>");
                            echo "
                                             ";
                        } else {
                            // line 52
                            echo " 
                                                  ";
                            // line 53
                            echo "<div class=\"box\">";
                            echo "
                                                       ";
                            // line 54
                            echo "<div class=\"box-heading\">";
                            echo "
                                                            ";
                            // line 55
                            echo (($__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc = (($__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55 = $context["module"]) && is_array($__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55) || $__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55 instanceof ArrayAccess ? ($__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55["content"] ?? null) : null)) && is_array($__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc) || $__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc instanceof ArrayAccess ? ($__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc["title"] ?? null) : null);
                            echo "
                                                       ";
                            // line 56
                            echo "</div>";
                            echo "
                                                       ";
                            // line 57
                            echo "<div class=\"strip-line\"></div>";
                            echo "
                                                       ";
                            // line 58
                            echo "<div class=\"box-content\">";
                            echo "
                                                            ";
                            // line 59
                            echo (($__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba = (($__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78 = $context["module"]) && is_array($__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78) || $__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78 instanceof ArrayAccess ? ($__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78["content"] ?? null) : null)) && is_array($__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba) || $__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba instanceof ArrayAccess ? ($__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba["text"] ?? null) : null);
                            echo "
                                                       ";
                            // line 60
                            echo "</div>";
                            echo "
                                                  ";
                            // line 61
                            echo "</div>";
                            echo " 
                                             ";
                        }
                        // line 62
                        echo "   
                                        ";
                    } elseif (((($__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de =                     // line 63
$context["module"]) && is_array($__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de) || $__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de instanceof ArrayAccess ? ($__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de["type"] ?? null) : null) == "links")) {
                        echo " 
                                             ";
                        // line 64
                        $this->loadTemplate(("wokiee/template/extension/module/advanced_grid/links/" . twig_replace_filter((($__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828 = (($__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd = $context["module"]) && is_array($__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd) || $__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd instanceof ArrayAccess ? ($__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd["content"] ?? null) : null)) && is_array($__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828) || $__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828 instanceof ArrayAccess ? ($__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828["module_template"] ?? null) : null), [".tpl" => ".twig"])), "wokiee/template/extension/module/advanced_grid/advanced_grid.twig", 64)->display($context);
                        // line 65
                        echo "                                        ";
                    } elseif (((($__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6 = $context["module"]) && is_array($__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6) || $__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6 instanceof ArrayAccess ? ($__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6["type"] ?? null) : null) == "newsletter")) {
                        echo " 
                                             ";
                        // line 66
                        $this->loadTemplate(("wokiee/template/extension/module/advanced_grid/newsletter/" . twig_replace_filter((($__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855 = (($__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b = $context["module"]) && is_array($__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b) || $__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b instanceof ArrayAccess ? ($__internal_0228c5445a74540c89ea8a758478d405796357800f8af831a7f7e1e2c0159d9b["content"] ?? null) : null)) && is_array($__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855) || $__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855 instanceof ArrayAccess ? ($__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855["module_template"] ?? null) : null), [".tpl" => ".twig"])), "wokiee/template/extension/module/advanced_grid/advanced_grid.twig", 66)->display($context);
                        echo " 
                                        ";
                    } elseif (((($__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f =                     // line 67
$context["module"]) && is_array($__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f) || $__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f instanceof ArrayAccess ? ($__internal_6fb04c4457ec9ffa7dd6fd2300542be8b961b6e5f7858a80a282f47b43ddae5f["type"] ?? null) : null) == "latest_blogs")) {
                        echo " 
                                             ";
                        // line 68
                        $this->loadTemplate(("wokiee/template/extension/module/advanced_grid/latest_blogs/" . twig_replace_filter((($__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0 = (($__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55 = $context["module"]) && is_array($__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55) || $__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55 instanceof ArrayAccess ? ($__internal_af3439635eb343262861f05093b3dcce5d4dae1e20a47bc25a2eef28135b0d55["content"] ?? null) : null)) && is_array($__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0) || $__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0 instanceof ArrayAccess ? ($__internal_417a1a95b289c75779f33186a6dc0b71d01f257b68beae7dcb9d2d769acca0e0["module_template"] ?? null) : null), [".tpl" => ".twig"])), "wokiee/template/extension/module/advanced_grid/advanced_grid.twig", 68)->display($context);
                        echo " 
                                        ";
                    } elseif (((($__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a =                     // line 69
$context["module"]) && is_array($__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a) || $__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a instanceof ArrayAccess ? ($__internal_b16f7904bcaaa7a87404cbf85addc7a8645dff94eef4e8ae7182b86e3638e76a["type"] ?? null) : null) == "products")) {
                        echo " 
                                             ";
                        // line 70
                        $this->loadTemplate(("wokiee/template/extension/module/advanced_grid/products/" . twig_replace_filter((($__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88 = (($__internal_be1db6a1ea9fa5c04c40f99df0ec5af053ca391863fc6256c5c4ee249724f758 = $context["module"]) && is_array($__internal_be1db6a1ea9fa5c04c40f99df0ec5af053ca391863fc6256c5c4ee249724f758) || $__internal_be1db6a1ea9fa5c04c40f99df0ec5af053ca391863fc6256c5c4ee249724f758 instanceof ArrayAccess ? ($__internal_be1db6a1ea9fa5c04c40f99df0ec5af053ca391863fc6256c5c4ee249724f758["content"] ?? null) : null)) && is_array($__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88) || $__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88 instanceof ArrayAccess ? ($__internal_462377748602ccf3a44a10ced4240983cec8df1ad86ab53e582fcddddb98fc88["module_template"] ?? null) : null), [".tpl" => ".twig"])), "wokiee/template/extension/module/advanced_grid/advanced_grid.twig", 70)->display($context);
                        echo " 
                                        ";
                    } elseif (((($__internal_6e6eda1691934a8f5855a3221f5a9f036391304a5cda73a3a2009f2961a84c35 =                     // line 71
$context["module"]) && is_array($__internal_6e6eda1691934a8f5855a3221f5a9f036391304a5cda73a3a2009f2961a84c35) || $__internal_6e6eda1691934a8f5855a3221f5a9f036391304a5cda73a3a2009f2961a84c35 instanceof ArrayAccess ? ($__internal_6e6eda1691934a8f5855a3221f5a9f036391304a5cda73a3a2009f2961a84c35["type"] ?? null) : null) == "products_tabs")) {
                        echo " 
                                             ";
                        // line 72
                        $this->loadTemplate(("wokiee/template/extension/module/advanced_grid/products_tabs/" . twig_replace_filter((($__internal_51c633083c79004f3cb5e9e2b2f3504f650f1561800582801028bcbcf733a06b = (($__internal_064553f1273f2ea50405f85092d06733f3f2fe5d0fc42fda135e1fdc91ff26ae = $context["module"]) && is_array($__internal_064553f1273f2ea50405f85092d06733f3f2fe5d0fc42fda135e1fdc91ff26ae) || $__internal_064553f1273f2ea50405f85092d06733f3f2fe5d0fc42fda135e1fdc91ff26ae instanceof ArrayAccess ? ($__internal_064553f1273f2ea50405f85092d06733f3f2fe5d0fc42fda135e1fdc91ff26ae["content"] ?? null) : null)) && is_array($__internal_51c633083c79004f3cb5e9e2b2f3504f650f1561800582801028bcbcf733a06b) || $__internal_51c633083c79004f3cb5e9e2b2f3504f650f1561800582801028bcbcf733a06b instanceof ArrayAccess ? ($__internal_51c633083c79004f3cb5e9e2b2f3504f650f1561800582801028bcbcf733a06b["module_template"] ?? null) : null), [".tpl" => ".twig"])), "wokiee/template/extension/module/advanced_grid/advanced_grid.twig", 72)->display($context);
                        echo " 
                                        ";
                    } else {
                        // line 73
                        echo " 
                                             Unknown error.
                                        ";
                    }
                    // line 75
                    echo " 
                                   ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 76
                echo " 
                              </div>
                         ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['column'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 79
            echo "                     </div>
               </div>
          </div>
     </div>
";
        }
    }

    public function getTemplateName()
    {
        return "wokiee/template/extension/module/advanced_grid/advanced_grid.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  415 => 79,  399 => 76,  384 => 75,  379 => 73,  374 => 72,  370 => 71,  366 => 70,  362 => 69,  358 => 68,  354 => 67,  350 => 66,  345 => 65,  343 => 64,  339 => 63,  336 => 62,  331 => 61,  327 => 60,  323 => 59,  319 => 58,  315 => 57,  311 => 56,  307 => 55,  303 => 54,  299 => 53,  296 => 52,  291 => 51,  287 => 50,  282 => 49,  280 => 48,  276 => 47,  272 => 46,  268 => 45,  264 => 44,  260 => 43,  241 => 42,  237 => 41,  234 => 40,  231 => 39,  228 => 38,  226 => 37,  221 => 36,  218 => 35,  216 => 34,  211 => 33,  208 => 32,  206 => 31,  202 => 30,  199 => 29,  196 => 28,  194 => 27,  190 => 26,  187 => 25,  184 => 24,  182 => 23,  178 => 22,  175 => 21,  172 => 20,  170 => 19,  165 => 18,  162 => 17,  160 => 16,  140 => 14,  136 => 13,  125 => 11,  113 => 10,  93 => 9,  59 => 8,  53 => 6,  51 => 5,  46 => 4,  43 => 3,  41 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "wokiee/template/extension/module/advanced_grid/advanced_grid.twig", "");
    }
}

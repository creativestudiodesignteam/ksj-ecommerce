<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* wokiee/template/extension/payment/pagseguro_boleto.twig */
class __TwigTemplate_f0d57199351e653a6199eb9fac7570590f6b1abf4dae33668762e8d83821543c extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<style>
.hidden {
   display: none
}
</style>

";
        // line 7
        if (($context["warning"] ?? null)) {
            // line 8
            echo "  <div class=\"alert alert-danger\" id=\"warning\" role=\"alert\">";
            echo ($context["warning"] ?? null);
            echo "</div>
";
        } else {
            // line 10
            echo "  <div id=\"warning\" class=\"alert alert-danger\" role=\"alert\" style=\"display: none\"></div>
  <div id=\"info\" class=\"alert alert-info\" role=\"alert\" style=\"display: none\">";
            // line 11
            echo ($context["text_wait"] ?? null);
            echo ".</div>

  <div class=\"col-sm-5 col-md-offset-3\">
    <div class=\"form-group col-md-12\">
      <label for=\"cpf\">";
            // line 15
            echo ($context["entry_cpf"] ?? null);
            echo "</label>
      <input type=\"text\" name=\"cpf\" id=\"cpf\" value=\"";
            // line 16
            echo ($context["cpf"] ?? null);
            echo "\" placeholder=\"";
            echo ($context["entry_cpf"] ?? null);
            echo "\" class=\"form-control\">
    </div>

    <div class=\"form-group col-md-12\">
      <button type=\"button\" id=\"button-confirm\" data-loading-text=\"";
            // line 20
            echo ($context["text_wait"] ?? null);
            echo "\" class=\"btn btn-primary pull-left col-sm-12\">
        <i class=\"fa fa-barcode\"></i>
        ";
            // line 22
            echo ($context["button_confirm"] ?? null);
            echo "
      </button>

      <button type=\"button\" id=\"button-print\" data-loading-text=\"";
            // line 25
            echo ($context["text_wait"] ?? null);
            echo "\" class=\"btn btn-primary pull-left hidden\">
        <i class=\"fa fa-barcode\"></i>
        ";
            // line 27
            echo ($context["btn_print"] ?? null);
            echo "
      </button>

      <button type=\"button\" id=\"button-download\" data-loading-text=\"";
            // line 30
            echo ($context["text_wait"] ?? null);
            echo "\" class=\"btn btn-link pull-right hidden\">
        <i class=\"fa fa-download\"></i>
        ";
            // line 32
            echo ($context["btn_download"] ?? null);
            echo "
      </button>
    </div>
  </div>

  <script src=\"https://cdn.jsdelivr.net/npm/jquery-colorbox@1.6.4/jquery.colorbox.min.js\" async></script>
  <link rel=\"stylesheet\" href=\"https://cdn.jsdelivr.net/npm/jquery-colorbox@1.6.4/example1/colorbox.css\">

  <script type=\"text/javascript\">
    /**
     * Link do boleto
     * Código da transação
     * Identificador do pedido
     */
    let paymentLink;
    let paymentCode;
    let hash;

    /**
     * Carrega script do PagSeguro
     */
    ((src) => {
      \$('#button-confirm').button('loading');

      const script = document.createElement('script')
      script.async = true
      script.src = src
      script.addEventListener('load', loadedScriptSuccess)
      script.addEventListener('error', loadedScriptFailed)
      document.head.appendChild(script)
    })('";
            // line 62
            echo ($context["javascript_src"] ?? null);
            echo "')

    /**
     * Configuração inicial
     */
    function loadedScriptSuccess() {
      PagSeguroDirectPayment.setSessionId('";
            // line 68
            echo ($context["session"] ?? null);
            echo "');

      PagSeguroDirectPayment.onSenderHashReady(function(response){
        if(response.status == 'error') {
          \$('#warning').text(response.message);
          return;
        }

        hash = response.senderHash

        \$('#button-confirm').button('reset');
      });

      document.querySelector('#cpf').addEventListener('input', formatCpf)
      document.querySelector('#button-confirm').addEventListener('click', createSale)
      document.querySelector('#button-print').addEventListener('click', printBoleto)
      document.querySelector('#button-download').addEventListener('click', downloadBoleto)
    }

    /**
     * Ação para quando o script não for carregado
     */
    function loadedScriptFailed() {
      alert('Error loaded script')
    }

    /**
     * Formata o número do CPF
     */
    function formatCpf(e) {
      e.target.value = e.target
        .value
        .replace(/\\D/g, '')
        .replace(/(\\d{3})(\\d{3})?(\\d{3})?(\\d{2})?/g, '\$1.\$2.\$3-\$4')
    }

    /**
     * Envia as informações para o PagSeguro e cria uma transação
     */
    function createSale() {
      \$('#warning').html('').hide()

      const body = new FormData();
      body.set('hash', hash)
      body.set('cpf', \$('#cpf').val())

      const request = new Request('";
            // line 114
            echo ($context["action_create_sale"] ?? null);
            echo "'.replace(/&amp;/, '&'), {
        method: 'POST',
        body
      })

      \$('#button-confirm').button('loading');

      fetch(request)
        .then((response) => response.json())
        .then((response) => {
          if (response.payment_link) {
            return requestSuccess(response)
          }

          requestFailed(response)
        })
        .catch(requestFailed)
        .then(() => {
          \$('#button-confirm').button('reset');
        })
    }

    /**
     * Exibe boleto num modal
     */
    function printBoleto() {
      \$.colorbox({
        iframe: true,
        open: true,
        href: paymentLink,
        innerWidth: '90%',
        innerHeight: '90%',
        onClosed: confirmPayment
      })
    }

    /**
     * Realiza download do boleto
     */
    function downloadBoleto() {
      const anchor = document.createElement('a')
      anchor.download = true
      anchor.href = `";
            // line 156
            echo ($context["action_download_boleto"] ?? null);
            echo "\${paymentLink}`.replace(/&amp;/, '&')
      anchor.addEventListener('click', confirmPayment)
      anchor.click()
      anchor.remove()
    }

    /**
     * Exibe os botoões visualizar e baixar boleto
     * Esconde o botão de confirmação e o campo de CPF
     */
    function requestSuccess(response) {
      if (response.errors) {
        return requestFailed(response)
      }

      paymentLink = response.payment_link
      paymentCode = response.code

      document.querySelector('#cpf').closest('.form-group').classList.toggle('hidden')
      document.querySelector('#button-confirm').classList.toggle('hidden')
      document.querySelector('#button-print').classList.toggle('hidden')
      document.querySelector('#button-download').classList.toggle('hidden')
    }

    /**
     * Exibe mensagem(ns) de erro
     */
    function requestFailed(response) {
      const ul = document.createElement('ul')

      try {
        const errors = response.errors

        for (let error in errors) {
          ul.insertAdjacentHTML('beforeEnd', `<li>\${errors[error]}</li>`)
        }
      } catch (e) {
        console.log(e)
        ul.insertAdjacentHTML('beforeEnd', `<li>\${response}</li>`)
      }

      \$('#warning').html(ul).show()
    }

    /**
     * Confirma pedido
     */
    function confirmPayment() {
      setTimeout(() => location.href = `";
            // line 204
            echo ($context["confirm"] ?? null);
            echo "\${paymentCode}`.replace(/&amp;/, '&'), 1000)
    }
  </script>
";
        }
    }

    public function getTemplateName()
    {
        return "wokiee/template/extension/payment/pagseguro_boleto.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  290 => 204,  239 => 156,  194 => 114,  145 => 68,  136 => 62,  103 => 32,  98 => 30,  92 => 27,  87 => 25,  81 => 22,  76 => 20,  67 => 16,  63 => 15,  56 => 11,  53 => 10,  47 => 8,  45 => 7,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "wokiee/template/extension/payment/pagseguro_boleto.twig", "");
    }
}

<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* wokiee/template/blog/article_detail/default.twig */
class __TwigTemplate_ed9fcf89561ff13eea9257b62104a3056990947c887899897d2b5dda65e3148c extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo " 
";
        // line 2
        $this->loadTemplate("wokiee/template/new_elements/wrapper_top.twig", "wokiee/template/blog/article_detail/default.twig", 2)->display($context);
        // line 3
        echo "
";
        // line 4
        if (((($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = ($context["settings"] ?? null)) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4["comments_engine"] ?? null) : null) == "FACEBOOK")) {
            // line 5
            echo "<div id=\"fb-root\"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = \"//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4\";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
";
        }
        // line 13
        echo " 

";
        // line 15
        $context["theme_options"] = twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "get", [0 => "theme_options"], "method", false, false, false, 15);
        // line 16
        $context["config"] = twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "get", [0 => "config"], "method", false, false, false, 16);
        echo " 
";
        // line 17
        $context["page_direction"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "page_direction"], "method", false, false, false, 17);
        echo " 
";
        // line 18
        $context["language_id"] = twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 18);
        // line 19
        echo "
";
        // line 20
        if ((twig_length_filter($this->env, ($context["article"] ?? null)) > 0)) {
            // line 21
            echo "  <div class=\"row justify-content-center\">
    <div class=\"col-xs-12 col-md-10 col-lg-8 col-md-auto\">
      <div class=\"tt-post-single\">
        <div class=\"tt-tag\">
          ";
            // line 25
            if (($context["tags"] ?? null)) {
                echo " 
              ";
                // line 26
                $context["i"] = 0;
                // line 27
                echo "                ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(range($context["i"], ($context["tags"] ?? null)));
                $context['loop'] = [
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                ];
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                    echo " 
                  ";
                    // line 28
                    if ((twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 28) == 1)) {
                        echo "<a href=\"";
                        echo (($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = (($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = ($context["tags"] ?? null)) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b[$context["i"]] ?? null) : null)) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144["href"] ?? null) : null);
                        echo "\">";
                        echo (($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 = (($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 = ($context["tags"] ?? null)) && is_array($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4) || $__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 instanceof ArrayAccess ? ($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4[$context["i"]] ?? null) : null)) && is_array($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002) || $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 instanceof ArrayAccess ? ($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002["tag"] ?? null) : null);
                        echo "</a>";
                    }
                    // line 29
                    echo "              ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                echo " 
          ";
            }
            // line 30
            echo " 
        </div>
        <h1 class=\"tt-title\">
          ";
            // line 33
            echo ($context["heading_title"] ?? null);
            echo "
        </h1>
        <div class=\"tt-autor\">
            <span class=\"month\">
                ";
            // line 37
            echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "date", [0 => "M", 1 => twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "strtotime", [0 => (($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 = ($context["article"] ?? null)) && is_array($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666) || $__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 instanceof ArrayAccess ? ($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666["date_published"] ?? null) : null)], "method", false, false, false, 37)], "method", false, false, false, 37);
            echo " 
            </span>
            <span class=\"day\">
                ";
            // line 40
            echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "date", [0 => "d", 1 => twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "strtotime", [0 => (($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e = ($context["article"] ?? null)) && is_array($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e) || $__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e instanceof ArrayAccess ? ($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e["date_published"] ?? null) : null)], "method", false, false, false, 40)], "method", false, false, false, 40);
            echo ",
            </span>
            <span class=\"day\">
                ";
            // line 43
            echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "date", [0 => "Y", 1 => twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "strtotime", [0 => (($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 = ($context["article"] ?? null)) && is_array($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52) || $__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 instanceof ArrayAccess ? ($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52["date_published"] ?? null) : null)], "method", false, false, false, 43)], "method", false, false, false, 43);
            echo " 
            </span>
        </div>
        <div class=\"tt-post-content\">
          ";
            // line 47
            if ((twig_length_filter($this->env, (($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 = ($context["article"] ?? null)) && is_array($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136) || $__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 instanceof ArrayAccess ? ($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136["gallery"] ?? null) : null)) > 0)) {
                // line 48
                echo "              ";
                if (((($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 = ($context["article"] ?? null)) && is_array($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386) || $__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 instanceof ArrayAccess ? ($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386["article_list_gallery_display"] ?? null) : null) == "CLASSIC")) {
                    // line 49
                    echo "                    ";
                    echo (($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9 = (($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae = (($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f = ($context["article"] ?? null)) && is_array($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f) || $__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f instanceof ArrayAccess ? ($__internal_25c0fab8152b8dd6b90603159c0f2e8a936a09ab76edb5e4d7bc95d9a8d2dc8f["gallery"] ?? null) : null)) && is_array($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae) || $__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae instanceof ArrayAccess ? ($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae[0] ?? null) : null)) && is_array($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9) || $__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9 instanceof ArrayAccess ? ($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9["output"] ?? null) : null);
                    echo "
              ";
                }
                // line 50
                echo " 
              ";
                // line 51
                if (((($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40 = ($context["article"] ?? null)) && is_array($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40) || $__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40 instanceof ArrayAccess ? ($__internal_f769f712f3484f00110c86425acea59f5af2752239e2e8596bcb6effeb425b40["article_list_gallery_display"] ?? null) : null) == "SLIDER")) {
                    // line 52
                    echo "                <div class=\"tt-slider-blog-single slick-animated-show-js\">
                  ";
                    // line 53
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable((($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f = ($context["article"] ?? null)) && is_array($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f) || $__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f instanceof ArrayAccess ? ($__internal_98e944456c0f58b2585e4aa36e3a7e43f4b7c9038088f0f056004af41f4a007f["gallery"] ?? null) : null));
                    foreach ($context['_seq'] as $context["_key"] => $context["gallery"]) {
                        // line 54
                        echo "                  <div>";
                        echo (($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760 = $context["gallery"]) && is_array($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760) || $__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760 instanceof ArrayAccess ? ($__internal_a06a70691a7ca361709a372174fa669f5ee1c1e4ed302b3a5b61c10c80c02760["output"] ?? null) : null);
                        echo "</div>
                  ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['gallery'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 55
                    echo " 
                </div>
                <div class=\"tt-slick-row\">
                  <div class=\"item\">
                    <div class=\"tt-slick-quantity\">
                      <span class=\"account-number\">1</span> / <span class=\"total\">1</span>
                    </div>
                  </div>
                  <div class=\"item\">
                    <div class=\"tt-slick-button\">
                      <button type=\"button\" class=\"slick-arrow slick-prev\">Previous</button>
                      <button type=\"button\" class=\"slick-arrow slick-next\">Next</button>
                    </div>
                  </div>
                </div>
              ";
                }
                // line 70
                echo " 
          ";
            }
            // line 71
            echo " 
          ";
            // line 72
            echo (($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce = ($context["article"] ?? null)) && is_array($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce) || $__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce instanceof ArrayAccess ? ($__internal_653499042eb14fd8415489ba6fa87c1e85cff03392e9f57b26d0da09b9be82ce["content"] ?? null) : null);
            echo "
        </div>
        <div class=\"post-meta\">
          <span class=\"item\">
            ";
            // line 76
            if (($context["tags"] ?? null)) {
                echo " 
                ";
                // line 77
                $context["i"] = 0;
                // line 78
                echo "                  ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(range($context["i"], ($context["tags"] ?? null)));
                $context['loop'] = [
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                ];
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                    echo " 
                    <a href=\"";
                    // line 79
                    echo (($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b = (($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c = ($context["tags"] ?? null)) && is_array($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c) || $__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c instanceof ArrayAccess ? ($__internal_73db8eef4d2582468dab79a6b09c77ce3b48675a610afd65a1f325b68804a60c[$context["i"]] ?? null) : null)) && is_array($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b) || $__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b instanceof ArrayAccess ? ($__internal_ba9f0a3bb95c082f61c9fbf892a05514d732703d52edc77b51f2e6284135900b["href"] ?? null) : null);
                    echo "\">";
                    echo (($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972 = (($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216 = ($context["tags"] ?? null)) && is_array($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216) || $__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216 instanceof ArrayAccess ? ($__internal_df39c71428eaf37baa1ea2198679e0077f3699bdd31bb5ba10d084710b9da216[$context["i"]] ?? null) : null)) && is_array($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972) || $__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972 instanceof ArrayAccess ? ($__internal_d8ad5934f1874c52fa2ac9a4dfae52038b39b8b03cfc82eeb53de6151d883972["tag"] ?? null) : null);
                    echo "</a>";
                    if ( !twig_get_attribute($this->env, $this->source, $context["loop"], "last", [], "any", false, false, false, 79)) {
                        echo ",";
                    }
                    // line 80
                    echo "                ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                echo " 
            ";
            }
            // line 81
            echo " 
          </span>
        </div>
      </div>

      ";
            // line 86
            if (((twig_length_filter($this->env, ($context["articles"] ?? null)) > 0) && ((($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0 = ($context["settings"] ?? null)) && is_array($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0) || $__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0 instanceof ArrayAccess ? ($__internal_bf0e189d688dc2ad611b50a437a32d3692fb6b8be90d2228617cfa6db44e75c0["article_related_status"] ?? null) : null) == 1))) {
                echo " 
        ";
                // line 87
                $this->loadTemplate(("wokiee/template/blog/article_related/" . twig_replace_filter((($__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c = ($context["settings"] ?? null)) && is_array($__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c) || $__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c instanceof ArrayAccess ? ($__internal_674c0abf302105af78b0a38907d86c5dd0028bdc3ee5f24bf52771a16487760c["article_related_template"] ?? null) : null), [".tpl" => ".twig"])), "wokiee/template/blog/article_detail/default.twig", 87)->display($context);
                echo " 
      ";
            }
            // line 88
            echo "  

       ";
            // line 90
            if (((twig_length_filter($this->env, ($context["products"] ?? null)) > 0) && ((($__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f = ($context["settings"] ?? null)) && is_array($__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f) || $__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f instanceof ArrayAccess ? ($__internal_dd839fbfcab68823c49af471c7df7659a500fe72e71b58d6b80d896bdb55e75f["product_related_status"] ?? null) : null) == 1))) {
                echo " 
          <div class=\"comments-single-post\" style=\"margin-top: 52px\">
            <h6 class=\"tt-title-border\">";
                // line 92
                echo ($context["text_related_products"] ?? null);
                echo "</h6>
            <div class=\"row tt-layout-product-item\">
                ";
                // line 94
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["products"] ?? null));
                $context['loop'] = [
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                ];
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                    // line 95
                    echo "                  ";
                    if ((twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 95) < 4)) {
                        // line 96
                        echo "                    <div class=\"col-6 col-md-4\">
                      ";
                        // line 97
                        $this->loadTemplate("wokiee/template/new_elements/product.twig", "wokiee/template/blog/article_detail/default.twig", 97)->display($context);
                        // line 98
                        echo "                    </div>
                  ";
                    }
                    // line 100
                    echo "                ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                echo " 
            </div>
          </div>
       ";
            }
            // line 103
            echo "  

      ";
            // line 105
            if ((($__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc = ($context["article"] ?? null)) && is_array($__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc) || $__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc instanceof ArrayAccess ? ($__internal_a7ed47878554bdc32b70e1ba5ccc67d2302196876fbf62b4c853b20cb9e029fc["status_comments"] ?? null) : null)) {
                echo " 
        ";
                // line 106
                if (((($__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55 = ($context["settings"] ?? null)) && is_array($__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55) || $__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55 instanceof ArrayAccess ? ($__internal_e5d7b41e16b744b68da1e9bb49047b8028ced86c782900009b4b4029b83d4b55["comments_engine"] ?? null) : null) == "LOCAL")) {
                    // line 107
                    echo "            <div class=\"comments-single-post\" style=\"margin-top: 52px\">
              <h6 class=\"tt-title-border\">";
                    // line 108
                    echo ($context["text_comments"] ?? null);
                    echo ": ";
                    echo (($__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba = ($context["article"] ?? null)) && is_array($__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba) || $__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba instanceof ArrayAccess ? ($__internal_9e93f398968fa0576dce82fd00f280e95c734ad3f84e7816ff09158ae224f5ba["comments_count"] ?? null) : null);
                    echo "</h6>
                 <div class=\"box-content\">
                      ";
                    // line 110
                    if ((twig_length_filter($this->env, ($context["comments"] ?? null)) > 0)) {
                        // line 111
                        echo "                      <div class=\"tt-comments-layout\">
                          ";
                        // line 112
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(($context["comments"] ?? null));
                        foreach ($context['_seq'] as $context["_key"] => $context["comment"]) {
                            // line 113
                            echo "                            <div class=\"tt-item\">
                              <div class=\"tt-comments-level-1\">
                                <div class=\"tt-avatar\"></div>
                                <div class=\"tt-content\">
                                  <div class=\"tt-comments-title\">
                                    <span class=\"username\">by <span>";
                            // line 118
                            echo (($__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78 = $context["comment"]) && is_array($__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78) || $__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78 instanceof ArrayAccess ? ($__internal_0795e3de58b6454b051261c0c2b5be48852e17f25b59d4aeef29fb07c614bd78["name"] ?? null) : null);
                            echo "</span></span>
                                    <span class=\"time\">";
                            // line 119
                            echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "date", [0 => "d-m-Y H:i", 1 => twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "strtotime", [0 => (($__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de = $context["comment"]) && is_array($__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de) || $__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de instanceof ArrayAccess ? ($__internal_fecb0565c93d0b979a95c352ff76e401e0ae0c73bb8d3b443c8c6133e1c190de["date_added"] ?? null) : null)], "method", false, false, false, 119)], "method", false, false, false, 119);
                            echo "</span>
                                  </div>
                                  <p>
                                    ";
                            // line 122
                            echo (($__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828 = $context["comment"]) && is_array($__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828) || $__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828 instanceof ArrayAccess ? ($__internal_87570a635eac7f6e150744bd218085d17aff15d92d9c80a66d3b911e3355b828["content"] ?? null) : null);
                            echo " 
                                  </p>
                                </div>
                              </div>
                            </div>
                          ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['comment'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 127
                        echo " 
                      </div> 
                      ";
                    } else {
                        // line 129
                        echo " 
                      <p>";
                        // line 130
                        echo ($context["text_no_comments"] ?? null);
                        echo "</p>
                      ";
                    }
                    // line 131
                    echo " 
                </div>
            </div>
            
            <div class=\"comments-single-post leave-reply\" style=\"margin-top: 52px\">
              <h6 class=\"tt-title-border\">";
                    // line 136
                    echo ($context["text_leave_reply"] ?? null);
                    echo "</h6>
                 <div class=\"box-content\">
                       <p style=\"padding-bottom: 20px\">";
                    // line 138
                    echo ($context["text_required_info"] ?? null);
                    echo "<abbr class=\"required\">*</abbr> </p>
                       <div class=\"form-default\">
                         <form class=\"form-horizontal\" method=\"post\" id=\"form-comment\">
                             <div class=\"form-group required\">
                                 <label class=\"control-label\" for=\"input-name\">";
                    // line 142
                    echo ($context["text_name"] ?? null);
                    echo " *</label>
                                 <input type=\"text\" name=\"name\" value=\"\" id=\"input-name\" class=\"form-control\">
                              </div>
                              <div class=\"form-group required\">
                                 <label class=\"control-label\" for=\"input-email\">";
                    // line 146
                    echo ($context["text_email"] ?? null);
                    echo " *</label>
                                 <input type=\"text\" name=\"email\" value=\"\" id=\"input-email\" class=\"form-control\">
                             </div>
                             <div class=\"form-group required\">
                                 <label class=\"control-label\" for=\"input-content\">";
                    // line 150
                    echo ($context["text_content"] ?? null);
                    echo " *</label>
                                 <textarea rows=\"10\" id=\"input-content\" name=\"content\" class=\"form-control\"></textarea>
                             </div>
         
                                <button class=\"btn\" id=\"button-comment\" type=\"submit\">";
                    // line 154
                    echo ($context["button_post_comment"] ?? null);
                    echo "</button>
                         </form>
                       </div>
                 </div>
                 
            </div>
            ";
                }
                // line 160
                echo " 

            ";
                // line 162
                if (((($__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd = ($context["settings"] ?? null)) && is_array($__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd) || $__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd instanceof ArrayAccess ? ($__internal_17b5b5f9aaeec4b528bfeed02b71f624021d6a52d927f441de2f2204d0e527cd["comments_engine"] ?? null) : null) == "DISQUS")) {
                    // line 163
                    echo "              <div style=\"height: 58px\"></div>
              <div id=\"disqus_thread\"></div>
              <script type=\"text/javascript\">
                  /* * * CONFIGURATION VARIABLES * * */
                  var disqus_shortname = '";
                    // line 167
                    echo (($__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6 = ($context["settings"] ?? null)) && is_array($__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6) || $__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6 instanceof ArrayAccess ? ($__internal_0db9a23306660395861a0528381e0668025e56a8a99f399e9ec23a4b392422d6["disqus_name"] ?? null) : null);
                    echo "';

                  /* * * DON'T EDIT BELOW THIS LINE * * */
                  (function() {
                      var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
                      dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
                      (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
                  })();
              </script>
              <noscript>Please enable JavaScript to view the <a href=\"https://disqus.com/?ref_noscript\" rel=\"nofollow\">comments powered by Disqus.</a></noscript>
            ";
                }
                // line 177
                echo " 

            ";
                // line 179
                if (((($__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855 = ($context["settings"] ?? null)) && is_array($__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855) || $__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855 instanceof ArrayAccess ? ($__internal_0a23ad2f11a348e49c87410947e20d5a4e711234ce49927662da5dddac687855["comments_engine"] ?? null) : null) == "FACEBOOK")) {
                    // line 180
                    echo "                 <div style=\"height: 58px\"></div>
                <div class=\"fb-comments\" data-href=\"";
                    // line 181
                    echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "facebook", [], "method", false, false, false, 181);
                    echo "\" data-width=\"100%\"></div>
            ";
                }
                // line 182
                echo " 
        ";
            }
            // line 184
            echo "    </div>
  </div>

<script>
    \$(function(){
        \$('#button-comment').on('click', function(e) {
            e.preventDefault();
            \$.ajax({
                url: 'index.php?route=blog/article/write&article_id=";
            // line 192
            echo ($context["article_id"] ?? null);
            echo "',
                type: 'post',
                dataType: 'json',
                data: \$(\"#form-comment\").serialize(),
                beforeSend: function() {
                    \$('#button-comment').button('loading');
                },
                complete: function() {
                    \$('#button-comment').button('reset');
                },
                success: function(json) {
                    \$('.alert-success, .alert-danger').remove();

                    if (json['error']) {
                        \$('.leave-reply .box-content > *:first-child').before('<div class=\"alert alert-danger\"><i class=\"fa fa-exclamation-circle\"></i> ' + json['error'] + '</div>');
                    }

                    if (json['success']) {
                        \$('.leave-reply .box-content > *:first-child').before('<div class=\"alert alert-success\"><i class=\"fa fa-check-circle\"></i> ' + json['success'] + '</div>');

                        \$('input[name=\\'name\\']').val('');
                        \$('input[name=\\'email\\']').val('');
                        \$('textarea[name=\\'content\\']').val('');
                    }
                }
            });
        });
    });
</script>

";
        }
        // line 223
        echo "
";
        // line 224
        $this->loadTemplate("wokiee/template/new_elements/wrapper_bottom.twig", "wokiee/template/blog/article_detail/default.twig", 224)->display($context);
        // line 225
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "wokiee/template/blog/article_detail/default.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  575 => 225,  573 => 224,  570 => 223,  536 => 192,  526 => 184,  522 => 182,  517 => 181,  514 => 180,  512 => 179,  508 => 177,  494 => 167,  488 => 163,  486 => 162,  482 => 160,  472 => 154,  465 => 150,  458 => 146,  451 => 142,  444 => 138,  439 => 136,  432 => 131,  427 => 130,  424 => 129,  419 => 127,  407 => 122,  401 => 119,  397 => 118,  390 => 113,  386 => 112,  383 => 111,  381 => 110,  374 => 108,  371 => 107,  369 => 106,  365 => 105,  361 => 103,  342 => 100,  338 => 98,  336 => 97,  333 => 96,  330 => 95,  313 => 94,  308 => 92,  303 => 90,  299 => 88,  294 => 87,  290 => 86,  283 => 81,  266 => 80,  258 => 79,  238 => 78,  236 => 77,  232 => 76,  225 => 72,  222 => 71,  218 => 70,  200 => 55,  191 => 54,  187 => 53,  184 => 52,  182 => 51,  179 => 50,  173 => 49,  170 => 48,  168 => 47,  161 => 43,  155 => 40,  149 => 37,  142 => 33,  137 => 30,  120 => 29,  112 => 28,  92 => 27,  90 => 26,  86 => 25,  80 => 21,  78 => 20,  75 => 19,  73 => 18,  69 => 17,  65 => 16,  63 => 15,  59 => 13,  48 => 5,  46 => 4,  43 => 3,  41 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "wokiee/template/blog/article_detail/default.twig", "");
    }
}

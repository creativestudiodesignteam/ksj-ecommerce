<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* extension/payment/pagseguro.twig */
class __TwigTemplate_3b01c20ba86d2c29bcb9137409b8f8b2c968be252da9b2a08f7ef0afbed7827a extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo ($context["column_left"] ?? null);
        echo "
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <h1>";
        // line 5
        echo ($context["heading_title"] ?? null);
        echo "</h1>

      <ul class=\"breadcrumb\">
        ";
        // line 8
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 9
            echo "        <li><a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 9);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 9);
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 11
        echo "      </ul>

      <div class=\"pull-right\">
        <div class=\"btn-group\">
          <button class=\"btn btn-primary dropdown-toggle\" data-toggle=\"dropdown\">
            <i class=\"fa fa-question\"></i>
            ";
        // line 17
        echo ($context["text_question"] ?? null);
        echo "
          </button>

          <ul class=\"dropdown-menu\">
            <li><a href=\"https://github.com/opencart-extension/PagSeguro-Checkout-Transparente\" target=\"_blank\">GitHub</a></li>
            <li><a href=\"https://opencart-extension.github.io/PagSeguro-Checkout-Transparente\" target=\"_blank\">Documentação</a></li>
            <li role=\"separator\" class=\"divider\"></li>
            <li><a href=\"https://www.valdeirsantana.com.br\" target=\"_blank\">Portfolio</a></li>
          </ul>
        </div>

        <button class=\"btn btn-danger\" id=\"debug-clear\" data-toggle=\"tooltip\" title=\"";
        // line 28
        echo ($context["text_debug_clear"] ?? null);
        echo "\"><i class=\"fa fa-trash\"></i></button>
        <button class=\"btn btn-success\" id=\"debug-download\" data-toggle=\"tooltip\" title=\"";
        // line 29
        echo ($context["text_debug_download"] ?? null);
        echo "\"><i class=\"fa fa-download\"></i></button>
        <button type=\"submit\" form=\"form-payment\" data-toggle=\"tooltip\" title=\"";
        // line 30
        echo ($context["button_save"] ?? null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-save\"></i></button>
        <a href=\"";
        // line 31
        echo ($context["cancel"] ?? null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo ($context["button_cancel"] ?? null);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-reply\"></i></a>
      </div>
    </div>
  </div>

  <div class=\"container-fluid\">
    <div class=\"panel panel-default\">
      <div class=\"panel-heading\">
        <h3 class=\"panel-title\">
          <i class=\"fa fa-pencil\"></i>
          ";
        // line 41
        echo ($context["heading_title"] ?? null);
        echo "
        </h3>
      </div>

      <div class=\"panel-body\">
        <!-- ---- -->
        <!-- Tabs -->
        <!-- ---- -->
        <ul class=\"nav nav-tabs\" role=\"tablist\">
          <li class=\"active\"><a href=\"#tab-home\" data-toggle=\"tab\">";
        // line 50
        echo ($context["tab_home"] ?? null);
        echo "</a></li>
          <li><a href=\"#tab-custom-fields\" data-toggle=\"tab\">";
        // line 51
        echo ($context["tab_custom_fields"] ?? null);
        echo "</a></li>
          <li><a href=\"#tab-discount\" data-toggle=\"tab\">";
        // line 52
        echo ($context["tab_discount"] ?? null);
        echo "</a></li>
          <li><a href=\"#tab-order-status\" data-toggle=\"tab\">";
        // line 53
        echo ($context["tab_order_status"] ?? null);
        echo "</a></li>
          <li><a href=\"#tab-geo\" data-toggle=\"tab\">";
        // line 54
        echo ($context["tab_geo"] ?? null);
        echo "</a></li>
          <li><a href=\"#tab-installment\" data-toggle=\"tab\">";
        // line 55
        echo ($context["tab_installment"] ?? null);
        echo "</a></li>
          <li><a href=\"#tab-methods\" data-toggle=\"tab\">";
        // line 56
        echo ($context["tab_methods"] ?? null);
        echo "</a></li>
          <li><a href=\"#tab-layout\" data-toggle=\"tab\">";
        // line 57
        echo ($context["tab_layout"] ?? null);
        echo "</a></li>
          <li><a href=\"#tab-debug\" data-toggle=\"tab\">";
        // line 58
        echo ($context["tab_debug"] ?? null);
        echo "</a></li>
        </ul>

        <form action=\"";
        // line 61
        echo ($context["action"] ?? null);
        echo "\" class=\"form form-horizontal tab-content\" id=\"form-payment\" method=\"POST\" psr>
          <!-- -------- -->
          <!-- Tab Home -->
          <!-- -------- -->
          <div class=\"tab-pane active\" id=\"tab-home\">
            <div class=\"form-group required\">
              <label for=\"input-status\" class=\"control-label col-sm-2\">";
        // line 67
        echo ($context["entry_status"] ?? null);
        echo "</label>
              <div class=\"col-sm-10\">
                <select name=\"status\" id=\"input-status\" class=\"form-control\">
                  <option value=\"1\" ";
        // line 70
        echo (($context["status"]) ?? ("selected"));
        echo ">";
        echo ($context["text_enabled"] ?? null);
        echo "</option>
                  <option value=\"0\" ";
        // line 71
        echo ((($context["status"] ?? null)) ? (($context["status"] ?? null)) : ("selected"));
        echo ">";
        echo ($context["text_disabled"] ?? null);
        echo "</option>
                </select>
                ";
        // line 73
        if (($context["error_field_status"] ?? null)) {
            // line 74
            echo "                <span class=\"text-danger\">";
            echo ($context["error_field_status"] ?? null);
            echo "</span>
                ";
        }
        // line 76
        echo "              </div>
            </div>

            <div class=\"form-group required\">
              <label for=\"input-email\" class=\"control-label col-sm-2\">";
        // line 80
        echo ($context["entry_email"] ?? null);
        echo "</label>
              <div class=\"col-sm-10\">
                <input type=\"email\" name=\"email\" id=\"input-email\" value=\"";
        // line 82
        echo ($context["email"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_email"] ?? null);
        echo "\" class=\"form-control\" required />
                ";
        // line 83
        if ((isset($context["error_field_email"]) || array_key_exists("error_field_email", $context))) {
            // line 84
            echo "                <span class=\"text-danger\">";
            echo ($context["error_field_email"] ?? null);
            echo "</span>
                ";
        }
        // line 86
        echo "              </div>
            </div>

            <div class=\"form-group required\">
              <label for=\"input-token\" class=\"control-label col-sm-2\">";
        // line 90
        echo ($context["entry_token"] ?? null);
        echo "</label>
              <div class=\"col-sm-10\">
                <input type=\"text\" name=\"token\" id=\"input-token\" value=\"";
        // line 92
        echo ($context["token"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_token"] ?? null);
        echo "\" class=\"form-control\" required />
                ";
        // line 93
        if (($context["error_field_token"] ?? null)) {
            // line 94
            echo "                <span class=\"text-danger\">";
            echo ($context["error_field_token"] ?? null);
            echo "</span>
                ";
        }
        // line 96
        echo "              </div>
            </div>

            <div class=\"form-group required\">
              <label for=\"input-sandbox\" class=\"control-label col-sm-2\">";
        // line 100
        echo ($context["entry_sandbox"] ?? null);
        echo "</label>
              <div class=\"col-sm-10\">
                <select name=\"sandbox\" id=\"input-sandbox\" class=\"form-control\">
                  <option value=\"1\" ";
        // line 103
        echo (($context["sandbox"]) ?? ("selected"));
        echo ">";
        echo ($context["text_enabled"] ?? null);
        echo "</option>
                  <option value=\"0\" ";
        // line 104
        echo ((($context["sandbox"] ?? null)) ? (($context["sandbox"] ?? null)) : ("selected"));
        echo ">";
        echo ($context["text_disabled"] ?? null);
        echo "</option>
                </select>
                ";
        // line 106
        if (($context["error_field_sandbox"] ?? null)) {
            // line 107
            echo "                <span class=\"text-danger\">";
            echo ($context["error_field_sandbox"] ?? null);
            echo "</span>
                ";
        }
        // line 109
        echo "              </div>
            </div>

            <div class=\"form-group required\">
              <label for=\"input-debug\" class=\"control-label col-sm-2\">";
        // line 113
        echo ($context["entry_debug"] ?? null);
        echo "</label>
              <div class=\"col-sm-10\">
                <select name=\"debug\" id=\"input-debug\" class=\"form-control\">
                  <option value=\"1\" ";
        // line 116
        echo (($context["debug"]) ?? ("selected"));
        echo ">";
        echo ($context["text_enabled"] ?? null);
        echo "</option>
                  <option value=\"0\" ";
        // line 117
        echo ((($context["debug"] ?? null)) ? (($context["debug"] ?? null)) : ("selected"));
        echo ">";
        echo ($context["text_disabled"] ?? null);
        echo "</option>
                </select>
              </div>
            </div>

            <div class=\"form-group required\">
              <label for=\"input-customer-notify\" class=\"control-label col-sm-2\">";
        // line 123
        echo ($context["entry_customer_notify"] ?? null);
        echo "</label>
              <div class=\"col-sm-10\">
                <select name=\"customer_notify\" id=\"input-customer-notify\" class=\"form-control\">
                  <option value=\"1\" ";
        // line 126
        echo (($context["customer_notify"]) ?? ("selected"));
        echo ">";
        echo ($context["text_enabled"] ?? null);
        echo "</option>
                  <option value=\"0\" ";
        // line 127
        echo ((($context["customer_notify"] ?? null)) ? (($context["customer_notify"] ?? null)) : ("selected"));
        echo ">";
        echo ($context["text_disabled"] ?? null);
        echo "</option>
                </select>
                ";
        // line 129
        if (($context["error_field_notify"] ?? null)) {
            // line 130
            echo "                <span class=\"text-danger\">";
            echo ($context["error_field_notify"] ?? null);
            echo "</span>
                ";
        }
        // line 132
        echo "              </div>
            </div>

            <div class=\"form-group required\">
              <label for=\"input-customer-notify\" class=\"control-label col-sm-2\">";
        // line 136
        echo ($context["entry_callback_token"] ?? null);
        echo "</label>
              <div class=\"col-sm-10\">
                <input type=\"text\" name=\"callback_token\" value=\"";
        // line 138
        echo ($context["callback_token"] ?? null);
        echo "\" class=\"form-control\">
                ";
        // line 139
        if (($context["error_field_callback_token"] ?? null)) {
            // line 140
            echo "                <span class=\"text-danger\">";
            echo ($context["error_field_callback_token"] ?? null);
            echo "</span>
                ";
        }
        // line 142
        echo "              </div>
            </div>

            <div class=\"form-group required\">
              <label for=\"input-telemetry\" class=\"control-label col-sm-2\">";
        // line 146
        echo ($context["entry_telemetry"] ?? null);
        echo "</label>
              <div class=\"col-sm-10\">
                <select name=\"telemetry\" id=\"input-telemetry\" class=\"form-control\">
                  <option value=\"1\">";
        // line 149
        echo ($context["text_enabled"] ?? null);
        echo "</option>
                  <option value=\"0\">";
        // line 150
        echo ($context["text_disabled"] ?? null);
        echo "</option>
                </select>
              </div>
            </div>

            <div class=\"form-group\">
              <label for=\"input-newsletter\" class=\"control-label col-sm-2\">";
        // line 156
        echo ($context["entry_newsletter"] ?? null);
        echo "</label>
              <div class=\"col-sm-10\">
                <input type=\"text\" name=\"newsletter\" id=\"input-newsletter\" class=\"form-control\" />
              </div>
            </div>
          </div>

          <!-- ----------------- -->
          <!-- Tab Custom Fields -->
          <!-- ----------------- -->
          <div class=\"tab-pane\" id=\"tab-custom-fields\">
            <div class=\"form-group required\">
              <label for=\"input-custom-fields-cpf\" class=\"control-label col-sm-2\">";
        // line 168
        echo ($context["entry_custom_field_cpf"] ?? null);
        echo "</label>
              <div class=\"col-sm-10\">
                <select name=\"custom_fields_cpf\" id=\"input-custom-fields-cpf\" class=\"form-control\">
                  <option value=\"\">";
        // line 171
        echo ($context["text_none"] ?? null);
        echo "</option>

                  ";
        // line 173
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["custom_fields"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["custom_field"]) {
            // line 174
            echo "                    ";
            if ((twig_get_attribute($this->env, $this->source, $context["custom_field"], "custom_field_id", [], "any", false, false, false, 174) == ($context["custom_fields_cpf"] ?? null))) {
                // line 175
                echo "                    <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["custom_field"], "custom_field_id", [], "any", false, false, false, 175);
                echo "\" selected>";
                echo twig_get_attribute($this->env, $this->source, $context["custom_field"], "name", [], "any", false, false, false, 175);
                echo "</option>
                    ";
            } else {
                // line 177
                echo "                    <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["custom_field"], "custom_field_id", [], "any", false, false, false, 177);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["custom_field"], "name", [], "any", false, false, false, 177);
                echo "</option>
                    ";
            }
            // line 179
            echo "                  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['custom_field'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 180
        echo "                </select>
                ";
        // line 181
        if (($context["error_field_custom_fields_cpf"] ?? null)) {
            // line 182
            echo "                <span class=\"text-danger\">";
            echo ($context["error_field_custom_fields_cpf"] ?? null);
            echo "</span>
                ";
        }
        // line 184
        echo "              </div>
            </div>

            <div class=\"form-group required\">
              <label for=\"input-custom-fields-number\" class=\"control-label col-sm-2\">";
        // line 188
        echo ($context["entry_custom_field_address_number"] ?? null);
        echo "</label>
              <div class=\"col-sm-10\">
                <select name=\"custom_fields_number\" id=\"input-custom-fields-number\" class=\"form-control\">
                  <option value=\"\">";
        // line 191
        echo ($context["text_none"] ?? null);
        echo "</option>

                  ";
        // line 193
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["custom_fields"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["custom_field"]) {
            // line 194
            echo "                    ";
            if ((twig_get_attribute($this->env, $this->source, $context["custom_field"], "custom_field_id", [], "any", false, false, false, 194) == ($context["custom_fields_number"] ?? null))) {
                // line 195
                echo "                    <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["custom_field"], "custom_field_id", [], "any", false, false, false, 195);
                echo "\" selected>";
                echo twig_get_attribute($this->env, $this->source, $context["custom_field"], "name", [], "any", false, false, false, 195);
                echo "</option>
                    ";
            } else {
                // line 197
                echo "                    <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["custom_field"], "custom_field_id", [], "any", false, false, false, 197);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["custom_field"], "name", [], "any", false, false, false, 197);
                echo "</option>
                    ";
            }
            // line 199
            echo "                  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['custom_field'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 200
        echo "                </select>
                ";
        // line 201
        if (($context["error_field_custom_fields_number"] ?? null)) {
            // line 202
            echo "                <span class=\"text-danger\">";
            echo ($context["error_field_custom_fields_number"] ?? null);
            echo "</span>
                ";
        }
        // line 204
        echo "              </div>
            </div>

            <div class=\"form-group\">
              <label for=\"input-custom-fields-birthday\" class=\"control-label col-sm-2\">";
        // line 208
        echo ($context["entry_custom_field_birthdate"] ?? null);
        echo "</label>
              <div class=\"col-sm-10\">
                <select name=\"custom_fields_birthday\" id=\"input-custom-fields-birthday\" class=\"form-control\">
                  <option value=\"\">";
        // line 211
        echo ($context["text_none"] ?? null);
        echo "</option>

                  ";
        // line 213
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["custom_fields"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["custom_field"]) {
            // line 214
            echo "                    ";
            if ((twig_get_attribute($this->env, $this->source, $context["custom_field"], "custom_field_id", [], "any", false, false, false, 214) == ($context["custom_fields_birthday"] ?? null))) {
                // line 215
                echo "                    <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["custom_field"], "custom_field_id", [], "any", false, false, false, 215);
                echo "\" selected>";
                echo twig_get_attribute($this->env, $this->source, $context["custom_field"], "name", [], "any", false, false, false, 215);
                echo "</option>
                    ";
            } else {
                // line 217
                echo "                    <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["custom_field"], "custom_field_id", [], "any", false, false, false, 217);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["custom_field"], "name", [], "any", false, false, false, 217);
                echo "</option>
                    ";
            }
            // line 219
            echo "                  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['custom_field'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 220
        echo "                </select>
                ";
        // line 221
        if (($context["error_field_custom_fields_birthday"] ?? null)) {
            // line 222
            echo "                <span class=\"text-danger\">";
            echo ($context["error_field_custom_fields_birthday"] ?? null);
            echo "</span>
                ";
        }
        // line 224
        echo "              </div>
            </div>
          </div>

          <!-- ------------ -->
          <!-- Tab Discount -->
          <!-- ------------ -->
          <div class=\"tab-pane\" id=\"tab-discount\">
            <fieldset>
              <legend>";
        // line 233
        echo ($context["text_discount"] ?? null);
        echo "</legend>

              <div class=\"form-group\">
                <label for=\"input-discount-boleto\" class=\"control-label col-sm-2\">";
        // line 236
        echo ($context["entry_discount_boleto"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <input type=\"text\" name=\"discount_boleto\" value=\"";
        // line 238
        echo ($context["discount_boleto"] ?? null);
        echo "\" id=\"input-discount-boleto\" class=\"form-control\" />
                </div>
              </div>

              <div class=\"form-group\">
                <label for=\"input-discount-credit\" class=\"control-label col-sm-2\">";
        // line 243
        echo ($context["entry_discount_credit"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <input type=\"text\" name=\"discount_credit\" value=\"";
        // line 245
        echo ($context["discount_credit"] ?? null);
        echo "\" id=\"input-discount-credit\" class=\"form-control\" />
                </div>
              </div>

              <div class=\"form-group\">
                <label for=\"input-discount-debit\" class=\"control-label col-sm-2\">";
        // line 250
        echo ($context["entry_discount_debit"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <input type=\"text\" name=\"discount_debit\" value=\"";
        // line 252
        echo ($context["discount_debit"] ?? null);
        echo "\" id=\"input-discount-debit\" class=\"form-control\" />
                </div>
              </div>
            </fieldset>

            <fieldset>
              <legend>";
        // line 258
        echo ($context["text_fee"] ?? null);
        echo "</legend>

              <div class=\"form-group\">
                <label for=\"input-fee-boleto\" class=\"control-label col-sm-2\">";
        // line 261
        echo ($context["entry_fee_boleto"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <input type=\"text\" name=\"fee_boleto\" value=\"";
        // line 263
        echo ($context["fee_boleto"] ?? null);
        echo "\" id=\"input-fee-boleto\" class=\"form-control\" />
                </div>
              </div>

              <div class=\"form-group\">
                <label for=\"input-fee-credit\" class=\"control-label col-sm-2\">";
        // line 268
        echo ($context["entry_fee_credit"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <input type=\"text\" name=\"fee_credit\" value=\"";
        // line 270
        echo ($context["fee_credit"] ?? null);
        echo "\" id=\"input-fee-credit\" class=\"form-control\" />
                </div>
              </div>

              <div class=\"form-group\">
                <label for=\"input-fee-debit\" class=\"control-label col-sm-2\">";
        // line 275
        echo ($context["entry_fee_debit"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <input type=\"text\" name=\"fee_debit\" value=\"";
        // line 277
        echo ($context["fee_debit"] ?? null);
        echo "\" id=\"input-fee-debit\" class=\"form-control\" />
                </div>
              </div>
            </fieldset>
          </div>

          <!-- ---------------- -->
          <!-- Tab Order Status -->
          <!-- ---------------- -->
          <div class=\"tab-pane\" id=\"tab-order-status\">
            <div class=\"form-group required\">
              <label for=\"input-order-status-pending\" class=\"control-label col-sm-2\">";
        // line 288
        echo ($context["entry_order_status_pending"] ?? null);
        echo "</label>
              <div class=\"col-sm-10\">
                <select name=\"order_status_pending\" id=\"input-order-status-pending\" class=\"form-control\">
                  <option value=\"\">";
        // line 291
        echo ($context["text_none"] ?? null);
        echo "</option>

                  ";
        // line 293
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["statuses"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["status"]) {
            // line 294
            echo "                    ";
            if ((twig_get_attribute($this->env, $this->source, $context["status"], "order_status_id", [], "any", false, false, false, 294) == ($context["order_status_pending"] ?? null))) {
                // line 295
                echo "                    <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["status"], "order_status_id", [], "any", false, false, false, 295);
                echo "\" selected>";
                echo twig_get_attribute($this->env, $this->source, $context["status"], "name", [], "any", false, false, false, 295);
                echo "</option>
                    ";
            } else {
                // line 297
                echo "                    <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["status"], "order_status_id", [], "any", false, false, false, 297);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["status"], "name", [], "any", false, false, false, 297);
                echo "</option>
                    ";
            }
            // line 299
            echo "                  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['status'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 300
        echo "                </select>

                ";
        // line 302
        if (($context["error_field_order_status_pending"] ?? null)) {
            // line 303
            echo "                <span class=\"text-danger\">";
            echo ($context["error_field_order_status_pending"] ?? null);
            echo "</span>
                ";
        }
        // line 305
        echo "              </div>
            </div>

            <div class=\"form-group required\">
              <label for=\"input-order-status-analysing\" class=\"control-label col-sm-2\">";
        // line 309
        echo ($context["entry_order_status_analysing"] ?? null);
        echo "</label>
              <div class=\"col-sm-10\">
                <select name=\"order_status_analysing\" id=\"input-order-status-analysing\" class=\"form-control\">
                  <option value=\"\">";
        // line 312
        echo ($context["text_none"] ?? null);
        echo "</option>

                  ";
        // line 314
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["statuses"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["status"]) {
            // line 315
            echo "                    ";
            if ((twig_get_attribute($this->env, $this->source, $context["status"], "order_status_id", [], "any", false, false, false, 315) == ($context["order_status_analysing"] ?? null))) {
                // line 316
                echo "                    <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["status"], "order_status_id", [], "any", false, false, false, 316);
                echo "\" selected>";
                echo twig_get_attribute($this->env, $this->source, $context["status"], "name", [], "any", false, false, false, 316);
                echo "</option>
                    ";
            } else {
                // line 318
                echo "                    <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["status"], "order_status_id", [], "any", false, false, false, 318);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["status"], "name", [], "any", false, false, false, 318);
                echo "</option>
                    ";
            }
            // line 320
            echo "                  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['status'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 321
        echo "                </select>

                ";
        // line 323
        if (($context["error_field_order_status_analysing"] ?? null)) {
            // line 324
            echo "                <span class=\"text-danger\">";
            echo ($context["error_field_order_status_analysing"] ?? null);
            echo "</span>
                ";
        }
        // line 326
        echo "              </div>
            </div>

            <div class=\"form-group required\">
              <label for=\"input-order-status-paid\" class=\"control-label col-sm-2\">";
        // line 330
        echo ($context["entry_order_status_paid"] ?? null);
        echo "</label>
              <div class=\"col-sm-10\">
                <select name=\"order_status_paid\" id=\"input-order-status-paid\" class=\"form-control\">
                  <option value=\"\">";
        // line 333
        echo ($context["text_none"] ?? null);
        echo "</option>

                  ";
        // line 335
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["statuses"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["status"]) {
            // line 336
            echo "                    ";
            if ((twig_get_attribute($this->env, $this->source, $context["status"], "order_status_id", [], "any", false, false, false, 336) == ($context["order_status_paid"] ?? null))) {
                // line 337
                echo "                    <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["status"], "order_status_id", [], "any", false, false, false, 337);
                echo "\" selected>";
                echo twig_get_attribute($this->env, $this->source, $context["status"], "name", [], "any", false, false, false, 337);
                echo "</option>
                    ";
            } else {
                // line 339
                echo "                    <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["status"], "order_status_id", [], "any", false, false, false, 339);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["status"], "name", [], "any", false, false, false, 339);
                echo "</option>
                    ";
            }
            // line 341
            echo "                  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['status'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 342
        echo "                </select>

                ";
        // line 344
        if (($context["error_field_order_status_paid"] ?? null)) {
            // line 345
            echo "                <span class=\"text-danger\">";
            echo ($context["error_field_order_status_paid"] ?? null);
            echo "</span>
                ";
        }
        // line 347
        echo "              </div>
            </div>

            <div class=\"form-group required\">
              <label for=\"input-order-status-available\" class=\"control-label col-sm-2\">";
        // line 351
        echo ($context["entry_order_status_available"] ?? null);
        echo "</label>
              <div class=\"col-sm-10\">
                <select name=\"order_status_available\" id=\"input-order-status-available\" class=\"form-control\">
                  <option value=\"\">";
        // line 354
        echo ($context["text_none"] ?? null);
        echo "</option>

                  ";
        // line 356
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["statuses"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["status"]) {
            // line 357
            echo "                    ";
            if ((twig_get_attribute($this->env, $this->source, $context["status"], "order_status_id", [], "any", false, false, false, 357) == ($context["order_status_available"] ?? null))) {
                // line 358
                echo "                    <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["status"], "order_status_id", [], "any", false, false, false, 358);
                echo "\" selected>";
                echo twig_get_attribute($this->env, $this->source, $context["status"], "name", [], "any", false, false, false, 358);
                echo "</option>
                    ";
            } else {
                // line 360
                echo "                    <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["status"], "order_status_id", [], "any", false, false, false, 360);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["status"], "name", [], "any", false, false, false, 360);
                echo "</option>
                    ";
            }
            // line 362
            echo "                  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['status'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 363
        echo "                </select>

                ";
        // line 365
        if (($context["error_field_order_status_available"] ?? null)) {
            // line 366
            echo "                <span class=\"text-danger\">";
            echo ($context["error_field_order_status_available"] ?? null);
            echo "</span>
                ";
        }
        // line 368
        echo "              </div>
            </div>

            <div class=\"form-group required\">
              <label for=\"input-order-status-disputed\" class=\"control-label col-sm-2\">";
        // line 372
        echo ($context["entry_order_status_disputed"] ?? null);
        echo "</label>
              <div class=\"col-sm-10\">
                <select name=\"order_status_disputed\" id=\"input-order-status-disputed\" class=\"form-control\">
                  <option value=\"\">";
        // line 375
        echo ($context["text_none"] ?? null);
        echo "</option>

                  ";
        // line 377
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["statuses"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["status"]) {
            // line 378
            echo "                    ";
            if ((twig_get_attribute($this->env, $this->source, $context["status"], "order_status_id", [], "any", false, false, false, 378) == ($context["order_status_disputed"] ?? null))) {
                // line 379
                echo "                    <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["status"], "order_status_id", [], "any", false, false, false, 379);
                echo "\" selected>";
                echo twig_get_attribute($this->env, $this->source, $context["status"], "name", [], "any", false, false, false, 379);
                echo "</option>
                    ";
            } else {
                // line 381
                echo "                    <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["status"], "order_status_id", [], "any", false, false, false, 381);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["status"], "name", [], "any", false, false, false, 381);
                echo "</option>
                    ";
            }
            // line 383
            echo "                  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['status'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 384
        echo "                </select>

                ";
        // line 386
        if (($context["error_field_order_status_disputed"] ?? null)) {
            // line 387
            echo "                <span class=\"text-danger\">";
            echo ($context["error_field_order_status_disputed"] ?? null);
            echo "</span>
                ";
        }
        // line 389
        echo "              </div>
            </div>

            <div class=\"form-group required\">
              <label for=\"input-order-status-returned\" class=\"control-label col-sm-2\">";
        // line 393
        echo ($context["entry_order_status_returned"] ?? null);
        echo "</label>
              <div class=\"col-sm-10\">
                <select name=\"order_status_returned\" id=\"input-order-status-returned\" class=\"form-control\">
                  <option value=\"\">";
        // line 396
        echo ($context["text_none"] ?? null);
        echo "</option>

                  ";
        // line 398
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["statuses"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["status"]) {
            // line 399
            echo "                    ";
            if ((twig_get_attribute($this->env, $this->source, $context["status"], "order_status_id", [], "any", false, false, false, 399) == ($context["order_status_returned"] ?? null))) {
                // line 400
                echo "                    <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["status"], "order_status_id", [], "any", false, false, false, 400);
                echo "\" selected>";
                echo twig_get_attribute($this->env, $this->source, $context["status"], "name", [], "any", false, false, false, 400);
                echo "</option>
                    ";
            } else {
                // line 402
                echo "                    <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["status"], "order_status_id", [], "any", false, false, false, 402);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["status"], "name", [], "any", false, false, false, 402);
                echo "</option>
                    ";
            }
            // line 404
            echo "                  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['status'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 405
        echo "                </select>

                ";
        // line 407
        if (($context["error_field_order_status_returned"] ?? null)) {
            // line 408
            echo "                <span class=\"text-danger\">";
            echo ($context["error_field_order_status_returned"] ?? null);
            echo "</span>
                ";
        }
        // line 410
        echo "              </div>
            </div>

            <div class=\"form-group required\">
              <label for=\"input-order-status-cancelled\" class=\"control-label col-sm-2\">";
        // line 414
        echo ($context["entry_order_status_cancelled"] ?? null);
        echo "</label>
              <div class=\"col-sm-10\">
                <select name=\"order_status_cancelled\" id=\"input-order-status-cancelled\" class=\"form-control\">
                  <option value=\"\">";
        // line 417
        echo ($context["text_none"] ?? null);
        echo "</option>

                  ";
        // line 419
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["statuses"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["status"]) {
            // line 420
            echo "                    ";
            if ((twig_get_attribute($this->env, $this->source, $context["status"], "order_status_id", [], "any", false, false, false, 420) == ($context["order_status_cancelled"] ?? null))) {
                // line 421
                echo "                    <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["status"], "order_status_id", [], "any", false, false, false, 421);
                echo "\" selected>";
                echo twig_get_attribute($this->env, $this->source, $context["status"], "name", [], "any", false, false, false, 421);
                echo "</option>
                    ";
            } else {
                // line 423
                echo "                    <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["status"], "order_status_id", [], "any", false, false, false, 423);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["status"], "name", [], "any", false, false, false, 423);
                echo "</option>
                    ";
            }
            // line 425
            echo "                  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['status'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 426
        echo "                </select>

                ";
        // line 428
        if (($context["error_field_order_status_cancelled"] ?? null)) {
            // line 429
            echo "                <span class=\"text-danger\">";
            echo ($context["error_field_order_status_cancelled"] ?? null);
            echo "</span>
                ";
        }
        // line 431
        echo "              </div>
            </div>
          </div>

          <!-- ------- -->
          <!-- Tab Geo -->
          <!-- ------- -->
          <div class=\"tab-pane\" id=\"tab-geo\">
            <div class=\"form-group\">
              <label for=\"input-geo-zone-id\" class=\"control-label col-sm-2\">";
        // line 440
        echo ($context["entry_geo_zone_id"] ?? null);
        echo "</label>
              <div class=\"col-sm-10\">
                <select name=\"geo_zone_id\" id=\"input-geo-zone-id\" class=\"form-control\">
                  <option value=\"0\">All</option>

                  ";
        // line 445
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["geo_zones"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["zone"]) {
            // line 446
            echo "                    ";
            if ((twig_get_attribute($this->env, $this->source, $context["zone"], "geo_zone_id", [], "any", false, false, false, 446) == ($context["geo_zone_id"] ?? null))) {
                // line 447
                echo "                    <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["zone"], "geo_zone_id", [], "any", false, false, false, 447);
                echo "\" selected>";
                echo twig_get_attribute($this->env, $this->source, $context["zone"], "name", [], "any", false, false, false, 447);
                echo "</option>
                    ";
            } else {
                // line 449
                echo "                    <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["zone"], "geo_zone_id", [], "any", false, false, false, 449);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["zone"], "name", [], "any", false, false, false, 449);
                echo "</option>
                    ";
            }
            // line 451
            echo "                  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['zone'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 452
        echo "                </select>
              </div>
            </div>

            <div class=\"form-group\">
              <label for=\"input-geo-sort-order\" class=\"control-label col-sm-2\">";
        // line 457
        echo ($context["entry_geo_sort_order"] ?? null);
        echo "</label>
              <div class=\"col-sm-10\">
                <input type=\"text\" name=\"geo_sort_order\" value=\"";
        // line 459
        echo ($context["geo_sort_order"] ?? null);
        echo "\" id=\"input-geo-sort-order\" class=\"form-control\" />
              </div>
            </div>

            <div class=\"form-group\">
              <label for=\"input-geo-stores\" class=\"control-label col-sm-2\">";
        // line 464
        echo ($context["entry_geo_stores"] ?? null);
        echo "</label>
              <div class=\"col-sm-10\">
                <select name=\"geo_stores\" id=\"input-geo-stores\" multiple class=\"form-control\">
                  ";
        // line 467
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["stores"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["store"]) {
            // line 468
            echo "                    ";
            if ((twig_get_attribute($this->env, $this->source, $context["store"], "store_id", [], "any", false, false, false, 468) == ($context["geo_stores"] ?? null))) {
                // line 469
                echo "                    <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["store"], "store_id", [], "any", false, false, false, 469);
                echo "\" selected>";
                echo twig_get_attribute($this->env, $this->source, $context["store"], "name", [], "any", false, false, false, 469);
                echo "</option>
                    ";
            } else {
                // line 471
                echo "                    <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["store"], "store_id", [], "any", false, false, false, 471);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["store"], "name", [], "any", false, false, false, 471);
                echo "</option>
                    ";
            }
            // line 473
            echo "                  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['store'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 474
        echo "                </select>
              </div>
            </div>
          </div>

          <!-- --------------- -->
          <!-- Tab Installment -->
          <!-- --------------- -->
          <div class=\"tab-pane\" id=\"tab-installment\">
            <div class=\"form-group required\">
              <label for=\"input-installment-total\" class=\"control-label col-sm-2\">";
        // line 484
        echo ($context["entry_installment_total"] ?? null);
        echo "</label>
              <div class=\"col-sm-10\">
                <input type=\"text\" name=\"installment_total\" value=\"";
        // line 486
        echo ($context["installment_total"] ?? null);
        echo "\" id=\"input-installment-total\" class=\"form-control\" required />
                ";
        // line 487
        if (($context["error_field_installment_total"] ?? null)) {
            // line 488
            echo "                <span class=\"text-danger\">";
            echo ($context["error_field_installment_total"] ?? null);
            echo "</span>
                ";
        }
        // line 490
        echo "              </div>
            </div>

            <div class=\"form-group\">
              <label for=\"input-installment-free\" class=\"control-label col-sm-2\">";
        // line 494
        echo ($context["entry_installment_free"] ?? null);
        echo "</label>
              <div class=\"col-sm-10\">
                <input type=\"text\" name=\"installment_free\" value=\"";
        // line 496
        echo ($context["installment_free"] ?? null);
        echo "\" id=\"input-installment-free\" class=\"form-control\" required />
                ";
        // line 497
        if (($context["error_field_installment_free"] ?? null)) {
            // line 498
            echo "                <span class=\"text-danger\">";
            echo ($context["error_field_installment_free"] ?? null);
            echo "</span>
                ";
        }
        // line 500
        echo "              </div>
            </div>

            <div class=\"form-group required\">
              <label for=\"input-installment-minimum-value\" class=\"control-label col-sm-2\">";
        // line 504
        echo ($context["entry_installment_minimum_value"] ?? null);
        echo "</label>
              <div class=\"col-sm-10\">
                <input type=\"text\" name=\"installment_minimum_value\" value=\"";
        // line 506
        echo ($context["installment_minimum_value"] ?? null);
        echo "\" id=\"input-installment-minimum-value\" class=\"form-control\" required />
                ";
        // line 507
        if (($context["error_field_installment_minimum_value"] ?? null)) {
            // line 508
            echo "                <span class=\"text-danger\">";
            echo ($context["error_field_installment_minimum_value"] ?? null);
            echo "</span>
                ";
        }
        // line 510
        echo "              </div>
            </div>
          </div>

          <!-- ------------------- -->
          <!-- Tab Payment Methods -->
          <!-- ------------------- -->
          <div class=\"tab-pane\" id=\"tab-methods\">
            <fieldset>
              <legend>";
        // line 519
        echo ($context["text_boleto"] ?? null);
        echo "</legend>

              <div class=\"form-group\">
                <label for=\"input-methods-boleto-status\" class=\"control-label col-sm-2\">";
        // line 522
        echo ($context["entry_status"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <select name=\"methods_boleto_status\" id=\"input-methods-boleto-status\" class=\"form-control\">
                    <option value=\"1\" ";
        // line 525
        echo (($context["methods_boleto_status"]) ?? ("selected"));
        echo ">";
        echo ($context["text_enabled"] ?? null);
        echo "</option>
                    <option value=\"0\" ";
        // line 526
        echo ((($context["methods_boleto_status"] ?? null)) ? (($context["methods_boleto_status"] ?? null)) : ("selected"));
        echo ">";
        echo ($context["text_disabled"] ?? null);
        echo "</option>
                  </select>
                </div>
              </div>

              <div class=\"form-group\">
                <label for=\"input-methods-boleto-minimum-amount\" class=\"control-label col-sm-2\">";
        // line 532
        echo ($context["entry_minimum_amount"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <input type=\"text\" name=\"methods_boleto_minimum_amount\" value=\"";
        // line 534
        echo ($context["methods_boleto_minimum_amount"] ?? null);
        echo "\" id=\"input-methods-boleto-minimum-amount\" class=\"form-control\" />

                  <div class=\"bg-info\" style=\"padding: 15px\">";
        // line 536
        echo ($context["text_boleto_fee"] ?? null);
        echo "</div>
                </div>
              </div>
            </fieldset>

            <fieldset>
              <legend>";
        // line 542
        echo ($context["text_credit"] ?? null);
        echo "</legend>

              <div class=\"form-group\">
                <label for=\"input-methods-credit-status\" class=\"control-label col-sm-2\">";
        // line 545
        echo ($context["entry_status"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <select name=\"methods_credit_status\" id=\"input-methods-credit-status\" class=\"form-control\">
                    <option value=\"1\" ";
        // line 548
        echo (($context["methods_credit_status"]) ?? ("selected"));
        echo ">";
        echo ($context["text_enabled"] ?? null);
        echo "</option>
                    <option value=\"0\" ";
        // line 549
        echo ((($context["methods_credit_status"] ?? null)) ? (($context["methods_credit_status"] ?? null)) : ("selected"));
        echo ">";
        echo ($context["text_disabled"] ?? null);
        echo "</option>
                  </select>
                </div>
              </div>

              <div class=\"form-group\">
                <label for=\"input-methods-credit-minimum-amount\" class=\"control-label col-sm-2\">";
        // line 555
        echo ($context["entry_minimum_amount"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <input type=\"text\" name=\"methods_credit_minimum_amount\" value=\"";
        // line 557
        echo ($context["methods_credit_minimum_amount"] ?? null);
        echo "\" id=\"input-methods-credit-minimum-amount\" class=\"form-control\" />
                </div>
              </div>
            </fieldset>

            <fieldset>
              <legend>";
        // line 563
        echo ($context["text_debit"] ?? null);
        echo "</legend>

              <div class=\"form-group\">
                <label for=\"input-methods-debit-status\" class=\"control-label col-sm-2\">";
        // line 566
        echo ($context["entry_status"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <select name=\"methods_debit_status\" id=\"input-methods-debit-status\" class=\"form-control\">
                    <option value=\"1\" ";
        // line 569
        echo (($context["methods_debit_status"]) ?? ("selected"));
        echo ">";
        echo ($context["text_enabled"] ?? null);
        echo "</option>
                    <option value=\"0\" ";
        // line 570
        echo ((($context["methods_debit_status"] ?? null)) ? (($context["methods_debit_status"] ?? null)) : ("selected"));
        echo ">";
        echo ($context["text_disabled"] ?? null);
        echo "</option>
                  </select>
                </div>
              </div>

              <div class=\"form-group\">
                <label for=\"input-methods-debit-minimum-amount\" class=\"control-label col-sm-2\">";
        // line 576
        echo ($context["entry_minimum_amount"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <input type=\"text\" name=\"methods_debit_minimum_amount\" value=\"";
        // line 578
        echo ($context["methods_debit_minimum_amount"] ?? null);
        echo "\" id=\"input-methods-debit-minimum-amount\" class=\"form-control\" />
                </div>
              </div>
            </fieldset>
          </div>

          <!-- ---------- -->
          <!-- Tab Layout -->
          <!-- ---------- -->
          <div class=\"tab-pane\" id=\"tab-layout\">
            <div class=\"form group required\">
              <label for=\"input-layout\" class=\"control-label col-sm-2\">";
        // line 589
        echo ($context["entry_layout_credit"] ?? null);
        echo "</label>
              <div class=\"col-sm-10\">
                <select name=\"layout\" id=\"input-layout\" class=\"form-control\">
                  <option value=\"default\">";
        // line 592
        echo ($context["text_default"] ?? null);
        echo "</option>
                </select>

                ";
        // line 595
        if (($context["error_field_layout"] ?? null)) {
            // line 596
            echo "                <span class=\"text-danger\">";
            echo ($context["error_field_layout"] ?? null);
            echo "</span>
                ";
        }
        // line 598
        echo "              </div>
            </div>
          </div>

          <!-- --------- -->
          <!-- Tab Debug -->
          <!-- --------- -->
          <div class=\"tab-pane\" id=\"tab-debug\">
            <div class=\"row\">
              <div class=\"col-sm-12\">
                <div class=\"col-sm-3\">
                    <fieldset>
                      <caption>Filtrar por data</caption>
                      <input type=\"text\" id=\"filter-log-date\" class=\"form-control\" />
                    </fieldset>
                </div>

                <div class=\"col-sm-3\">
                  <fieldset>
                    <caption>Filtrar por nível</caption>
                    <select id=\"filter-log-level\" class=\"form-control\">
                      <option value=\"0\">";
        // line 619
        echo ($context["text_select_all"] ?? null);
        echo "</option>
                      <option value=\"100\">Debug</option>
                      <option value=\"200\">Info</option>
                      <option value=\"250\">Notice</option>
                      <option value=\"300\">Warning</option>
                      <option value=\"400\">Error</option>
                      <option value=\"500\">Critical</option>
                      <option value=\"550\">Alert</option>
                      <option value=\"600\">Emergency</option>
                    </select>
                  </fieldset>
                </div>
              </div>
            </div>

            <div class=\"clearfix\">&nbsp;</div>

            <pre class=\"pre-scrollable\" style=\"min-height: 400px\">
              <code></code>
            </pre>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<div class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" id=\"modal-doc\">
  <div class=\"modal-dialog\" role=\"document\">
    <div class=\"modal-content\">
      <div class=\"modal-body\">
        <a href=\"https://opencart-extension.github.io/PagSeguro-Checkout-Transparente/\" target=\"_blank\">
          <img src=\"./view/image/payment/pagseguro_doc.png\" />
        </a>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
  const docCount = localStorage.getItem('docCount') ?? 0

  if (docCount % 3 === 0) {
    \$('#modal-doc').modal()
  }

  localStorage.setItem('docCount', docCount + 1)

  \$('a[data-toggle=\"tab\"]').on('show.bs.tab', function(e) {
    if (e.target.href.endsWith('#tab-debug')) {
      \$('#debug-clear, #debug-download').fadeIn()
    } else {
      \$('#debug-clear, #debug-download').fadeOut()
    }
  })

  \$(function() {
    \$('#input-geo-sort-order, #input-installment-total').mask(\"#0\", {reverse: true});

    const floats = [
      '#input-discount-boleto',
      '#input-discount-credit',
      '#input-discount-debit',
      '#input-fee-boleto',
      '#input-fee-credit',
      '#input-fee-debit',
      '#input-installment-free',
      '#input-installment-minimum-value',
      '#input-methods-boleto-minimum-amount',
      '#methods_boleto_minimum_amount',
      '#methods_credit_minimum_amount',
      '#methods_debit_minimum_amount',
    ];

    \$(floats.join(',')).mask(\"#.00\", {reverse: true});
  })

  \$(function() {
    \$('#filter-log-date').datetimepicker({
      format: 'YYYY-MM-DD',
      date: '";
        // line 699
        echo twig_last($this->env, ($context["logs_date"] ?? null));
        echo "',
      enabledDates: JSON.parse('";
        // line 700
        echo json_encode(($context["logs_date"] ?? null));
        echo "'),
      icons: {
        previous: 'fa fa-arrow-left',
        next: 'fa fa-arrow-right',
      }
    })
    .on('dp.change', function(newDate, oldDate) {
      loadLog(newDate.currentTarget.value)
    })

    loadLog('";
        // line 710
        echo twig_last($this->env, ($context["logs_date"] ?? null));
        echo "')
  })
</script>

<script>
/**
 * Filtra log por nível
 */
 const inputFilterLogLevel = document.querySelector('#filter-log-level')
 inputFilterLogLevel.addEventListener('change', filterLogByLevel)

 function filterLogByLevel(e) {
   document.querySelectorAll('#tab-debug pre > code > div[data-level]')
    .forEach((item) => {
      let display = 'none'

      if ([0, parseInt(item.dataset.level)].includes(parseInt(e.target.value))) {
        display = 'block'
      }

      item.style.display = display
    })
 }

/**
 * Realiza limpeza do log
 */
 const inputFilterLogDate = document.querySelector('#filter-log-date')

document.querySelector('#debug-clear')
  .addEventListener('click', clearLog)

function clearLog() {
  const date = inputFilterLogDate.value
  fetch(`/admin/index.php?route=extension/payment/pagseguro/log&user_token=\${getURLVar('user_token')}&date=\${date}`, {
    method: 'DELETE'
  })
}

/**
 * Realiza o download do log
 */
document.querySelector('#debug-download')
  .addEventListener('click', downloadLog)

function downloadLog() {
  const logs = querySelectorLog()
  const logsHtml = elementToHtmlText(logs)

  const content = new Blob(logsHtml, { mime: 'text/html' })

  const anchor = document.createElement('a')
  anchor.href = URL.createObjectURL(content)
  anchor.download = `log_pagseguro_\${inputFilterLogDate.value}_\${inputFilterLogLevel.value}_psr.html`
  anchor.click()
  anchor.remove()
}

/**
 * Captura todos os logs ativos
 *
 * @returns Node[]
 */
function querySelectorLog() {
  const logs = document.querySelectorAll('#tab-debug > pre > code > div')

  return Array.from(logs).filter((item) => {
    return item.getBoundingClientRect().x !== 0
  })
}

/**
 * Converte um elemento HTML para texto
 *
 * @params {HTMLElement} elements
 *
 * @returns string[]
 */
function elementToHtmlText(elements) {
  return elements.map((el) => {
    return el.outerHTML
  })
}

/**
 * Carrega log
 */
 function loadLog(date) {
  fetch(`/admin/index.php?route=extension/payment/pagseguro/log&user_token=\${getURLVar('user_token')}&date=\${date}`)
    .then(response => response.text())
    .then(response => {
      const code = document.querySelector('#tab-debug pre > code')
      code.innerHTML = '';
      code.insertAdjacentHTML('beforeEnd', response)
    })
}
</script>

<style>
#debug-clear, #debug-download {
  display: none
}

.day:not([disabled]) {
  background: #337ab76b;
}

#modal-doc .modal-content {
  box-shadow: none;
  background: transparent;
}

#modal-doc .modal-dialog {
  width: 50%;
}

#modal-doc .modal-body {
  border: none;
  padding: 0;
}

#modal-doc .modal-body img {
  border: 15px solid white;
  width: 100%;
}
</style>
";
        // line 836
        echo ($context["footer"] ?? null);
        echo "
";
    }

    public function getTemplateName()
    {
        return "extension/payment/pagseguro.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1676 => 836,  1547 => 710,  1534 => 700,  1530 => 699,  1447 => 619,  1424 => 598,  1418 => 596,  1416 => 595,  1410 => 592,  1404 => 589,  1390 => 578,  1385 => 576,  1374 => 570,  1368 => 569,  1362 => 566,  1356 => 563,  1347 => 557,  1342 => 555,  1331 => 549,  1325 => 548,  1319 => 545,  1313 => 542,  1304 => 536,  1299 => 534,  1294 => 532,  1283 => 526,  1277 => 525,  1271 => 522,  1265 => 519,  1254 => 510,  1248 => 508,  1246 => 507,  1242 => 506,  1237 => 504,  1231 => 500,  1225 => 498,  1223 => 497,  1219 => 496,  1214 => 494,  1208 => 490,  1202 => 488,  1200 => 487,  1196 => 486,  1191 => 484,  1179 => 474,  1173 => 473,  1165 => 471,  1157 => 469,  1154 => 468,  1150 => 467,  1144 => 464,  1136 => 459,  1131 => 457,  1124 => 452,  1118 => 451,  1110 => 449,  1102 => 447,  1099 => 446,  1095 => 445,  1087 => 440,  1076 => 431,  1070 => 429,  1068 => 428,  1064 => 426,  1058 => 425,  1050 => 423,  1042 => 421,  1039 => 420,  1035 => 419,  1030 => 417,  1024 => 414,  1018 => 410,  1012 => 408,  1010 => 407,  1006 => 405,  1000 => 404,  992 => 402,  984 => 400,  981 => 399,  977 => 398,  972 => 396,  966 => 393,  960 => 389,  954 => 387,  952 => 386,  948 => 384,  942 => 383,  934 => 381,  926 => 379,  923 => 378,  919 => 377,  914 => 375,  908 => 372,  902 => 368,  896 => 366,  894 => 365,  890 => 363,  884 => 362,  876 => 360,  868 => 358,  865 => 357,  861 => 356,  856 => 354,  850 => 351,  844 => 347,  838 => 345,  836 => 344,  832 => 342,  826 => 341,  818 => 339,  810 => 337,  807 => 336,  803 => 335,  798 => 333,  792 => 330,  786 => 326,  780 => 324,  778 => 323,  774 => 321,  768 => 320,  760 => 318,  752 => 316,  749 => 315,  745 => 314,  740 => 312,  734 => 309,  728 => 305,  722 => 303,  720 => 302,  716 => 300,  710 => 299,  702 => 297,  694 => 295,  691 => 294,  687 => 293,  682 => 291,  676 => 288,  662 => 277,  657 => 275,  649 => 270,  644 => 268,  636 => 263,  631 => 261,  625 => 258,  616 => 252,  611 => 250,  603 => 245,  598 => 243,  590 => 238,  585 => 236,  579 => 233,  568 => 224,  562 => 222,  560 => 221,  557 => 220,  551 => 219,  543 => 217,  535 => 215,  532 => 214,  528 => 213,  523 => 211,  517 => 208,  511 => 204,  505 => 202,  503 => 201,  500 => 200,  494 => 199,  486 => 197,  478 => 195,  475 => 194,  471 => 193,  466 => 191,  460 => 188,  454 => 184,  448 => 182,  446 => 181,  443 => 180,  437 => 179,  429 => 177,  421 => 175,  418 => 174,  414 => 173,  409 => 171,  403 => 168,  388 => 156,  379 => 150,  375 => 149,  369 => 146,  363 => 142,  357 => 140,  355 => 139,  351 => 138,  346 => 136,  340 => 132,  334 => 130,  332 => 129,  325 => 127,  319 => 126,  313 => 123,  302 => 117,  296 => 116,  290 => 113,  284 => 109,  278 => 107,  276 => 106,  269 => 104,  263 => 103,  257 => 100,  251 => 96,  245 => 94,  243 => 93,  237 => 92,  232 => 90,  226 => 86,  220 => 84,  218 => 83,  212 => 82,  207 => 80,  201 => 76,  195 => 74,  193 => 73,  186 => 71,  180 => 70,  174 => 67,  165 => 61,  159 => 58,  155 => 57,  151 => 56,  147 => 55,  143 => 54,  139 => 53,  135 => 52,  131 => 51,  127 => 50,  115 => 41,  100 => 31,  96 => 30,  92 => 29,  88 => 28,  74 => 17,  66 => 11,  55 => 9,  51 => 8,  45 => 5,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "extension/payment/pagseguro.twig", "");
    }
}

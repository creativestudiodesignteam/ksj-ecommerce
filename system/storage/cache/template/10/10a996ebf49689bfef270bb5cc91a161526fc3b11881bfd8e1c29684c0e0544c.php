<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* wokiee/template/common/home.twig */
class __TwigTemplate_f575be32345d664645665fdf4ed7d0f6b24687792ee3fa15f7307122879c1fdb extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo " 

";
        // line 3
        if ((twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "has", [0 => "theme_options"], "method", false, false, false, 3) == twig_constant("true"))) {
            // line 4
            echo "\t";
            $context["theme_options"] = twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "get", [0 => "theme_options"], "method", false, false, false, 4);
            // line 5
            echo "\t";
            $context["config"] = twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "get", [0 => "config"], "method", false, false, false, 5);
            echo " 
\t";
            // line 6
            $context["grid_center"] = 12;
            echo " 
\t";
            // line 7
            if ((($context["column_left"] ?? null) != "")) {
                echo " ";
                $context["grid_center"] = (($context["grid_center"] ?? null) - 3);
            }
            echo " 
\t";
            // line 8
            if ((($context["column_right"] ?? null) != "")) {
                echo " ";
                $context["grid_center"] = (($context["grid_center"] ?? null) - 3);
            }
            // line 9
            echo "
\t<div id=\"tt-pageContent\">
\t\t";
            // line 11
            $context["slideshow"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "slideshow"], "method", false, false, false, 11);
            // line 12
            echo "\t\t";
            if ((twig_length_filter($this->env, ($context["slideshow"] ?? null)) > 0)) {
                echo " 
\t\t\t";
                // line 13
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["slideshow"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                    echo " 
\t\t\t\t";
                    // line 14
                    echo $context["module"];
                    echo "
\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 16
                echo "\t\t";
            }
            echo " 

\t\t";
            // line 18
            $context["preface_left"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "preface_left"], "method", false, false, false, 18);
            // line 19
            echo "\t\t";
            if ((twig_length_filter($this->env, ($context["preface_left"] ?? null)) > 0)) {
                // line 20
                echo "\t\t\t";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["preface_left"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                    echo " 
\t\t\t\t";
                    // line 21
                    echo $context["module"];
                    echo "
\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 23
                echo "\t\t";
            }
            // line 24
            echo "\t\t\t
\t\t";
            // line 25
            $context["preface_right"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "preface_right"], "method", false, false, false, 25);
            // line 26
            echo "\t\t";
            if ((twig_length_filter($this->env, ($context["preface_right"] ?? null)) > 0)) {
                // line 27
                echo "\t\t\t";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["preface_right"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                    echo " 
\t\t\t\t";
                    // line 28
                    echo $context["module"];
                    echo "
\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 30
                echo "\t\t";
            }
            echo " 
\t\t
\t\t";
            // line 32
            $context["preface_fullwidth"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "preface_fullwidth"], "method", false, false, false, 32);
            // line 33
            echo "\t\t";
            if ((twig_length_filter($this->env, ($context["preface_fullwidth"] ?? null)) > 0)) {
                echo " 
\t\t\t";
                // line 34
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["preface_fullwidth"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                    echo " 
\t\t\t\t";
                    // line 35
                    echo $context["module"];
                    echo "
\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 37
                echo "\t\t";
            }
            echo " 
\t\t
\t\t\t
\t\t";
            // line 40
            $context["columnleft"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "column_left"], "method", false, false, false, 40);
            // line 41
            echo "\t\t";
            if ((twig_length_filter($this->env, ($context["columnleft"] ?? null)) > 0)) {
                echo " 
\t\t\t";
                // line 42
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["columnleft"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                    echo " 
\t\t\t\t";
                    // line 43
                    echo $context["module"];
                    echo "
\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 45
                echo "\t\t";
            }
            echo " 

\t\t";
            // line 47
            $context["content_big_column"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "content_big_column"], "method", false, false, false, 47);
            // line 48
            echo "\t\t";
            if ((twig_length_filter($this->env, ($context["content_big_column"] ?? null)) > 0)) {
                echo " 
\t\t\t";
                // line 49
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["content_big_column"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                    echo " 
\t\t\t\t";
                    // line 50
                    echo $context["module"];
                    echo "
\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 52
                echo "\t\t";
            }
            echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t";
            // line 54
            $context["content_top"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "content_top"], "method", false, false, false, 54);
            // line 55
            echo "\t\t";
            if ((twig_length_filter($this->env, ($context["content_top"] ?? null)) > 0)) {
                echo " 
\t\t\t";
                // line 56
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["content_top"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                    echo " 
\t\t\t\t";
                    // line 57
                    echo $context["module"];
                    echo "
\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 59
                echo "\t\t";
            }
            echo " 
\t\t\t\t\t
\t\t";
            // line 61
            $context["column_right"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "column_right"], "method", false, false, false, 61);
            echo " 
\t\t";
            // line 62
            if ((twig_length_filter($this->env, ($context["column_right"] ?? null)) > 0)) {
                echo " 
\t\t\t";
                // line 63
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["column_right"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                    echo " 
\t\t\t\t";
                    // line 64
                    echo $context["module"];
                    echo "
\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 65
                echo " 
\t\t";
            }
            // line 66
            echo " 

\t\t";
            // line 68
            $context["contentbottom"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "content_bottom"], "method", false, false, false, 68);
            // line 69
            echo "\t\t";
            if ((twig_length_filter($this->env, ($context["contentbottom"] ?? null)) > 0)) {
                echo " 
\t\t\t";
                // line 70
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["contentbottom"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                    echo " 
\t\t\t\t";
                    // line 71
                    echo $context["module"];
                    echo "
\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 73
                echo "\t\t";
            }
            echo " 
\t</div>
";
        }
        // line 76
        echo "
";
        // line 77
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "wokiee/template/common/home.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  321 => 77,  318 => 76,  311 => 73,  303 => 71,  297 => 70,  292 => 69,  290 => 68,  286 => 66,  282 => 65,  274 => 64,  268 => 63,  264 => 62,  260 => 61,  254 => 59,  246 => 57,  240 => 56,  235 => 55,  233 => 54,  227 => 52,  219 => 50,  213 => 49,  208 => 48,  206 => 47,  200 => 45,  192 => 43,  186 => 42,  181 => 41,  179 => 40,  172 => 37,  164 => 35,  158 => 34,  153 => 33,  151 => 32,  145 => 30,  137 => 28,  130 => 27,  127 => 26,  125 => 25,  122 => 24,  119 => 23,  111 => 21,  104 => 20,  101 => 19,  99 => 18,  93 => 16,  85 => 14,  79 => 13,  74 => 12,  72 => 11,  68 => 9,  63 => 8,  56 => 7,  52 => 6,  47 => 5,  44 => 4,  42 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "wokiee/template/common/home.twig", "");
    }
}

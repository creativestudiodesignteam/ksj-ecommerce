<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* extension/payment/mp_transparente_payment_methods_partial.twig */
class __TwigTemplate_a0d5ac4dc56ef90c337d011903095622e875035afe1066d094b79aa37424d9d2 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo ($context["column_left"] ?? null);
        echo "
<div class=\"form-group\">
";
        // line 3
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["methods"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["method"]) {
            // line 4
            echo "    <div style=\"";
            echo ($context["payment_style"] ?? null);
            echo "\" id=\"";
            echo twig_get_attribute($this->env, $this->source, $context["method"], "name", [], "any", false, false, false, 4);
            echo "\">
      <img src=\"";
            // line 5
            echo twig_get_attribute($this->env, $this->source, $context["method"], "secure_thumbnail", [], "any", false, false, false, 5);
            echo "\"><br /><input name=\"payment_mp_transparente_methods[]\" type=\"checkbox\" value=\"";
            echo twig_get_attribute($this->env, $this->source, $context["method"], "id", [], "any", false, false, false, 5);
            echo "\" style=\"margin-left:25%;\">
    </div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['method'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 8
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "extension/payment/mp_transparente_payment_methods_partial.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  65 => 8,  54 => 5,  47 => 4,  43 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "extension/payment/mp_transparente_payment_methods_partial.twig", "");
    }
}

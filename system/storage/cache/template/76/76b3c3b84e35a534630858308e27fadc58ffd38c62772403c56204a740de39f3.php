<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* wokiee/template/new_elements/wrapper_bottom.twig */
class __TwigTemplate_8b7b8c3d8fafa2f9de9b0625dc7f75f07ccd225ab571e5ec2fd695d4f467d429 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ((twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "has", [0 => "theme_options"], "method", false, false, false, 1) == twig_constant("true"))) {
            echo " 
\t";
            // line 2
            $context["theme_options"] = twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "get", [0 => "theme_options"], "method", false, false, false, 2);
            // line 3
            echo "\t";
            $context["config"] = twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "get", [0 => "config"], "method", false, false, false, 3);
            // line 4
            echo "
\t";
            // line 5
            $context["columnleft"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "column_left"], "method", false, false, false, 5);
            // line 6
            echo "\t";
            $context["grid_center"] = 12;
            echo " 
\t";
            // line 7
            if ((twig_length_filter($this->env, ($context["columnleft"] ?? null)) > 0)) {
                echo " 
\t\t";
                // line 8
                $context["grid_center"] = 9;
                echo " 
\t";
            }
            // line 9
            echo " 

\t";
            // line 11
            $context["grid_content_right"] = 3;
            // line 12
            echo "\t";
            $context["column_right"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "column_right"], "method", false, false, false, 12);
            echo " 
\t";
            // line 13
            if ((twig_length_filter($this->env, ($context["column_right"] ?? null)) > 0)) {
                // line 14
                echo "\t\t";
                if ((($context["grid_center"] ?? null) == 9)) {
                    // line 15
                    echo "\t\t\t";
                    $context["grid_content_right"] = 4;
                    // line 16
                    echo "\t\t";
                } else {
                    echo " 
\t\t\t";
                    // line 17
                    $context["grid_content_right"] = 3;
                    // line 18
                    echo "\t\t";
                }
                // line 19
                echo "\t";
            }
            // line 20
            echo "
\t\t";
            // line 21
            if ( !($context["product_page"] ?? null)) {
                // line 22
                echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t";
                // line 24
                if ((twig_length_filter($this->env, ($context["column_right"] ?? null)) > 0)) {
                    echo " 
\t\t\t\t\t\t\t<div class=\"col-lg-";
                    // line 25
                    echo ($context["grid_content_right"] ?? null);
                    echo " leftColumn rightColumn ";
                    if (($context["categoryPage"] ?? null)) {
                        echo "aside";
                    }
                    echo " ";
                    if ((((($context["categoryPage"] ?? null) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_grid_type"], "method", false, false, false, 25) == "1")) || (($context["categoryPage"] ?? null) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_grid_type"], "method", false, false, false, 25) == "2"))) || (($context["categoryPage"] ?? null) && (twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "product_grid_type"], "method", false, false, false, 25) == "3")))) {
                        echo "desctop-no-sidebar";
                    }
                    echo "\" id=\"column-right\">
\t\t\t\t\t\t\t\t";
                    // line 26
                    if (($context["categoryPage"] ?? null)) {
                        // line 27
                        echo "\t\t\t\t\t\t\t\t\t<div class=\"tt-btn-col-close\">
\t\t\t\t\t\t\t\t\t\t<a href=\"#\">";
                        // line 28
                        if ((twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "close_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 28)], "method", false, false, false, 28) != "")) {
                            echo " ";
                            echo twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "get", [0 => "close_text", 1 => twig_get_attribute($this->env, $this->source, ($context["config"] ?? null), "get", [0 => "config_language_id"], "method", false, false, false, 28)], "method", false, false, false, 28);
                            echo " ";
                        } else {
                            echo "Close";
                        }
                        echo "</a>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"tt-collapse open tt-filter-detach-option\">
\t\t\t\t\t\t\t\t\t\t<div class=\"tt-collapse-content\" style=\"\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"filters-mobile\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"filters-row-select\">

\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
                    }
                    // line 40
                    echo "\t\t\t\t\t\t\t\t";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(($context["column_right"] ?? null));
                    foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                        echo " 
\t\t\t\t\t\t\t\t\t";
                        // line 41
                        echo $context["module"];
                        echo "
\t\t\t\t\t\t\t\t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 42
                    echo " 
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t";
                }
                // line 44
                echo " 
\t\t\t\t\t\t</div> 
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t
\t\t\t\t<div class=\"row\">\t
\t\t\t\t\t<div class=\"col-md-12\">\t
\t\t\t\t\t\t";
                // line 51
                $context["contentbottom"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "content_bottom"], "method", false, false, false, 51);
                // line 52
                echo "\t\t\t\t\t\t";
                if ((twig_length_filter($this->env, ($context["contentbottom"] ?? null)) > 0)) {
                    echo " 
\t\t\t\t\t\t\t";
                    // line 53
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(($context["contentbottom"] ?? null));
                    foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                        echo " 
\t\t\t\t\t\t\t\t";
                        // line 54
                        echo $context["module"];
                        echo "
\t\t\t\t\t\t\t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 56
                    echo "\t\t\t\t\t\t";
                }
                echo " 
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>\t
\t\t";
            } else {
                // line 62
                echo "\t\t\t";
                if ((twig_length_filter($this->env, ($context["column_right"] ?? null)) > 0)) {
                    echo " 
\t\t\t\t";
                    // line 63
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(($context["column_right"] ?? null));
                    foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                        echo " 
\t\t\t\t\t";
                        // line 64
                        echo $context["module"];
                        echo "
\t\t\t\t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 66
                    echo "\t\t\t";
                }
                // line 67
                echo "\t\t\t";
                $context["contentbottom"] = twig_get_attribute($this->env, $this->source, ($context["theme_options"] ?? null), "getModules", [0 => "content_bottom"], "method", false, false, false, 67);
                // line 68
                echo "\t\t\t";
                if ((twig_length_filter($this->env, ($context["contentbottom"] ?? null)) > 0)) {
                    echo " 
\t\t\t\t";
                    // line 69
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(($context["contentbottom"] ?? null));
                    foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                        echo " 
\t\t\t\t\t";
                        // line 70
                        echo $context["module"];
                        echo "
\t\t\t\t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 72
                    echo "\t\t\t";
                }
                echo " 
\t\t";
            }
            // line 73
            echo "\t\t\t  
\t</div>
";
        }
    }

    public function getTemplateName()
    {
        return "wokiee/template/new_elements/wrapper_bottom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  258 => 73,  252 => 72,  244 => 70,  238 => 69,  233 => 68,  230 => 67,  227 => 66,  219 => 64,  213 => 63,  208 => 62,  198 => 56,  190 => 54,  184 => 53,  179 => 52,  177 => 51,  168 => 44,  163 => 42,  155 => 41,  148 => 40,  127 => 28,  124 => 27,  122 => 26,  110 => 25,  106 => 24,  102 => 22,  100 => 21,  97 => 20,  94 => 19,  91 => 18,  89 => 17,  84 => 16,  81 => 15,  78 => 14,  76 => 13,  71 => 12,  69 => 11,  65 => 9,  60 => 8,  56 => 7,  51 => 6,  49 => 5,  46 => 4,  43 => 3,  41 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "wokiee/template/new_elements/wrapper_bottom.twig", "/home2/kadore/public_html/catalog/view/theme/wokiee/template/new_elements/wrapper_bottom.twig");
    }
}

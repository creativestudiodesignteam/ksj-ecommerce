<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* wokiee/template/extension/module/custom_module.twig */
class __TwigTemplate_b050c5ae4ad805bf87045fc6944c4f95c94005e04edddfcb158115c0ad6f803a extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ((($context["type"] ?? null) == 1)) {
            // line 2
            echo "\t";
            if (((((((($context["position"] ?? null) == "footer_bottom") || (($context["position"] ?? null) == "footer_top")) || (($context["position"] ?? null) == "footer_left")) || (($context["position"] ?? null) == "footer_right")) || (($context["position"] ?? null) == "customfooter_top")) || (($context["position"] ?? null) == "customfooter_bottom"))) {
                // line 3
                echo "\t\t";
                echo (("<h4>" . ($context["block_heading"] ?? null)) . "</h4>");
                echo "
\t\t";
                // line 4
                echo "<div class=\"strip-line\"></div>";
                echo "
\t\t";
                // line 5
                echo ($context["block_content"] ?? null);
                echo "
\t";
            } else {
                // line 6
                echo " 
\t\t";
                // line 7
                echo "<div class=\"box\">";
                echo "
\t\t\t";
                // line 8
                echo "<div class=\"box-heading\">";
                echo "
\t\t\t\t";
                // line 9
                echo ($context["block_heading"] ?? null);
                echo "
\t\t\t";
                // line 10
                echo "</div>";
                echo "
\t\t\t";
                // line 11
                echo "<div class=\"strip-line\"></div>";
                echo "
\t\t\t";
                // line 12
                echo "<div class=\"box-content\">";
                echo "
\t\t\t\t";
                // line 13
                echo ($context["block_content"] ?? null);
                echo "
\t\t\t";
                // line 14
                echo "</div>";
                echo "
\t\t";
                // line 15
                echo "</div>";
                echo "\t
\t";
            }
            // line 16
            echo "\t
";
        } else {
            // line 17
            echo " 
\t";
            // line 18
            echo ($context["text"] ?? null);
            echo "
";
        }
    }

    public function getTemplateName()
    {
        return "wokiee/template/extension/module/custom_module.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  103 => 18,  100 => 17,  96 => 16,  91 => 15,  87 => 14,  83 => 13,  79 => 12,  75 => 11,  71 => 10,  67 => 9,  63 => 8,  59 => 7,  56 => 6,  51 => 5,  47 => 4,  42 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "wokiee/template/extension/module/custom_module.twig", "");
    }
}

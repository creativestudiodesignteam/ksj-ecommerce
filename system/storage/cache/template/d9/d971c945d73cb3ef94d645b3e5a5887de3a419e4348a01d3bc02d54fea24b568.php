<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* wokiee/template/extension/module/product_questions.twig */
class __TwigTemplate_57354ce96c45d56394688308c483e1ef587a85eb7ac5e95329cb5cdf3ebb9acd extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ((twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "has", [0 => "theme_options"], "method", false, false, false, 1) == twig_constant("true"))) {
            echo " 
  ";
            // line 2
            $context["theme_options"] = twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "get", [0 => "theme_options"], "method", false, false, false, 2);
            // line 3
            echo "  ";
            $context["config"] = twig_get_attribute($this->env, $this->source, ($context["registry"] ?? null), "get", [0 => "config"], "method", false, false, false, 3);
            echo " 
<div class=\"product-information-buttons\">
  <a href=\"javascript:;\" class=\"mespr button-product-question\" data-toggle=\"modal\" data-target=\"#popup-product-question-";
            // line 5
            echo ($context["module_id"] ?? null);
            echo "\">
       ";
            // line 6
            if (((($context["icon"] ?? null) != "") && (($context["icon_position"] ?? null) == "left"))) {
                echo " ";
                echo (("<img src=\"image/" . ($context["icon"] ?? null)) . "\" align=\"left\" class=\"icon-enquiry\" alt=\"Icon\">");
                echo " ";
            }
            echo " 
       <span class=\"text-enquiry\"><i class=\"icon-f-72\"></i> ";
            // line 7
            echo ($context["button_text"] ?? null);
            echo "</span>
       ";
            // line 8
            if (((($context["icon"] ?? null) != "") && (($context["icon_position"] ?? null) == "right"))) {
                echo " ";
                echo (("<img src=\"image/" . ($context["icon"] ?? null)) . "\" align=\"right\" class=\"icon-enquiry\" alt=\"Icon\">");
                echo " ";
            }
            echo " 
  </a>
</div>
       
  <div class=\"modal fade\" id=\"popup-product-question-";
            // line 12
            echo ($context["module_id"] ?? null);
            echo "\" tabindex=\"-1\" role=\"dialog\" aria-label=\"myModalLabel\" aria-hidden=\"true\">
    <div class=\"modal-dialog modal-small\" id=\"popup-";
            // line 13
            echo ($context["module_id"] ?? null);
            echo "\">
      <div class=\"modal-content \">
        <div class=\"modal-header\">
          <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\"><span class=\"icon icon-clear\"></span></button>
        </div>
        <div class=\"modal-body\">
          <div class=\"tt-modal-verifyage\">

            ";
            // line 21
            if ((($context["block_title"] ?? null) != "")) {
                echo "<h5>";
                echo ($context["block_title"] ?? null);
                echo "</h5>";
            }
            echo " 
            <form action=\"\" method=\"post\" enctype=\"multipart/form-data\" id=\"contact\" class=\"form-contact popup-contact form-default\">
                 ";
            // line 23
            if ((isset($context["product_id"]) || array_key_exists("product_id", $context))) {
                echo " 
                 <input type=\"hidden\" name=\"product_id\" value=\"";
                // line 24
                echo ($context["product_id"] ?? null);
                echo "\" />
                 ";
            } elseif (            // line 25
(isset($context["url"]) || array_key_exists("url", $context))) {
                echo " 
                 <input type=\"hidden\" name=\"url\" value=\"";
                // line 26
                echo ($context["url"] ?? null);
                echo "\" />
                 ";
            }
            // line 27
            echo " 

                 <fieldset>
                   <div class=\"contact-label\">
                     <div>
                       <input type=\"text\" name=\"name\" value=\"\" id=\"input-name\" placeholder=\"";
            // line 32
            echo ($context["entry_name"] ?? null);
            echo "\" class=\"form-control\" />
                     </div>
                   </div>
                   <div class=\"contact-label\">
                     <div>
                       <input type=\"text\" name=\"email\" value=\"\" id=\"input-email\" placeholder=\"";
            // line 37
            echo ($context["entry_email"] ?? null);
            echo "\" class=\"form-control\" />
                     </div>
                   </div>
                   <div class=\"contact-label\">
                     <div>
                       <textarea name=\"enquiry\" rows=\"7\" id=\"input-enquiry\" placeholder=\"";
            // line 42
            echo ($context["entry_enquiry"] ?? null);
            echo "\" class=\"form-control\"></textarea>
                     </div>
                   </div>
                 </fieldset>
                 

                <input class=\"btn btn-primary\" type=\"submit\" value=\"";
            // line 48
            echo ($context["button_submit"] ?? null);
            echo "\" />
            </form>

            <script type=\"text/javascript\">
                 function IsEmail(email) {
                      var regex = /^([a-zA-Z0-9_.+-])+\\@(([a-zA-Z0-9-])+\\.)+([a-zA-Z0-9]{2,4})+\$/;
                      return regex.test(email);
                 }

                 \$('#popup-";
            // line 57
            echo ($context["module_id"] ?? null);
            echo " #contact').bind('submit', function(){
                      var valid = true;
                    
                      if(\$('#popup-product-question-";
            // line 60
            echo ($context["module_id"] ?? null);
            echo " #input-name').val().length < 3 || \$('#popup-product-question-";
            echo ($context["module_id"] ?? null);
            echo " #input-name').val().length > 32){
                           \$('#popup-product-question-";
            // line 61
            echo ($context["module_id"] ?? null);
            echo " #input-name').addClass(\"error\");
                           valid = false;
                      } else {
                           \$('#popup-product-question-";
            // line 64
            echo ($context["module_id"] ?? null);
            echo " #input-name').removeClass(\"error\");

                      }

                      if(IsEmail(\$('#popup-product-question-";
            // line 68
            echo ($context["module_id"] ?? null);
            echo " #input-email').val())) {
                           \$('#popup-product-question-";
            // line 69
            echo ($context["module_id"] ?? null);
            echo " #input-email').removeClass(\"error\");
                      } else {
                           \$('#popup-product-question-";
            // line 71
            echo ($context["module_id"] ?? null);
            echo " #input-email').addClass(\"error\");
                           valid = false;
                      }
                     

                      if(\$('#popup-product-question-";
            // line 76
            echo ($context["module_id"] ?? null);
            echo " #input-enquiry').val().length < 10 || \$('#popup-product-question-";
            echo ($context["module_id"] ?? null);
            echo " #input-enquiry').val().length > 3000){
                           \$('#popup-product-question-";
            // line 77
            echo ($context["module_id"] ?? null);
            echo " #input-enquiry').addClass(\"error\");
                           valid = false;
                      } else {
                           \$('#popup-product-question-";
            // line 80
            echo ($context["module_id"] ?? null);
            echo " #input-enquiry').removeClass(\"error\");
                      }

                      if(valid) {
                           \$.ajax({
                                  type: \"POST\",
                                  url: \"";
            // line 86
            echo ($context["contact_url"] ?? null);
            echo "\",
                                  data: \$(\"#popup-product-question-";
            // line 87
            echo ($context["module_id"] ?? null);
            echo " #contact\").serialize(), // serializes the form's elements.
                                  success: function(data)
                                  {
                                      alert(data); // show response from the php script.
                                      \$('#popup-product-question-";
            // line 91
            echo ($context["module_id"] ?? null);
            echo "').modal('hide')
                                  }
                                });

                           return false;
                      } else {
                           return false;
                      }
                 });
            </script>

          </div>
        </div>
      </div>
    </div>
  </div>
";
        }
    }

    public function getTemplateName()
    {
        return "wokiee/template/extension/module/product_questions.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  235 => 91,  228 => 87,  224 => 86,  215 => 80,  209 => 77,  203 => 76,  195 => 71,  190 => 69,  186 => 68,  179 => 64,  173 => 61,  167 => 60,  161 => 57,  149 => 48,  140 => 42,  132 => 37,  124 => 32,  117 => 27,  112 => 26,  108 => 25,  104 => 24,  100 => 23,  91 => 21,  80 => 13,  76 => 12,  65 => 8,  61 => 7,  53 => 6,  49 => 5,  43 => 3,  41 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "wokiee/template/extension/module/product_questions.twig", "");
    }
}

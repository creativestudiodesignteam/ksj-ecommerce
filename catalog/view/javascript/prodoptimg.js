var prodoptimg_route = '';
var prodoptimgJson = '';
var prodoptimg = {
	'swapimg': function(smallimg, popimage) {
		$('.targetprodoptimg').find('img').fadeOut('fast',function(){
			$(this).attr('src', smallimg).fadeIn('fast');
			$(this).attr('data-zoom-image', popimage).fadeIn('fast');
			$('.zoomWindowContainer div').stop().css("background-image","url("+ JSON.stringify(String(popimage)) +")");
		}); 
		//$('.targetprodoptimg').attr('href', popimage);
	},
	'resetimg': function(product_option_id) {
		$('#input-option'+product_option_id).find('.act').removeClass('act');
		prodoptimg.swapimg($('.targetprodoptimg_src').val(), $('.targetprodoptimg_src').attr('popimg'));
	},
	'getprodhtml': function(json, rs) {
		var html = '';
		$.each(rs, function( product_option_id, optval ) {
			var valhtml = '';
			var req = '';
			var option_name = '';
			var sort_order = 1;
			$.each(optval, function( product_option_value_id, v ) {
				//console.log( "Key: " + product_option_value_id + ", Value: " + v );
				valhtml += '<div class="radio prodoptimgradio">';
				var sq = (json['style'] == 2) ? 'prodoptimg_sq' : '';
				if(v['prodoptimg_color']) { 
 					valhtml += '<label data-toggle="tooltip" title="'+v['option_value_name']+'" class="prodoptimg '+sq+'" style="background:'+v['prodoptimg_color']+';">';
				} else {
 					valhtml += '<label data-toggle="tooltip" title="'+v['option_value_name']+'" class="prodoptimg prodoptimg_bg '+sq+'" style="background-image:url(\''+v['prodoptimg_prod_disp']+'\');">';
				}
				valhtml += '<input type="radio" class="prodoptimg_hide" name="option['+product_option_id+']" value="'+product_option_value_id+'" />';
				if (v['prodoptimg_prod']) {
					valhtml += '<input type="hidden" class="swapimg" value="'+v['prodoptimg_prod']+'" popimg="'+v['prodoptimg_prodpop']+'" />';
				}
				/*valhtml += '<input type="hidden" value="'+product_option_id+'" />';*/
				valhtml += '</label>';
				valhtml += '</div>';
				
				req = (v['required']) ? ' required' : '';
				option_name = v['option_name'];
				sort_order = v['sort_order'];
			});
			
			html += '<div class="form-group' + req +'">';
			if(json['resetbtntext']) { 
				html += '<label class="control-label"> '+ option_name +' &nbsp; <span onclick="prodoptimg.resetimg('+product_option_id+');" class="label label-danger btnclear_prodoptimg">'+json['resetbtntext']+'</span> </label>';
			}
			html += '<div id="input-option'+product_option_id+'">';
			
			html += valhtml; 
			
			html += '<div style="clear:both"></div>';
			html += '</div>';
			html += '</div>';
		});	
		return html;
	},
	'prodpage': function(json) {
		if(prodoptimg_route.indexOf('product/product') != -1) {
			var product_id= $("input[name='product_id']").val();
			if (typeof product_id !== 'undefined') {
				var optiondata = json['optiondata'][product_id];
				if(optiondata) {
					htmlresp = prodoptimg.getprodhtml(json, optiondata);
					
					$('.tt-product-single-info').find('.tt-price').after(htmlresp);
					$('#product').before('<input type="hidden" class="targetprodoptimg_src"/>');
			
					$('.zoom-product').parent().addClass('targetprodoptimg');
					$('.prodoptimg').each(function() {
						$(this).click(function() {
							$(this).parent().parent().find('.act').removeClass('act');
							$(this).addClass('act'); 
							prodoptimg.swapimg($(this).find('.swapimg').val(), $(this).find('.swapimg').attr('popimg'));
						});
					});
					$('.targetprodoptimg_src').val($('.targetprodoptimg').find('img').attr('src')).attr('popimg', $('.targetprodoptimg').attr('href'));
				}
			}
		}
	},
	'getgridhtml': function(json, rs) {
		var html = '<div class="gridoptimg">';
		$.each(rs, function( product_option_id, optval ) {
			var valhtml = '';
 			$.each(optval, function( product_option_value_id, v ) {
				//console.log( "Key: " + product_option_value_id + ", Value: " + v );
				valhtml += '<div class="radio prodoptimgradio">';
				var sq = (json['style'] == 2) ? 'prodoptimg_sq' : '';
				if(v['prodoptimg_color']) { 
 					valhtml += '<label data-toggle="tooltip" title="'+v['option_value_name']+'" class="prodoptimg '+sq+'" style="background:'+v['prodoptimg_color']+';">';
				} else {
 					valhtml += '<label data-toggle="tooltip" title="'+v['option_value_name']+'" class="prodoptimg prodoptimg_bg '+sq+'" style="background-image:url(\''+v['prodoptimg_prod_disp']+'\');">';
				}
				if (v['prodoptimg_grid']) {
					valhtml += '<input type="hidden" class="swapimg" value="'+v['prodoptimg_grid']+'" />';
				}
				valhtml += '</label>';
				valhtml += '</div>';
				
				req = (v['required']) ? ' required' : '';
				option_name = v['option_name'];
				sort_order = v['sort_order'];
			});
  			html += valhtml; 
  		});	
		html += '<div style="clear:both"></div>';
		return html;
	},
	'applyingrid': function(json) {
		$("[onclick*='cart.add']").each(function() {
			var htmlresp = '';
			var product_id = $(this).attr('onclick').match(/[0-9]+/);
			var optiondata = json['optiondata'][product_id];
			if(optiondata && $(this).closest('.tt-product').hasClass('btnprodoptimg') == false) {
				htmlresp = prodoptimg.getgridhtml(json, optiondata);
				$(this).closest('.tt-product').addClass('btnprodoptimg');
				$(this).closest('.tt-product').find('.tt-price').after(htmlresp);
			}
		});	
		$('.prodoptimg').each(function() {
			$(this).click(function() {
				$(this).parent().parent().find('.act').removeClass('act');
				$(this).addClass('act'); 
				$(this).closest('.tt-product').find('img').attr('src', $(this).find('.swapimg').val()).fadeIn();
			});
		});		
	},
	'initjson': function() {
		$.ajax({
			url: 'index.php?route=extension/prodoptimg/getcache',
			dataType: 'json',
			cache: true,
			success: function(json) {
				prodoptimgJson = json;
				prodoptimg_route = json['prodoptimg_route'];
				prodoptimg.prodpage(json);
				prodoptimg.applyingrid(json);
				$(document).ajaxStop(function() { prodoptimg.applyingrid(json); });
				//console.log(prodoptimgJson);
			}
		});
	}
}
$(document).ready(function() {
	prodoptimg.initjson();	
});
/*setTimeout(function(){ console.log(prodoptimgJson); }, 500);*/
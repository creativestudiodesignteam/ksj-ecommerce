<?php
class ControllerExtensionprodoptimg extends Controller {
	private $modpath = 'extension/prodoptimg'; 
 	private $modname = 'prodoptimg';
  	private $modssl = 'SSL';
   	private $langid = 0;
	private $storeid = 0;
	private $custgrpid = 0;
	
	public function __construct($registry) {
		parent::__construct($registry);
 		$this->langid = (int)$this->config->get('config_language_id');
 		
		if(substr(VERSION,0,3)>='3.0' || substr(VERSION,0,3)=='2.3' || substr(VERSION,0,3)=='2.2') { 
  			$this->modssl = true;
 		} 
 		if(substr(VERSION,0,3)>='3.0') { 
			$this->modname = 'module_prodoptimg';
		} 
  	}	
	public function getcache() {
		if($this->config->get($this->modname.'_status')) {
			$json['prodoptimg_route'] = isset($this->session->data['prodoptimg_route']) ? $this->session->data['prodoptimg_route'] : '';			
			$json['style'] = $this->config->get($this->modname.'_style');
			$resetbtntext = $this->config->get($this->modname.'_resetbtntext');
			$json['resetbtntext'] = $resetbtntext[$this->langid];
			
			$dispimgw = $this->config->get($this->modname.'_dispimgw');	
			$dispimgh = $this->config->get($this->modname.'_dispimgh');	
			
			$gridimgw = $this->config->get($this->modname.'_gridimgw');	
			$gridimgh = $this->config->get($this->modname.'_gridimgh');	
			
			$prodimgw = $this->config->get($this->modname.'_prodimgw');	
			$prodimgh = $this->config->get($this->modname.'_prodimgh');
			$prodpopimgw = $this->config->get($this->modname.'_prodpopimgw');	
			$prodpopimgh = $this->config->get($this->modname.'_prodpopimgh');
			
			$this->load->model('tool/image');
			
			$json['optiondata'] = array();
							
			$query = $this->db->query("SELECT pov.*, po.required, o.sort_order, od.name as option_name, ovd.name as option_value_name, p.tax_class_id FROM `" . DB_PREFIX . "product_option_value` pov 
			inner join `" . DB_PREFIX . "product_option` po on po.product_option_id = pov.product_option_id 
			inner join `" . DB_PREFIX . "option` o on o.option_id = pov.option_id 
			inner join `" . DB_PREFIX . "option_value` ov on ov.option_value_id = pov.option_value_id 
			inner join `" . DB_PREFIX . "product` p on p.product_id = pov.product_id
			inner join `" . DB_PREFIX . "option_description` od on pov.option_id = od.option_id
			inner join `" . DB_PREFIX . "option_value_description` ovd on pov.option_value_id = ovd.option_value_id
			where 1 and o.prodoptimgflag = 1 and od.language_id = '" . (int)$this->config->get('config_language_id') . "' 
			and ovd.language_id = '" . (int)$this->config->get('config_language_id') . "' 
			order by o.sort_order, ov.sort_order");
			foreach ($query->rows as $rs) {					
				$rs['price'] = $this->getprice($rs);	
				if($rs['price']) { 
					$rs['option_value_name'] = $rs['option_value_name'] . '('. $rs['price_prefix'] . $rs['price'] . ')';
				}
				$rs['prodoptimg_grid'] = false; $rs['prodoptimg_prod'] = false; $rs['prodoptimg_prodpop'] = false;
				if((!empty($rs['prodoptimg'])) && is_file(DIR_IMAGE . $rs['prodoptimg'])) {
					$rs['prodoptimg_grid'] = $this->model_tool_image->resize($rs['prodoptimg'], $gridimgw, $gridimgh);
					$rs['prodoptimg_prod'] = $this->model_tool_image->resize($rs['prodoptimg'], $prodimgw, $prodimgh);
					$rs['prodoptimg_prodpop'] = $this->model_tool_image->resize($rs['prodoptimg'], $prodpopimgw, $prodpopimgh);
					$rs['prodoptimg_prod_disp'] = $this->model_tool_image->resize($rs['prodoptimg'], $dispimgw, $dispimgh);
				}
				
				$json['optiondata'][$rs['product_id']][$rs['product_option_id']][$rs['product_option_value_id']] = $rs;
			}
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));
		}
	}
	
	private function getcurval($taxprc) {
		if(substr(VERSION,0,3)>='3.0' || substr(VERSION,0,3)=='2.3' || substr(VERSION,0,3)=='2.2') { 
			$taxprc = $this->currency->format($taxprc, $this->session->data['currency']);
		} else {
			$taxprc = $this->currency->format($taxprc);
		}	
		return $taxprc;
	}
	private function getprice($rs) {
		$price = false;
		if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$rs['price']) {
			$price = $this->getcurval($this->tax->calculate($rs['price'] , $rs['tax_class_id'], $this->config->get('config_tax')));			
		} 
		return $price;
	}
}